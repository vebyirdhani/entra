<?php

namespace frontend\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use yii\web\UploadedFile;

use yii\web\NotFoundHttpException;
/**
 * Site controller
 */
class SiteController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex() {
         $komentar = new \backend\models\Contacts;
        $komentar->tanggal = date('Y-m-d');
        if ($komentar->load(Yii::$app->request->post()) && $komentar->save()) {

             return $this->redirect(['site/']);
        }
        return $this->render('index',[
            'komentar'=>$komentar,
        ]);
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin() {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout() {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return mixed
     */
    public function actionContact() {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
                Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');
            } else {
                Yii::$app->session->setFlash('error', 'There was an error sending your message.');
            }

            return $this->refresh();
        } else {
            return $this->render('contact', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionAbout() {
        return $this->render('about');
    }

    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup() 
    {
        
         $a = \backend\models\User::find()->orderBy('id DESC')->one();
           $id=$a->id+1;
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            $model->role = 1;
            if ($user = $model->signup()) {
 		$email = \Yii::$app->mailer->compose()
			->setTo($user->email)
			->setFrom([\Yii::$app->params['supportEmail']  => ' Tangan Di Atas'])
			->setSubject('Signup Confirmation')
			->setTextBody("
		Click this link ".Yii::$app->urlManager->createAbsoluteUrl(
		['site/confirm','key'=>$user->auth_key]
		)
		)
		->send();
                $baru = new \backend\models\DatabaseUser();
                $baru->nama = $user->username;
                $baru->owner = $user->id;
                $baru->save();
                $user->database_id = $baru->id;
                $user->save();
                return $this->redirect(['/site/signup2', 'id' => $id]);
            }
            
        }
        return $this->render('signup', [
                    'model' => $model,
        ]);
    }
     public function actionSignup2($id) 
    {
         $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $model->picture = UploadedFile::getInstance($model, 'picture');
              if ($model->picture){
                   $model->foto = $model->id.  rand() . '.' . $model->picture->extension;
                    $model->save();
               $model->picture->saveAs('../public_html/admin/img/user/' . $model->foto);
                Yii::$app->session->setFlash('success', 'Success');
                return $this->redirect(['index']);
              }else {
                  $model->save();
Yii::$app->session->setFlash('success', 'Success');
              }

        return $this->redirect(['index']);
        } else {
            return $this->render('signup2', [
                'model' => $model,
            ]);
        }
    }
    
protected function findModel($id)
    {
        if (($model = \backend\models\User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
public function actionConfirm($key)
		{
			$user = \common\models\User::find()->where([
				
				'auth_key'=>$key,
				'status'=>0,
				])->one();
			if(!empty($user)){
				$user->status=10;
				$user->save();
			Yii::$app->getSession()->setFlash('success','Sukses! Silahkan lakukan login');
				}
			else{
			Yii::$app->getSession()->setFlash('danger','Failed!');
			}
		return $this->goHome();
		}
    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset() {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for the provided email address.');
            }
        }

        return $this->render('requestPasswordResetToken', [
                    'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token) {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
                    'model' => $model,
        ]);
    }

    public function actionListregs($id = null) {
        if ($id) {
            $countRegs = \backend\models\Regencies::find()
                    ->where(['province_id' => $id])
                    ->count();

            $regs = \backend\models\Regencies::find()
                    ->where(['province_id' => $id])
                    ->orderBy([
                        'name' => SORT_ASC,
                    ])
                    ->all();

            if ($countRegs > 0) {
                foreach ($regs as $reg) {
                    echo "<option value='" . $reg->id . "'>" . $reg->name . "</option>";
                }
            } else {
                echo "<option>-</option>";
            }
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionListdis($id = null) {
        if ($id) {
            $countRegs = \backend\models\Districts::find()
                    ->where(['regency_id' => $id])
                    ->count();

            $regs = \backend\models\Districts::find()
                    ->where(['regency_id' => $id])
                    ->orderBy([
                        'name' => SORT_ASC,
                    ])
                    ->all();

            if ($countRegs > 0) {
                foreach ($regs as $reg) {
                    echo "<option value='" . $reg->id . "'>" . $reg->name . "</option>";
                }
            } else {
                echo "<option>-</option>";
            }
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionListvil($id = null) {
        if ($id) {
            $countRegs = \backend\models\Villages::find()
                    ->where(['district_id' => $id])
                    ->count();

            $regs = \backend\models\Villages::find()
                    ->where(['district_id' => $id])
                    ->orderBy([
                        'name' => SORT_ASC,
                    ])
                    ->all();

            if ($countRegs > 0) {
                foreach ($regs as $reg) {
                    echo "<option value='" . $reg->id . "'>" . $reg->name . "</option>";
                }
            } else {
                echo "<option>-</option>";
            }
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
