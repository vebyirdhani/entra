<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
//        'css/bootstrap.min.css',
//        'css/flexslider.css',
//        'css/animsition.min.css',
//        'css/lightbox.min.css',
//        'css/owl.carousel.min.css',
//        'css/owl.theme.css',
//        'css/style.css',
//        'https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css',
//        'https://cdn.linearicons.com/free/1.0.0/icon-font.min.css',
    ];
    public $js = [
//       'js/jquery-2.2.3.min.js',
//       'js/bootstrap.min.js',
//       'js/animated-headline.js',
//        'js/modernizr.js',
//        'js/isotope.pkgd.min.js',
//        'js/jquery.flexslider-min.js',
//        'js/jquery.animsition.min.js',
//        'js/lightbox.min.js',
//        'js/init.js',
//        'js/main.js',
//        'js/smooth-scroll.js',
//        'js/numscroller.js',
//        'js/wow.min.js',
//        'js/owl.carousel.min.js',
    ];
    public $depends = [
     
    ];
}
