<?php

namespace frontend\controllers;

use Yii;
use yii\rest\Controller;
use yii\web\Response;
use yii\web\UploadedFile;
use yii\imagine\Image;
use yii\db\Expression;
use common\models\LoginForm;
use frontend\models\Profile;
use common\model\JadwalSiaran;

class Service1Controller extends Controller {

    public function actionListCoordinate(){
        Yii::$app->response->format = Response::FORMAT_JSON;
        
        $cari= \common\models\Coordinat::find()->all();
        if ($cari){
            $data=[];
            foreach ($cari as $key=>$value){
                $coordinat = \common\models\Coordinat::find()->where(['id_coordinat'=>$value->id_coordinat])->one();
                $data[$key]['id_coordinat']= $value->id_coordinat;
                $data[$key]['nama']= $value->nama;
                $data[$key]['kategori']= $value->kategori;
                $data[$key]['latitdude']= $value->latitdude;
                $data[$key]['longitude']= $value->longitude;
                
            }$key++;
            return[
                'status'=>'success',
                'data'=>$data,
            ];
        }else{
            return[
                'status'=>'error',
                'message'=>'Data tidak ditemukan',
            ];
        }
    }
    public function actionSearchExperience() {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $new = new \common\models\SearchForm();
        $user = Yii::$app->user->identity;

        if ($new->load(Yii::$app->request->post())) {
            $cari = \common\models\Experience::find()->where(['like', 'nama', $new->keyword])->all();
            if ($cari) {
                 $count = \common\models\Experience::find()->where(['like', 'nama', $new->keyword])->count();
                if ($user) {
                    $new->id_user = $user->id;
                }
                $new->save(false);
                $data = [];
                foreach ($cari as $key => $val) {
                    $data[$key]['id'] = $val->id_experience;
                    $data[$key]['nama'] = $val->nama;
                    $data[$key]['harga'] = Yii::$app->formatter->asInteger($val->price);
                    $data[$key]['deskripsi'] = $val->deskripsi;
                    $data[$key]['alamat'] = $val->alamat;
                    $data[$key]['rating'] = $val->rating;
                    $carikategori = \common\models\KategoriExperience::find()->where(['id_kategori'=>$val->id_kategori])->one();
                    if ($carikategori){
                            $data[$key]['kategori'] = $carikategori->nama;
                    }
                    $desa = \common\models\Villages::find()->where(['id' => $val->id_desa])->one();
                    if ($desa) {
                        $data[$key]['desa'] = $desa->name;
                    }

                    $onephoto = \common\models\FotoExperience::find()->where(['id_experience' => $val->id_experience])->one();

                    if ($onephoto) {

                      $data[$key]['foto'] = 'http://entra.learnjsforfree.com/uploads/experience/' . $onephoto->foto;
                    }
                }
                $key++;
                return[
                    'status' => 'success',
                    'data' => $data,
                    'lengthData'=>$count,
                ];
            } else {
                return[
                    'status' => 'error',
                    'message' => 'Data tidak ditemukan'
                ];
            }
        } else {
            return[
                'status' => 'error',
                'message' => 'Gagal mencari data'
            ];
        }
    }

    public function actionSearchAkomodasi() {
        Yii::$app->response->format = Response::FORMAT_JSON;
          $user = Yii::$app->user->identity;
        $model = new \common\models\SearchForm();
        if ($model->load(Yii::$app->request->post())) {
            $cari = \common\models\Accommodation::find()->where(['like', 'nama_homestay', $model->keyword])->all();
            if ($cari) {
                 $count = \common\models\Accommodation::find()->where(['like', 'nama_homestay', $model->keyword])->count();
                 if ($user) {
                    $model->id_user = $user->id;
                }
               
                $model->save(false);
                $data = [];
                foreach ($cari as $key => $val) {
                    $foto = \common\models\FotoAccommodation::find()->where(['id_homestay' => $val->id_homestay])->all();
                    $data[$key]['id'] = $val->id_homestay;
                    $data[$key]['nama'] = $val->nama_homestay;
                    $data[$key]['rating'] = $val->rating;
                    $data[$key]['harga'] =Yii::$app->formatter->asInteger($val->harga);
                    $carikategori= \common\models\KategoriAccommodation::find()->where(['id_kategori'=>$val->id_kategori])->one();
                    if ($carikategori){ 
                    $data[$key]['kategori'] = $carikategori->nama_kategori;
                    }
                    $data[$key]['alamat'] = $val->alamat;
                    $onephoto = \common\models\FotoAccommodation::find()->where(['id_homestay' => $val->id_homestay])->one();
//                    if ($foto) {
//                        $sub = [];
//                        foreach ($foto as $ft) {
//                            $sub_name['foto'] = 'http://entra.learnjsforfree.com/uploads/akomodasi/' . $ft['foto'];
//                            array_push($sub, $sub_name);
//                        }
//                        $data[$key]['gallery'] = $sub;
//                    }
                    if ($onephoto) {
                        $data[$key]['foto'] = 'http://entra.learnjsforfree.com/uploads/akomodasi/' .$onephoto->foto;
                    }
                }
                $key++;
                return[
                    'status' => 'success',
                    'data' => $data,
                    'lenghtData'=>$count,
                ];
            } else {
                return[
                    'status' => 'error',
                    'message' => 'Data tidak ditemukan'
                ];
            }
        } else {
            return[
                'status' => 'error',
                'message' => 'Data tidak bisa dicari'
            ];
        }
    }
    
    

    public function actionSearchTransportasi() {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $user = Yii::$app->user->identity;
        $model = new \common\models\SearchForm();
        if ($model->load(Yii::$app->request->post())) {
            $cari = \common\models\Transportation::find()->where(['like', 'nama', $model->keyword])->all();

            if ($cari) {
                $count = \common\models\Transportation::find()->where(['like', 'nama', $model->keyword])->count();
                if ($user) {
                    $model->id_user = $user->id;
                }
                $model->save(false);
                $data = [];
                foreach ($cari as $key => $val) {
                    $data[$key]['id'] = $val->id_transportation;
                    $data[$key]['nama'] = $val->nama;
                    $carikategori = \common\models\KategoriTransportasi::find()->where(['id_kategori'=>$val->jenis_transportasi])->one();
                    if ($carikategori){
                    $data[$key]['kategori'] = $carikategori->nama;
                    }
                    $data[$key]['harga'] = Yii::$app->formatter->asInteger($val->price);
                    $data[$key]['deskripsi'] = $val->deskripsi;
                     $data[$key]['rating'] = $val->rating;
                       $data[$key]['alamat'] = $val->alamat;
                     $carifoto = \common\models\FotoTransportasi::find()->where(['id_transportasi'=>$val->id_transportation])->one();
                             if ($carifoto){ 
                    $data[$key]['foto'] = 'http://entra.learnjsforfree.com/uploads/transport/' . $carifoto->foto;
                             }
                    
                             } $key++;
                return[
                    'status' => 'success',
                    'data' => $data,
                     'lenghtData'=>$count,
                ];
            } else {
                return[
                    'status' => 'error',
                    'message' => 'Data tidak ditemukan'
                ];
            }
        } else {
            return[
                'status' => 'error',
                'message' => 'Data tidak bisa dicari'
            ];
        }
    }
    


public function actionSearchTujuanTransportasi() {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $user = Yii::$app->user->identity;
        $model = new \common\models\Rute();
        if ($model->load(Yii::$app->request->post())) {
            $cari = \common\models\Rute::find()->where(['like', 'daerah_mulai', $model->daerah_mulai])
            ->andWhere(['like', 'daerah_akhir', $model->daerah_akhir])->all();

            if ($cari) {
                $count = \common\models\Rute::find()->where(['like', 'daerah_mulai', $model->daerah_mulai])
            ->andWhere(['like', 'daerah_akhir', $model->daerah_akhir])->count();
              
                $data = [];
                foreach ($cari as $key => $val) {
     //   $transport= \common\models\Transportation::find->where(['id_transport'=>$val->id_transport])->one();
           $transport = \common\models\Transportation::find()->where(['id_transportation' => $val->id_transport])->one();
               
                       $fotobaru = \common\models\FotoTransportasi::find()->where(['id_transportasi' => $val->id_transport])->one();
                    $data[$key]['id'] = $val->id_transport;
                    $data[$key]['nama'] = $transport->nama;
                    $carikategori = \common\models\KategoriTransportasi::find()->where(['id_kategori'=>$val->jenis_transportasi])->one();
                    if ($carikategori){
                    $data[$key]['kategori'] = $carikategori->nama;
                    }
                    $data[$key]['harga'] = Yii::$app->formatter->asInteger($transport->price);
                    $data[$key]['deskripsi'] = $transport->deskripsi;
                     $data[$key]['rating'] = $transport->rating;
                     if ($fotobaru){
                          $data[$key]['foto'] = 'http://entra.learnjsforfree.com/uploads/transport/' . $fotobaru->foto;
                     }
                      $data[$key]['tujuan_awal'] = $val->daerah_mulai;
                       $data[$key]['tujuan_akhir'] = $val->daerah_akhir;
                  
                } $key++;
                return[
                    'status' => 'success',
                    'data' => $data,
                     'lenghtData'=>$count,
                ];
            } else {
                return[
                    'status' => 'error',
                    'message' => 'Data tidak ditemukan'
                ];
            }
        } else {
            return[
                'status' => 'error',
                'message' => 'Data tidak bisa dicari'
            ];
        }
    }
    public function actionSearchSouvenir() {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $user = Yii::$app->user->identity;
        $model = new \common\models\SearchForm();
        if ($model->load(Yii::$app->request->post())) {
            $cari = \common\models\Merchandise::find()->where(['like', 'nama', $model->keyword])->all();

            if ($cari) {
                  $count = \common\models\Merchandise::find()->where(['like', 'nama', $model->keyword])->count();
                if ($user) {
                    $model->id_user = $user->id;
                }
                $model->save(false);
                $data = [];
                foreach ($cari as $key => $val) {
                    $foto = \common\models\FotoMerchandise::find()->where(['id_merchandise' => $val->id_merchandise])->all();
                    $data[$key]['id'] = $val->id_merchandise;
                    $data[$key]['nama'] = $val->nama;
                    $carikategori = \common\models\KategoriMerchandise::find()->where(['id_kategori'=>$val->kategori])->one();
                    if ($carikategori){
                    $data[$key]['kategori'] = $carikategori->nama;
                    }
                    $data[$key]['harga'] = Yii::$app->formatter->asInteger($val->harga);
                    $data[$key]['deskripsi'] = $val->deskripsi;
                    $data[$key]['alamat'] = $val->alamat;
                     $data[$key]['rating'] = $val->rating;
                    $photoone = \common\models\FotoMerchandise::find()->where(['id_merchandise' => $val->id_merchandise])->one();
//                    if ($foto) {
//                        $sub = [];
//                        foreach ($foto as $ft) {
//                            $sub_name['foto'] = 'http://entra.learnjsforfree.com/uploads/merchant/' . $ft['foto'];
//                            array_push($sub, $sub_name);
//                        }
//                        $data[$key]['gallery'] = $sub;
//                    }
                    if ($photoone) {
                        $data[$key]['foto'] ='http://entra.learnjsforfree.com/uploads/merchant/' . $photoone->foto;
                    }
                } $key++;
                return[
                    'status' => 'success',
                    'data' => $data,
                      'lenghtData'=>$count,
                ];
            } else {
                return[
                    'status' => 'error',
                    'message' => 'Data tidak ditemukan'
                ];
            }
        } else {
            return[
                'status' => 'error',
                'message' => 'Data tidak bisa dicari'
            ];
        }
    }

    public function actionSearchDesa() {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $model = new \common\models\SearchForm();
        $user = Yii::$app->user->identity;
        if ($model->load(Yii::$app->request->post())) {
            $cari = \common\models\DesaTraveling::find()->where(['like', 'nama', $model->keyword])->all();

            if ($cari) {
                 $count = \common\models\DesaTraveling::find()->where(['like', 'nama', $model->keyword])->count();
                if ($user) {
                    $model->id_user = $user->id;
                }
                $model->save(false);
                $data = [];
                foreach ($cari as $key => $val) {
                    $foto = \common\models\FotoDesatraveling::find()->where(['id_desa' => $val->id])->all();
                    $data[$key]['id'] = $val->id;
                    $data[$key]['nama'] = $val->nama;
                    $data[$key]['rating'] = $val->rating;

                    $data[$key]['alamat'] = $val->alamat;
                     $data[$key]['deskripsi'] = $val->deskripsi;
                    $photoone = \common\models\FotoDesaTraveling::find()->where(['id_desa' => $val->id])->one();
                    
//                    if ($foto) {
//                        $sub = [];
//                        foreach ($foto as $ft) {
//                            $sub_name['foto'] = 'http://entra.learnjsforfree.com/uploads/desa/' . $ft['foto'];
//                            array_push($sub, $sub_name);
//                        }
//                        $data[$key]['gallery'] = $sub;
//                    }
                    if ($photoone) {
                        $data[$key]['foto'] = 'http://entra.learnjsforfree.com/uploads/desa/' . $photoone->foto;
                    }
                } $key++;
                return[
                    'status' => 'success',
                    'data' => $data,
                      'lenghtData'=>$count,
                ];
            } else {
                return[
                    'status' => 'error',
                    'message' => 'Data tidak ditemukan'
                ];
            }
        } else {
            return[
                'status' => 'error',
                'message' => 'Data tidak bisa dicari'
            ];
        }
    }

        public function actionSearchAnything() {
        Yii::$app->response->format = Response::FORMAT_JSON;
          $user = Yii::$app->user->identity;
        $model = new \common\models\SearchForm();
        if ($model->load(Yii::$app->request->post())) {
           
            $akomodasi = \common\models\Accommodation::find()->where(['like', 'nama_homestay', $model->keyword])->all();
            $transport = \common\models\Transportation::find()->where(['like', 'nama', $model->keyword])->all();
            $experience = \common\models\Experience::find()->where(['like', 'nama', $model->keyword])->all();
              $merchant = \common\models\Merchandise::find()->where(['like', 'nama', $model->keyword])->all();
              $desa = \common\models\Villages::find()->where(['like', 'name', $model->keyword])->all();
            if ($akomodasi) {
                 $count = \common\models\Accommodation::find()->where(['like', 'nama_homestay', $model->keyword])->count();
                 if ($user) {
                    $model->id_user = $user->id;
                }
               
                $model->save(false);
                $data = [];
                foreach ($akomodasi as $key => $val) {
                    $foto = \common\models\FotoAccommodation::find()->where(['id_homestay' => $val->id_homestay])->all();
                    $data[$key]['id'] = $val->id_homestay;
                    $data[$key]['nama'] = $val->nama_homestay;
                    $data[$key]['rating'] = $val->rating;
                    $data[$key]['harga'] = $val->harga;
                    $carikategori = \common\models\KategoriAccommodation::find()->where(['id_kategori'=>$val->id_kategori])->one();
                    if ($carikategori){
                    $data[$key]['kategori'] = $carikategori->nama_kategori;
                    }
                    $data[$key]['alamat'] = $val->alamat;
                    $onephoto = \common\models\FotoAccommodation::find()->where(['id_homestay' => $val->id_homestay])->one();

                    if ($onephoto) {
                        $data[$key]['foto'] = 'http://entra.learnjsforfree.com/uploads/akomodasi/' .$onephoto->foto;
                    }
                }
                $key++;
                return[
                    'status' => 'success',
                    'data' => $data,
                    'lenghtData'=>$count,
                ];
            } else if ($transport){
                  $count = \common\models\Transportation::find()->where(['like', 'nama', $model->keyword])->count();
                if ($user) {
                    $model->id_user = $user->id;
                }
                $model->save(false);
                $data = [];
                foreach ($transport as $key => $val) {
                    $data[$key]['id'] = $val->id_transportation;
                    $data[$key]['nama'] = $val->nama;
                    $carikategori = \common\models\KategoriTransportasi::find()->where(['id_kategori'=>$val->jenis_transportasi])->one();
                    if ($carikategori){
                    $data[$key]['kategori'] = $carikategori->nama;
                    }
                    $data[$key]['harga'] = $val->price;
                    $data[$key]['deskripsi'] = $val->deskripsi;
                     $data[$key]['rating'] = $val->rating;
                    $data[$key]['foto'] = 'http://entra.learnjsforfree.com/uploads/transport/' . $val->foto;
                } $key++;
                return[
                    'status' => 'success',
                    'data' => $data,
                     'lenghtData'=>$count,
                ];
            }else if ($experience){
                  $count = \common\models\Experience::find()->where(['like', 'nama', $model->keyword])->count();
                if ($user) {
                    $model->id_user = $user->id;
                }
                $model->save(false);
                $data = [];
                foreach ($experience as $key => $val) {
                    $data[$key]['id'] = $val->id_experience;
                    $data[$key]['nama'] = $val->nama;
                    $data[$key]['harga'] = $val->price;
                    $data[$key]['deskripsi'] = $val->deskripsi;
                    $data[$key]['alamat'] = $val->alamat;
                    $data[$key]['rating'] = $val->rating;
                    $carikategori = \common\models\KategoriExperience::find()->where(['id_kategori'=>$val->id_kategori])->one();
                    if ($carikategori){
                            $data[$key]['kategori'] = $carikategori->nama;
                    }
                    $desa = \common\models\Villages::find()->where(['id' => $val->id_desa])->one();
                    if ($desa) {
                        $data[$key]['desa'] = $desa->name;
                    }

                    $onephoto = \common\models\FotoExperience::find()->where(['id_experience' => $val->id_experience])->one();

                    if ($onephoto) {

                        $data[$key]['foto'] = 'http://entra.learnjsforfree.com/uploads/experience/' . $onephoto->foto;
                    }
                }
                $key++;
                return[
                    'status' => 'success',
                    'data' => $data,
                    'lengthData'=>$count,
                ];
            }else if ($merchant){
                      $count = \common\models\Merchandise::find()->where(['like', 'nama', $model->keyword])->count();
                if ($user) {
                    $model->id_user = $user->id;
                }
                $model->save(false);
                $data = [];
                foreach ($merchant as $key => $val) {
                    $foto = \common\models\FotoMerchandise::find()->where(['id_merchandise' => $val->id_merchandise])->all();
                    $data[$key]['id'] = $val->id_merchandise;
                    $data[$key]['nama'] = $val->nama;
                    $carikategori = \common\models\KategoriMerchandise::find()->where(['id_kategori'=>$val->kategori])->one();
                    if ($carikategori){
                    $data[$key]['kategori'] = $carikategori->nama;
                    }
                    $data[$key]['harga'] = $val->harga;
                    $data[$key]['deskripsi'] = $val->deskripsi;
                    $data[$key]['alamat'] = $val->alamat;
                     $data[$key]['rating'] = $val->rating;
                    $photoone = \common\models\FotoMerchandise::find()->where(['id_merchandise' => $val->id_merchandise])->one();

                    if ($photoone) {
                        $data[$key]['foto'] ='http://entra.learnjsforfree.com/uploads/merchant/' . $photoone->foto;
                    }
                } $key++;
                return[
                    'status' => 'success',
                    'data' => $data,
                      'lenghtData'=>$count,
                ];
            }else if ($desa){
                       $count = \common\models\Villages::find()->where(['like', 'name', $model->keyword])->count();
                if ($user) {
                    $model->id_user = $user->id;
                }
                $model->save(false);
                $data = [];
                foreach ($desa as $key => $val) {
                    $foto = \common\models\FotoDesa::find()->where(['id_desa' => $val->id])->all();
                    $data[$key]['id'] = $val->id;
                    $data[$key]['nama'] = $val->name;
                    $data[$key]['rating'] = $val->rating;

                    $data[$key]['alamat'] = $val->address;
                     $data[$key]['deskripsi'] = $val->description;
                    $photoone = \common\models\FotoDesa::find()->where(['id_desa' => $val->id])->one();

                    if ($photoone) {
                        $data[$key]['foto'] = 'http://entra.learnjsforfree.com/uploads/desa/' . $photoone->foto;
                    }
                } $key++;
                return[
                    'status' => 'success',
                    'data' => $data,
                      'lenghtData'=>$count,
                ];
            }
            else {
                return[
                    'status' => 'error',
                    'message' => 'Data tidak ditemukan'
                ];
            }
        } else {
            return[
                'status' => 'error',
                'message' => 'Data tidak bisa dicari'
            ];
        }
    }

    public function actionAllExperience($pulau = NULL, $provinsi = NULL, $kecamatan = NULL , $desa=NULL, $harga=NULL, $kategori=NULL, $limit=NULL, $rating=NULL) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        if ($pulau){
             $list = \common\models\Experience::find()->where(['id_pulau'=>$pulau])->all();
        if ($list) {
            $listcount = \common\models\Experience::find()->where(['id_pulau'=>$pulau])->count();
            $data = [];
            foreach ($list as $key => $val) {
                $data[$key]['id'] = $val->id_experience;
                $data[$key]['nama'] = $val->nama;
                $data[$key]['harga'] = Yii::$app->formatter->asInteger($val->price);
                $data[$key]['deskripsi'] = $val->deskripsi;
                $data[$key]['alamat'] = $val->alamat;
                 $data[$key]['rating'] = $val->rating;
                 $carikategori = \common\models\KategoriExperience::find()->where(['id_kategori'=>$val->id_kategori])->one();
                 if ($carikategori){
                     $data[$key]['kategori'] = $carikategori->nama;
                 }
                 
                $desa = \common\models\Villages::find()->where(['id' => $val->id_desa])->one();
                if ($desa) {
                    $data[$key]['desa'] = $desa->name;
                }
                $foto = \common\models\FotoExperience::find()->where(['id_experience' => $val->id_experience])->all();
                $onephoto = \common\models\FotoExperience::find()->where(['id_experience' => $val->id_experience])->one();
//                if ($foto) {
//                    $sub = [];
//                    foreach ($foto as $ft) {
//                        $sub_name['foto'] = 'http://entra.learnjsforfree.com/uploads/experience/' . $ft['foto'];
//                        array_push($sub, $sub_name);
//                    }
//                    $data[$key]['gallery'] = $sub;
//                }
                if ($onephoto) {

                    $data[$key]['foto'] = 'http://entra.learnjsforfree.com/uploads/experience/' . $onephoto->foto;
                }
            }
            $key++;
            return[
                'status' => 'success',
                'data' => $data,
                'lengthData' => $listcount,
            ];
        } else {

            return[
                'status' => 'error',
                'message' => 'Data tidak ditemukan'
            ];
        }
        }else if ($provinsi){
 $list = \common\models\Experience::find()->where(['id_provinsi'=>$provinsi])->all();
        if ($list) {
            $listcount = \common\models\Experience::find()->where(['id_provinsi'=>$provinsi])->count();
            $data = [];
            foreach ($list as $key => $val) {
                $data[$key]['id'] = $val->id_experience;
                $data[$key]['nama'] = $val->nama;
                $data[$key]['harga'] = Yii::$app->formatter->asInteger($val->price);
                $data[$key]['deskripsi'] = $val->deskripsi;
                $data[$key]['alamat'] = $val->alamat;
                 $data[$key]['rating'] = $val->rating;
                $carikategori = \common\models\KategoriExperience::find()->where(['id_kategori'=>$val->id_kategori])->one();
                 if ($carikategori){
                     $data[$key]['kategori'] = $carikategori->nama;
                 }
                $desa = \common\models\Villages::find()->where(['id' => $val->id_desa])->one();
                if ($desa) {
                    $data[$key]['desa'] = $desa->name;
                }
                $foto = \common\models\FotoExperience::find()->where(['id_experience' => $val->id_experience])->all();
                $onephoto = \common\models\FotoExperience::find()->where(['id_experience' => $val->id_experience])->one();
//                if ($foto) {
//                    $sub = [];
//                    foreach ($foto as $ft) {
//                        $sub_name['foto'] = 'http://entra.learnjsforfree.com/uploads/experience/' . $ft['foto'];
//                        array_push($sub, $sub_name);
//                    }
//                    $data[$key]['gallery'] = $sub;
//                }
                if ($onephoto) {

                    $data[$key]['foto'] = 'http://entra.learnjsforfree.com/uploads/experience/' . $onephoto->foto;
                }
            }
            $key++;
            return[
                'status' => 'success',
                'data' => $data,
                'lengthData' => $listcount,
            ];
        } else {

            return[
                'status' => 'error',
                'message' => 'Data tidak ditemukan'
            ];
        }
        }else if ($kecamatan){
 $list = \common\models\Experience::find()->where(['id_kecamatan'=>$kecamatan])->all();
        if ($list) {
            $listcount = \common\models\Experience::find()->where(['id_kecamatan'=>$kecamatan])->count();
            $data = [];
            foreach ($list as $key => $val) {
                $data[$key]['id'] = $val->id_experience;
                $data[$key]['nama'] = $val->nama;
                $data[$key]['harga'] = Yii::$app->formatter->asInteger($val->price);
                $data[$key]['deskripsi'] = $val->deskripsi;
                $data[$key]['alamat'] = $val->alamat;
                 $data[$key]['rating'] = $val->rating;
               $carikategori = \common\models\KategoriExperience::find()->where(['id_kategori'=>$val->id_kategori])->one();
                 if ($carikategori){
                     $data[$key]['kategori'] = $carikategori->nama;
                 }
                $desa = \common\models\Villages::find()->where(['id' => $val->id_desa])->one();
                if ($desa) {
                    $data[$key]['desa'] = $desa->name;
                }
                $foto = \common\models\FotoExperience::find()->where(['id_experience' => $val->id_experience])->all();
                $onephoto = \common\models\FotoExperience::find()->where(['id_experience' => $val->id_experience])->one();
//                if ($foto) {
//                    $sub = [];
//                    foreach ($foto as $ft) {
//                        $sub_name['foto'] = 'http://entra.learnjsforfree.com/uploads/experience/' . $ft['foto'];
//                        array_push($sub, $sub_name);
//                    }
//                    $data[$key]['gallery'] = $sub;
//                }
                if ($onephoto) {

                    $data[$key]['foto'] = 'http://entra.learnjsforfree.com/uploads/experience/' . $onephoto->foto;
                }
            }
            $key++;
            return[
                'status' => 'success',
                'data' => $data,
                'lengthData' => $listcount,
            ];
        } else {

            return[
                'status' => 'error',
                'message' => 'Data tidak ditemukan'
            ];
        }
        }else if ($desa){
 $list = \common\models\Experience::find()->where(['id_desa'=>$desa])->all();
        if ($list) {
            $listcount = \common\models\Experience::find()->where(['id_desa'=>$desa])->count();
            $data = [];
            foreach ($list as $key => $val) {
                $data[$key]['id'] = $val->id_experience;
                $data[$key]['nama'] = $val->nama;
                $data[$key]['harga'] = Yii::$app->formatter->asInteger($val->price);
                $data[$key]['deskripsi'] = $val->deskripsi;
                $data[$key]['alamat'] = $val->alamat;
                 $data[$key]['rating'] = $val->rating;
                $carikategori = \common\models\KategoriExperience::find()->where(['id_kategori'=>$val->id_kategori])->one();
                 if ($carikategori){
                     $data[$key]['kategori'] = $carikategori->nama;
                 }
                $desa = \common\models\Villages::find()->where(['id' => $val->id_desa])->one();
                if ($desa) {
                    $data[$key]['desa'] = $desa->name;
                }
                $foto = \common\models\FotoExperience::find()->where(['id_experience' => $val->id_experience])->all();
                $onephoto = \common\models\FotoExperience::find()->where(['id_experience' => $val->id_experience])->one();
//                if ($foto) {
//                    $sub = [];
//                    foreach ($foto as $ft) {
//                        $sub_name['foto'] = 'http://entra.learnjsforfree.com/uploads/experience/' . $ft['foto'];
//                        array_push($sub, $sub_name);
//                    }
//                    $data[$key]['gallery'] = $sub;
//                }
                if ($onephoto) {

                    $data[$key]['foto'] = 'http://entra.learnjsforfree.com/uploads/experience/' . $onephoto->foto;
                }
            }
            $key++;
            return[
                'status' => 'success',
                'data' => $data,
                'lengthData' => $listcount,
            ];
        } else {

            return[
                'status' => 'error',
                'message' => 'Data tidak ditemukan'
            ];
        }
        }else if ($harga){
            if ($harga=='Terendah'){
 $list = \common\models\Experience::find()->orderBy('price  ASC')->all();
            } else if ($harga=='Tertinggi'){
                $list = \common\models\Experience::find()->orderBy('price DESC')->all();
            }
        if ($list) {
            if ($harga=='Terendah'){
            $listcount = \common\models\Experience::find()->orderBy('price  ASC')->count();
            }else if ($harga=='Tertinggi'){
                  $listcount = \common\models\Experience::find()->orderBy('price  DESC')->count();
            }
            $data = [];
            foreach ($list as $key => $val) {
                $data[$key]['id'] = $val->id_experience;
                $data[$key]['nama'] = $val->nama;
                $data[$key]['harga'] = Yii::$app->formatter->asInteger($val->price);
                $data[$key]['deskripsi'] = $val->deskripsi;
                $data[$key]['alamat'] = $val->alamat;
                 $data[$key]['rating'] = $val->rating;
                 $carikategori = \common\models\KategoriExperience::find()->where(['id_kategori'=>$val->id_kategori])->one();
                 if ($carikategori){
                     $data[$key]['kategori'] = $carikategori->nama;
                 }
                $desa = \common\models\Villages::find()->where(['id' => $val->id_desa])->one();
                if ($desa) {
                    $data[$key]['desa'] = $desa->name;
                }
                $foto = \common\models\FotoExperience::find()->where(['id_experience' => $val->id_experience])->all();
                $onephoto = \common\models\FotoExperience::find()->where(['id_experience' => $val->id_experience])->one();
//                if ($foto) {
//                    $sub = [];
//                    foreach ($foto as $ft) {
//                        $sub_name['foto'] = 'http://entra.learnjsforfree.com/uploads/experience/' . $ft['foto'];
//                        array_push($sub, $sub_name);
//                    }
//                    $data[$key]['gallery'] = $sub;
//                }
                if ($onephoto) {

                    $data[$key]['foto'] = 'http://entra.learnjsforfree.com/uploads/experience/' . $onephoto->foto;
                }
            }
            $key++;
            return[
                'status' => 'success',
                'data' => $data,
                'lengthData' => $listcount,
            ];
        } else {

            return[
                'status' => 'error',
                'message' => 'Data tidak ditemukan'
            ];
        }
        }else if ($kategori){
 $list = \common\models\Experience::find()->where(['id_kategori'=>$kategori])->all();
        if ($list) {
            $listcount = \common\models\Experience::find()->where(['id_kategori'=>$kategori])->count();
            $data = [];
            foreach ($list as $key => $val) {
                $data[$key]['id'] = $val->id_experience;
                $data[$key]['nama'] = $val->nama;
                $data[$key]['harga'] = Yii::$app->formatter->asInteger($val->price);
                $data[$key]['deskripsi'] = $val->deskripsi;
                $data[$key]['alamat'] = $val->alamat;
                 $data[$key]['rating'] = $val->rating;
               $carikategori = \common\models\KategoriExperience::find()->where(['id_kategori'=>$val->id_kategori])->one();
                 if ($carikategori){
                     $data[$key]['kategori'] = $carikategori->nama;
                 }
                $desa = \common\models\Villages::find()->where(['id' => $val->id_desa])->one();
                if ($desa) {
                    $data[$key]['desa'] = $desa->name;
                }
                $foto = \common\models\FotoExperience::find()->where(['id_experience' => $val->id_experience])->all();
                $onephoto = \common\models\FotoExperience::find()->where(['id_experience' => $val->id_experience])->one();
//                if ($foto) {
//                    $sub = [];
//                    foreach ($foto as $ft) {
//                        $sub_name['foto'] = 'http://entra.learnjsforfree.com/uploads/experience/' . $ft['foto'];
//                        array_push($sub, $sub_name);
//                    }
//                    $data[$key]['gallery'] = $sub;
//                }
                if ($onephoto) {

                    $data[$key]['foto'] = 'http://entra.learnjsforfree.com/uploads/experience/' . $onephoto->foto;
                }
            }
            $key++;
            return[
                'status' => 'success',
                'data' => $data,
                'lengthData' => $listcount,
            ];
        } else {

            return[
                'status' => 'error',
                'message' => 'Data tidak ditemukan'
            ];
        }
        }else if ($limit){
 $list = \common\models\Experience::find()->limit($limit)->all();
        if ($list) {
            $listcount = \common\models\Experience::find()->count();
            if ($listcount>$limit){
                $total= $limit;
            }else {
                $total = $listcount;
            }
            $data = [];
            foreach ($list as $key => $val) {
                $data[$key]['id'] = $val->id_experience;
                $data[$key]['nama'] = $val->nama;
                $data[$key]['harga'] = Yii::$app->formatter->asInteger($val->price);
                $data[$key]['deskripsi'] = $val->deskripsi;
                $data[$key]['alamat'] = $val->alamat;
                 $data[$key]['rating'] = $val->rating;
               $carikategori = \common\models\KategoriExperience::find()->where(['id_kategori'=>$val->id_kategori])->one();
                 if ($carikategori){
                     $data[$key]['kategori'] = $carikategori->nama;
                 }
                $desa = \common\models\Villages::find()->where(['id' => $val->id_desa])->one();
                if ($desa) {
                    $data[$key]['desa'] = $desa->name;
                }
                $foto = \common\models\FotoExperience::find()->where(['id_experience' => $val->id_experience])->all();
                $onephoto = \common\models\FotoExperience::find()->where(['id_experience' => $val->id_experience])->one();
//                if ($foto) {
//                    $sub = [];
//                    foreach ($foto as $ft) {
//                        $sub_name['foto'] = 'http://entra.learnjsforfree.com/uploads/experience/' . $ft['foto'];
//                        array_push($sub, $sub_name);
//                    }
//                    $data[$key]['gallery'] = $sub;
//                }
                if ($onephoto) {

                    $data[$key]['foto'] = 'http://entra.learnjsforfree.com/uploads/experience/' . $onephoto->foto;
                }
            }
            $key++;
            return[
                'status' => 'success',
                'data' => $data,
                'lengthData' => $total,
            ];
        } else {

            return[
                'status' => 'error',
                'message' => 'Data tidak ditemukan'
            ];
        }
        }else {
             $list = \common\models\Experience::find()->all();
        if ($list) {
            $listcount = \common\models\Experience::find()->count();
            $data = [];
            foreach ($list as $key => $val) {
                $data[$key]['id'] = $val->id_experience;
                $data[$key]['nama'] = $val->nama;
                $data[$key]['harga'] = Yii::$app->formatter->asInteger($val->price);
                $data[$key]['deskripsi'] = $val->deskripsi;
                $data[$key]['alamat'] = $val->alamat;
                 $data[$key]['rating'] = $val->rating;
               $carikategori = \common\models\KategoriExperience::find()->where(['id_kategori'=>$val->id_kategori])->one();
                 if ($carikategori){
                     $data[$key]['kategori'] = $carikategori->nama;
                 }
                $desa = \common\models\Villages::find()->where(['id' => $val->id_desa])->one();
                if ($desa) {
                    $data[$key]['desa'] = $desa->name;
                }
                $foto = \common\models\FotoExperience::find()->where(['id_experience' => $val->id_experience])->all();
                $onephoto = \common\models\FotoExperience::find()->where(['id_experience' => $val->id_experience])->one();
//                if ($foto) {
//                    $sub = [];
//                    foreach ($foto as $ft) {
//                        $sub_name['foto'] = 'http://entra.learnjsforfree.com/uploads/experience/' . $ft['foto'];
//                        array_push($sub, $sub_name);
//                    }
//                    $data[$key]['gallery'] = $sub;
//                }
                if ($onephoto) {

                    $data[$key]['foto'] = 'http://entra.learnjsforfree.com/uploads/experience/' . $onephoto->foto;
                }
            }
            $key++;
            return[
                'status' => 'success',
                'data' => $data,
                'lengthData' => $listcount,
            ];
        } else {

            return[
                'status' => 'error',
                'message' => 'Data tidak ditemukan'
            ];
        }
        }
       
    }

    public function actionAllAkomodasi($pulau = NULL, $provinsi = NULL, $kecamatan = NULL , $desa=NULL, $harga=NULL, $kategori=NULL, $limit=NULL) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        if ($pulau){
 $akomodasi = \common\models\Accommodation::find()->where(['id_pulau'=>$pulau])->all();
        $data = [];
        if ($akomodasi){
             $count = \common\models\Accommodation::find()->where(['id_pulau'=>$pulau])->count();
              foreach ($akomodasi as $key => $val) {

            $desa = \common\models\Villages::find()->where(['id' => $val->id_desa])->one();
            $provinsi = \common\models\Provinces::find()->where(['id' => $val->id_provinsi])->one();
            $pulau = \common\models\Island::find()->where(['id' => $provinsi->island_id])->one();
            $foto = \common\models\FotoAccommodation::find()->where(['id_homestay' => $val->id_homestay])->all();
            $data[$key]['id'] = $val->id_homestay;
            $data[$key]['nama'] = $val->nama_homestay;
            $data[$key]['rating'] = $val->rating;
            $data[$key]['harga'] = Yii::$app->formatter->asInteger($val->harga);
            $carikategori = \common\models\KategoriAccommodation::find()->where(['id_kategori'=>$val->id_kategori])->one();
            if ($carikategori){
                  $data[$key]['kategori'] = $carikategori->nama_kategori;
            }
          
            $data[$key]['alamat'] = $val->alamat;
            if ($pulau) {
                $data[$key]['pulau'] = $pulau->name;
            }

               $onephoto = \common\models\FotoAccommodation::find()->where(['id_homestay' => $val->id_homestay])->one();
         
               if ($onephoto) {

                    $data[$key]['foto'] = 'http://entra.learnjsforfree.com/uploads/akomodasi/' . $onephoto->foto;
                }
        } $key++;

        return [
            'status' => 'success',
            'data' => $data,
            'lengthData'=>$count,
        ];
        }else{
            return[
                'status'=>'error',
                'message'=>'Data tidak ditemukan'
            ];
        } 
        }else if ($provinsi){
 $akomodasi = \common\models\Accommodation::find()->where(['id_provinsi'=>$provinsi])->all();
        $data = [];
        if ($akomodasi){
             $count = \common\models\Accommodation::find()->where(['id_provinsi'=>$provinsi])->count();
              foreach ($akomodasi as $key => $val) {

            $desa = \common\models\Villages::find()->where(['id' => $val->id_desa])->one();
            $provinsi = \common\models\Provinces::find()->where(['id' => $val->id_provinsi])->one();
            $pulau = \common\models\Island::find()->where(['id' => $provinsi->island_id])->one();
            $foto = \common\models\FotoAccommodation::find()->where(['id_homestay' => $val->id_homestay])->all();
            $data[$key]['id'] = $val->id_homestay;
            $data[$key]['nama'] = $val->nama_homestay;
            $data[$key]['rating'] = $val->rating;
            $data[$key]['harga'] = Yii::$app->formatter->asInteger($val->harga);
            $carikategori = \common\models\KategoriAccommodation::find()->where(['id_kategori'=>$val->id_kategori])->one();
            if ($carikategori){
                  $data[$key]['kategori'] = $carikategori->nama_kategori;
            }
            $data[$key]['alamat'] = $val->alamat;
            if ($pulau) {
                $data[$key]['pulau'] = $pulau->name;
            }


               $onephoto = \common\models\FotoAccommodation::find()->where(['id_homestay' => $val->id_homestay])->one();
         
               if ($onephoto) {

                    $data[$key]['foto'] = 'http://entra.learnjsforfree.com/uploads/akomodasi/' . $onephoto->foto;
                }
        } $key++;

        return [
            'status' => 'success',
            'data' => $data,
            'lengthData'=>$count,
        ];
        }else{
            return[
                'status'=>'error',
                'message'=>'Data tidak ditemukan'
            ];
        }
        }else if ($kecamatan){
 $akomodasi = \common\models\Accommodation::find()->where(['id_kecamatan'=>$kecamatan])->all();
        $data = [];
        if ($akomodasi){
             $count = \common\models\Accommodation::find()->where(['id_kecamatan'=>$kecamatan])->count();
              foreach ($akomodasi as $key => $val) {

            $desa = \common\models\Villages::find()->where(['id' => $val->id_desa])->one();
            $provinsi = \common\models\Provinces::find()->where(['id' => $val->id_provinsi])->one();
            $pulau = \common\models\Island::find()->where(['id' => $provinsi->island_id])->one();
            $foto = \common\models\FotoAccommodation::find()->where(['id_homestay' => $val->id_homestay])->all();
            $data[$key]['id'] = $val->id_homestay;
            $data[$key]['nama'] = $val->nama_homestay;
            $data[$key]['rating'] = $val->rating;
            $data[$key]['harga'] = Yii::$app->formatter->asInteger($val->harga);
           $carikategori = \common\models\KategoriAccommodation::find()->where(['id_kategori'=>$val->id_kategori])->one();
            if ($carikategori){
                  $data[$key]['kategori'] = $carikategori->nama_kategori;
            }
            $data[$key]['alamat'] = $val->alamat;
            if ($pulau) {
                $data[$key]['pulau'] = $pulau->name;
            }


               $onephoto = \common\models\FotoAccommodation::find()->where(['id_homestay' => $val->id_homestay])->one();
         
               if ($onephoto) {

                    $data[$key]['foto'] = 'http://entra.learnjsforfree.com/uploads/akomodasi/' . $onephoto->foto;
                }
        } $key++;

        return [
            'status' => 'success',
            'data' => $data,
            'lengthData'=>$count,
        ];
        }else{
            return[
                'status'=>'error',
                'message'=>'Data tidak ditemukan'
            ];
        }
        }else if ($desa){
 $akomodasi = \common\models\Accommodation::find()->where(['id_desa'=>$desa])->all();
        $data = [];
        if ($akomodasi){
             $count = \common\models\Accommodation::find()->count();
              foreach ($akomodasi as $key => $val) {

            $desa = \common\models\Villages::find()->where(['id' => $val->id_desa])->one();
            $provinsi = \common\models\Provinces::find()->where(['id' => $val->id_provinsi])->one();
            $pulau = \common\models\Island::find()->where(['id' => $provinsi->island_id])->one();
            $foto = \common\models\FotoAccommodation::find()->where(['id_homestay' => $val->id_homestay])->all();
            $data[$key]['id'] = $val->id_homestay;
            $data[$key]['nama'] = $val->nama_homestay;
            $data[$key]['rating'] = $val->rating;
            $data[$key]['harga'] = Yii::$app->formatter->asInteger($val->harga);
           $carikategori = \common\models\KategoriAccommodation::find()->where(['id_kategori'=>$val->id_kategori])->one();
            if ($carikategori){
                  $data[$key]['kategori'] = $carikategori->nama_kategori;
            }
            $data[$key]['alamat'] = $val->alamat;
            if ($pulau) {
                $data[$key]['pulau'] = $pulau->name;
            }


               $onephoto = \common\models\FotoAccommodation::find()->where(['id_homestay' => $val->id_homestay])->one();
         
               if ($onephoto) {

                    $data[$key]['foto'] = 'http://entra.learnjsforfree.com/uploads/akomodasi/' . $onephoto->foto;
                }
        } $key++;

        return [
            'status' => 'success',
            'data' => $data,
            'lengthData'=>$count,
        ];
        }else{
            return[
                'status'=>'error',
                'message'=>'Data tidak ditemukan'
            ];
        }
        }else if ($kategori){
 $akomodasi = \common\models\Accommodation::find()->where(['id_kategori'=>$kategori])->all();
        $data = [];
        if ($akomodasi){
             $count = \common\models\Accommodation::find()->where(['id_kategori'=>$kategori])->count();
              foreach ($akomodasi as $key => $val) {

            $desa = \common\models\Villages::find()->where(['id' => $val->id_desa])->one();
            $provinsi = \common\models\Provinces::find()->where(['id' => $val->id_provinsi])->one();
            $pulau = \common\models\Island::find()->where(['id' => $provinsi->island_id])->one();
            $foto = \common\models\FotoAccommodation::find()->where(['id_homestay' => $val->id_homestay])->all();
            $data[$key]['id'] = $val->id_homestay;
            $data[$key]['nama'] = $val->nama_homestay;
            $data[$key]['rating'] = $val->rating;
            $data[$key]['harga'] = Yii::$app->formatter->asInteger($val->harga);
            $carikategori = \common\models\KategoriAccommodation::find()->where(['id_kategori'=>$val->id_kategori])->one();
            if ($carikategori){
                  $data[$key]['kategori'] = $carikategori->nama_kategori;
            }
            $data[$key]['alamat'] = $val->alamat;
            if ($pulau) {
                $data[$key]['pulau'] = $pulau->name;
            }



//            if ($foto) {
//                $sub = [];
//                foreach ($foto as $ft) {
//                    $sub_name['foto'] = 'http://entra.learnjsforfree.com/uploads/akomodasi/' . $ft['foto'];
//                    array_push($sub, $sub_name);
//                }
//                   $data[$key]['foto'] = $sub;
//            }
               $onephoto = \common\models\FotoAccommodation::find()->where(['id_homestay' => $val->id_homestay])->one();
         
               if ($onephoto) {

                    $data[$key]['foto'] = 'http://entra.learnjsforfree.com/uploads/akomodasi/' . $onephoto->foto;
                }
        } $key++;

        return [
            'status' => 'success',
            'data' => $data,
            'lengthData'=>$count,
        ];
        }else{
            return[
                'status'=>'error',
                'message'=>'Data tidak ditemukan'
            ];
        }
        }else if ($limit){
 $akomodasi = \common\models\Accommodation::find()->limit($limit)->all();
        $data = [];
        if ($akomodasi){
             $count = \common\models\Accommodation::find()->count();
             if ($count>$limit){
                $total= $limit;
            }else {
                $total = $count;
            }
              foreach ($akomodasi as $key => $val) {

            $desa = \common\models\Villages::find()->where(['id' => $val->id_desa])->one();
            $provinsi = \common\models\Provinces::find()->where(['id' => $val->id_provinsi])->one();
            $pulau = \common\models\Island::find()->where(['id' => $provinsi->island_id])->one();
            $foto = \common\models\FotoAccommodation::find()->where(['id_homestay' => $val->id_homestay])->all();
            $data[$key]['id'] = $val->id_homestay;
            $data[$key]['nama'] = $val->nama_homestay;
            $data[$key]['rating'] = $val->rating;
            $data[$key]['harga'] = Yii::$app->formatter->asInteger($val->harga);
           $carikategori = \common\models\KategoriAccommodation::find()->where(['id_kategori'=>$val->id_kategori])->one();
            if ($carikategori){
                  $data[$key]['kategori'] = $carikategori->nama_kategori;
            }
            $data[$key]['alamat'] = $val->alamat;
            if ($pulau) {
                $data[$key]['pulau'] = $pulau->name;
            }



//            if ($foto) {
//                $sub = [];
//                foreach ($foto as $ft) {
//                    $sub_name['foto'] = 'http://entra.learnjsforfree.com/uploads/akomodasi/' . $ft['foto'];
//                    array_push($sub, $sub_name);
//                }
//                   $data[$key]['foto'] = $sub;
//            }
               $onephoto = \common\models\FotoAccommodation::find()->where(['id_homestay' => $val->id_homestay])->one();
         
               if ($onephoto) {

                    $data[$key]['foto'] = 'http://entra.learnjsforfree.com/uploads/akomodasi/' . $onephoto->foto;
                }
        } $key++;

        return [
            'status' => 'success',
            'data' => $data,
            'lengthData'=>$count,
        ];
        }else{
            return[
                'status'=>'error',
                'message'=>'Data tidak ditemukan'
            ];
        }
        }else if ($harga){
            if ($harga=='Terendah'){
 $akomodasi = \common\models\Accommodation::find()->orderBy('harga ASC')->all();
            }else if ($harga=='Tertinggi'){
                $akomodasi = \common\models\Accommodation::find()->orderBy('harga DESC')->all();
            }
        $data = [];
        if ($akomodasi){
            if ($harga=='Terendah'){
                
            
             $count = \common\models\Accommodation::find()->orderBy('harga ASC')->count();
            }else if ($harga=='Terendah'){
              $count = \common\models\Accommodation::find()->orderBy('harga DESC')->count();
            }
              foreach ($akomodasi as $key => $val) {

            $desa = \common\models\Villages::find()->where(['id' => $val->id_desa])->one();
            $provinsi = \common\models\Provinces::find()->where(['id' => $val->id_provinsi])->one();
            $pulau = \common\models\Island::find()->where(['id' => $provinsi->island_id])->one();
            $foto = \common\models\FotoAccommodation::find()->where(['id_homestay' => $val->id_homestay])->all();
            $data[$key]['id'] = $val->id_homestay;
            $data[$key]['nama'] = $val->nama_homestay;
            $data[$key]['rating'] = $val->rating;
            $data[$key]['harga'] = Yii::$app->formatter->asInteger($val->harga);
           $carikategori = \common\models\KategoriAccommodation::find()->where(['id_kategori'=>$val->id_kategori])->one();
            if ($carikategori){
                  $data[$key]['kategori'] = $carikategori->nama_kategori;
            }
            $data[$key]['alamat'] = $val->alamat;
            if ($pulau) {
                $data[$key]['pulau'] = $pulau->name;
            }



//            if ($foto) {
//                $sub = [];
//                foreach ($foto as $ft) {
//                    $sub_name['foto'] = 'http://entra.learnjsforfree.com/uploads/akomodasi/' . $ft['foto'];
//                    array_push($sub, $sub_name);
//                }
//                   $data[$key]['foto'] = $sub;
//            }
               $onephoto = \common\models\FotoAccommodation::find()->where(['id_homestay' => $val->id_homestay])->one();
         
               if ($onephoto) {

                    $data[$key]['foto'] = 'http://entra.learnjsforfree.com/uploads/akomodasi/' . $onephoto->foto;
                }
        } $key++;

        return [
            'status' => 'success',
            'data' => $data,
            'lengthData'=>$count,
        ];
        }else{
            return[
                'status'=>'error',
                'message'=>'Data tidak ditemukan'
            ];
        }
        }else {
             $akomodasi = \common\models\Accommodation::find()->all();
        $data = [];
        if ($akomodasi){
             $count = \common\models\Accommodation::find()->count();
              foreach ($akomodasi as $key => $val) {

            $desa = \common\models\Villages::find()->where(['id' => $val->id_desa])->one();
            $provinsi = \common\models\Provinces::find()->where(['id' => $val->id_provinsi])->one();
            $pulau = \common\models\Island::find()->where(['id' => $provinsi->island_id])->one();
            $foto = \common\models\FotoAccommodation::find()->where(['id_homestay' => $val->id_homestay])->all();
            $data[$key]['id'] = $val->id_homestay;
            $data[$key]['nama'] = $val->nama_homestay;
            $data[$key]['rating'] = $val->rating;
            $data[$key]['harga'] = Yii::$app->formatter->asInteger($val->harga);
           $carikategori = \common\models\KategoriAccommodation::find()->where(['id_kategori'=>$val->id_kategori])->one();
            if ($carikategori){
                  $data[$key]['kategori'] = $carikategori->nama_kategori;
            }
            $data[$key]['alamat'] = $val->alamat;
            if ($pulau) {
                $data[$key]['pulau'] = $pulau->name;
            }



//            if ($foto) {
//                $sub = [];
//                foreach ($foto as $ft) {
//                    $sub_name['foto'] = 'http://entra.learnjsforfree.com/uploads/akomodasi/' . $ft['foto'];
//                    array_push($sub, $sub_name);
//                }
//                   $data[$key]['foto'] = $sub;
//            }
               $onephoto = \common\models\FotoAccommodation::find()->where(['id_homestay' => $val->id_homestay])->one();
         
               if ($onephoto) {

                    $data[$key]['foto'] = 'http://entra.learnjsforfree.com/uploads/akomodasi/' . $onephoto->foto;
                }
        } $key++;

        return [
            'status' => 'success',
            'data' => $data,
            'lengthData'=>$count,
        ];
        }else{
            return[
                'status'=>'error',
                'message'=>'Data tidak ditemukan'
            ];
        }
        }
       
      
    }

    public function actionAllTransportasi($pulau = NULL, $provinsi = NULL, $kecamatan = NULL , $desa=NULL, $harga=NULL, $kategori=NULL, $limit=NULL, $rating=NULL) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        if ($pulau){
                        $transportdata = \common\models\Transportation::find()->where(['id_pulau'=>$pulau])->all();

        $data = [];
        if ($transportdata){
               $count = \common\models\Transportation::find()->where(['id_pulau'=>$pulau])->count();

                foreach ($transportdata as $key => $val) {
            $desa = \common\models\Villages::find()->where(['id' => $val->id_desa])->one();
            $provinsi = \common\models\Provinces::find()->where(['id' => $val->id_provinsi])->one();
            $pulau = \common\models\Island::find()->where(['id' => $provinsi->island_id])->one();

            $data[$key]['id'] = $val->id_transportation;
            $carikategori =\common\models\KategoriTransportasi::find()->where(['id_kategori'=>$val->jenis_transportasi])->one();
            if ($carikategori){
            $data[$key]['kategori'] = $carikategori->nama;
            }
            $data[$key]['nama'] = $val->nama;
           
            $data[$key]['deskripsi'] = $val->deskripsi;
             $data[$key]['rating'] = $val->rating;
         $data[$key]['alamat'] = $val->alamat;
            $data[$key]['desa'] = $desa->name;
            $data[$key]['provinsi'] = $provinsi->name;
            if ($pulau) {
                $data[$key]['pulau'] = $pulau->name;
            }
            $data[$key]['harga'] = Yii::$app->formatter->asInteger($val->price);
            $onephoto = \common\models\FotoTransportasi::find()->where(['id_transportasi' => $val->id_transportation])->one();
         
               if ($onephoto) {

                    $data[$key]['foto'] = 'http://entra.learnjsforfree.com/uploads/transport/' . $onephoto->foto;
                }
        }$key++;

        return [
            'status' => 'success',
            'data' => $data,
            'lengthData'=>$count,
        ];
        }else{
            return[
                'status'=>'error',
                'message'=>'Data tidak ditemukan'
            ];
        }
        }else if ($provinsi){
                        $transportdata = \common\models\Transportation::find()->where(['id_provinsi'=>$provinsi])->all();

        $data = [];
        if ($transportdata){
               $count = \common\models\Transportation::find()->where(['id_provinsi'=>$provinsi])->count();

                foreach ($transportdata as $key => $val) {
            $desa = \common\models\Villages::find()->where(['id' => $val->id_desa])->one();
            $provinsi = \common\models\Provinces::find()->where(['id' => $val->id_provinsi])->one();
            $pulau = \common\models\Island::find()->where(['id' => $provinsi->island_id])->one();

            $data[$key]['id'] = $val->id_transportation;
           $carikategori =\common\models\KategoriTransportasi::find()->where(['id_kategori'=>$val->jenis_transportasi])->one();
            if ($carikategori){
            $data[$key]['kategori'] = $carikategori->nama;
            }
            $data[$key]['nama'] = $val->nama;
          
            $data[$key]['deskripsi'] = $val->deskripsi;
             $data[$key]['rating'] = $val->rating;
              $data[$key]['alamat'] = $val->alamat;
            $data[$key]['desa'] = $desa->name;
            $data[$key]['provinsi'] = $provinsi->name;
            if ($pulau) {
                $data[$key]['pulau'] = $pulau->name;
            }
            $data[$key]['harga'] = Yii::$app->formatter->asInteger($val->price);
            $onephoto = \common\models\FotoTransportasi::find()->where(['id_transportasi' => $val->id_transportation])->one();
         
               if ($onephoto) {

                    $data[$key]['foto'] = 'http://entra.learnjsforfree.com/uploads/transport/' . $onephoto->foto;
                }
        }$key++;

        return [
            'status' => 'success',
            'data' => $data,
            'lengthData'=>$count,
        ];
        }else{
            return[
                'status'=>'error',
                'message'=>'Data tidak ditemukan'
            ];
        }
        }else if ($kecamatan){
                        $transportdata = \common\models\Transportation::find()->where(['id_kecamatan'=>$kecamatan])->all();

        $data = [];
        if ($transportdata){
               $count = \common\models\Transportation::find()->where(['id_kecamatan'=>$kecamatan])->count();

                foreach ($transportdata as $key => $val) {
            $desa = \common\models\Villages::find()->where(['id' => $val->id_desa])->one();
            $provinsi = \common\models\Provinces::find()->where(['id' => $val->id_provinsi])->one();
            $pulau = \common\models\Island::find()->where(['id' => $provinsi->island_id])->one();

            $data[$key]['id'] = $val->id_transportation;
           $carikategori =\common\models\KategoriTransportasi::find()->where(['id_kategori'=>$val->jenis_transportasi])->one();
            if ($carikategori){
            $data[$key]['kategori'] = $carikategori->nama;
            }
            $data[$key]['nama'] = $val->nama;
          
            $data[$key]['deskripsi'] = $val->deskripsi;
             $data[$key]['rating'] = $val->rating;
              $data[$key]['alamat'] = $val->alamat;
            $data[$key]['desa'] = $desa->name;
            $data[$key]['provinsi'] = $provinsi->name;
            if ($pulau) {
                $data[$key]['pulau'] = $pulau->name;
            }
            $data[$key]['harga'] = Yii::$app->formatter->asInteger($val->price);
            $onephoto = \common\models\FotoTransportasi::find()->where(['id_transportasi' => $val->id_transportation])->one();
         
               if ($onephoto) {

                    $data[$key]['foto'] = 'http://entra.learnjsforfree.com/uploads/transport/' . $onephoto->foto;
                }
        }$key++;

        return [
            'status' => 'success',
            'data' => $data,
            'lengthData'=>$count,
        ];
        }else{
            return[
                'status'=>'error',
                'message'=>'Data tidak ditemukan'
            ];
        }
        }else if ($desa){
                        $transportdata = \common\models\Transportation::find()->where(['id_desa'=>$desa])->all();

        $data = [];
        if ($transportdata){
               $count = \common\models\Transportation::find()->where(['id_desa'=>$desa])->count();

                foreach ($transportdata as $key => $val) {
            $desa = \common\models\Villages::find()->where(['id' => $val->id_desa])->one();
            $provinsi = \common\models\Provinces::find()->where(['id' => $val->id_provinsi])->one();
            $pulau = \common\models\Island::find()->where(['id' => $provinsi->island_id])->one();

            $data[$key]['id'] = $val->id_transportation;
          $carikategori =\common\models\KategoriTransportasi::find()->where(['id_kategori'=>$val->jenis_transportasi])->one();
            if ($carikategori){
            $data[$key]['kategori'] = $carikategori->nama;
            }
            $data[$key]['nama'] = $val->nama;
            
            $data[$key]['deskripsi'] = $val->deskripsi;
             $data[$key]['rating'] = $val->rating;
              $data[$key]['alamat'] = $val->alamat;
            $data[$key]['desa'] = $desa->name;
            $data[$key]['provinsi'] = $provinsi->name;
            if ($pulau) {
                $data[$key]['pulau'] = $pulau->name;
            }
            $data[$key]['harga'] = Yii::$app->formatter->asInteger($val->price);
            $onephoto = \common\models\FotoTransportasi::find()->where(['id_transportasi' => $val->id_transportation])->one();
         
               if ($onephoto) {

                    $data[$key]['foto'] = 'http://entra.learnjsforfree.com/uploads/transport/' . $onephoto->foto;
                }
        }$key++;

        return [
            'status' => 'success',
            'data' => $data,
            'lengthData'=>$count,
        ];
        }else{
            return[
                'status'=>'error',
                'message'=>'Data tidak ditemukan'
            ];
        }
        }else if ($harga){
            if($harga=='Terendah'){
                        $transportdata = \common\models\Transportation::find()->orderBy('price ASC')->all();
            }else if ($harga=='Tertinggi'){
                  $transportdata = \common\models\Transportation::find()->orderBy('price DESC')->all();
            }
        $data = [];
        if ($transportdata){
               $count = \common\models\Transportation::find()->orderBy('price '.$harga)->count();

                foreach ($transportdata as $key => $val) {
            $desa = \common\models\Villages::find()->where(['id' => $val->id_desa])->one();
            $provinsi = \common\models\Provinces::find()->where(['id' => $val->id_provinsi])->one();
            $pulau = \common\models\Island::find()->where(['id' => $provinsi->island_id])->one();

            $data[$key]['id'] = $val->id_transportation;
           $carikategori =\common\models\KategoriTransportasi::find()->where(['id_kategori'=>$val->jenis_transportasi])->one();
            if ($carikategori){
            $data[$key]['kategori'] = $carikategori->nama;
            }
            $data[$key]['nama'] = $val->nama;
           
            $data[$key]['deskripsi'] = $val->deskripsi;
             $data[$key]['rating'] = $val->rating;
              $data[$key]['alamat'] = $val->alamat;
            $data[$key]['desa'] = $desa->name;
            $data[$key]['provinsi'] = $provinsi->name;
            if ($pulau) {
                $data[$key]['pulau'] = $pulau->name;
            }
            $data[$key]['harga'] = Yii::$app->formatter->asInteger($val->price);
            $onephoto = \common\models\FotoTransportasi::find()->where(['id_transportasi' => $val->id_transportation])->one();
         
               if ($onephoto) {

                    $data[$key]['foto'] = 'http://entra.learnjsforfree.com/uploads/transport/' . $onephoto->foto;
                }
        }$key++;

        return [
            'status' => 'success',
            'data' => $data,
            'lengthData'=>$count,
        ];
        }else{
            return[
                'status'=>'error',
                'message'=>'Data tidak ditemukan'
            ];
        }
        }else if ($kategori){
                        $transportdata = \common\models\Transportation::find()->where(['jenis_transportasi'=>$kategori])->all();

        $data = [];
        if ($transportdata){
               $count = \common\models\Transportation::find()->where(['jenis_transportasi'=>$kategori])->count();

                foreach ($transportdata as $key => $val) {
            $desa = \common\models\Villages::find()->where(['id' => $val->id_desa])->one();
            $provinsi = \common\models\Provinces::find()->where(['id' => $val->id_provinsi])->one();
            $pulau = \common\models\Island::find()->where(['id' => $provinsi->island_id])->one();

            $data[$key]['id'] = $val->id_transportation;
           $carikategori =\common\models\KategoriTransportasi::find()->where(['id_kategori'=>$val->jenis_transportasi])->one();
            if ($carikategori){
            $data[$key]['kategori'] = $carikategori->nama;
            }
            $data[$key]['nama'] = $val->nama;
            
            $data[$key]['deskripsi'] = $val->deskripsi;
             $data[$key]['rating'] = $val->rating;
              $data[$key]['alamat'] = $val->alamat;
            $data[$key]['desa'] = $desa->name;
            $data[$key]['provinsi'] = $provinsi->name;
            if ($pulau) {
                $data[$key]['pulau'] = $pulau->name;
            }
            $data[$key]['harga'] = Yii::$app->formatter->asInteger($val->price);
            $onephoto = \common\models\FotoTransportasi::find()->where(['id_transportasi' => $val->id_transportation])->one();
         
               if ($onephoto) {

                    $data[$key]['foto'] = 'http://entra.learnjsforfree.com/uploads/transport/' . $onephoto->foto;
                }
        }$key++;

        return [
            'status' => 'success',
            'data' => $data,
            'lengthData'=>$count,
        ];
        }else{
            return[
                'status'=>'error',
                'message'=>'Data tidak ditemukan'
            ];
        }
        }else if ($limit){
                        $transportdata = \common\models\Transportation::find()->limit($limit)->all();

        $data = [];
        if ($transportdata){
               $count = \common\models\Transportation::find()->count();
                if ($count>$limit){
                $total= $limit;
            }else {
                $total = $count;
            }
                foreach ($transportdata as $key => $val) {
            $desa = \common\models\Villages::find()->where(['id' => $val->id_desa])->one();
            $provinsi = \common\models\Provinces::find()->where(['id' => $val->id_provinsi])->one();
            $pulau = \common\models\Island::find()->where(['id' => $provinsi->island_id])->one();

            $data[$key]['id'] = $val->id_transportation;
           $carikategori =\common\models\KategoriTransportasi::find()->where(['id_kategori'=>$val->jenis_transportasi])->one();
            if ($carikategori){
            $data[$key]['kategori'] = $carikategori->nama;
            }
            $data[$key]['nama'] = $val->nama;
          
            $data[$key]['deskripsi'] = $val->deskripsi;
             $data[$key]['rating'] = $val->rating;
              $data[$key]['alamat'] = $val->alamat;
            $data[$key]['desa'] = $desa->name;
            $data[$key]['provinsi'] = $provinsi->name;
            if ($pulau) {
                $data[$key]['pulau'] = $pulau->name;
            }
            $data[$key]['harga'] = Yii::$app->formatter->asInteger($val->price);
            $onephoto = \common\models\FotoTransportasi::find()->where(['id_transportasi' => $val->id_transportation])->one();
         
               if ($onephoto) {

                    $data[$key]['foto'] = 'http://entra.learnjsforfree.com/uploads/transport/' . $onephoto->foto;
                }
        }$key++;

        return [
            'status' => 'success',
            'data' => $data,
            'lengthData'=>$total,
        ];
        }else{
            return[
                'status'=>'error',
                'message'=>'Data tidak ditemukan'
            ];
        }
        }else if ($rating){
                                    $transportdata = \common\models\Transportation::find()->where(['rating'=>$rating])->all();

        $data = [];
        if ($transportdata){
               $count = \common\models\Transportation::find()->where(['rating'=>$rating])->count();

                foreach ($transportdata as $key => $val) {
            $desa = \common\models\Villages::find()->where(['id' => $val->id_desa])->one();
            $provinsi = \common\models\Provinces::find()->where(['id' => $val->id_provinsi])->one();
            $pulau = \common\models\Island::find()->where(['id' => $provinsi->island_id])->one();

            $data[$key]['id'] = $val->id_transportation;
           $carikategori =\common\models\KategoriTransportasi::find()->where(['id_kategori'=>$val->jenis_transportasi])->one();
            if ($carikategori){
            $data[$key]['kategori'] = $carikategori->nama;
            }
            $data[$key]['nama'] = $val->nama;
          
            $data[$key]['deskripsi'] = $val->deskripsi;
             $data[$key]['rating'] = $val->rating;
              $data[$key]['alamat'] = $val->alamat;
            $data[$key]['desa'] = $desa->name;
            $data[$key]['provinsi'] = $provinsi->name;
            if ($pulau) {
                $data[$key]['pulau'] = $pulau->name;
            }
            $data[$key]['harga'] = Yii::$app->formatter->asInteger($val->price);
            $onephoto = \common\models\FotoTransportasi::find()->where(['id_transportasi' => $val->id_transportation])->one();
         
               if ($onephoto) {

                    $data[$key]['foto'] = 'http://entra.learnjsforfree.com/uploads/transport/' . $onephoto->foto;
                }
        }$key++;

        return [
            'status' => 'success',
            'data' => $data,
            'lengthData'=>$count,
        ];
        }else{
            return[
                'status'=>'error',
                'message'=>'Data tidak ditemukan'
            ];
        }
        }
        
        
        else {
                $transportdata = \common\models\Transportation::find()->all();

        $data = [];
        if ($transportdata){
               $count = \common\models\Transportation::find()->count();

                foreach ($transportdata as $key => $val) {
            $desa = \common\models\Villages::find()->where(['id' => $val->id_desa])->one();
            $provinsi = \common\models\Provinces::find()->where(['id' => $val->id_provinsi])->one();
            $pulau = \common\models\Island::find()->where(['id' => $provinsi->island_id])->one();

            $data[$key]['id'] = $val->id_transportation;
           $carikategori =\common\models\KategoriTransportasi::find()->where(['id_kategori'=>$val->jenis_transportasi])->one();
            if ($carikategori){
            $data[$key]['kategori'] = $carikategori->nama;
            }
            $data[$key]['nama'] = $val->nama;
           
            $data[$key]['deskripsi'] = $val->deskripsi;
             $data[$key]['rating'] = $val->rating;
              $data[$key]['alamat'] = $val->alamat;
            $data[$key]['desa'] = $desa->name;
            $data[$key]['provinsi'] = $provinsi->name;
            if ($pulau) {
                $data[$key]['pulau'] = $pulau->name;
            }
            $data[$key]['harga'] = Yii::$app->formatter->asInteger($val->price);
            $onephoto = \common\models\FotoTransportasi::find()->where(['id_transportasi' => $val->id_transportation])->one();
         
               if ($onephoto) {

                    $data[$key]['foto'] = 'http://entra.learnjsforfree.com/uploads/transport/' . $onephoto->foto;
                }
        }$key++;

        return [
            'status' => 'success',
            'data' => $data,
            'lengthData'=>$count,
        ];
        }else{
            return[
                'status'=>'error',
                'message'=>'Data tidak ditemukan'
            ];
        }            
        }

    
    }

    public function actionAllMerchant($pulau =NULL, $provinsi= NULL, $kecamatan=NULL, $desa=NULL, $kategori=NULL, $harga=NULL, $limit=NULL ) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        if ($pulau){
        $merchant = \common\models\Merchandise::find()->where(['id_pulau'=>$pulau])->all();

        $data = [];
        if ($merchant){
              $count = \common\models\Merchandise::find()->where(['id_pulau'=>$pulau])->count();
              foreach ($merchant as $key => $val) {
            $foto = \common\models\FotoMerchandise::find()->where(['id_merchandise' => $val->id_merchandise])->orderBy('id_foto DESC')->one();
            $desa = \common\models\Villages::find()->where(['id' => $val->id_desa])->one();
            $provinsi = \common\models\Provinces::find()->where(['id' => $val->id_provinsi])->one();

            $data[$key]['id'] = $val->id_merchandise;
            $carikategori =\common\models\KategoriMerchandise::find()->where(['id_kategori'=>$val->kategori])->one();
            if ($carikategori){
            $data[$key]['kategori'] = $carikategori->nama;
            }
            $data[$key]['nama'] = $val->nama;
            $data[$key]['alamat'] = $val->alamat;
            $data[$key]['deskripsi'] = $val->deskripsi;
             $data[$key]['rating'] = $val->rating;
            $data[$key]['desa'] = $desa->name;
            if ($provinsi) {
                $data[$key]['provinsi'] = $provinsi->name;
                $pulau = \common\models\Island::find()->where(['id' => $provinsi->island_id])->one();
                if ($pulau) {

                    $data[$key]['pulau'] = $pulau->name;
                }
            }

            $data[$key]['harga'] = Yii::$app->formatter->asInteger($val->harga);
//            if ($foto) {
//                $sub = [];
//                foreach ($foto as $ft) {
//                    $sub_name['foto'] = 'http://entra.learnjsforfree.com/uploads/merchant/' . $ft['foto'];
//                    array_push($sub, $sub_name);
//                } $data[$key]['foto'] = $sub;
//            }
           
             $onephoto = \common\models\FotoMerchandise::find()->where(['id_merchandise' => $val->id_merchandise])->one();
         
               if ($onephoto) {

                    $data[$key]['foto'] = 'http://entra.learnjsforfree.com/uploads/merchant/' . $onephoto->foto;
                }
        } $key++;

        return [
            'status' => 'success',
            'data' => $data,
            'lengthData'=>$count,
        ];
        }else{
            return [
            'status' => 'error',
            'message'=>'Data tidak ditemukan'
        ]; 
        }
        } else if($provinsi){
            
        $merchant = \common\models\Merchandise::find()->where(['id_provinsi'=>$provinsi])->all();

        $data = [];
        if ($merchant){
              $count = \common\models\Merchandise::find()->where(['id_provinsi'=>$provinsi])->count();
              foreach ($merchant as $key => $val) {
            $foto = \common\models\FotoMerchandise::find()->where(['id_merchandise' => $val->id_merchandise])->orderBy('id_foto DESC')->one();
            $desa = \common\models\Villages::find()->where(['id' => $val->id_desa])->one();
            $provinsi = \common\models\Provinces::find()->where(['id' => $val->id_provinsi])->one();

            $data[$key]['id'] = $val->id_merchandise;
            $carikategori =\common\models\KategoriMerchandise::find()->where(['id_kategori'=>$val->kategori])->one();
            if ($carikategori){
            $data[$key]['kategori'] = $carikategori->nama;
            }
            $data[$key]['nama'] = $val->nama;
            $data[$key]['alamat'] = $val->alamat;
            $data[$key]['deskripsi'] = $val->deskripsi;
             $data[$key]['rating'] = $val->rating;
            $data[$key]['desa'] = $desa->name;
            if ($provinsi) {
                $data[$key]['provinsi'] = $provinsi->name;
                $pulau = \common\models\Island::find()->where(['id' => $provinsi->island_id])->one();
                if ($pulau) {

                    $data[$key]['pulau'] = $pulau->name;
                }
            }

            $data[$key]['harga'] = Yii::$app->formatter->asInteger($val->harga);
//            if ($foto) {
//                $sub = [];
//                foreach ($foto as $ft) {
//                    $sub_name['foto'] = 'http://entra.learnjsforfree.com/uploads/merchant/' . $ft['foto'];
//                    array_push($sub, $sub_name);
//                } $data[$key]['foto'] = $sub;
//            }
           
             $onephoto = \common\models\FotoMerchandise::find()->where(['id_merchandise' => $val->id_merchandise])->one();
         
               if ($onephoto) {

                    $data[$key]['foto'] = 'http://entra.learnjsforfree.com/uploads/merchant/' . $onephoto->foto;
                }
        } $key++;

        return [
            'status' => 'success',
            'data' => $data,
            'lengthData'=>$count,
        ];
        }else{
            return [
            'status' => 'error',
            'message'=>'Data tidak ditemukan'
        ]; 
        }
        } else if ($kecamatan){
            
        $merchant = \common\models\Merchandise::find()->where(['id_kecamatan'=>$kecamatan])->all();

        $data = [];
        if ($merchant){
              $count = \common\models\Merchandise::find()->where(['id_kecamatan'=>$kecamatan])->count();
              foreach ($merchant as $key => $val) {
            $foto = \common\models\FotoMerchandise::find()->where(['id_merchandise' => $val->id_merchandise])->orderBy('id_foto DESC')->one();
            $desa = \common\models\Villages::find()->where(['id' => $val->id_desa])->one();
            $provinsi = \common\models\Provinces::find()->where(['id' => $val->id_provinsi])->one();

            $data[$key]['id'] = $val->id_merchandise;
            $carikategori =\common\models\KategoriMerchandise::find()->where(['id_kategori'=>$val->kategori])->one();
            if ($carikategori){
            $data[$key]['kategori'] = $carikategori->nama;
            }
            $data[$key]['nama'] = $val->nama;
            $data[$key]['alamat'] = $val->alamat;
            $data[$key]['deskripsi'] = $val->deskripsi;
             $data[$key]['rating'] = $val->rating;
            $data[$key]['desa'] = $desa->name;
            if ($provinsi) {
                $data[$key]['provinsi'] = $provinsi->name;
                $pulau = \common\models\Island::find()->where(['id' => $provinsi->island_id])->one();
                if ($pulau) {

                    $data[$key]['pulau'] = $pulau->name;
                }
            }

            $data[$key]['harga'] = Yii::$app->formatter->asInteger($val->harga);
//            if ($foto) {
//                $sub = [];
//                foreach ($foto as $ft) {
//                    $sub_name['foto'] = 'http://entra.learnjsforfree.com/uploads/merchant/' . $ft['foto'];
//                    array_push($sub, $sub_name);
//                } $data[$key]['foto'] = $sub;
//            }
           
             $onephoto = \common\models\FotoMerchandise::find()->where(['id_merchandise' => $val->id_merchandise])->one();
         
               if ($onephoto) {

                    $data[$key]['foto'] = 'http://entra.learnjsforfree.com/uploads/merchant/' . $onephoto->foto;
                }
        } $key++;

        return [
            'status' => 'success',
            'data' => $data,
            'lengthData'=>$count,
        ];
        }else{
            return [
            'status' => 'error',
            'message'=>'Data tidak ditemukan'
        ]; 
        }
        } else if ($desa){
            
        $merchant = \common\models\Merchandise::find()->where(['id_desa'=>$desa])->all();

        $data = [];
        if ($merchant){
              $count = \common\models\Merchandise::find()->where(['id_desa'=>$desa])->count();
              foreach ($merchant as $key => $val) {
            $foto = \common\models\FotoMerchandise::find()->where(['id_merchandise' => $val->id_merchandise])->orderBy('id_foto DESC')->one();
            $desa = \common\models\Villages::find()->where(['id' => $val->id_desa])->one();
            $provinsi = \common\models\Provinces::find()->where(['id' => $val->id_provinsi])->one();

            $data[$key]['id'] = $val->id_merchandise;
            $carikategori =\common\models\KategoriMerchandise::find()->where(['id_kategori'=>$val->kategori])->one();
            if ($carikategori){
            $data[$key]['kategori'] = $carikategori->nama;
            }
            $data[$key]['nama'] = $val->nama;
            $data[$key]['alamat'] = $val->alamat;
            $data[$key]['deskripsi'] = $val->deskripsi;
             $data[$key]['rating'] = $val->rating;
            $data[$key]['desa'] = $desa->name;
            if ($provinsi) {
                $data[$key]['provinsi'] = $provinsi->name;
                $pulau = \common\models\Island::find()->where(['id' => $provinsi->island_id])->one();
                if ($pulau) {

                    $data[$key]['pulau'] = $pulau->name;
                }
            }

            $data[$key]['harga'] = Yii::$app->formatter->asInteger($val->harga);
//            if ($foto) {
//                $sub = [];
//                foreach ($foto as $ft) {
//                    $sub_name['foto'] = 'http://entra.learnjsforfree.com/uploads/merchant/' . $ft['foto'];
//                    array_push($sub, $sub_name);
//                } $data[$key]['foto'] = $sub;
//            }
           
             $onephoto = \common\models\FotoMerchandise::find()->where(['id_merchandise' => $val->id_merchandise])->one();
         
               if ($onephoto) {

                    $data[$key]['foto'] = 'http://entra.learnjsforfree.com/uploads/merchant/' . $onephoto->foto;
                }
        } $key++;

        return [
            'status' => 'success',
            'data' => $data,
            'lengthData'=>$count,
        ];
        }else{
            return [
            'status' => 'error',
            'message'=>'Data tidak ditemukan'
        ]; 
        }
        } else if ($kategori){
            
        $merchant = \common\models\Merchandise::find()->where(['kategori'=>$kategori])->all();

        $data = [];
        if ($merchant){
              $count = \common\models\Merchandise::find()->where(['kategori'=>$kategori])->count();
              foreach ($merchant as $key => $val) {
            $foto = \common\models\FotoMerchandise::find()->where(['id_merchandise' => $val->id_merchandise])->orderBy('id_foto DESC')->one();
            $desa = \common\models\Villages::find()->where(['id' => $val->id_desa])->one();
            $provinsi = \common\models\Provinces::find()->where(['id' => $val->id_provinsi])->one();

            $data[$key]['id'] = $val->id_merchandise;
            $carikategori =\common\models\KategoriMerchandise::find()->where(['id_kategori'=>$val->kategori])->one();
            if ($carikategori){
            $data[$key]['kategori'] = $carikategori->nama;
            }
            $data[$key]['nama'] = $val->nama;
            $data[$key]['alamat'] = $val->alamat;
            $data[$key]['deskripsi'] = $val->deskripsi;
             $data[$key]['rating'] = $val->rating;
            $data[$key]['desa'] = $desa->name;
            if ($provinsi) {
                $data[$key]['provinsi'] = $provinsi->name;
                $pulau = \common\models\Island::find()->where(['id' => $provinsi->island_id])->one();
                if ($pulau) {

                    $data[$key]['pulau'] = $pulau->name;
                }
            }

            $data[$key]['harga'] = Yii::$app->formatter->asInteger($val->harga);
//            if ($foto) {
//                $sub = [];
//                foreach ($foto as $ft) {
//                    $sub_name['foto'] = 'http://entra.learnjsforfree.com/uploads/merchant/' . $ft['foto'];
//                    array_push($sub, $sub_name);
//                } $data[$key]['foto'] = $sub;
//            }
           
             $onephoto = \common\models\FotoMerchandise::find()->where(['id_merchandise' => $val->id_merchandise])->one();
         
               if ($onephoto) {

                    $data[$key]['foto'] = 'http://entra.learnjsforfree.com/uploads/merchant/' . $onephoto->foto;
                }
        } $key++;

        return [
            'status' => 'success',
            'data' => $data,
            'lengthData'=>$count,
        ];
        }else{
            return [
            'status' => 'error',
            'message'=>'Data tidak ditemukan'
        ]; 
        }
        }
        else if ($harga){
        if ($harga=='Terendah'){
        $merchant = \common\models\Merchandise::find()->orderBy('harga ASC')->all();
        } else if ($harga=='Tertinggi'){
        $merchant = \common\models\Merchandise::find()->orderBy('harga DESC')->all();
        }
        $data = [];
        if ($merchant){
            if ($harga =='Terendah'){
                
              $count = \common\models\Merchandise::find()->orderBy('harga ASC')->count();
            } else if ($harga =='Tertinggi'){
                $count = \common\models\Merchandise::find()->orderBy('harga DESC')->count();
            }
              foreach ($merchant as $key => $val) {
            $foto = \common\models\FotoMerchandise::find()->where(['id_merchandise' => $val->id_merchandise])->orderBy('id_foto DESC')->one();
            $desa = \common\models\Villages::find()->where(['id' => $val->id_desa])->one();
            $provinsi = \common\models\Provinces::find()->where(['id' => $val->id_provinsi])->one();

            $data[$key]['id'] = $val->id_merchandise;
            $carikategori =\common\models\KategoriMerchandise::find()->where(['id_kategori'=>$val->kategori])->one();
            if ($carikategori){
            $data[$key]['kategori'] = $carikategori->nama;
            }
            $data[$key]['nama'] = $val->nama;
            $data[$key]['alamat'] = $val->alamat;
            $data[$key]['deskripsi'] = $val->deskripsi;
             $data[$key]['rating'] = $val->rating;
            $data[$key]['desa'] = $desa->name;
            if ($provinsi) {
                $data[$key]['provinsi'] = $provinsi->name;
                $pulau = \common\models\Island::find()->where(['id' => $provinsi->island_id])->one();
                if ($pulau) {

                    $data[$key]['pulau'] = $pulau->name;
                }
            }

            $data[$key]['harga'] = Yii::$app->formatter->asInteger($val->harga);
//            if ($foto) {
//                $sub = [];
//                foreach ($foto as $ft) {
//                    $sub_name['foto'] = 'http://entra.learnjsforfree.com/uploads/merchant/' . $ft['foto'];
//                    array_push($sub, $sub_name);
//                } $data[$key]['foto'] = $sub;
//            }
           
             $onephoto = \common\models\FotoMerchandise::find()->where(['id_merchandise' => $val->id_merchandise])->one();
         
               if ($onephoto) {

                    $data[$key]['foto'] = 'http://entra.learnjsforfree.com/uploads/merchant/' . $onephoto->foto;
                }
        } $key++;

        return [
            'status' => 'success',
            'data' => $data,
            'lengthData'=>$count,
        ];
        }else{
            return [
            'status' => 'error',
            'message'=>'Data tidak ditemukan'
        ]; 
        }
        }else  if ($limit){
            
        $merchant = \common\models\Merchandise::find()->limit($limit)->all();

        $data = [];
        if ($merchant){
              $count = \common\models\Merchandise::find()->count();
                if ($count>$limit){
                $total= $limit;
            }else {
                $total = $count;
            }
              foreach ($merchant as $key => $val) {
            $foto = \common\models\FotoMerchandise::find()->where(['id_merchandise' => $val->id_merchandise])->orderBy('id_foto DESC')->one();
            $desa = \common\models\Villages::find()->where(['id' => $val->id_desa])->one();
            $provinsi = \common\models\Provinces::find()->where(['id' => $val->id_provinsi])->one();

            $data[$key]['id'] = $val->id_merchandise;
            $carikategori =\common\models\KategoriMerchandise::find()->where(['id_kategori'=>$val->kategori])->one();
            if ($carikategori){
            $data[$key]['kategori'] = $carikategori->nama;
            }
            $data[$key]['nama'] = $val->nama;
            $data[$key]['alamat'] = $val->alamat;
            $data[$key]['deskripsi'] = $val->deskripsi;
             $data[$key]['rating'] = $val->rating;
            $data[$key]['desa'] = $desa->name;
            if ($provinsi) {
                $data[$key]['provinsi'] = $provinsi->name;
                $pulau = \common\models\Island::find()->where(['id' => $provinsi->island_id])->one();
                if ($pulau) {

                    $data[$key]['pulau'] = $pulau->name;
                }
            }

            $data[$key]['harga'] = Yii::$app->formatter->asInteger($val->harga);
//            if ($foto) {
//                $sub = [];
//                foreach ($foto as $ft) {
//                    $sub_name['foto'] = 'http://entra.learnjsforfree.com/uploads/merchant/' . $ft['foto'];
//                    array_push($sub, $sub_name);
//                } $data[$key]['foto'] = $sub;
//            }
           
             $onephoto = \common\models\FotoMerchandise::find()->where(['id_merchandise' => $val->id_merchandise])->one();
         
               if ($onephoto) {

                    $data[$key]['foto'] = 'http://entra.learnjsforfree.com/uploads/merchant/' . $onephoto->foto;
                }
        } $key++;

        return [
            'status' => 'success',
            'data' => $data,
            'lengthData'=>$total,
        ];
        }else{
            return [
            'status' => 'error',
            'message'=>'Data tidak ditemukan'
        ]; 
        }
        }
        else {
             $merchant = \common\models\Merchandise::find()->all();

        $data = [];
        if ($merchant){
              $count = \common\models\Merchandise::find()->count();
              foreach ($merchant as $key => $val) {
            $foto = \common\models\FotoMerchandise::find()->where(['id_merchandise' => $val->id_merchandise])->orderBy('id_foto DESC')->one();
            $desa = \common\models\Villages::find()->where(['id' => $val->id_desa])->one();
            $provinsi = \common\models\Provinces::find()->where(['id' => $val->id_provinsi])->one();

            $data[$key]['id'] = $val->id_merchandise;
            $carikategori =\common\models\KategoriMerchandise::find()->where(['id_kategori'=>$val->kategori])->one();
            if ($carikategori){
            $data[$key]['kategori'] = $carikategori->nama;
            }
            $data[$key]['nama'] = $val->nama;
            $data[$key]['alamat'] = $val->alamat;
            $data[$key]['deskripsi'] = $val->deskripsi;
             $data[$key]['rating'] = $val->rating;
            $data[$key]['desa'] = $desa->name;
            if ($provinsi) {
                $data[$key]['provinsi'] = $provinsi->name;
                $pulau = \common\models\Island::find()->where(['id' => $provinsi->island_id])->one();
                if ($pulau) {

                    $data[$key]['pulau'] = $pulau->name;
                }
            }

            $data[$key]['harga'] = Yii::$app->formatter->asInteger($val->harga);
//            if ($foto) {
//                $sub = [];
//                foreach ($foto as $ft) {
//                    $sub_name['foto'] = 'http://entra.learnjsforfree.com/uploads/merchant/' . $ft['foto'];
//                    array_push($sub, $sub_name);
//                } $data[$key]['foto'] = $sub;
//            }
           
             $onephoto = \common\models\FotoMerchandise::find()->where(['id_merchandise' => $val->id_merchandise])->one();
         
               if ($onephoto) {

                    $data[$key]['foto'] = 'http://entra.learnjsforfree.com/uploads/merchant/' . $onephoto->foto;
                }
        } $key++;

        return [
            'status' => 'success',
            'data' => $data,
            'lengthData'=>$count,
        ];
        }else{
            return [
            'status' => 'error',
            'message'=>'Data tidak ditemukan'
        ]; 
        }
        }
    }

        public function actionAllDesatraveling($pulau=NULL,$provinsi=NULL, $kecamatan=NULL,  $rating=NULL, $limit=NULL) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        if ($pulau){
            
            
        $desatraveling = \common\models\DesaTraveling::find()->where(['id_pulau'=>$pulau])->all();

        $data = [];
        if ($desatraveling){
             $count = \common\models\DesaTraveling::find()->where(['id_pulau'=>$pulau])->count();
            foreach ($desatraveling as $key => $val) {
            $foto = \common\models\FotoDesatraveling::find()->where(['id_desa' => $val->id_desa])->orderBy('id_foto DESC')->one();
            $desa = \common\models\Villages::find()->where(['id' => $val->id_desa])->one();
            $provinsi = \common\models\Provinces::find()->where(['id' => $val->id_provinsi])->one();

            $data[$key]['id'] = $val->id;
         
            $data[$key]['nama'] = $val->nama;
            $data[$key]['alamat'] = $val->alamat;
            $data[$key]['deskripsi'] = $val->deskripsi;
             $data[$key]['rating'] = $val->rating;
            $data[$key]['desa'] = $desa->name;
            if ($provinsi) {
                $data[$key]['provinsi'] = $provinsi->name;
                $pulau = \common\models\Island::find()->where(['id' => $provinsi->island_id])->one();
                if ($pulau) {

                    $data[$key]['pulau'] = $pulau->name;
                }
            }

//            if ($foto) {
//                $sub = [];
//                foreach ($foto as $ft) {
//                    $sub_name['foto'] = 'http://entra.learnjsforfree.com/uploads/merchant/' . $ft['foto'];
//                    array_push($sub, $sub_name);
//                } $data[$key]['foto'] = $sub;
//            }
           
             $onephoto = \common\models\FotoDesatraveling::find()->where(['id_desa' => $val->id])->one();
         
               if ($onephoto) {

                    $data[$key]['foto'] = 'http://entra.learnjsforfree.com/uploads/desa/' . $onephoto->foto;
                }
        } $key++;

        return [
            'status' => 'success',
            'data' => $data,
            'lengthData'=>$count,
        ]; 
        }else{
            return[
                'status'=>'error',
                'message'=>'Data tidak ditemukan',
            ];
        }
        }else if ($provinsi){
            
        $desatraveling = \common\models\DesaTraveling::find()->where(['id_provinsi'=>$provinsi])->all();

        $data = [];
        if ($desatraveling){
             $count = \common\models\DesaTraveling::find()->where(['id_provinsi'=>$provinsi])->count();
            foreach ($desatraveling as $key => $val) {
            $foto = \common\models\FotoDesatraveling::find()->where(['id_desa' => $val->id_desa])->orderBy('id_foto DESC')->one();
            $desa = \common\models\Villages::find()->where(['id' => $val->id_desa])->one();
            $provinsi = \common\models\Provinces::find()->where(['id' => $val->id_provinsi])->one();

            $data[$key]['id'] = $val->id;
         
            $data[$key]['nama'] = $val->nama;
            $data[$key]['alamat'] = $val->alamat;
            $data[$key]['deskripsi'] = $val->deskripsi;
             $data[$key]['rating'] = $val->rating;
            $data[$key]['desa'] = $desa->name;
            if ($provinsi) {
                $data[$key]['provinsi'] = $provinsi->name;
                $pulau = \common\models\Island::find()->where(['id' => $provinsi->island_id])->one();
                if ($pulau) {

                    $data[$key]['pulau'] = $pulau->name;
                }
            }


             $onephoto = \common\models\FotoDesatraveling::find()->where(['id_desa' => $val->id])->one();
         
               if ($onephoto) {

                    $data[$key]['foto'] = 'http://entra.learnjsforfree.com/uploads/desa/' . $onephoto->foto;
                }
        } $key++;

        return [
            'status' => 'success',
            'data' => $data,
            'lengthData'=>$count,
        ]; 
        }else{
            return[
                'status'=>'error',
                'message'=>'Data tidak ditemukan',
            ];
        }
        }else if ($kecamatan){
            
        $desatraveling = \common\models\DesaTraveling::find()->where(['id_kecamatan'=>$kecamatan])->all();

        $data = [];
        if ($desatraveling){
             $count = \common\models\DesaTraveling::find()->where(['id_kecamatan'=>$kecamatan])->count();
            foreach ($desatraveling as $key => $val) {
            $foto = \common\models\FotoDesatraveling::find()->where(['id_desa' => $val->id_desa])->orderBy('id_foto DESC')->one();
            $desa = \common\models\Villages::find()->where(['id' => $val->id_desa])->one();
            $provinsi = \common\models\Provinces::find()->where(['id' => $val->id_provinsi])->one();

            $data[$key]['id'] = $val->id;
         
            $data[$key]['nama'] = $val->nama;
            $data[$key]['alamat'] = $val->alamat;
            $data[$key]['deskripsi'] = $val->deskripsi;
             $data[$key]['rating'] = $val->rating;
            $data[$key]['desa'] = $desa->name;
            if ($provinsi) {
                $data[$key]['provinsi'] = $provinsi->name;
                $pulau = \common\models\Island::find()->where(['id' => $provinsi->island_id])->one();
                if ($pulau) {

                    $data[$key]['pulau'] = $pulau->name;
                }
            }

//            if ($foto) {
//                $sub = [];
//                foreach ($foto as $ft) {
//                    $sub_name['foto'] = 'http://entra.learnjsforfree.com/uploads/merchant/' . $ft['foto'];
//                    array_push($sub, $sub_name);
//                } $data[$key]['foto'] = $sub;
//            }
           
             $onephoto = \common\models\FotoDesatraveling::find()->where(['id_desa' => $val->id])->one();
         
               if ($onephoto) {

                    $data[$key]['foto'] = 'http://entra.learnjsforfree.com/uploads/desa/' . $onephoto->foto;
                }
        } $key++;

        return [
            'status' => 'success',
            'data' => $data,
            'lengthData'=>$count,
        ]; 
        }else{
            return[
                'status'=>'error',
                'message'=>'Data tidak ditemukan',
            ];
        }
        }else if ($rating){
              $desatraveling = \common\models\DesaTraveling::find()->where(['rating'=>$rating])->all();

        $data = [];
        if ($desatraveling){
             $count = \common\models\DesaTraveling::find()->where(['rating'=>$rating])->count();
            foreach ($desatraveling as $key => $val) {
            $foto = \common\models\FotoDesatraveling::find()->where(['id_desa' => $val->id_desa])->orderBy('id_foto DESC')->one();
            $desa = \common\models\Villages::find()->where(['id' => $val->id_desa])->one();
            $provinsi = \common\models\Provinces::find()->where(['id' => $val->id_provinsi])->one();

            $data[$key]['id'] = $val->id;
         
            $data[$key]['nama'] = $val->nama;
            $data[$key]['alamat'] = $val->alamat;
            $data[$key]['deskripsi'] = $val->deskripsi;
             $data[$key]['rating'] = $val->rating;
            $data[$key]['desa'] = $desa->name;
            if ($provinsi) {
                $data[$key]['provinsi'] = $provinsi->name;
                $pulau = \common\models\Island::find()->where(['id' => $provinsi->island_id])->one();
                if ($pulau) {

                    $data[$key]['pulau'] = $pulau->name;
                }
            }


           
             $onephoto = \common\models\FotoDesatraveling::find()->where(['id_desa' => $val->id])->one();
         
               if ($onephoto) {

                    $data[$key]['foto'] = 'http://entra.learnjsforfree.com/uploads/desa/' . $onephoto->foto;
                }
        } $key++;

        return [
            'status' => 'success',
            'data' => $data,
            'lengthData'=>$count,
        ]; 
        }else{
            return[
                'status'=>'error',
                'message'=>'Data tidak ditemukan',
            ];
        }
        }else if ($limit){
            
              $desatraveling = \common\models\DesaTraveling::find()->limit($limit)->all();

        $data = [];
        if ($desatraveling){
             $count = \common\models\DesaTraveling::find()->count();
              if ($count>$limit){
                $total= $limit;
            }else {
                $total = $count;
            }
            foreach ($desatraveling as $key => $val) {
            $foto = \common\models\FotoDesatraveling::find()->where(['id_desa' => $val->id_desa])->orderBy('id_foto DESC')->one();
            $desa = \common\models\Villages::find()->where(['id' => $val->id_desa])->one();
            $provinsi = \common\models\Provinces::find()->where(['id' => $val->id_provinsi])->one();

            $data[$key]['id'] = $val->id;
         
            $data[$key]['nama'] = $val->nama;
            $data[$key]['alamat'] = $val->alamat;
            $data[$key]['deskripsi'] = $val->deskripsi;
             $data[$key]['rating'] = $val->rating;
            $data[$key]['desa'] = $desa->name;
            if ($provinsi) {
                $data[$key]['provinsi'] = $provinsi->name;
                $pulau = \common\models\Island::find()->where(['id' => $provinsi->island_id])->one();
                if ($pulau) {

                    $data[$key]['pulau'] = $pulau->name;
                }
            }


           
             $onephoto = \common\models\FotoDesatraveling::find()->where(['id_desa' => $val->id])->one();
         
               if ($onephoto) {

                    $data[$key]['foto'] = 'http://entra.learnjsforfree.com/uploads/desa/' . $onephoto->foto;
                }
        } $key++;

        return [
            'status' => 'success',
            'data' => $data,
            'lengthData'=>$total,
        ]; 
        }else{
            return[
                'status'=>'error',
                'message'=>'Data tidak ditemukan',
            ];
        }
        }else{
              $desatraveling = \common\models\DesaTraveling::find()->all();

        $data = [];
        if ($desatraveling){
             $count = \common\models\DesaTraveling::find()->count();
            foreach ($desatraveling as $key => $val) {
            $foto = \common\models\FotoDesatraveling::find()->where(['id_desa' => $val->id_desa])->orderBy('id_foto DESC')->one();
            $desa = \common\models\Villages::find()->where(['id' => $val->id_desa])->one();
            $provinsi = \common\models\Provinces::find()->where(['id' => $val->id_provinsi])->one();

            $data[$key]['id'] = $val->id;
         
            $data[$key]['nama'] = $val->nama;
            $data[$key]['alamat'] = $val->alamat;
            $data[$key]['deskripsi'] = $val->deskripsi;
             $data[$key]['rating'] = $val->rating;
            $data[$key]['desa'] = $desa->name;
            if ($provinsi) {
                $data[$key]['provinsi'] = $provinsi->name;
                $pulau = \common\models\Island::find()->where(['id' => $provinsi->island_id])->one();
                if ($pulau) {

                    $data[$key]['pulau'] = $pulau->name;
                }
            }


           
             $onephoto = \common\models\FotoDesatraveling::find()->where(['id_desa' => $val->id])->one();
         
               if ($onephoto) {

                    $data[$key]['foto'] = 'http://entra.learnjsforfree.com/uploads/desa/' . $onephoto->foto;
                }
        } $key++;

        return [
            'status' => 'success',
            'data' => $data,
            'lengthData'=>$count,
        ]; 
        }else{
            return[
                'status'=>'error',
                'message'=>'Data tidak ditemukan',
            ];
        }
        }
       
    }
    public function actionListPulau() {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $provinsi = \common\models\Island::find()->all();

        $data = [];

        if ($provinsi) {
            foreach ($provinsi as $key => $val) {
                $data[$key]['id'] = $val->id;
                $data[$key]['name'] = $val->name;
            }
            $key++;
            return[
                'status' => 'success',
                'data' => $data,
            ];
        } else {
            return[
                'status' => 'error',
                'message' => 'Data tidak ditemukan',
            ];
        }
    }

    public function actionListProvinsi() {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $provinsi = \common\models\Provinces::find()->all();

        $data = [];

        if ($provinsi) {
            foreach ($provinsi as $key => $val) {
                $data[$key]['id'] = $val->id;
                $data[$key]['name'] = $val->name;
                $data[$key]['id_island'] = $val->island_id;
            }
            $key++;
            return[
                'status' => 'success',
                'data' => $data,
            ];
        } else {
            return[
                'status' => 'error',
                'message' => 'Data tidak ditemukan',
            ];
        }
    }

    public function actionListKecamatan() {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $kecamatan = \common\models\Districts::find()->all();

        $data = [];

        if ($kecamatan) {
            foreach ($kecamatan as $key => $val) {
                $data[$key]['id'] = $val->id;
                $data[$key]['name'] = $val->name;
                $data[$key]['regency_id'] = $val->regency_id;
            }
            $key++;
            return[
                'status' => 'success',
                'data' => $data,
            ];
        } else {
            return[
                'status' => 'error',
                'message' => 'Data tidak ditemukan',
            ];
        }
    }

    public function actionListNamaDesa() {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $desa = \common\models\Villages::find()->all();

        $data = [];

        if ($desa) {
            foreach ($desa as $key => $val) {

                $provinsi = \common\models\Provinces::find()->where(['id' => $val->provinsi])->one();


                $data[$key]['id'] = $val->id;
                $data[$key]['name'] = $val->name;
                if ($provinsi) {
                    $pulau = \common\models\Island::find()->where(['id' => $provinsi->island_id])->one();
                    $data[$key]['pulau'] = $pulau->name;
                }
                $data[$key]['provinsi'] = $val->provinsi;
                $data[$key]['district_id'] = $val->district_id;
            }
            $key++;
            return[
                'status' => 'success',
                'data' => $data,
            ];
        } else {
            return[
                'status' => 'error',
                'message' => 'Data tidak ditemukan',
            ];
        }
    }
       public function actionListNamaDesaTraveling() {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $desa = \common\models\DesaTraveling::find()->all();

        $data = [];

        if ($desa) {
             $count = \common\models\DesaTraveling::find()->count();

            foreach ($desa as $key => $val) {

                $provinsi = \common\models\Provinces::find()->where(['id' => $val->id_provinsi])->one();
                $kecamatan = \common\models\Districts::find()->where(['id'=>$val->id_kecamatan])->one();

                $data[$key]['id'] = $val->id;
                $data[$key]['nama'] = $val->nama;
                if ($provinsi) {
                    $pulau = \common\models\Island::find()->where(['id' => $provinsi->island_id])->one();
                    $data[$key]['idPulau'] = $pulau->name;
                      $data[$key]['idProvinsi'] = $provinsi->name;
                }
                if($kecamatan){
              
                $data[$key]['idKecamatan'] = $kecamatan->name;}
            }
            $key++;
            return[
                'status' => 'success',
                'data' => $data,
                'lengthData'=>$count,
            ];
        } else {
            return[
                'status' => 'error',
                'message' => 'Data tidak ditemukan',
            ];
        }
    }

    public function actionDetailDesa($id) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $desa = \common\models\DesaTraveling::find()->where(['id' => $id])->one();
    
        if ($desa) {
            $provinsi = \common\models\Provinces::find()->where(['id' => $desa->id_provinsi])->one();
            $kecamatan = \common\models\Districts::find()->where(['id' => $desa->id_kecamatan])->one();
            //$desa = \common\models\Villages::find()->where(['id'=>$desa->id_desa])->one();
            $pulau = \common\models\Island::find()->where(['id' => $desa->id_pulau])->one();
            $desa->id_provinsi= $provinsi->name;
            $desa->id_kecamatan= $kecamatan->name;
            //$desa->id_desa = $desa->name;
            $desa->id_pulau= $pulau->name;
            $onefoto =\common\models\FotoDesatraveling::find()->where(['id_desa'=>$id])->orderBy('id_foto ASC')->one();
            $hitung =\common\models\FotoDesatraveling::find()->where(['id_desa'=>$id])->count();
          //  $kurang = $hitung-1;
            $data_foto = \common\models\FotoDesatraveling::find()->where(['id_desa' => $id])->orderBy('id_foto DESC')->all();
            
            $foto = [];
            if ($data_foto) {
                foreach ($data_foto as  $value) {
                   
                     $foto_name['foto'] = 'http://entra.learnjsforfree.com/uploads/desa/' .$value['foto'];
                    array_push($foto, $foto_name);
                }

               
            } 
            $coordinate = \common\models\Coordinat::find()->where(['id_data'=>$id])->andWhere(['kategori'=>'Desa'])->one();
            $sub2=[];
                //foreach ($coordinate as $val1){
                    
                      
                //      $sub_name2['latitude'] = $val1['latitdude'];
                 //         $sub_name2['longitude'] = $val1['longitude'];
                         
               
                //   array_push($sub2, $sub_name2);
                //}
            $desa->foto = 'http://entra.learnjsforfree.com/uploads/desa/'. $onefoto->foto;
            $desa->fotolist = $foto;
            $desa->coordinat= $coordinate;
             return[
                'status' => 'success',
                'data' => $desa,
            ];
        } else {
            return[
                'status' => 'error',
                'message' => 'Data tidak ditemukan',
            ];
        }
    }

    // public function actionDetailExperience($id) {
    //     Yii::$app->response->format = Response::FORMAT_JSON;
    //     $cari = \common\models\Experience::find()->where(['id_experience' => $id])->one();
    //        if ($cari) {
    //         $pulau = \common\models\Island::find()->where(['id'=>$cari->id_pulau])->one();
    //         $provinsi = \common\models\Provinces::find()->where(['id'=>$cari->id_provinsi])->one();
    //         $kecamatan = \common\models\Districts::find()->where(['id'=>$cari->id_kecamatan])->one();
    //         $desa = \common\models\Villages::find()->where(['id'=>$cari->id_desa])->one();
    //         if ($pulau){
    //             $cari->id_pulau= $pulau->name;
    //         }
    //         if ($provinsi){
    //             $cari->id_provinsi = $provinsi->name;
    //         }
    //         if ($kecamatan){
    //              $cari->id_kecamatan= $kecamatan->name;
    //         }
    //         if ($desa){
    //             $cari->id_desa= $desa->name;
    //         }
    //        $foto = \common\models\FotoExperience::find()->where(['id_experience' => $cari->id_experience])->all();
    //                 if ($foto) {
    //                     $sub = [];
    //                     foreach ($foto as $ft) {
    //                         $sub_name['foto'] = 'http://entra.learnjsforfree.com/uploads/experience/'.$ft['foto'];
    //                         array_push($sub, $sub_name);
    //                     }
    //                      $cari['gallery']= $sub;
    //                 }
    //         return[
    //             'status' => 'success',
    //             'cari' => $cari,
    //         ];
    //     } else {
    //         return[
    //             'status' => 'error',
    //             'message' => 'Data tidak ditemukan'
    //         ];
    //     }
    // }


    public function actionDetailExperience($id) {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $list = \common\models\Experience::find()->where(['id_experience' => $id])->all();
        if ($list) {

            $data = [];
            foreach ($list as $key => $val) {
                $pulau = \common\models\Island::find()->where(['id' => $val->id_pulau])->one();
                $provinsi = \common\models\Provinces::find()->where(['id' => $val->id_provinsi])->one();
                $kecamatan = \common\models\Districts::find()->where(['id' => $val->id_kecamatan])->one();
                $desa = \common\models\Villages::find()->where(['id' => $val->id_desa])->one();
                $foto = \common\models\FotoExperience::find()->where(['id_experience' => $val->id_experience])->all();
                $onephoto = \common\models\FotoExperience::find()->where(['id_experience' => $val->id_experience])->one();
                if ($onephoto) {

                    $data[$key]['fotoUtama'] = 'http://entra.learnjsforfree.com/uploads/experience/' . $onephoto->foto;
                }
                if ($foto) {
                    $sub = [];
                    foreach ($foto as $ft) {
                        $sub_name['foto'] = 'http://entra.learnjsforfree.com/uploads/experience/' . $ft['foto'];
                        array_push($sub, $sub_name);
                    }
                    $data[$key]['fotoList'] = $sub;
                }

                $sub1 = [];
                $coordinate = \common\models\Coordinat::find()->where(['id_data'=>$val->id_experience])
                ->andWhere(['kategori'=>'Experience'])->one();
              //  foreach ($coordinate as $ls) {

                //    $sub_name1['latitude'] = $ls['latitdude'];
                //    $sub_name1['longitude'] = $ls['longitude'];
                //    array_push($sub1, $sub_name1);
              //  }
                $data[$key]['coordinate'] = $coordinate;


                $data[$key]['nama'] = $val->nama;
                $data[$key]['contact'] = $val->contact;
                $data[$key]['alamat'] = $val->alamat;
                $data[$key]['deskripsi'] = $val->deskripsi;

                $data[$key]['rating'] = $val->rating;
                 $carikategori=\common\models\KategoriExperience::find()->where(['id_kategori'=>$val->id_kategori])->one();
                 if ($carikategori){
                $data[$key]['kategori'] = $carikategori->nama;
                 }
                if ($desa) {
                    $data[$key]['desa'] = $desa->name;
                }
                if ($provinsi) {
                    $data[$key]['provinsi'] = $provinsi->name;
                }

                if ($kecamatan) {
                    $data[$key]['kecamatan'] = $kecamatan->name;
                }



                $operational = \common\models\OperationalExperience::find()->where(['id_experience'=>$val->id_experience])->one();

                $sub2 = [];
                foreach ($list as $ls2) {
                    $sub_name2['hariBuka'] = $ls2['hari'];
                    $sub_name2['jamBuka'] = $ls2['jam_mulai'];
                    $sub_name2['jamTutup'] = $ls2['jam_berakhir'];
                  
                  //  $sub_name2['hargaTiket'] = Yii::$app->formatter->asInteger($ls2['price']);
                     $sub_name2['hargaTiket'] = $ls2['price'];
                    $sub_name2['statusTersedia'] = $ls2['status'];
                    array_push($sub2, $sub_name2);
                }
                if ($operational){
                    //$operational->harga = Yii::$app->formatter->asInteger($operational->harga);
                    $operational->harga = $operational->harga;
                $data[$key]['operational'] = $operational;
                }
                $sub3 = [];
                $listaktifitas = \common\models\Aktivitas::find()->where(['id_experience' => $val->id_experience])->all();
                if ($listaktifitas) {
                    foreach ($listaktifitas as $ls3) {
                        $sub_name3['id'] = $ls3['id_aktivitas'];
                        $sub_name3['description'] = $ls3['deskripsi'];

                        array_push($sub3, $sub_name3);
                    }
                    $data[$key]['activityList'] = $sub3;
                }
            }
            $key++;
            return[
                'status' => 'success',
                'data' => $data,
            ];
        } else {

            return[
                'status' => 'error',
                'message' => 'Data tidak ditemukan'
            ];
        }
    }

    public function actionDetailAkomodasi($id) {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $list = \common\models\Accommodation::find()->where(['id_homestay' => $id])->all();
        if ($list) {

            $data = [];
            foreach ($list as $key => $val) {
                $pulau = \common\models\Island::find()->where(['id' => $val->id_pulau])->one();
                $provinsi = \common\models\Provinces::find()->where(['id' => $val->id_provinsi])->one();
                $kecamatan = \common\models\Districts::find()->where(['id' => $val->id_kecamatan])->one();
                $desa = \common\models\Villages::find()->where(['id' => $val->id_desa])->one();
                $countfoto = \common\models\FotoAccommodation::find()->where(['id_homestay' => $val->id_homestay])->count();
                $kurang = $countfoto-1;
                $foto = \common\models\FotoAccommodation::find()->where(['id_homestay' => $val->id_homestay])->orderBy('id_foto DESC')->limit($kurang)->all();
                $onephoto = \common\models\FotoAccommodation::find()->where(['id_homestay' => $val->id_homestay])->one();
                if ($onephoto) {

                    $data[$key]['fotoUtama'] = 'http://entra.learnjsforfree.com/uploads/akomodasi/' . $onephoto->foto;
                }
                if ($foto) {
                    $sub = [];
                    foreach ($foto as $ft) {
                        $sub_name['foto'] = 'http://entra.learnjsforfree.com/uploads/akomodasi/' . $ft['foto'];
                        array_push($sub, $sub_name);
                    }
                    $data[$key]['fotoList'] = $sub;
                }

                $sub1 = [];
                  $coordinate = \common\models\Coordinat::find()->where(['id_data'=>$val->id_homestay])
                ->andWhere(['kategori'=>'Accommodation'])->one();
              //  foreach ($coordinate as $ls) {
                //    $sub_name1['latitude'] = $ls['latitdude'];
                  //  $sub_name1['longitude'] = $ls['longitude'];
                    //array_push($sub1, $sub_name1);
                //}
                $data[$key]['coordinate'] = $coordinate;


                $data[$key]['nama'] = $val->nama_homestay;

                $data[$key]['alamat'] = $val->alamat;
                 $data[$key]['contact'] = $val->contact;
                $data[$key]['deskripsi'] = $val->deskripsi;

                $data[$key]['rating'] = $val->rating;
                $carikategori = \common\models\KategoriAccommodation::find()->where(['id_kategori'=>$val->id_kategori])->one();
            if ($carikategori){
            $data[$key]['kategori'] = $carikategori->nama_kategori; }
                if ($desa) {
                    $data[$key]['desa'] = $desa->name;
                }
                if ($provinsi) {
                    $data[$key]['provinsi'] = $provinsi->name;
                }

                if ($kecamatan) {
                    $data[$key]['kecamatan'] = $kecamatan->name;
                }

                $operational = \common\models\Operational::find()->where(['id_akomodasi'=>$val->id_homestay])->one();

               // $sub2 = [];
              //  foreach ($list as $ls2) {
                //    $sub_name2['total_room'] = $ls2['total_room'];
                //    $sub_name2['checkin'] = $ls2['checkin'];
                //    $sub_name2['checkout'] = $ls2['checkout'];
                //    $sub_name2['statusTersedia'] = $ls2['status'];
                //    $sub_name2['hargaTiket'] = Yii::$app->formatter->asInteger($ls2['harga']);
            
                 //   array_push($sub2, $sub_name2);
              //  }
                $data[$key]['operational'] = $operational;
                $sub3 = [];
                $listaktifitas = \common\models\Fasilitas::find()->where(['id_akomodasi' => $val->id_homestay])->one();
                
                if ($listaktifitas) {
                   // foreach ($listaktifitas as $ls3) {
                       // $sub_name3['id'] = $ls3['id_fasilitas'];
                      // $sub_name3['wifi'] = $ls3['wifi'];
                      //    $sub_name3['restauran'] = $ls3['restauran'];
                       //     $sub_name3['hot_water'] = $ls3['hot_water'];
                      //        $sub_name3['free_park'] = $ls3['free_park'];
                     //   array_push($sub3, $sub_name3);
                   // }
                   if ($listaktifitas->wifi==1){
                    $listaktifitas->wifi=true;
                }else{
                    $listaktifitas->wifi=false;
                }
                if ($listaktifitas->restauran==1){
                    $listaktifitas->restauran=true;
                }else{
                    $listaktifitas->restauran=false;
                }
                if ($listaktifitas->hot_water==1){
                    $listaktifitas->hot_water=true;
                }else{
                    $listaktifitas->hot_water=false;
                }
                if ($listaktifitas->free_park==1){
                    $listaktifitas->free_park=true;
                }else{
                    $listaktifitas->free_park=false;
                }
                    $data[$key]['facility'] = $listaktifitas;
                }
            }
            $key++;
            return[
                'status' => 'success',
                'data' => $data,
            ];
        } else {

            return[
                'status' => 'error',
                'message' => 'Data tidak ditemukan'
            ];
        }
    }

     public function actionDetailTransport($id) {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $list = \common\models\Transportation::find()->where(['id_transportation' => $id])->all();
        if ($list) {

            $data = [];
            foreach ($list as $key => $val) {
                $pulau = \common\models\Island::find()->where(['id' => $val->id_pulau])->one();
                $provinsi = \common\models\Provinces::find()->where(['id' => $val->id_provinsi])->one();
                $kecamatan = \common\models\Districts::find()->where(['id' => $val->id_kecamatan])->one();
                $desa = \common\models\Villages::find()->where(['id' => $val->id_desa])->one();
                $foto = \common\models\FotoTransportasi::find()->where(['id_transportasi' => $val->id_transportation])->all();
                $onephoto = \common\models\FotoTransportasi::find()->where(['id_transportasi' => $val->id_transportation])->one();
                if ($onephoto) {

                    $data[$key]['fotoUtama'] = 'http://entra.learnjsforfree.com/uploads/transport/' . $onephoto->foto;
                }
                if ($foto) {
                    $sub = [];
                    foreach ($foto as $ft) {
                        $sub_name['foto'] = 'http://entra.learnjsforfree.com/uploads/transport/' . $ft['foto'];
                        array_push($sub, $sub_name);
                    }
                    $data[$key]['fotoList'] = $sub;
                }

              
                

                $data[$key]['nama'] = $val->nama;
                 $data[$key]['contact'] = $val->contact;
                 $data[$key]['hari'] = $val->hari;
                $data[$key]['alamatMerchant'] = $val->alamat;
                $data[$key]['deskripsi'] = $val->deskripsi;

                $data[$key]['rating'] = $val->rating;
                $carikategori = \common\models\KategoriTransportasi::find()->where(['id_kategori'=>$val->jenis_transportasi])->one();
                if ($carikategori){
                $data[$key]['kategoriKendaraan'] = $carikategori->nama;
                }
                  $data[$key]['tipeKendaraan'] = $val->tipe_kendaraan;
                if ($desa) {
                    $data[$key]['desa'] = $desa->name;
                }
                if ($provinsi) {
                    $data[$key]['provinsi'] = $provinsi->name;
                }

                if ($kecamatan) {
                    $data[$key]['kecamatan'] = $kecamatan->name;
                }
                 $coordinate = \common\models\Coordinat::find()->where(['id_data'=>$val->id_transportation])
                ->where(['kategori'=>'Transportation'])->one();
                //  $sub1 = [];
                //foreach ($coordinate as $ls) {
                  //  $sub_name1['latitude'] = $ls['latitdude'];
                    //$sub_name1['longitude'] = $ls['longitude'];
                    //array_push($sub1, $sub_name1);
                //}
                $data[$key]['coordinate'] = $coordinate;
                $rute = \common\models\Rute::find()->where(['id_transport'=>$val->id_transportation])->all();
              
                  if ($rute) {
                    $sub3 = [];
                    foreach ($rute as $rt) {
                        $sub_name3['id'] = $rt['id_rute'];
                        $sub_name3['daerah_mulai'] = $rt['daerah_mulai'];
                         $sub_name3['daerah_akhir'] = $rt['daerah_akhir'];
                          $sub_name3['harga'] = $rt['harga'];
                          
                        array_push($sub3, $sub_name3);
                    }
                    $data[$key]['rute'] = $sub3;
                }
                $fasilitas = \common\models\FasilitasTransport::find()->where(['id_transportasi'=>$val->id_transportation])->one();
                  if ($fasilitas) {
                 //   $sub2 = [];
                  //  foreach ($fasilitas as $fs) {
                      
                    //   $sub_name2['wifi'] = $fs['wifi'];
                     //     $sub_name2['restauran'] = $fs['restauran'];
                     //       $sub_name2['hot_water'] = $fs['hot_water'];
                      //        $sub_name2['free_park'] = $fs['free_park'];
                            
                      //  array_push($sub2, $sub_name2);
                 //   }
                  if ($fasilitas->wifi==1){
                    $fasilitas->wifi=true;
                }else{
                    $fasilitas->wifi=false;
                }
                if ($fasilitas->restauran==1){
                    $fasilitas->restauran=true;
                }else{
                    $fasilitas->restauran=false;
                }
                if ($fasilitas->hot_water==1){
                    $fasilitas->hot_water=true;
                }else{
                    $fasilitas->hot_water=false;
                }
                if ($fasilitas->free_park==1){
                    $fasilitas->free_park=true;
                }else{
                    $fasilitas->free_park=false;
                }
                    $data[$key]['fasilitas'] = $fasilitas;
                }


                
            }
            $key++;
            return[
                'status' => 'success',
                'data' => $data,
            ];
        } else {

            return[
                'status' => 'error',
                'message' => 'Data tidak ditemukan'
            ];
        }
    }


     public function actionDetailMerchant($id) {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $list = \common\models\Merchandise::find()->where(['id_merchandise' => $id])->all();
        if ($list) {

            $data = [];
            foreach ($list as $key => $val) {
                $pulau = \common\models\Island::find()->where(['id' => $val->id_pulau])->one();
                $provinsi = \common\models\Provinces::find()->where(['id' => $val->id_provinsi])->one();
                $kecamatan = \common\models\Districts::find()->where(['id' => $val->id_kecamatan])->one();
                $desa = \common\models\Villages::find()->where(['id' => $val->id_desa])->one();
                $foto = \common\models\FotoMerchandise::find()->where(['id_merchandise' => $val->id_merchandise])->all();
                $onephoto = \common\models\FotoMerchandise::find()->where(['id_merchandise' => $val->id_merchandise])->one();
                if ($onephoto) {

                    $data[$key]['fotoUtama'] = 'http://entra.learnjsforfree.com/uploads/merchant/' . $onephoto->foto;
                }
             

                $sub1 = [];
                 $coordinate = \common\models\Coordinat::find()->where(['id_data'=>$val->id_merchandise])
                ->where(['kategori'=>'Merchandise'])->one();
              
                $data[$key]['coordinate'] = $coordinate;


                $data[$key]['nama'] = $val->nama;
                $data[$key]['harga'] = $val->harga;
                $data[$key]['alamatMerchant'] = $val->alamat;
                $data[$key]['deskripsi'] = $val->deskripsi;

           //     $data[$key]['rating'] = $val->rating;
                    $carikategori = \common\models\KategoriMerchandise::find()->where(['id_kategori'=>$val->kategori])->one();
                    if ($carikategori){
                        
                $data[$key]['kategoriMerchant'] = $carikategori->nama;
                    }
                if ($desa) {
                    $data[$key]['desa'] = $desa->name;
                }
                if ($provinsi) {
                    $data[$key]['provinsi'] = $provinsi->name;
                }

                if ($kecamatan) {
                    $data[$key]['kecamatan'] = $kecamatan->name;
                }


            }
            $key++;
            return[
                'status' => 'success',
                'data' => $data,
            ];
        } else {

            return[
                'status' => 'error',
                'message' => 'Data tidak ditemukan'
            ];
        }
    }


    public function actionKategoriAkomodasi() {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $kategori = \common\models\KategoriAccommodation::find()->all();

        $data = [];
        if ($kategori) {
            foreach ($kategori as $key => $val) {
                $data[$key]['id'] = $val->id_kategori;
                $data[$key]['nama'] = $val->nama_kategori;
            }
            $key++;
            return[
                'status' => 'success',
                'data' => $data,
            ];
        } else {
            return[
                'status' => 'error',
                'message' => 'Data tidak ditemukan'
            ];
        }
    }

    public function actionKategoriTransportasi() {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $kategori = \common\models\KategoriTransportasi::find()->all();

        $data = [];
        if ($kategori) {
            foreach ($kategori as $key => $val) {
                $data[$key]['id'] = $val->id_kategori;
                $data[$key]['nama'] = $val->nama;
            }
            $key++;
            return[
                'status' => 'success',
                'data' => $data,
            ];
        } else {
            return[
                'status' => 'error',
                'message' => 'Data tidak ditemukan'
            ];
        }
    }

    public function actionKategoriMerchant() {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $kategori = \common\models\KategoriMerchandise::find()->all();

        $data = [];
        if ($kategori) {
            foreach ($kategori as $key => $val) {
                $data[$key]['id'] = $val->id_kategori;
                $data[$key]['nama'] = $val->nama;
            }
            $key++;
            return[
                'status' => 'success',
                'data' => $data,
            ];
        } else {
            return[
                'status' => 'error',
                'message' => 'Data tidak ditemukan'
            ];
        }
    }

    public function actionLogin() {
        Yii::$app->response->format = Response::FORMAT_JSON;

        if (!Yii::$app->user->isGuest) {
            return [
                'status' => 'error',
                'message' => 'Sudah login',
            ];
        }

        $model = new \common\models\LoginApi();
        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                $data = $model->login();
                if ($data) {

                    return $data;
                }
            } else {
                return [
                    'status' => 'error-validasi',
                    'data' => $model->errors,
                ];
            }
        }
        return [
            'status' => 'error',
            'message' => 'Gagal login',
        ];
    }

    public function actionLogout() {
        Yii::$app->user->logout();

        return [
            'status' => 'success',
            'message' => 'sukses logout',
        ];
    }

    public function actionAddPlan() {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $user = Yii::$app->user->identity;
        if ($user) {
            $new = new \common\models\Plan();
            
            $new->created_at = date('Y-m-d');
            $new->id_user = $user->id;
            if ($new->load(Yii::$app->request->post())) {
                $new->save(false);
                return[
                    'status' => 'success',
                    'data' => $new
                ];
            } else {
                return[
                    'status' => 'error',
                    'message' => 'Data tidak bisa disimpan'
                ];
            }
        } else {
            return[
                'status' => 'error',
                'message' => 'Silahkan login terlebih dahulu'
            ];
        }
    }
    
    
    
    public function actionCreatePlan($id,$kategori){
        Yii::$app->response->format = Response::FORMAT_JSON;
        $user = Yii::$app->user->identity;
        if ($user){
            if ($kategori=='Accommodation'){
                $caridata = \common\models\Accommodation::find()->where(['id_homestay'=>$id])->one();
                if ($caridata){
                $model = new \common\models\DetailPlan();
                $model->id_user = $user->id;
                $model->id_data = $caridata->id_homestay;
                $model->kategori= $kategori;
                $model->harga =$caridata->harga;
                   if ($model->load(Yii::$app->request->post())) {
                       $model->biaya = $caridata->harga*$model->jumlah_orang;
                       $model->save(false);
                       return[
                       'status'=>'success',
                       'data'=>$model,
                       ];
                   }else {
                    return[
                    'status'=>'error',
                    'message'=>'Gagal menyimpan data',
                    ];
                }
                }else {
                    return[
                    'status'=>'error',
                    'message'=>'Data tidak ditemukan',
                    ];
                }
            }else if ($kategori=='Merchandise'){
                $caridata = \common\models\Merchandise::find()->where(['id_merchandise'=>$id])->one();
                if ($caridata){
                $model = new \common\models\DetailPlan();
                $model->id_user = $user->id;
                $model->id_data = $caridata->id_merchandise;
                $model->kategori= $kategori;
                $model->harga  =$caridata->harga;
                   if ($model->load(Yii::$app->request->post())) {
                       $model->biaya = $caridata->harga*$model->jumlah_orang;
                       $model->save(false);
                       return[
                       'status'=>'success',
                       'data'=>$model,
                       ];
                   }else {
                    return[
                    'status'=>'error',
                    'message'=>'Gagal menyimpan data',
                    ];
                }
                }else {
                    return[
                    'status'=>'error',
                    'message'=>'Data tidak ditemukan',
                    ];
                }
            }else if ($kategori=='Transportation'){
                $caridata = \common\models\Transportation::find()->where(['id_transportation'=>$id])->one();
                if ($caridata){
                $model = new \common\models\DetailPlan();
                $model->id_user = $user->id;
                $model->id_data = $caridata->id_transportation;
                $model->kategori= $kategori;
                $model->harga  =$caridata->price;
                   if ($model->load(Yii::$app->request->post())) {
                       $model->biaya = $caridata->price*$model->jumlah_orang;
                       $model->save(false);
                       return[
                       'status'=>'success',
                       'data'=>$model,
                       ];
                   }else {
                    return[
                    'status'=>'error',
                    'message'=>'Gagal menyimpan data',
                    ];
                }
                }else {
                    return[
                    'status'=>'error',
                    'message'=>'Data tidak ditemukan',
                    ];
                }
            }else if ($kategori=='Experience'){
                $caridata = \common\models\Experience::find()->where(['id_experience'=>$id])->one();
                if ($caridata){
                $model = new \common\models\DetailPlan();
                $model->id_user = $user->id;
                $model->id_data = $caridata->id_experience;
                $model->kategori= $kategori;
                $model->harga  =$caridata->price;
                   if ($model->load(Yii::$app->request->post())) {
                       $model->biaya = $caridata->price*$model->jumlah_orang;
                       $model->save(false);
                       return[
                       'status'=>'success',
                       'data'=>$model,
                       ];
                   }else {
                    return[
                    'status'=>'error',
                    'message'=>'Gagal menyimpan data',
                    ];
                }
                }else {
                    return[
                    'status'=>'error',
                    'message'=>'Data tidak ditemukan',
                    ];
                }
            }else{
            return[
            'status'=>'error',
            'message'=>'Kategori yang dipilih tidak ditemukan',
            ];
        }
        }else{
            return[
            'status'=>'error',
            'message'=>'Silahkan login terlebih dahulu',
            ];
        }
    }
    
        public function actionOrder($id, $kategori){
            Yii::$app->response->format = Response::FORMAT_JSON;
            $user = Yii::$app->user->identity;
            if ($user){
                if ($kategori=='Accommodation'){
                    $caridata = \common\models\Accommodation::find()->where(['id_homestay'=>$id])->one();
                    if($caridata){
                        $model= new \common\models\DetailOrder();
                            $model->id_user = $user->id;
                $model->id_data = $caridata->id_homestay;
                $model->kategori= $kategori;
                $model->harga =$caridata->harga;
                $model->nama = $caridata->nama_homestay;
                $model->alamat =$caridata->alamat;
                              
                                
                $model->status=0;
                           if ($model->load(Yii::$app->request->post())) {
                                 $model->total_bayar = $caridata->harga*$model->jumlah_orang;
                                 $model->save(false);
                                    $cariorder = \common\models\Order::find()->where(['id_desa'=>$caridata->id_desa])
                                ->andWhere(['id_user'=>$user->id])->andWhere(['status'=>0])->one();
                                if (!$cariorder){
                                    $new = new \common\models\Order;
                                    $new->id_user = $user->id;
                                    $new->id_desa = $caridata->id_desa;
                                    $new->total_pembayaran = $model->total_bayar;
                                    $new->id_plan = $model->id_plan;
                                    $new->catatan = $model->catatan;
                                    $new->status = $model->status;
                                    $new->created_at = date('d-M-Y');
                                    $new->save(false);
                                    $model->id_order = $new->id_order;
                                    $model->save(false);
                                    }else{
                                    $model->id_order = $cariorder->id_order;
                                    $model->save(false);
                                    }
                                     return[
                                   'status'=>'success',
                                   'data'=>$model,
                               ];
                           }else {
                               return[
                                   'status'=>'error',
                                   'message'=>'Data gagal disimpan'
                               ];
                           }
                     
                    }else{
                        return[
                            'status'=>'status',
                            'message'=>'Data akomodasi tidak ditemukan'
                        ];
                    }
                }else if ($kategori=='Experience'){
                  $caridata = \common\models\Experience::find()->where(['id_experience'=>$id])->one();
                    if($caridata){
                             $model = new \common\models\DetailOrder();
                                $model->id_user = $user->id;
                $model->id_data = $caridata->id_experience;
                $model->kategori= $kategori;
                $model->harga =$caridata->price;
                $model->nama = $caridata->nama;
                $model->alamat =$caridata->alamat;
                                $model->status=0;
                           if ($model->load(Yii::$app->request->post())) {
                                 $model->total_bayar = $caridata->price*$model->jumlah_orang;
                                 $model->save(false);
                                  $cariorder = \common\models\Order::find()->where(['id_desa'=>$caridata->id_desa])
                                ->andWhere(['id_user'=>$user->id])->andWhere(['status'=>0])->one();
                        if (!$cariorder){
                            $new = new \common\models\Order;
                            $new->id_user = $user->id;
                            $new->id_desa = $caridata->id_desa;
                            $new->total_pembayaran = $model->total_bayar;
                            $new->id_plan = $model->id_plan;
                            $new->catatan = $model->catatan;
                            $new->status = $model->status;
                            $new->created_at = date('d-M-Y');
                            $new->save(false);
                            $model->id_order = $new->id_order;
                            $model->save(false);
                        }else{
                                    $model->id_order = $cariorder->id_order;
                                    $model->save(false);
                                    }
                        
                         return[
                                   'status'=>'success',
                                   'data'=>$model,
                               ];
                           }else {
                               return[
                                   'status'=>'error',
                                   'message'=>'Data gagal disimpan'
                               ];
                           }
                       
                    }else{
                        return[
                            'status'=>'error',
                            'message'=>'Data experience tidak ditemukan',
                        ];
                    }
             
                    
                    
                }else if ($kategori=='Merchandise'){
                    
                    
                 $caridata = \common\models\Merchandise::find()->where(['id_merchandise'=>$id])->one();
                    if($caridata){
                             $model = new \common\models\DetailOrder();
                             $model->id_user = $user->id;
                             $model->id_data = $caridata->id_merchandise;
                             $model->kategori= $kategori;
                 $model->harga =$caridata->harga;
                 $model->nama = $caridata->nama;
                 $model->alamat =$caridata->alamat;
                             $model->status=0;
                           if ($model->load(Yii::$app->request->post())) {
                                 $model->total_bayar = $caridata->harga*$model->jumlah_orang;
                                 $model->save(false);
                                 $cariorder = \common\models\Order::find()->where(['id_desa'=>$caridata->id_desa])
                                ->andWhere(['id_user'=>$user->id])->andWhere(['status'=>0])->one();
                                if (!$cariorder){
                            $new = new \common\models\Order;
                            $new->id_user = $user->id;
                            $new->id_desa = $caridata->id_desa;
                            $new->total_pembayaran = $model->total_bayar;
                            $new->id_plan = $model->id_plan;
                            $new->catatan = $model->catatan;
                            $new->status = $model->status;
                            $new->created_at = date('d-M-Y');
                            $new->save(false);
                                    $model->id_order = $new->id_order;
                                    $model->save(false);
                                }else{
                                    $model->id_order = $cariorder->id_order;
                                    $model->save(false);
                                    }
                                 return[
                                   'status'=>'success',
                                   'data'=>$model,
                               ];
                           }else{
                               return[
                                   'status'=>'error',
                                   'message'=>'data gagal disimpan'
                               ];
                           }
                        
                    }else{
                        return[
                            'status'=>'error',
                            'message'=>'Data merchandise tidak ditemukan',
                        ];
                    }
                }else if ($kategori=='Transportation'){
                         $caridata = \common\models\Transportation::find()->where(['id_transportation'=>$id])->one();
                    if($caridata){
                             $model = new \common\models\DetailOrder();
                  $model->id_user = $user->id;
                $model->id_data = $caridata->id_transportation;
                $model->kategori= $kategori;
                $model->harga =$caridata->price;
                $model->nama = $caridata->nama;
                $model->alamat =$caridata->alamat;
                  $model->status=0;
                           if ($model->load(Yii::$app->request->post())) {
                                 $model->total_bayar = $caridata->price*$model->jumlah_orang;
                                 $model->save(false);
                                   $cariorder = \common\models\Order::find()->where(['id_desa'=>$caridata->id_desa])
                                ->andWhere(['id_user'=>$user->id])->andWhere(['status'=>0])->one();
                                     if (!$cariorder){
                                    $new = new \common\models\Order;
                                    $new->id_user = $user->id;
                                    $new->id_desa = $caridata->id_desa;
                                    $new->total_pembayaran = $model->total_bayar;
                                    $new->id_plan = $model->id_plan;
                                    $new->catatan = $model->catatan;
                                    $new->status = $model->status;
                                    $new->created_at = date('d-M-Y');
                                    $new->save(false);
                                    $model->id_order = $new->id_order;
                                    $model->save(false);
                                }else{
                                    $model->id_order = $cariorder->id_order;
                                    $model->save(false);
                                    }
                                 return[
                                   'status'=>'success',
                                   'data'=>$model,
                               ];
                           }else{
                               return[
                                   'status'=>'error',
                                   'message'=>'Data gagal disimpan'
                               ];
                           }
                      
                    }else{
                        return[
                            'status'=>'error',
                            'message'=>'Data transportasi tidak ditemukan',
                        ];
                    }
                }
            }else{ 
                return[
                    'status'=>'error',
                    'message'=>'Silahkan login terlebih dahulu'
                ];
            }
            
        }
    
       public function actionListOrder($status){
        Yii::$app->response->format = Response::FORMAT_JSON;
        $user = Yii::$app->user->identity;

        if($user){
            if ($status==0){
                 $cariorder= \common\models\Order::find()->where(['id_user'=>$user->id])
                 ->andWhere(['status'=>$status])->all();
                
                 if ($cariorder){
                    $data=[];
                    foreach ($cariorder as $key=>$value){
                        $caridesa1 = \common\models\DesaTraveling::find()->where(['id_desa'=>$value->id_desa])->one();
                        $data[$key]['id']= $value->id_order;
                        if($caridesa1){
                        $data[$key]['namaDesa']= $caridesa1->nama;
                        }
                        $caridesa = \common\models\DetailOrder::find()->where(['id_order'=>$value->id_order])->all();
                        if ($caridesa){
                            $sub=[];
                            foreach ($caridesa as $cd){
                                $subname['id']=$cd['id_detail'];
                                $subname['nama']=$cd['nama'];
                                $subname['alamat']=$cd['alamat'];
                                $subname['tanggal']=$cd['tanggal_awal'].'-'.$cd['tanggal_akhir'];
                                $subname['jumlah_orang']=$cd['jumlah_orang'];
                                $subname['totalbayar']=$cd['total_bayar'];
                                   if ($cd->kategori=='Experience'){
                                    $carifoto = \common\models\FotoExperience::find()->where(['id_experience'=>$cd->id_data])->one();
                                    if ($carifoto){
                                     $subname['foto']='http://entra.learnjsforfree.com/uploads/experience/' . $carifoto->foto;
                                    }
                                }else if($cd->kategori=='Transportation'){
                                    $carifoto = \common\models\FotoTransportasi::find()->where(['id_transportasi'=>$cd->id_data])->one();
                                    if($carifoto){
                                        $subname['foto']='http://entra.learnjsforfree.com/uploads/transport/' . $carifoto->foto;
                                    }
                                }else if ($cd->kategori=='Merchandise'){
                                    $carifoto = \common\models\FotoMerchandise::find()->where(['id_merchandise'=>$cd->id_data])->one();
                                    if ($carifoto){
                                        $subname['foto']= 'http://entra.learnjsforfree.com/uploads/merchant/' . $carifoto->foto;
                                    }
                                }else if ($cd->kategori=='Accommodation'){
                                    $carifoto = \common\models\FotoAccommodation::find()->where(['id_homestay'=>$cd->id_data])->one();
                                    if ($carifoto){
                                        $subname['foto']='http://entra.learnjsforfree.com/uploads/akomodasi/' . $carifoto->foto;
                                    }
                                }

                                array_push($sub, $subname);
                            }
                            $data[$key]['listData']=$sub;
                        }
                        $data[$key]['status']=$value->status;
                     
                    } $key++;
                       return[
                            'status'=>'success',
                            'data'=>$data,
                        ];
                 }else{
                    return[
                        'status'=>'error',
                        'message'=>'Data tidak ditemukan'
                    ];
                 }
            }else {
                $cariorder= \common\models\Order::find()->where(['id_user'=>$user->id])
                 ->andWhere(['status'=>$status])->all();
                 
                 if ($cariorder){
                    $data=[];
                    foreach ($cariorder as $key=>$value){
                        $count = \common\models\DetailOrder::find()->where(['id_order'=>$value->id_order])->count();
                        $data[$key]['id']= $value->id_order;
                        $data[$key]['total']= $count;
                        $caridata = \common\models\DetailOrder::find()->where(['id_order'=>$value->id_order])->all();
                     
                        if($caridata){
                            $sub2=[];
                            foreach ($caridata as $vl){
                                $caridesa1 = \common\models\DesaTraveling::find()->where(['id_desa'=>$value->id_desa])->one();
                                $subname1['id']=$vl['id_detail'];
                                if ($caridesa1){
                                $subname1['desa']=$caridesa1->nama;
                                }
                                $subname1['jumlah']=$vl['jumlah_orang'];
                                $subname1['totalbayar']=$vl['total_bayar'];
                                if($status==2){
                                $subname1['selesaibayar']=true;
                                }else{
                                    $subname1['selesaibayar']=false;
                                }
                                array_push($sub2, $subname1);
                            }
                            $data[$key]['ListSudahBayar']=$sub2;
                        }
                       
                    } $key++;
                     return[
                            'status'=>'success',
                            'data'=>$data,
                        ];
                 }else{
                    return[
                        'status'=>'error',
                        'message'=>'Data tidak ditemukan'
                    ];
                 }
            }
           
        }else{
            return[
                'status'=>'error',
                'message'=>'Silahkan login terlebih dahulu'
            ];
        }

    }
    
       public function actionDeleteActivity($id){
        Yii::$app->response->format = Response::FORMAT_JSON;
        $user = Yii::$app->user->identity;
        if ($user){
            $cari = \common\models\DetailPlan::find()->where(['id_detail'=>$id])->one();
            if($cari){
                $cari->delete();

                return[
                    'status'=>'success',
                    'message'=>'Data berhasil dihapus'
                ];
            }else{
                return[
                    'status'=>'error',
                    'message'=>'Data tidak ditemukan'
                ];
            }
        }else{
            return[
                'status'=>'error',
                'message'=>'Silahkan login terlebih dahulu'
            ];
        }
    }
    
        public function actionDeletePlan($id){
        Yii::$app->response->format = Response::FORMAT_JSON;
        $user = Yii::$app->user->identity;
        if($user){
            $caridata = \common\models\Plan::find()->where(['id_plan'=>$id])->one();
            if($caridata){
                $detail =\common\models\DetailPlan::find()->where(['id_plan'=>$id])->all();
                if($detail){
                    foreach ($detail as $dt){
                        $dt->delete();
                    }
                    $caridata->delete();
                     return[
                    'status'=>'success',
                    'message'=>'Data plan Berhasil dihapus'
                ];
                }else{
                $caridata->delete();
                    return[
                    'status'=>'success',
                    'message'=>'Data plan Berhasil dihapus'
                ];
                }
            }else{
                return[
                    'status'=>'error',
                    'message'=>'Data tidak ditemukan'
                ];
            }
        }else{
            return[
                'status'=>'error',
                'message'=>'Silahkan login terlebih dahulu'
            ];
        }
    }

    public function actionUpdatePlan($id){
        Yii::$app->response->format = Response::FORMAT_JSON;
        $user = Yii::$app->user->identity;
        if($user){
            $cariplan = \common\models\Plan::find()->where(['id_plan'=>$id])->one();
            if($cariplan){
               if ($cariplan->load(Yii::$app->request->post())) {
                $cariplan->save(false);
                  return[
                    'status'=>'success',
                    'data'=>$cariplan,
                    ];
               }else{
                return[
                'status'=>'error',
                'message'=>'Data gagal disimpan'
                ];
            }
            }else{
                return[
                'status'=>'error',
                'message'=>'Data tidak ditemukan'
                ];
            }
        }else{
            return[
                'status'=>'error',
                'message'=>'Silahkan login terlebih dahulu'
            ];
        }
    }

    public function actionUpdateActivity($id){
        Yii::$app->response->format = Response::FORMAT_JSON;
        $user = Yii::$app->user->identity;
        if($user){
            $update =\common\models\DetailPlan::find()->where(['id_detail'=>$id])->one();
            if($update){
                  if ($update->load(Yii::$app->request->post())) {
                    $update->save(false);
                    return[
                        'status'=>'success',
                        'data'=>$update,
                    ];
                  }else{
                    return[
                    'status'=>'error',
                    'message'=>'Data tidak ditemukan'
                    ];
            }
            }else{
                return[
                    'status'=>'error',
                    'message'=>'Data tidak ditemukan'
                ];
            }
        }else{
            return[
                'status'=>'error',
                'message'=>'Silahkan login terlebih dahulu'
            ];
        }
    }

    public function actionDetailPlan($id){
        Yii::$app->response->format= Response::FORMAT_JSON;
        $user = Yii::$app->user->identity;
        if ($user){
            $caridata = \common\models\Plan::find()->where(['id_plan'=>$id])->one();
            
                if($caridata){
                $one = \common\models\DetailPlan::find()->where(['id_plan'=>$id])->all();
                if ($one){
                $jumlahaktifitas = \common\models\DetailPlan::find()->where(['id_plan'=>$id])->count();
                $total = \common\models\DetailPlan::find()->where(['id_plan'=>$id])->sum('biaya');
                
                $caridata->activity= $jumlahaktifitas;
                $sub2=[];
                foreach ($one as $val1){
                    
                     // $sub_name2['foto'] = $onefoto->foto;
                       $sub_name2['id_activity'] =  $val1['id_detail'];
                          $sub_name2['jumlah_orang'] =  $val1['jumlah_orang'];
                      $sub_name2['tanggal_awal'] = $val1['tanggal_awal'];
                          $sub_name2['tanggal_akhir'] = $val1['tanggal_akhir'];
                            $sub_name2['kategori'] = $val1['kategori'];
                              if ($val1['kategori']=='Accommodation'){
                                $databaru = \common\models\Accommodation::find()->where(['id_homestay'=>$val1['id_data']])->one();
                                
                                
                                if ($databaru){
                                    
                              $sub_name2['nama'] = $databaru->nama_homestay;
                              $desa = \common\models\DesaTraveling::find()->where(['id_desa'=>$databaru->id_desa])->one();
                              if($desa){
                              $sub_name2['desa'] = $desa->nama;
                              }
                              $foto = \common\models\FotoAccommodation::find()->where(['id_homestay'=>$val1['id_data']])->one();
                              if ($foto){
                              $sub_name2['foto'] = 'http://entra.learnjsforfree.com/uploads/akomodasi/' . $foto->foto;
                              }
                                }
                              } else if ($val1['kategori']=='Transportation'){
                                $databaru = \common\models\Transportation::find()->where(['id_transportation'=>$val1['id_data']])->one();
                                if ($databaru){
                              $sub_name2['nama'] = $databaru->nama;
                               $desa = \common\models\DesaTraveling::find()->where(['id_desa'=>$databaru->id_desa])->one();
                              if($desa){
                              $sub_name2['desa'] = $desa->nama;
                              }
                               $foto = \common\models\FotoTransportasi::find()->where(['id_transportasi'=>$val1['id_data']])->one();
                              if ($foto){
                              $sub_name2['foto'] = 'http://entra.learnjsforfree.com/uploads/transport/' . $foto->foto;
                              }
                                }
                              } else  if ($val1['kategori']=='Merchandise'){
                                $databaru = \common\models\Merchandise::find()->where(['id_merchandise'=>$val1['id_data']])->one();
                                if ($databaru){
                              $sub_name2['nama'] = $databaru->nama;
                               $desa = \common\models\DesaTraveling::find()->where(['id_desa'=>$databaru->id_desa])->one();
                              if($desa){
                              $sub_name2['desa'] = $desa->nama;
                              }
                               $foto = \common\models\FotoMerchandise::find()->where(['id_merchandise'=>$val1['id_data']])->one();
                              if ($foto){
                              $sub_name2['foto'] = 'http://entra.learnjsforfree.com/uploads/merchant/' . $foto->foto;
                              }
                                }
                              } if ($val1['kategori']=='Experience'){
                                $databaru = \common\models\Experience::find()->where(['id_experience'=>$val1['id_data']])->one();
                                if ($databaru){
                              $sub_name2['nama'] = $databaru->nama;
                               $desa = \common\models\DesaTraveling::find()->where(['id_desa'=>$databaru->id_desa])->one();
                              if($desa){
                              $sub_name2['desa'] = $desa->nama;
                              }
                               $foto = \common\models\FotoExperience::find()->where(['id_experience'=>$val1['id_data']])->one();
                              if ($foto){
                              $sub_name2['foto'] = 'http://entra.learnjsforfree.com/uploads/experience/' . $foto->foto;
                              }
                                }
                              }
               
                     array_push($sub2, $sub_name2);
                }$caridata->activity= $jumlahaktifitas;
                $caridata->list_plan  = $sub2;
                $caridata->total_pembayaran  =  Yii::$app->formatter->asInteger($total);
                return[
                    'status'=>'success',
                    'data'=>$caridata
                    ];
                }else{
                    if ($caridata->activity==NULL){
                        $caridata->activity=0;
                    }
                    return[
                    'status'=>'success',
                    'data'=>$caridata,
                    ];
                }
        }else{
            
        
                    return[
                    'status'=>'error',
                    'message'=>'Data plan tidak ditemukan'
                    ];
                
        }
            
        }else{
            return[
                'status'=>'error',
                'message'=>'Silahkan login terlebih dahulu',

            ];
        }
        
    }
    
    
    public function actionListPlan() {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $user = Yii::$app->user->identity;
        if ($user) {
            $carilist = \common\models\Plan::find()->where(['id_user' => $user->id])->all();
            
            if ($carilist) {
//$count = \common\models\Plan::find()->where(['id_user' => $user->id])->count();
                
                $data = [];
                foreach ($carilist as $key => $value) {
                      $count = \common\models\DetailPlan::find()->where(['id_plan' => $value->id_plan])->count();
                    //   $foto = \common\models\Plan::find()->where(['id_plan' => $value->id_plan])->one();
                    $data[$key]['id'] = $value->id_plan;
                    $data[$key]['judul'] = $value->judul;
                      $data[$key]['foto'] = $value->foto;
                      if ($count){
                        $data[$key]['activity'] = $count;
                      }else{
                            $data[$key]['activity'] = 0;
                      }
                    //  if ($foto){
                        //  $data[$key]['fotoplan']= $foto->foto;
                     // }
                    
                }
                $key++;
                return[
                    'status' => 'success',
                    'data' => $data,
                    
                ];
            } else {
                return[
                    'status' => 'error',
                    'message' => 'Data tidak ada'
                ];
            }
        } else {
            return[
                'status' => 'error',
                'message' => 'Silahkan login terlebih dahulu'
            ];
        }
    }
    
    public function actionHitungBookmark(){
        Yii::$app->response->format = Response::FORMAT_JSON;
        $user = Yii::$app->user->identity;
        if ($user){
            $bookmark = \common\models\Bookmark::find()->where(['id_user'=>$user->id])->count();
            return[
            'status'=>'success',
            'HitungBookmark'=>$bookmark
            ];
        }else{
            return[
            'status'=>'error',
            'message'=>'Silahkan login terlebih dahulu'
            ];
        }
    }

    public function actionAddBookmark($id, $kategori) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $user = Yii::$app->user->identity;
        if ($user) {
            if ($kategori == 'Experience') {
                $cari = \common\models\Experience::find()->where(['id_experience' => $id])->one();
                if ($cari) {
                    $foto = \common\models\FotoExperience::find()->where(['id_experience' => $cari->id_experience])->one();
                    $datalama = \common\models\Bookmark::find()->where(['id_data' => $id])->andWhere(['kategori' => $kategori])
                            ->andWhere(['id_user'=>$user->id])->all();

                    if ($datalama) {
                        return[
                            'status' => 'error',
                            'message' => 'Data sudah ada',
                        ];
                    } else {
                        $new = new \common\models\Bookmark();
                        $new->id_data = $cari->id_experience;
                        $new->kategori = $kategori;
                        $new->nama = $cari->nama;
                        $new->harga = Yii::$app->formatter->asInteger($cari->price);
                        $new->id_desa = $cari->id_desa;
                        $new->id_user = $user->id;
                        $new->foto = 'http://entra.learnjsforfree.com/uploads/experience/' . $foto->foto;
                        $new->save(false);

                        $coba = new \common\models\BookmarkDesa;
                        $caridulu = \common\models\BookmarkDesa::find()->where(['id_user' => $new->id_user])
                                        ->andWhere(['id_desa' => $new->id_desa])->all();

                        if (!$caridulu) {
                            $coba->id_desa = $new->id_desa;
                            $coba->id_user = $new->id_user;
                            $coba->id_bookmark = $new->id_bookmark;
                            $coba->save(false);
                        }
                        return[
                            'status' => 'success',
                            'data' => $new,
                        ];
                    }
                } else {
                    return[
                        'status' => 'error',
                        'message' => 'Data experience tidak ditemukan'
                    ];
                }
            } else if ($kategori == 'Accommodation') {
                $cari = \common\models\Accommodation::find()->where(['id_homestay' => $id])->one();
                if ($cari) {
                    $foto = \common\models\FotoAccommodation::find()->where(['id_homestay' => $cari->id_homestay])->one();
                    $datalama = \common\models\Bookmark::find()->where(['id_data' => $id])->andWhere(['kategori' => $kategori])
                            ->andWhere(['id_user'=>$user->id])->all();

                    if ($datalama) {
                        return[
                            'status' => 'error',
                            'message' => 'Data sudah ada',
                        ];
                    } else {
                        $new = new \common\models\Bookmark();
                        $new->id_data = $cari->id_homestay;
                        $new->kategori = $kategori;
                        $new->nama = $cari->nama_homestay;
                        $new->harga = Yii::$app->formatter->asInteger($cari->harga);
                        $new->id_desa = $cari->id_desa;
                        $new->id_user = $user->id;
                        $new->foto = 'http://entra.learnjsforfree.com/uploads/akomodasi/' . $foto->foto;

                        $new->save(false);
                        $coba = new \common\models\BookmarkDesa;
                        $caridulu = \common\models\BookmarkDesa::find()->where(['id_user' => $new->id_user])
                                        ->andWhere(['id_desa' => $new->id_desa])->all();
                        if (!$caridulu) {
                            $coba->id_desa = $new->id_desa;
                            $coba->id_user = $new->id_user;
                            $coba->id_bookmark = $new->id_bookmark;
                            $coba->save(false);
                        }
                        return[
                            'status' => 'success',
                            'data' => $new,
                        ];
                    }
                } else {
                    return[
                        'status' => 'error',
                        'message' => 'Data accomodation tidak ditemukan'
                    ];
                }
            } else if ($kategori == 'Transportation') {
                $cari = \common\models\Transportation::find()->where(['id_transportation' => $id])->one();
                if ($cari) {
                    $datalama = \common\models\Bookmark::find()->where(['id_data' => $id])
                            ->andWhere(['id_user'=>$user->id])->andWhere(['kategori' => $kategori])->all();

                    if ($datalama) {
                        return[
                            'status' => 'error',
                            'message' => 'Data sudah ada',
                        ];
                    } else {
                        $new = new \common\models\Bookmark();
                        $new->id_data = $cari->id_transportation;
                        $new->kategori = $kategori;
                        $new->nama = $cari->nama;
                        $new->harga = Yii::$app->formatter->asInteger($cari->price);
                        $new->id_desa = $cari->id_desa;
                        $new->id_user = $user->id;
                        $new->foto = 'http://entra.learnjsforfree.com/uploads/transport/' . $cari->foto;
                        $new->save(false);
                        $coba = new \common\models\BookmarkDesa;
                        $caridulu = \common\models\BookmarkDesa::find()->where(['id_user' => $new->id_user])
                                        ->andWhere(['id_desa' => $new->id_desa])->all();
                        if (!$caridulu) {
                            $coba->id_desa = $new->id_desa;
                            $coba->id_user = $new->id_user;
                            $coba->id_bookmark = $new->id_bookmark;
                            $coba->save(false);
                        }
                        return[
                            'status' => 'success',
                            'data' => $new,
                        ];
                    }
                } else {
                    return[
                        'status' => 'error',
                        'message' => 'Data transportasi tidak ditemukan'
                    ];
                }
            } else if ($kategori == 'Merchandise') {
                $cari = \common\models\Merchandise::find()->where(['id_merchandise' => $id])->one();
                if ($cari) {
                    $foto = \common\models\FotoMerchandise::find()->where(['id_merchandise' => $cari->id_merchandise])->one();
                    $datalama = \common\models\Bookmark::find()->where(['id_data' => $id])
                            ->andWhere(['id_user'=>$user->id])->andWhere(['kategori' => $kategori])->all();

                    if ($datalama) {
                        return[
                            'status' => 'error',
                            'message' => 'Data sudah ada',
                        ];
                    } else {
                        $new = new \common\models\Bookmark();
                        $new->id_data = $cari->id_merchandise;
                        $new->kategori = $kategori;
                        $new->nama = $cari->nama;
                        $new->harga = Yii::$app->formatter->asInteger($cari->harga);
                        $new->id_desa = $cari->id_desa;
                        $new->id_user = $user->id;
                        $new->foto = 'http://entra.learnjsforfree.com/uploads/merchant/' . $foto->foto;
                        $new->save(false);
                        $coba = new \common\models\BookmarkDesa;
                        $caridulu = \common\models\BookmarkDesa::find()->where(['id_user' => $new->id_user])
                                        ->andWhere(['id_desa' => $new->id_desa])->all();

                        if (!$caridulu) {
                            $coba->id_desa = $new->id_desa;
                            $coba->id_user = $new->id_user;
                            $coba->id_bookmark = $new->id_bookmark;
                            $coba->save(false);
                        }
                        return[
                            'status' => 'success',
                            'data' => $new,
                        ];
                    }
                } else {
                    return[
                        'status' => 'error',
                        'message' => 'Data merchandise tidak ditemukan'
                    ];
                }
            } else {
                return[
                    'status' => 'error',
                    'message' => 'Data tidak ditemukan'
                ];
            }
        } else {
            return[
                'status' => 'error',
                'message' => 'Silahkan login terlebih dahulu'
            ];
        }
    }

    public function actionListBookmark() {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $user = Yii::$app->user->identity;
        if ($user) {
            $datadesa = \common\models\BookmarkDesa::find()->where(['id_user' => $user->id])->all();
            if ($datadesa) {
                $data = [];
                foreach ($datadesa as $key => $value) {
                    $desa = \common\models\Villages::find()->where(['id' => $value->id_desa])->one();
                    if ($desa) {
                        $data[$key]['id'] = $value->id_desa;
                        $data[$key]['nama_desa'] = $desa->name;
                    }

                    $bookmark = \common\models\Bookmark::find()->where(['id_desa' => $value->id_desa])
                            ->andWhere(['id_user'=>$user->id])->all();
                    $sub = [];
                    foreach ($bookmark as $ft) {
                        $sub_name['nama'] = $ft['nama'];
                        $sub_name['harga'] = Yii::$app->formatter->asInteger($ft['harga']);
                        $sub_name['foto'] = $ft['foto'];
                        array_push($sub, $sub_name);
                    }
                    $data[$key]['bookmark'] = $sub;
                }
                $key++;
                return[
                    'status' => 'success',
                    'data' => $data,
                ];
            } else {
                return[
                    'status' => 'error',
                    'message' => 'Dat tidak ada'
                ];
            }
        } else {
            
        }
    }
    
  
    public function actionRegistrasi() {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $model = new \common\models\SignupForm();

        $data = Yii::$app->request->post();

        if ($model->load($data) && $model->validate()) {
            $data = $model->signup();

            if ($data == 1) {
                return [
                    'status' => 'success',
                    'message' => 'Berhasil registrasi',
                    'data' => $data,
                ];
            } else {
                return [
                    'status' => 'error',
                    'message' => 'Gagal registrasi',
                    'data' => $data,
                ];
            }
        }

        return [
            'status' => 'error-validasi',
            'message' => 'Gagal validasi',
            'data' => $model->getErrors(),
        ];
    }

    public function actionAkomodasiRandom() {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $akomodasi = \common\models\Accommodation::find()->orderBy(new Expression('rand()'))->limit(10)->all();
        $data = [];

        if ($akomodasi){
            
             $count = \common\models\Accommodation::find()->count();
             
              foreach ($akomodasi as $key => $val) {

            $desa = \common\models\Villages::find()->where(['id' => $val->id_desa])->one();
            $provinsi = \common\models\Provinces::find()->where(['id' => $val->id_provinsi])->one();
            $pulau = \common\models\Island::find()->where(['id' => $provinsi->island_id])->one();
            $foto = \common\models\FotoAccommodation::find()->where(['id_homestay' => $val->id_homestay])->all();
            $data[$key]['id'] = $val->id_homestay;
            $data[$key]['nama'] = $val->nama_homestay;
            $data[$key]['rating'] = $val->rating;
            $data[$key]['harga'] = Yii::$app->formatter->asInteger($val->harga);
            $carikategori = \common\models\KategoriAccommodation::find()->where(['id_kategori'=>$val->id_kategori])->one();
            if ($carikategori){
                  $data[$key]['kategori'] = $carikategori->nama_kategori;
            }
          
            $data[$key]['alamat'] = $val->alamat;
            if ($pulau) {
                $data[$key]['pulau'] = $pulau->name;
            }

               $onephoto = \common\models\FotoAccommodation::find()->where(['id_homestay' => $val->id_homestay])->one();
         
               if ($onephoto) {

                    $data[$key]['foto'] = 'http://entra.learnjsforfree.com/uploads/akomodasi/' . $onephoto->foto;
                }
        } $key++;

        return [
            'status' => 'success',
            'data' => $data,
            'lengthData'=>$count,
        ];
        }else{
            return[
                'status'=>'error',
                'message'=>'Data tidak ditemukan'
            ];
        } 
    }
    public function actionDesaRandom(){
          Yii::$app->response->format = Response::FORMAT_JSON;
        
          $desatraveling = \common\models\DesaTraveling::find()->orderBy(new Expression('rand()'))->limit(10)->all();

        $data = [];
        if ($desatraveling){
             $count = \common\models\DesaTraveling::find()->count();
            foreach ($desatraveling as $key => $val) {
            $foto = \common\models\FotoDesatraveling::find()->where(['id_desa' => $val->id_desa])->orderBy('id_foto DESC')->one();
            $desa = \common\models\Villages::find()->where(['id' => $val->id_desa])->one();
            $provinsi = \common\models\Provinces::find()->where(['id' => $val->id_provinsi])->one();

            $data[$key]['id'] = $val->id;
         
            $data[$key]['nama'] = $val->nama;
            $data[$key]['alamat'] = $val->alamat;
            $data[$key]['deskripsi'] = $val->deskripsi;
             $data[$key]['rating'] = $val->rating;
            $data[$key]['desa'] = $desa->name;
            if ($provinsi) {
                $data[$key]['provinsi'] = $provinsi->name;
                $pulau = \common\models\Island::find()->where(['id' => $provinsi->island_id])->one();
                if ($pulau) {

                    $data[$key]['pulau'] = $pulau->name;
                }
            }


           
             $onephoto = \common\models\FotoDesatraveling::find()->where(['id_desa' => $val->id])->one();
         
               if ($onephoto) {

                    $data[$key]['foto'] = 'http://entra.learnjsforfree.com/uploads/desa/' . $onephoto->foto;
                }
        } $key++;

        return [
            'status' => 'success',
            'data' => $data,
            'lengthData'=>$count,
        ]; 
        }else{
            return[
                'status'=>'error',
                'message'=>'Data tidak ditemukan',
            ];
        }
    }

    public function actionExperienceRandom() {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $list = \common\models\Experience::find()->orderBy(new Expression('rand()'))->limit(10)->all();
        $data = [];
        if ($list) {
            $listcount = \common\models\Experience::find()->count();
            $data = [];
            foreach ($list as $key => $val) {
                $data[$key]['id'] = $val->id_experience;
                $data[$key]['nama'] = $val->nama;
                $data[$key]['harga'] = Yii::$app->formatter->asInteger($val->price);
                $data[$key]['deskripsi'] = $val->deskripsi;
                $data[$key]['alamat'] = $val->alamat;
                 $data[$key]['rating'] = $val->rating;
                 $carikategori = \common\models\KategoriExperience::find()->where(['id_kategori'=>$val->id_kategori])->one();
                 if ($carikategori){
                     $data[$key]['kategori'] = $carikategori->nama;
                 }
                 
                $desa = \common\models\Villages::find()->where(['id' => $val->id_desa])->one();
                if ($desa) {
                    $data[$key]['desa'] = $desa->name;
                }
                $foto = \common\models\FotoExperience::find()->where(['id_experience' => $val->id_experience])->all();
                $onephoto = \common\models\FotoExperience::find()->where(['id_experience' => $val->id_experience])->one();

                if ($onephoto) {

                    $data[$key]['foto'] = 'http://entra.learnjsforfree.com/uploads/experience/' . $onephoto->foto;
                }
            }
            $key++;
            return[
                'status' => 'success',
                'data' => $data,
                'lengthData' => $listcount,
            ];
        } else {

            return[
                'status' => 'error',
                'message' => 'Data tidak ditemukan'
            ];
        }
    }

    public function actionTransportasiRandom() {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $transportdata = \common\models\Transportation::find()->orderBy(new Expression('rand()'))->limit(10)->all();

        $data = [];

        if ($transportdata){
               $count = \common\models\Transportation::find()->count();

                foreach ($transportdata as $key => $val) {
            $desa = \common\models\Villages::find()->where(['id' => $val->id_desa])->one();
            $provinsi = \common\models\Provinces::find()->where(['id' => $val->id_provinsi])->one();
            $pulau = \common\models\Island::find()->where(['id' => $provinsi->island_id])->one();

            $data[$key]['id'] = $val->id_transportation;
            $carikategori =\common\models\KategoriTransportasi::find()->where(['id_kategori'=>$val->jenis_transportasi])->one();
            if ($carikategori){
            $data[$key]['kategori'] = $carikategori->nama;
            }
            $data[$key]['nama'] = $val->nama;
           
            $data[$key]['deskripsi'] = $val->deskripsi;
             $data[$key]['rating'] = $val->rating;
             $data[$key]['alamat'] = $val->alamat;
            $data[$key]['desa'] = $desa->name;
            $data[$key]['provinsi'] = $provinsi->name;
            if ($pulau) {
                $data[$key]['pulau'] = $pulau->name;
            }
            $data[$key]['harga'] = Yii::$app->formatter->asInteger($val->price);
            $onephoto = \common\models\FotoTransportasi::find()->where(['id_transportasi' => $val->id_transportation])->one();
         
               if ($onephoto) {

                    $data[$key]['foto'] = 'http://entra.learnjsforfree.com/uploads/transport/' . $onephoto->foto;
                }
        }$key++;

        return [
            'status' => 'success',
            'data' => $data,
            'lengthData'=>$count,
        ];
        }else{
            return[
                'status'=>'error',
                'message'=>'Data tidak ditemukan'
            ];
        }
    }

    public function actionMerchantRandom() {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $merchant = \common\models\Merchandise::find()->orderBy(new Expression('rand()'))->limit(10)->all();

        $data = [];

     if ($merchant){
              $count = \common\models\Merchandise::find()->count();
              foreach ($merchant as $key => $val) {
            $foto = \common\models\FotoMerchandise::find()->where(['id_merchandise' => $val->id_merchandise])->orderBy('id_foto DESC')->one();
            $desa = \common\models\Villages::find()->where(['id' => $val->id_desa])->one();
            $provinsi = \common\models\Provinces::find()->where(['id' => $val->id_provinsi])->one();

            $data[$key]['id'] = $val->id_merchandise;
            $carikategori =\common\models\KategoriMerchandise::find()->where(['id_kategori'=>$val->kategori])->one();
            if ($carikategori){
            $data[$key]['kategori'] = $carikategori->nama;
            }
            $data[$key]['nama'] = $val->nama;
            $data[$key]['alamat'] = $val->alamat;
            $data[$key]['deskripsi'] = $val->deskripsi;
             $data[$key]['rating'] = $val->rating;
            $data[$key]['desa'] = $desa->name;
            if ($provinsi) {
                $data[$key]['provinsi'] = $provinsi->name;
                $pulau = \common\models\Island::find()->where(['id' => $provinsi->island_id])->one();
                if ($pulau) {

                    $data[$key]['pulau'] = $pulau->name;
                }
            }

            $data[$key]['harga'] = Yii::$app->formatter->asInteger($val->harga);

           
             $onephoto = \common\models\FotoMerchandise::find()->where(['id_merchandise' => $val->id_merchandise])->one();
         
               if ($onephoto) {

                    $data[$key]['foto'] = 'http://entra.learnjsforfree.com/uploads/merchant/' . $onephoto->foto;
                }
        } $key++;

        return [
            'status' => 'success',
            'data' => $data,
            'lengthData'=>$count,
        ];
        }else{
            return [
            'status' => 'error',
            'message'=>'Data tidak ditemukan'
        ]; 
        }
    }

    public function actionDataExperience($pulau = NULL, $provinsi = NULL, $kecamatan = NULL) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        if ($provinsi) {
            $model = new \common\models\Experience();
            if ($model->load(Yii::$app->request->post())) {
                $cari_pulau = \common\models\Experience::find()->where(['like', 'nama', $model->nama])
                                ->andWhere(['id_provinsi' => $provinsi])->all();
                $data = [];
                foreach ($cari_pulau as $key => $value) {



                    $data[$key]['id'] = $value->id_experience;
                    $data[$key]['nama'] = $value->nama;
                }$key++;

                return[
                    'status' => 'success',
                    'data' => $data,
                ];
            }
        }
    }

    public function actionAddCard() {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $user = Yii::$app->user->identity;
        if ($user) {
            $model = new \common\models\CreditCard();
            if ($model->load(Yii::$app->request->post())) {
                $model->id_user = $user->id;
                $model->save();
                return[
                    'status' => 'success',
                    'data' => $model,
                ];
            } else {
                return[
                    'status' => 'error',
                    'message' => 'Data tidak bisa disimpan'
                ];
            }
        } else {
            return[
                'status' => 'error',
                'message' => 'Silahkan login terlebih dahulu'
            ];
        }
    }
    
    

    public function actionCreditcardData() {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $user = Yii::$app->user->identity;

        if ($user) {
            $cari = \common\models\CreditCard::find()->where(['id_user' => $user->id])->all();
            if ($cari) {
                $data = [];
                foreach ($cari as $key => $value) {
                    $data[$key]['id'] = $value->id_credit;
                    $data[$key]['name_card'] = $value->name_card;
                    $data[$key]['card_number'] = $value->card_number;
                    $data[$key]['expired_date'] = $value->expired_date;
                    $data[$key]['cvv'] = $value->cvv;
                    $data[$key]['user'] = $value->id_user;
                }$key++;
                return[
                    'status' => 'success',
                    'data' => $data
                ];
            } else {
                return[
                    'status' => 'error',
                    'message' => 'Data tidak ditemukan',
                ];
            }
        } else {
            return[
                'status' => 'error',
                'message' => 'Silahkan login terlebih dahulu'
            ];
        }
    }

    public function actionProfilData() {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $id = Yii::$app->user->identity;
        if ($id) {
            $model = \backend\models\User::find()->where(['id' => $id->id])->one();
            $profile = \common\models\Profile::find()->where(['user_id' => $id])->one();
            if ($model) {
                $model->photo = 'http://entra.learnjsforfree.com/uploads/foto_profil/' . $model->photo;
                return [
                    'status' => 'success',
                    'data' => $model,
                    'profile' => $profile,
                ];
            } else {
                return [
                    'status' => 'error',
                    'data' => 'Data tidak ditemukan',
                ];
            }
        } else {
            return [
                'status' => 'error',
                'data' => 'Harus login terlebih dahulu',
            ];
        }
    }
    
              public function actionUpdatePhoto(){
      Yii::$app->response->format = Response::FORMAT_JSON;
       $id = Yii::$app->user->identity;
       if($id){
      $model = \common\models\User::find()->where(['id'=>$id->id])->one();
      $data = Yii::$app->request->post();
        if ($model){ 

              $model->picture = UploadedFile::getInstance($model, 'picture');

            if ($model->picture){
                
              
                $foto = time()  .  rand() . '.' . $model->picture->extension;

               Yii::$app->db->createCommand("UPDATE user SET photo = '$foto' WHERE id = '$id->id' ")->execute();

                 if ($model->picture->saveAs(('uploads/foto_profil/') . $foto)) {
                            $val = Image::thumbnail(('uploads/foto_profil/') . $foto, 320, 320)
                                    ->save(('uploads/foto_profil/') . 'thumb-' . $foto, ['quality' => 50]);
                          
                                 return [
                    'status' => 'success',
                    'message' => 'Berhasil update profile',
                    'data' => 'http://entra.learnjsforfree.com/uploads/foto_profil/'.$foto,
                ];
                            
                        }
               
            }else if ($model->username ){
          if ($model->load(Yii::$app->request->post()) ) {
        $model->save(false);
          return [
                    'status' => 'success',
                 
                    'data' => $model,
                ];
          }else{
            return[
            'status'=>'error',
            'message'=>'Data gagal disimpan',
            
            ];
          }
      // $model->save(false);
          
           
      }
        else {
                return [
                    'status' => 'error',
                    'message' => 'Gagal registrasi',
                    'data' => $data,
                ];
            }
        }else {
             return [
                    'status' => 'error',
                    'message' => 'Data tidak ditemukan',
                    
                ];
        }
              }else{
                    return [
                    'status' => 'error',
                    'message' => 'Silahkan login terlebih dahulu',
                    
                ];
              }
      
    }
    
    public function actionChangePassword() {
         Yii::$app->response->format = Response::FORMAT_JSON;
        $user = \common\models\User::findOne(Yii::$app->user->id);
        $user->scenario = 'change_password';
         $coba = Yii::$app->user->identity;
        $model = Profile::findOne(['user_id' => $user->id]);

        if (!$model) {
            return false;
        }

      
        // var_dump($user);die;
        $loadedPost = $user->load(Yii::$app->request->post());

        if ($loadedPost  && $user->validate()) {

            $user->password = $user->newPassword;
        
            if ($user->save(false)) {
               return [
                    'status' => 'success',
                    'message' => 'Berhasil mengganti password',
                    'data'=>$user,
                    
                ];
            }
        }
        
        else{
             return [
                    'status' => 'error',
                    'message' => 'Gagal mengganti password',
                    
                ];
        }

        
    }

 public function actionUploadPembayaran($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $user = Yii::$app->user->identity;
        if ($user){
        
          $model =\common\models\Order::find()->where(['id_order'=>$id])->andWhere(['id_user'=>$user->id])->andWhere(['status'=>0])->one();
          if ($model){

      
       

              $model->picture = UploadedFile::getInstance($model, 'picture');

            if ($model->picture){
                
                $foto = time()  .  rand() . '.' . $model->picture->extension;
            
            //$data = Yii::$app->db->createCommand("UPDATE order SET foto = '$foto' WHERE id_order = '$id' ")->execute();
              $model->foto = $foto;
              $model->status=1;
              $model->tipe_pembayaran= 'Transfer';
              $model->save(false);
              $caridetail =\common\models\DetailOrder::find()->where(['id_order'=>$id])->all();
              if ($caridetail){
                  foreach ($caridetail as $dt){
                      $dt->status =1;
                      $dt->foto =$model->foto;
                      $dt->tipe_pembayaran = 'Transfer';
                      $dt->save(false);
                  }
              }
               if ($model->picture->saveAs(Yii::getAlias('uploads/bukti/') . $foto)) {
                    $val = Image::thumbnail(Yii::getAlias('uploads/bukti/') . $foto, 320, 320)
                          ->save(Yii::getAlias('uploads/bukti/') . 'thumb-' . $foto, ['quality' => 50]);
                            

                }
                $model->foto = 'http://entra.learnjsforfree.com/uploads/foto_profil/'. $foto;
                return [
                    'status' => 'success',
                    'message' => 'Berhasil upload bukti pembayaran',
                    'data'=>$model,
                ];
            }else{
               
                return [
                    'status' => 'error',
                    'message' => 'gagal upload bukti pembayaran',
                    
                ];
            }
      
        }else{
        return[
            'status'=>'error',
            'message'=>'Data tidak ditemukan'
        ];
        }
       
    }else{
        return[
        'status'=>'error',
        'message'=>'Silahkan login terlebih dahulu'
        ];
    }
    }

    
           public function actionPembayaranCreditCard($id, $idcard){
      Yii::$app->response->format = Response::FORMAT_JSON;
      $user = Yii::$app->user->identity;
        if ($user){
            
            $model = \common\models\Order::find()->where(['id_user'=>$user->id])->andWhere(['status'=>0])
                                                 ->andWhere(['id_order'=>$id])->one();
        
        if ($model){ 
            $caricard = \common\models\CreditCard::find()->where(['id_credit'=>$idcard])->one();
            
              $model->tipe_pembayaran ='CreditCard';
        
              $model->status = 2;
              $detailorder = \common\models\DetailOrder::find()->where(['id_order'=>$id])->all();
              foreach ($detailorder as $detail){
                  $detail->id_card=$caricard->id_credit;
                  $detail->tipe_pembayaran= 'CreditCard';
                  $detail->status=2;
                  $detail->save(false);
              }
              $model->save(false);
             
             return [
                    'status' => 'success',
                    'message' => 'Berhasil melakukan pembayaran',
                    'data'=>$model,
                   
                ];
       
        }else {
             return [
                    'status' => 'error',
                    'message' => 'Data tidak ditemukan',
                    
                ];
        }
     }else{
          return [
                    'status' => 'error',
                    'message' => 'Silahkan login terlebih dahulu',
                    
                ];
     }
    }
    
    public function actionSendTiket() {
        $model = new \common\models\User();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                // Yii::$app->session->setFlash('success', 'Check your email for further instructions.');
                return[
                    'status' => 'success',
                    'message' => 'Silahkan periksa email'
                ];
            } else {
               
                return[
                    'status' => 'error',
                    'message' => 'email tidak ditemukan'
                ];
            }
        } else {
            return[
                'status' => 'error',
                'message' => 'gagal mencari data'
            ];
        }
    }

        

}
