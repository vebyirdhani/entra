<?php

namespace frontend\controllers;

use Yii;
use yii\rest\Controller;
use yii\web\Response;
use yii\web\UploadedFile;
use yii\imagine\Image;
use yii\db\Expression;
use common\models\LoginForm;
use frontend\models\Profile;
use common\model\JadwalSiaran;

//use frontend\models\IntDikbangumBinaanLatihan;
//use frontend\models\IntDikbangumBinaanLatihan;
//use frontend\models\IntDikbangumBinaanLatihan;

class ServiceController extends Controller {



   public function actionUpdatePhoto(){
      Yii::$app->response->format = Response::FORMAT_JSON;
      $id = Yii::$app->user->identity;
      $model = \common\models\User::find()->where(['id'=>$id])->one();
      $data = Yii::$app->request->post();
    if ($id){
        if ($model->load($data) && $model->validate()){ 

              $model->picture = UploadedFile::getInstance($model, 'picture');

            if ($model->picture){
                
              
                $foto = time()  .  rand() . '.' . $model->picture->extension;

               Yii::$app->db->createCommand("UPDATE user SET photo = '$foto' WHERE id = '$id' ")->execute();

                 if ($model->picture->saveAs(('uploads/foto_profil/') . $foto)) {
                            $val = Image::thumbnail(('uploads/foto_profil/') . $foto, 320, 320)
                                    ->save(('uploads/foto_profil/') . 'thumb-' . $foto, ['quality' => 50]);
                          
                                 return [
                    'status' => 'success',
                    'message' => 'Berhasil update profile',
                    'data' => 'http://entra.learnjsforfree.com/uploads/foto_profil/' .$foto,
                ];
                            
                        }
               
            }else if ($model->username ){
          if ($model->load(Yii::$app->request->post()) ) {
        $model->save(false);
          return [
                    'status' => 'success',
                 
                    'data' => $model,
                ];
          }else{
            return[
            'status'=>'error',
            'message'=>'Data gagal disimpan',
            
            ];
          }
      // $model->save(false);
          
           
      }
        else {
                return [
                    'status' => 'error',
                    'message' => 'Gagal registrasi',
                    'data' => $data,
                ];
            }
        }else {
             return [
                    'status' => 'error',
                    'message' => 'Data gagal disimpan, pastikan ukuran gambar kurang dari 2MB',
                    
                ];
        }
      }else{
       return [
                    'status' => 'error',
                    'message' => 'Silahkan login terlebih dahulu',
                    
                ];
    }
    }
    public function actionDesa($provinsi) {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $desa = \common\models\Villages::find()->where(['provinsi' => $provinsi])->orderBy(new Expression('rand()'))->limit(10)->all();

        $data = [];

        foreach ($desa as $key => $val) {
            $kecamatan = \common\models\Districts::find()->where(['id' => $val->district_id])->one();
            $provinsi = \common\models\Provinces::find()->where(['id'=>$val->provinsi])->one();
           
            $foto = \common\models\FotoDesa::find()->where(['id_desa' => $val->id])->orderBy('id_foto DESC')->one();
            $data[$key]['id'] = $val->id;
            $data[$key]['daerah'] = $kecamatan->name . ' , ' . $val->provinsi;
            if($provinsi){
                 $pulau = \common\models\Island::find()->where(['id'=>$provinsi->island_id])->one();
                  $data[$key]['pulau'] = $pulau->name;
            }
            $data[$key]['name'] = $val->name;

            $data[$key]['rating'] = $val->rating;
            if ($foto) {
                $data[$key]['foto'] = $foto->foto;
            }
            $key++;
        }
            $transport= \common\models\Transportation::find()->where(['id_provinsi'=>$provinsi])->all();
            $list_transport = [];
             foreach ($transport as $key => $val) {
                  $list_transport[$key]['nama'] = $val->nama;
                  $list_transport[$key]['price'] = $val->price;
                  $key++;
             }
        return [
            'status' => 'success',
            'data' => $data,
            'list_transport'=>$list_transport,
        ];
    }
       public function actionDesaRandom() {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $desa = \common\models\Villages::find()->orderBy(new Expression('rand()'))->limit(10)->all();

        $data = [];

        foreach ($desa as $key => $val) {
            $kecamatan = \common\models\Districts::find()->where(['id' => $val->district_id])->one();
            $provinsi = \common\models\Provinces::find()->where(['id'=>$val->provinsi])->one();
           
            $foto = \common\models\FotoDesa::find()->where(['id_desa' => $val->id])->orderBy('id_foto DESC')->one();
            $data[$key]['id'] = $val->id;
            $data[$key]['daerah'] = $kecamatan->name . ' , ' . $val->provinsi;
            if($provinsi){
                 $pulau = \common\models\Island::find()->where(['id'=>$provinsi->island_id])->one();
                  $data[$key]['pulau'] = $pulau->name;
            }
            $data[$key]['name'] = $val->name;

            $data[$key]['rating'] = $val->rating;
            if ($foto) {
                $data[$key]['foto'] = $foto->foto;
            }
            $key++;
        }

        return [
            'status' => 'success',
            'data' => $data,
        ];
    }

   public function actionDetailDesa($id){
       Yii::$app->response->format = Response::FORMAT_JSON;
       $desa = \common\models\Villages::find()->where(['id'=>$id])->one();
       if ($desa){
           $data_foto = \common\models\FotoDesa::find()->where(['id_desa'=>$desa->id])->all();
           $foto =[];
           if ($data_foto){
               foreach ($data_foto as $key=> $value){
                      $foto[$key]['foto'] = $value->foto;
                       $key++;
               }
              
               return[
               'status'=>'success',
               'data'=>$desa,
               'foto'=>$foto,
           ]; 
           }else{
                 return[
               'status'=>'success',
               'data'=>$desa,
           ]; 
           }
          
       }else{
           return[
               'status'=>'error',
               'message'=>'Data tidak ditemukan',
           ];
       }
   }
   
   public function actionKategoriExperience(){
       Yii::$app->response->format= Response::FORMAT_JSON;
       $kategori= \common\models\KategoriExperience::find()->all();
     
       $data=[];
       
       if ($kategori){
           foreach ($kategori as $key=>$val){
               $data[$key]['id_kategori']=$val->id_kategori;
               $data[$key]['nama']= $val->nama;
                $key++;
             
           }
             return[
                   'status'=>'success',
                   'data'=>$data,
               ];
       }else{
           return[
                   'status'=>'error',
                   'message'=>'Data tidak ditemukan',
               ]; 
       }
   }
   
   
    public function actionAkomodasi($provinsi) {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $akomodasi = \common\models\Accommodation::find()->where(['id_provinsi' => $provinsi])->orderBy(new Expression('rand()'))->limit(5)->all();

        $data = [];

        foreach ($akomodasi as $key => $val) {

            $foto = \common\models\FotoAccommodation::find()->where(['id_homestay' => $val->id_homestay])->orderBy('id_foto DESC')->one();
            $desa = \common\models\Villages::find()->where(['id' => $val->id_desa])->one();
            $provinsi = \common\models\Provinces::find()->where(['id' => $val->id_provinsi])->one();
            $pulau = \common\models\Island::find()->where(['id' => $provinsi->island_id])->one();
            $data[$key]['id'] = $val->id_homestay;
            $data[$key]['kategori'] = $val->id_kategori;
            $data[$key]['nama'] = $val->nama_homestay;

            $data[$key]['desa'] = $desa->name;
            $data[$key]['provinsi'] = $provinsi->name;
            if ($pulau) {
                $data[$key]['pulau'] = $pulau->name;
            }
            $data[$key]['rating'] = $val->rating;
            $data[$key]['harga'] = $val->harga;
            if ($foto) {
                $data[$key]['foto'] = $foto->foto;
            }
            $key++;
        }

        return [
            'status' => 'success',
            'data' => $data,
        ];
    }

    public function actionAkomodasiRandom() {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $akomodasi = \common\models\Transportation::find()->orderBy(new Expression('rand()'))->limit(10)->all();
        $data = [];

        foreach ($akomodasi as $key => $val) {

            $desa = \common\models\Villages::find()->where(['id' => $val->id_desa])->one();
            $provinsi = \common\models\Provinces::find()->where(['id' => $val->id_provinsi])->one();
            $pulau = \common\models\Island::find()->where(['id' => $provinsi->island_id])->one();
            $data[$key]['id'] = $val->id_transportation;
            $data[$key]['jenis_transportasi'] = $val->jenis_transportasi;
            $data[$key]['nama'] = $val->nama;
            $data[$key]['rute'] = $val->rute;
            $data[$key]['deskripsi'] = $val->deskripsi;

            $data[$key]['desa'] = $desa->name;
            $data[$key]['provinsi'] = $provinsi->name;
            if ($pulau) {
                $data[$key]['pulau'] = $pulau->name;
            }

            $data[$key]['price'] = $val->price;
            $data[$key]['foto'] = $val->foto;
            $key++;
        }

        return [
            'status' => 'success',
            'data' => $data,
        ];
    }
        public function actionAllAkomodasi() {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $akomodasi = \common\models\Accommodation::find()->all();
        $data = [];

        foreach ($akomodasi as $key => $val) {

            $desa = \common\models\Villages::find()->where(['id' => $val->id_desa])->one();
            $provinsi = \common\models\Provinces::find()->where(['id' => $val->id_provinsi])->one();
            $pulau = \common\models\Island::find()->where(['id' => $provinsi->island_id])->one();
            $data[$key]['id'] = $val->id_transportation;
            $data[$key]['jenis_transportasi'] = $val->jenis_transportasi;
            $data[$key]['nama'] = $val->nama;
            $data[$key]['rute'] = $val->rute;
            $data[$key]['deskripsi'] = $val->deskripsi;

            $data[$key]['desa'] = $desa->name;
            $data[$key]['provinsi'] = $provinsi->name;
            if ($pulau) {
                $data[$key]['pulau'] = $pulau->name;
            }

            $data[$key]['price'] = $val->price;
            $data[$key]['foto'] ='http://veby.mediasumbar.com/uploads/akomodasi/'.$val->foto;
            $key++;
        }

        return [
            'status' => 'success',
            'data' => $data,
        ];
    }

    public function actionTransportasi($provinsi) {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $akomodasi = \common\models\Transportation::find()->where(['id_provinsi' => $provinsi])->orderBy(new Expression('rand()'))->limit(5)->all();

        $data = [];

        foreach ($akomodasi as $key => $val) {
            $desa = \common\models\Villages::find()->where(['id' => $val->id_desa])->one();
            $provinsi = \common\models\Provinces::find()->where(['id' => $val->id_provinsi])->one();
            $pulau = \common\models\Island::find()->where(['id' => $provinsi->island_id])->one();

            $data[$key]['id'] = $val->id_transportation;
            $data[$key]['jenis_transportasi'] = $val->jenis_transportasi;
            $data[$key]['nama'] = $val->nama;
            $data[$key]['rute'] = $val->rute;
            $data[$key]['deskripsi'] = $val->deskripsi;

             $data[$key]['desa'] = $desa->name;
            $data[$key]['provinsi'] = $provinsi->name;
            if ($pulau) {
                $data[$key]['pulau'] = $pulau->name;
            }

            $data[$key]['price'] = $val->price;
            $data[$key]['foto'] = $val->foto;
            $key++;
        }

        return [
            'status' => 'success',
            'data' => $data,
        ];
    }

    public function actionTransportasiRandom() {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $akomodasi = \common\models\Transportation::find()->orderBy(new Expression('rand()'))->limit(10)->all();

        $data = [];

        foreach ($akomodasi as $key => $val) {
            $desa = \common\models\Villages::find()->where(['id' => $val->id_desa])->one();
            $provinsi = \common\models\Provinces::find()->where(['id' => $val->id_provinsi])->one();
            $pulau = \common\models\Island::find()->where(['id' => $provinsi->island_id])->one();

            $data[$key]['id'] = $val->id_transportation;
            $data[$key]['jenis_transportasi'] = $val->jenis_transportasi;
            $data[$key]['nama'] = $val->nama;
            $data[$key]['rute'] = $val->rute;
            $data[$key]['deskripsi'] = $val->deskripsi;

           $data[$key]['desa'] = $desa->name;
            $data[$key]['provinsi'] = $provinsi->name;
            if ($pulau) {
                $data[$key]['pulau'] = $pulau->name;
            }
            $data[$key]['price'] = $val->price;
            $data[$key]['foto'] = $val->foto;
            $key++;
        }

        return [
            'status' => 'success',
            'data' => $data,
        ];
    }
      public function actionAllTransportasi() {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $akomodasi = \common\models\Transportation::find()->all();

        $data = [];

        foreach ($akomodasi as $key => $val) {
            $desa = \common\models\Villages::find()->where(['id' => $val->id_desa])->one();
            $provinsi = \common\models\Provinces::find()->where(['id' => $val->id_provinsi])->one();
            $pulau = \common\models\Island::find()->where(['id' => $provinsi->island_id])->one();

            $data[$key]['id'] = $val->id_transportation;
            $data[$key]['jenis_transportasi'] = $val->jenis_transportasi;
            $data[$key]['nama'] = $val->nama;
            $data[$key]['rute'] = $val->rute;
            $data[$key]['deskripsi'] = $val->deskripsi;

           $data[$key]['desa'] = $desa->name;
            $data[$key]['provinsi'] = $provinsi->name;
            if ($pulau) {
                $data[$key]['pulau'] = $pulau->name;
            }
            $data[$key]['price'] = $val->price;
            $data[$key]['foto'] = 'http://veby.mediasumbar.com/uploads/transport/'.$val->foto;
            $key++;
        }

        return [
            'status' => 'success',
            'data' => $data,
        ];
    }

    public function actionMerchant($provinsi) {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $merchant = \common\models\Merchandise::find()->where(['id_provinsi' => $provinsi])->orderBy(new Expression('rand()'))->limit(5)->all();

        $data = [];

        foreach ($merchant as $key => $val) {
            $foto = \common\models\FotoMerchandise::find()->where(['id_merchandise' => $val->id_merchandise])->orderBy('id_foto DESC')->one();
            $desa = \common\models\Villages::find()->where(['id' => $val->id_desa])->one();
            $provinsi = \common\models\Provinces::find()->where(['id' => $val->id_provinsi])->one();
            $pulau = \common\models\Island::find()->where(['id' => $provinsi->island_id])->one();
            $data[$key]['id'] = $val->id_merchandise;
            $data[$key]['kategori'] = $val->kategori;
            $data[$key]['nama'] = $val->nama;
            $data[$key]['alamat'] = $val->alamat;
            $data[$key]['deskripsi'] = $val->deskripsi;

              $data[$key]['desa'] = $desa->name;
            $data[$key]['provinsi'] = $provinsi->name;
            if ($pulau) {
                $data[$key]['pulau'] = $pulau->name;
            }
            $data[$key]['harga'] = $val->harga;
            if ($foto) {
                $data[$key]['foto'] = $foto->foto;
            }
            $key++;
        }

        return [
            'status' => 'success',
            'data' => $data,
        ];
    }

    public function actionMerchantRandom() {
        Yii::$app->response->format = Response::FORMAT_JSON;
     
        $merchant = \common\models\Merchandise::find()->orderBy(new Expression('rand()'))->limit(10)->all();

        $data = [];

        foreach ($merchant as $key => $val) {
            $foto = \common\models\FotoMerchandise::find()->where(['id_merchandise' => $val->id_merchandise])->orderBy('id_foto DESC')->one();
            $desa = \common\models\Villages::find()->where(['id' => $val->id_desa])->one();
            $provinsi = \common\models\Provinces::find()->where(['id' => $val->id_provinsi])->one();
           
            $data[$key]['id'] = $val->id_merchandise;
            $data[$key]['kategori'] = $val->kategori;
            $data[$key]['nama'] = $val->nama;
            $data[$key]['alamat'] = $val->alamat;
            $data[$key]['deskripsi'] = $val->deskripsi;

             $data[$key]['desa'] = $desa->name;
             if ($provinsi){
                  $data[$key]['provinsi'] = $provinsi->name;
                   $pulau = \common\models\Island::find()->where(['id' => $provinsi->island_id])->one();
            if ($pulau) {
                
                $data[$key]['pulau'] = $pulau->name;
            }
             }
           
            $data[$key]['harga'] = $val->harga;
            if ($foto) {
                $data[$key]['foto'] = $foto->foto;
            }
            $key++;
        }

        return [
            'status' => 'success',
            'data' => $data,
        ];
    }
    
       public function actionAllMerchant() {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $merchant = \common\models\Merchandise::find()->all();

        $data = [];

        foreach ($merchant as $key => $val) {
            $foto = \common\models\FotoMerchandise::find()->where(['id_merchandise' => $val->id_merchandise])->orderBy('id_foto DESC')->one();
            $desa = \common\models\Villages::find()->where(['id' => $val->id_desa])->one();
            $provinsi = \common\models\Provinces::find()->where(['id' => $val->id_provinsi])->one();
           
            $data[$key]['id'] = $val->id_merchandise;
            $data[$key]['kategori'] = $val->kategori;
            $data[$key]['nama'] = $val->nama;
            $data[$key]['alamat'] = $val->alamat;
            $data[$key]['deskripsi'] = $val->deskripsi;

             $data[$key]['desa'] = $desa->name;
             if ($provinsi){
                  $data[$key]['provinsi'] = $provinsi->name;
                   $pulau = \common\models\Island::find()->where(['id' => $provinsi->island_id])->one();
            if ($pulau) {
                
                $data[$key]['pulau'] = $pulau->name;
            }
             }
           
            $data[$key]['harga'] = $val->harga;
            if ($foto) {
                $data[$key]['foto'] = $foto->foto;
            }
            $key++;
        }

        return [
            'status' => 'success',
            'data' => $data,
        ];
    }
    
      public function actionKategoriAkomodasi() {
        Yii::$app->response->format= Response::FORMAT_JSON;
        $kategori = \common\models\KategoriAccommodation::find()->all();
        
        $data=[];
        if ($kategori){
            foreach ($kategori as $key=>$val){
                $data[$key]['id']= $val->id_kategori;
                $data[$key]['nama']=$val->nama_kategori;
                
            }
            $key++;
            return[
               'status'=>'success',
                'data'=>$data,
            ];
        }else{
            return[
                'status'=>'error',
                'message'=>'Data tidak ditemukan'
            ];
        }
    }
    
    public function actionKategoriTransportasi() {
        Yii::$app->response->format = Response::FORMAT_JSON;
         $kategori = \common\models\KategoriTransportasi::find()->all();
        
        $data=[];
        if ($kategori){
            foreach ($kategori as $key=>$val){
                $data[$key]['id']= $val->id_kategori;
                $data[$key]['nama']=$val->nama;
                
            }
            $key++;
            return[
               'status'=>'success',
                'data'=>$data,
            ];
        }else{
            return[
                'status'=>'error',
                'message'=>'Data tidak ditemukan'
            ];
        }
    }
     public function actionKategoriMerchant(){
        Yii::$app->response->format = Response::FORMAT_JSON;
         $kategori = \common\models\KategoriMerchandise::find()->all();
        
        $data=[];
        if ($kategori){
            foreach ($kategori as $key=>$val){
                $data[$key]['id']= $val->id_kategori;
                $data[$key]['nama']=$val->nama;
                
            }
            $key++;
            return[
               'status'=>'success',
                'data'=>$data,
            ];
        }else{
            return[
                'status'=>'error',
                'message'=>'Data tidak ditemukan'
            ];
        }
    }
    
    public function actionDataProvinsi(){
        Yii::$app->response->format = Response::FORMAT_JSON;
        $provinsi = \common\models\Provinces::find()->all();
        
        $data =[];
        
        if ($provinsi){
            foreach ($provinsi as $key=>$val){
                $data[$key]['id']= $val->id;
                $data[$key]['name']=$val->name;
                $data[$key]['id_island']=$val->island_id; 
            }
            $key++;
            return[
                'status'=>'success',
                'data'=>$data,
            ];
        }else{
            return[
                'status'=>'error',
                'message'=>'Data tidak ditemukan',
            ];
        }
    }
    
    public function actionListPulau(){
         Yii::$app->response->format = Response::FORMAT_JSON;
        $provinsi = \common\models\Island::find()->all();
        
        $data =[];
        
        if ($provinsi){
            foreach ($provinsi as $key=>$val){
                $data[$key]['id']= $val->id;
                $data[$key]['name']=$val->name;
               
            }
            $key++;
            return[
                'status'=>'success',
                'data'=>$data,
            ];
        }else{
            return[
                'status'=>'error',
                'message'=>'Data tidak ditemukan',
            ];
        }
    }
 public function actionListNamaDesa(){
        Yii::$app->response->format = Response::FORMAT_JSON;
        $desa = \common\models\Villages::find()->all();
        
        $data =[];
        
        if ($desa){
             foreach ($desa as $key => $val) {
          
            $provinsi = \common\models\Provinces::find()->where(['id'=>$val->provinsi])->one();
           
         
            $data[$key]['id'] = $val->id;
             $data[$key]['name'] = $val->name;
            if($provinsi){
                 $pulau = \common\models\Island::find()->where(['id'=>$provinsi->island_id])->one();
                  $data[$key]['pulau'] = $pulau->name;
            }
            $data[$key]['provinsi'] = $val->provinsi;
			$data[$key]['district_id'] = $val->district_id;
          
           
        }
            $key++;
            return[
                'status'=>'success',
                'data'=>$data,
            ];
        }else{
            return[
                'status'=>'error',
                'message'=>'Data tidak ditemukan',
            ];
        }
    }
    
//    public function actionSearchExperience(){
//        Yii::$app->response->format = Response::FORMAT_JSON;
//        
//        $model = new \common\models\Experience();
//            if ($model->load(Yii::$app->request->post())) {
//                $cari = \common\models\Experience::find()->where([''])
//            }
//    }
    
    public function actionSearchAkomodasi(){
        Yii::$app->response->format = Response::FORMAT_JSON;
        
        $model= new \common\models\Accommodation();
        if ($model->load(Yii::$app->request->post())){
            $cari = \common\models\Accommodation::find()->where(['like','nama_homestay',$model->nama_homestay])->all();
            if ($cari){
                $data =[];
            foreach ($cari as $key=>$val){
                $foto = \common\models\FotoAccommodation::find()->where(['id_homestay'=>$val->id_homestay])->all();
                $data[$key]['id']=$val->id_homestay;
                $data[$key]['nama']=$val->nama_homestay;
                 $data[$key]['rating']=$val->rating;
                  $data[$key]['harga']=$val->harga;
                 $data[$key]['kategori']=$val->id_kategori;
                  $data[$key]['alamat']=$val->alamat;
                  if ($foto){
                      foreach ($foto as $vl){
                           $data[$key]['foto']='http://veby.mediasumbar.com/uploads/akomodasi/'.$vl->foto;
                      }
                      
                  }
            }
            $key++;
            return[
                'status'=>'success',
                'data'=>$data,
            ]; 
            }else{
                return[
                    'status'=>'error',
                    'message'=>'Data tidak ditemukan'
                ];
            }
           
           
        }else{
            return[
               'status'=>'error',
                'message'=>'Data tidak bisa dicari'
            ];
        }
    }
    
    public function actionSearchTransportasi(){
         Yii::$app->response->format = Response::FORMAT_JSON;
        
        $model= new \common\models\Transportation();
        if ($model->load(Yii::$app->request->post())){
            $cari = \common\models\Transportation::find()->where(['like','nama',$model->nama])->all();
          
            if ($cari){
                $data = [];
                foreach ($cari as $key=>$val){
                      $data[$key]['id']=$val->id_transportation;
                $data[$key]['nama']=$val->nama;
                $data[$key]['jenis_transportasi']=$val->jenis_transportasi;
                $data[$key]['harga']=$val->price;
                $data[$key]['deskripsi']=$val->deskripsi;
                
                $data[$key]['foto']='http://veby.mediasumbar.com/uploads/transport/'.$val->foto;
                }   $key++;
            return[
                'status'=>'success',
                'data'=>$data,
            ]; 
               
            }else{
            return[
               'status'=>'error',
                'message'=>'Data tidak ditemukan'
            ];
        }
        }
        else{
            return[
               'status'=>'error',
                'message'=>'Data tidak bisa dicari'
            ];
        }
    }
    public function actionSearchSouvenir(){
         Yii::$app->response->format = Response::FORMAT_JSON;
        
        $model= new \common\models\Merchandise();
        if ($model->load(Yii::$app->request->post())){
            $cari = \common\models\Merchandise::find()->where(['like','nama',$model->nama])->all();
          
            if ($cari){
                
                $data = [];
                foreach ($cari as $key=>$val){
                    $foto = \common\models\FotoMerchandise::find()->where(['id_merchandise'=>$val->id_merchandise])->all();
                      $data[$key]['id']=$val->id_merchandise;
                $data[$key]['nama']=$val->nama;
                $data[$key]['kategori']=$val->kategori;
                $data[$key]['harga']=$val->harga;
                $data[$key]['deskripsi']=$val->deskripsi;
                 $data[$key]['alamat']=$val->alamat;
               if ($foto){
                      foreach ($foto as $vl){
                           $data[$key]['foto']='http://veby.mediasumbar.com/uploads/merchant/'.$vl->foto;
                      }
                      
                  }
                }   $key++;
            return[
                'status'=>'success',
                'data'=>$data,
            ]; 
               
            }else{
            return[
               'status'=>'error',
                'message'=>'Data tidak ditemukan'
            ];
        }
        }
        else{
            return[
               'status'=>'error',
                'message'=>'Data tidak bisa dicari'
            ];
        }
    }
    
        public function actionSearchDesa(){
         Yii::$app->response->format = Response::FORMAT_JSON;
        
        $model= new \common\models\Villages();
        if ($model->load(Yii::$app->request->post())){
            $cari = \common\models\Villages::find()->where(['like','name',$model->name])->all();
          
            if ($cari){
                
                $data = [];
                foreach ($cari as $key=>$val){
                    $foto = \common\models\FotoDesa::find()->where(['id_desa'=>$val->id])->all();
                      $data[$key]['id']=$val->id;
                $data[$key]['nama']=$val->name;
                $data[$key]['rating']=$val->rating;
              
                 $data[$key]['alamat']=$val->address;
               if ($foto){
                      foreach ($foto as $vl){
                           $data[$key]['foto']='http://veby.mediasumbar.com/uploads/desa/'.$vl->foto;
                      }
                      
                  }
                }   $key++;
            return[
                'status'=>'success',
                'data'=>$data,
            ]; 
               
            }else{
            return[
               'status'=>'error',
                'message'=>'Data tidak ditemukan'
            ];
        }
        }
        else{
            return[
               'status'=>'error',
                'message'=>'Data tidak bisa dicari'
            ];
        }
    }

    public function actionAllExperience(){
      Yii::$app->response->format = Response::FORMAT_JSON;

      $list = \common\models\Experience::find()->all();
      if($list){
        $data =[];
      foreach ($list as $key=>$val){
        $data[$key]['id']=$val->id_experience;
        $data[$key]['nama']=$val->nama;
        $data[$key]['harga']=$val->price;
        $data[$key]['deskripsi']=$val->deskripsi;
        $data[$key]['alamat']=$val->alamat;
        $desa= \common\models\Villages::find()->where(['id'=>$val->id_desa])->one();
        if ($desa){
           $data[$key]['desa']=$desa->name;
        }
       $foto = \common\models\FotoExperience::find()->where(['id_experience'=>$val->id_experience])->all();
       if($foto){
        $sub                    = [];
          foreach($foto as $ft){
           $sub_name['foto']=$ft['foto'];
            array_push($sub, $sub_name);
          }
       }
     
        $data[$key]['foto'] = $sub;
      }
      $key++;
      return[
        'status'=>'success',
        'data'=>$data,
      ];
    }else{

        return[
               'status'=>'error',
                'message'=>'Data tidak ditemukan'
            ];
    }
    }

    public function actionSearchExperience(){
      Yii::$app->response->format= Response::FORMAT_JSON;
      $new = new \common\models\Experience();
        if ($new->load(Yii::$app->request->post())){
         $cari= \common\models\Experience::find()->where(['like','nama',$new->nama])->all();
         if ($cari){
           $data =[];
      foreach ($cari as $key=>$val){
        $data[$key]['id']=$val->id_experience;
        $data[$key]['nama']=$val->nama;
        $data[$key]['harga']=$val->price;
        $data[$key]['deskripsi']=$val->deskripsi;
        $data[$key]['alamat']=$val->alamat;
        $desa= \common\models\Villages::find()->where(['id'=>$val->id_desa])->one();
        if ($desa){
           $data[$key]['desa']=$desa->name;
        }
       $foto = \common\models\FotoExperience::find()->where(['id_experience'=>$val->id_experience])->all();
       if($foto){
        $sub                    = [];
          foreach($foto as $ft){
           $sub_name['foto']=$ft['foto'];
            array_push($sub, $sub_name);
          }
       }
     
        $data[$key]['foto'] = $sub;
      }
      $key++;
      return[
        'status'=>'success',
        'data'=>$data,
      ];
    }else{
      return[
               'status'=>'error',
                'message'=>'Data tidak ditemukan'
            ];
    }
        }

    }

    public function actionDetailExperience($id){
      Yii::$app->response->format = Response::FORMAT_JSON;
      $cari =\common\models\Experience::find()->where(['id_experience'=>$id])->one();
      if ($cari){
        return[
          'status'=>'success',
          'cari'=>$cari,
        ];
      }else{
        return[
          'status'=>'error',
          'message'=>'Data tidak ditemukan'
        ];
      }
    }
}
