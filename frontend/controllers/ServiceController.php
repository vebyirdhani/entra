<?php

namespace frontend\controllers;

use Yii;
use yii\rest\Controller;
use yii\web\Response;
use yii\web\UploadedFile;
use yii\imagine\Image;
use yii\db\Expression;
use common\models\LoginForm;
use frontend\models\Profile;
use common\model\JadwalSiaran;

class ServiceController extends Controller {

    public function actionSearchExperience() {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $new = new \common\models\SearchForm();
        $user = Yii::$app->user->identity;

        if ($new->load(Yii::$app->request->post())) {
            $cari = \common\models\Experience::find()->where(['like', 'nama', $new->keyword])->all();
            if ($cari) {
                 $count = \common\models\Experience::find()->where(['like', 'nama', $new->keyword])->count();
                if ($user) {
                    $new->id_user = $user->id;
                }
                $new->save(false);
                $data = [];
                foreach ($cari as $key => $val) {
                    $data[$key]['id'] = $val->id_experience;
                    $data[$key]['nama'] = $val->nama;
                    $data[$key]['harga'] = $val->price;
                    $data[$key]['deskripsi'] = $val->deskripsi;
                    $data[$key]['alamat'] = $val->alamat;
                    $data[$key]['rating'] = $val->rating;
                            $data[$key]['kategori'] = $val->id_kategori;
                    $desa = \common\models\Villages::find()->where(['id' => $val->id_desa])->one();
                    if ($desa) {
                        $data[$key]['desa'] = $desa->name;
                    }

                    $onephoto = \common\models\FotoExperience::find()->where(['id_experience' => $val->id_experience])->one();

                    if ($onephoto) {

                        $data[$key]['foto'] = 'http://veby.mediasumbar.com/uploads/transport/' . $onephoto->foto;
                    }
                }
                $key++;
                return[
                    'status' => 'success',
                    'data' => $data,
                    'lengthData'=>$count,
                ];
            } else {
                return[
                    'status' => 'error',
                    'message' => 'Data tidak ditemukan'
                ];
            }
        } else {
            return[
                'status' => 'error',
                'message' => 'Gagal mencari data'
            ];
        }
    }

    public function actionSearchAkomodasi() {
        Yii::$app->response->format = Response::FORMAT_JSON;
          $user = Yii::$app->user->identity;
        $model = new \common\models\SearchForm();
        if ($model->load(Yii::$app->request->post())) {
            $cari = \common\models\Accommodation::find()->where(['like', 'nama_homestay', $model->keyword])->all();
            if ($cari) {
                 $count = \common\models\Accommodation::find()->where(['like', 'nama_homestay', $model->keyword])->count();
                 if ($user) {
                    $model->id_user = $user->id;
                }
               
                $model->save(false);
                $data = [];
                foreach ($cari as $key => $val) {
                    $foto = \common\models\FotoAccommodation::find()->where(['id_homestay' => $val->id_homestay])->all();
                    $data[$key]['id'] = $val->id_homestay;
                    $data[$key]['nama'] = $val->nama_homestay;
                    $data[$key]['rating'] = $val->rating;
                    $data[$key]['harga'] = $val->harga;
                    $data[$key]['kategori'] = $val->id_kategori;
                    $data[$key]['alamat'] = $val->alamat;
                    $onephoto = \common\models\FotoAccommodation::find()->where(['id_homestay' => $val->id_homestay])->one();
//                    if ($foto) {
//                        $sub = [];
//                        foreach ($foto as $ft) {
//                            $sub_name['foto'] = 'http://veby.mediasumbar.com/uploads/akomodasi/' . $ft['foto'];
//                            array_push($sub, $sub_name);
//                        }
//                        $data[$key]['gallery'] = $sub;
//                    }
                    if ($onephoto) {
                        $data[$key]['foto'] = 'http://veby.mediasumbar.com/uploads/akomodasi/' .$onephoto->foto;
                    }
                }
                $key++;
                return[
                    'status' => 'success',
                    'data' => $data,
                    'lenghtData'=>$count,
                ];
            } else {
                return[
                    'status' => 'error',
                    'message' => 'Data tidak ditemukan'
                ];
            }
        } else {
            return[
                'status' => 'error',
                'message' => 'Data tidak bisa dicari'
            ];
        }
    }
    
    

    public function actionSearchTransportasi() {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $user = Yii::$app->user->identity;
        $model = new \common\models\SearchForm();
        if ($model->load(Yii::$app->request->post())) {
            $cari = \common\models\Transportation::find()->where(['like', 'nama', $model->keyword])->all();

            if ($cari) {
                $count = \common\models\Transportation::find()->where(['like', 'nama', $model->keyword])->count();
                if ($user) {
                    $model->id_user = $user->id;
                }
                $model->save(false);
                $data = [];
                foreach ($cari as $key => $val) {
                    $data[$key]['id'] = $val->id_transportation;
                    $data[$key]['nama'] = $val->nama;
                    $data[$key]['kategori'] = $val->jenis_transportasi;
                    $data[$key]['harga'] = $val->price;
                    $data[$key]['deskripsi'] = $val->deskripsi;
                     $data[$key]['rating'] = $val->rating;
                    $data[$key]['foto'] = 'http://veby.mediasumbar.com/uploads/transport/' . $val->foto;
                } $key++;
                return[
                    'status' => 'success',
                    'data' => $data,
                     'lenghtData'=>$count,
                ];
            } else {
                return[
                    'status' => 'error',
                    'message' => 'Data tidak ditemukan'
                ];
            }
        } else {
            return[
                'status' => 'error',
                'message' => 'Data tidak bisa dicari'
            ];
        }
    }

public function actionSearchTujuanTransportasi() {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $user = Yii::$app->user->identity;
        $model = new \common\models\Rute();
        if ($model->load(Yii::$app->request->post())) {
            $cari = \common\models\Rute::find()->where(['like', 'daerah_mulai', $model->daerah_mulai])
            ->andWhere(['like', 'daerah_akhir', $model->daerah_akhir])->all();

            if ($cari) {
                $count = \common\models\Rute::find()->where(['like', 'daerah_mulai', $model->daerah_mulai])
            ->andWhere(['like', 'daerah_akhir', $model->daerah_akhir])->count();
              
                $data = [];
                foreach ($cari as $key => $val) {
     //   $transport= \common\models\Transportation::find->where(['id_transport'=>$val->id_transport])->one();
           $transport = \common\models\Transportation::find()->where(['id_transportation' => $val->id_transport])->one();
               
                       $fotobaru = \common\models\FotoTransportasi::find()->where(['id_transportasi' => $val->id_transport])->one();
                    $data[$key]['id'] = $val->id_transport;
                    $data[$key]['nama'] = $transport->nama;
                    $data[$key]['kategori'] = $transport->jenis_transportasi;
                    $data[$key]['harga'] = $transport->price;
                    $data[$key]['deskripsi'] = $transport->deskripsi;
                     $data[$key]['rating'] = $transport->rating;
                     if ($fotobaru){
                          $data[$key]['foto'] = 'http://veby.mediasumbar.com/uploads/transport/' . $fotobaru->foto;
                     }
                      $data[$key]['tujuan_awal'] = $val->daerah_mulai;
                       $data[$key]['tujuan_akhir'] = $val->daerah_akhir;
                  
                } $key++;
                return[
                    'status' => 'success',
                    'data' => $data,
                     'lenghtData'=>$count,
                ];
            } else {
                return[
                    'status' => 'error',
                    'message' => 'Data tidak ditemukan'
                ];
            }
        } else {
            return[
                'status' => 'error',
                'message' => 'Data tidak bisa dicari'
            ];
        }
    }
    public function actionSearchSouvenir() {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $user = Yii::$app->user->identity;
        $model = new \common\models\SearchForm();
        if ($model->load(Yii::$app->request->post())) {
            $cari = \common\models\Merchandise::find()->where(['like', 'nama', $model->keyword])->all();

            if ($cari) {
                  $count = \common\models\Merchandise::find()->where(['like', 'nama', $model->keyword])->count();
                if ($user) {
                    $model->id_user = $user->id;
                }
                $model->save(false);
                $data = [];
                foreach ($cari as $key => $val) {
                    $foto = \common\models\FotoMerchandise::find()->where(['id_merchandise' => $val->id_merchandise])->all();
                    $data[$key]['id'] = $val->id_merchandise;
                    $data[$key]['nama'] = $val->nama;
                    $data[$key]['kategori'] = $val->kategori;
                    $data[$key]['harga'] = $val->harga;
                    $data[$key]['deskripsi'] = $val->deskripsi;
                    $data[$key]['alamat'] = $val->alamat;
                     $data[$key]['rating'] = $val->rating;
                    $photoone = \common\models\FotoMerchandise::find()->where(['id_merchandise' => $val->id_merchandise])->one();
//                    if ($foto) {
//                        $sub = [];
//                        foreach ($foto as $ft) {
//                            $sub_name['foto'] = 'http://veby.mediasumbar.com/uploads/merchant/' . $ft['foto'];
//                            array_push($sub, $sub_name);
//                        }
//                        $data[$key]['gallery'] = $sub;
//                    }
                    if ($photoone) {
                        $data[$key]['foto'] ='http://veby.mediasumbar.com/uploads/merchant/' . $photoone->foto;
                    }
                } $key++;
                return[
                    'status' => 'success',
                    'data' => $data,
                      'lenghtData'=>$count,
                ];
            } else {
                return[
                    'status' => 'error',
                    'message' => 'Data tidak ditemukan'
                ];
            }
        } else {
            return[
                'status' => 'error',
                'message' => 'Data tidak bisa dicari'
            ];
        }
    }

    public function actionSearchDesa() {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $model = new \common\models\SearchForm();
        $user = Yii::$app->user->identity;
        if ($model->load(Yii::$app->request->post())) {
            $cari = \common\models\Villages::find()->where(['like', 'name', $model->keyword])->all();

            if ($cari) {
                 $count = \common\models\Villages::find()->where(['like', 'name', $model->keyword])->count();
                if ($user) {
                    $model->id_user = $user->id;
                }
                $model->save(false);
                $data = [];
                foreach ($cari as $key => $val) {
                    $foto = \common\models\FotoDesa::find()->where(['id_desa' => $val->id])->all();
                    $data[$key]['id'] = $val->id;
                    $data[$key]['nama'] = $val->name;
                    $data[$key]['rating'] = $val->rating;

                    $data[$key]['alamat'] = $val->address;
                     $data[$key]['deskripsi'] = $val->description;
                    $photoone = \common\models\FotoDesa::find()->where(['id_desa' => $val->id])->one();
//                    if ($foto) {
//                        $sub = [];
//                        foreach ($foto as $ft) {
//                            $sub_name['foto'] = 'http://veby.mediasumbar.com/uploads/desa/' . $ft['foto'];
//                            array_push($sub, $sub_name);
//                        }
//                        $data[$key]['gallery'] = $sub;
//                    }
                    if ($photoone) {
                        $data[$key]['foto'] = 'http://veby.mediasumbar.com/uploads/desa/' . $photoone->foto;
                    }
                } $key++;
                return[
                    'status' => 'success',
                    'data' => $data,
                      'lenghtData'=>$count,
                ];
            } else {
                return[
                    'status' => 'error',
                    'message' => 'Data tidak ditemukan'
                ];
            }
        } else {
            return[
                'status' => 'error',
                'message' => 'Data tidak bisa dicari'
            ];
        }
    }

        public function actionSearchAnything() {
        Yii::$app->response->format = Response::FORMAT_JSON;
          $user = Yii::$app->user->identity;
        $model = new \common\models\SearchForm();
        if ($model->load(Yii::$app->request->post())) {
           
            $akomodasi = \common\models\Accommodation::find()->where(['like', 'nama_homestay', $model->keyword])->all();
            $transport = \common\models\Transportation::find()->where(['like', 'nama', $model->keyword])->all();
            $experience = \common\models\Experience::find()->where(['like', 'nama', $model->keyword])->all();
              $merchant = \common\models\Merchandise::find()->where(['like', 'nama', $model->keyword])->all();
              $desa = \common\models\Villages::find()->where(['like', 'name', $model->keyword])->all();
            if ($akomodasi) {
                 $count = \common\models\Accommodation::find()->where(['like', 'nama_homestay', $model->keyword])->count();
                 if ($user) {
                    $model->id_user = $user->id;
                }
               
                $model->save(false);
                $data = [];
                foreach ($akomodasi as $key => $val) {
                    $foto = \common\models\FotoAccommodation::find()->where(['id_homestay' => $val->id_homestay])->all();
                    $data[$key]['id'] = $val->id_homestay;
                    $data[$key]['nama'] = $val->nama_homestay;
                    $data[$key]['rating'] = $val->rating;
                    $data[$key]['harga'] = $val->harga;
                    $data[$key]['kategori'] = $val->id_kategori;
                    $data[$key]['alamat'] = $val->alamat;
                    $onephoto = \common\models\FotoAccommodation::find()->where(['id_homestay' => $val->id_homestay])->one();

                    if ($onephoto) {
                        $data[$key]['foto'] = 'http://veby.mediasumbar.com/uploads/akomodasi/' .$onephoto->foto;
                    }
                }
                $key++;
                return[
                    'status' => 'success',
                    'data' => $data,
                    'lenghtData'=>$count,
                ];
            } else if ($transport){
                  $count = \common\models\Transportation::find()->where(['like', 'nama', $model->keyword])->count();
                if ($user) {
                    $model->id_user = $user->id;
                }
                $model->save(false);
                $data = [];
                foreach ($transport as $key => $val) {
                    $data[$key]['id'] = $val->id_transportation;
                    $data[$key]['nama'] = $val->nama;
                    $data[$key]['kategori'] = $val->jenis_transportasi;
                    $data[$key]['harga'] = $val->price;
                    $data[$key]['deskripsi'] = $val->deskripsi;
                     $data[$key]['rating'] = $val->rating;
                    $data[$key]['foto'] = 'http://veby.mediasumbar.com/uploads/transport/' . $val->foto;
                } $key++;
                return[
                    'status' => 'success',
                    'data' => $data,
                     'lenghtData'=>$count,
                ];
            }else if ($experience){
                  $count = \common\models\Experience::find()->where(['like', 'nama', $model->keyword])->count();
                if ($user) {
                    $model->id_user = $user->id;
                }
                $model->save(false);
                $data = [];
                foreach ($experience as $key => $val) {
                    $data[$key]['id'] = $val->id_experience;
                    $data[$key]['nama'] = $val->nama;
                    $data[$key]['harga'] = $val->price;
                    $data[$key]['deskripsi'] = $val->deskripsi;
                    $data[$key]['alamat'] = $val->alamat;
                    $data[$key]['rating'] = $val->rating;
                            $data[$key]['kategori'] = $val->id_kategori;
                    $desa = \common\models\Villages::find()->where(['id' => $val->id_desa])->one();
                    if ($desa) {
                        $data[$key]['desa'] = $desa->name;
                    }

                    $onephoto = \common\models\FotoExperience::find()->where(['id_experience' => $val->id_experience])->one();

                    if ($onephoto) {

                        $data[$key]['foto'] = 'http://veby.mediasumbar.com/uploads/experience/' . $onephoto->foto;
                    }
                }
                $key++;
                return[
                    'status' => 'success',
                    'data' => $data,
                    'lengthData'=>$count,
                ];
            }else if ($merchant){
                      $count = \common\models\Merchandise::find()->where(['like', 'nama', $model->keyword])->count();
                if ($user) {
                    $model->id_user = $user->id;
                }
                $model->save(false);
                $data = [];
                foreach ($merchant as $key => $val) {
                    $foto = \common\models\FotoMerchandise::find()->where(['id_merchandise' => $val->id_merchandise])->all();
                    $data[$key]['id'] = $val->id_merchandise;
                    $data[$key]['nama'] = $val->nama;
                    $data[$key]['kategori'] = $val->kategori;
                    $data[$key]['harga'] = $val->harga;
                    $data[$key]['deskripsi'] = $val->deskripsi;
                    $data[$key]['alamat'] = $val->alamat;
                     $data[$key]['rating'] = $val->rating;
                    $photoone = \common\models\FotoMerchandise::find()->where(['id_merchandise' => $val->id_merchandise])->one();

                    if ($photoone) {
                        $data[$key]['foto'] ='http://veby.mediasumbar.com/uploads/merchant/' . $photoone->foto;
                    }
                } $key++;
                return[
                    'status' => 'success',
                    'data' => $data,
                      'lenghtData'=>$count,
                ];
            }else if ($desa){
                       $count = \common\models\Villages::find()->where(['like', 'name', $model->keyword])->count();
                if ($user) {
                    $model->id_user = $user->id;
                }
                $model->save(false);
                $data = [];
                foreach ($desa as $key => $val) {
                    $foto = \common\models\FotoDesa::find()->where(['id_desa' => $val->id])->all();
                    $data[$key]['id'] = $val->id;
                    $data[$key]['nama'] = $val->name;
                    $data[$key]['rating'] = $val->rating;

                    $data[$key]['alamat'] = $val->address;
                     $data[$key]['deskripsi'] = $val->description;
                    $photoone = \common\models\FotoDesa::find()->where(['id_desa' => $val->id])->one();

                    if ($photoone) {
                        $data[$key]['foto'] = 'http://veby.mediasumbar.com/uploads/desa/' . $photoone->foto;
                    }
                } $key++;
                return[
                    'status' => 'success',
                    'data' => $data,
                      'lenghtData'=>$count,
                ];
            }
            else {
                return[
                    'status' => 'error',
                    'message' => 'Data tidak ditemukan'
                ];
            }
        } else {
            return[
                'status' => 'error',
                'message' => 'Data tidak bisa dicari'
            ];
        }
    }

    public function actionAllExperience($pulau = NULL, $provinsi = NULL, $kecamatan = NULL , $desa=NULL, $harga=NULL, $kategori=NULL, $limit=NULL) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        if ($pulau){
             $list = \common\models\Experience::find()->where(['id_pulau'=>$pulau])->all();
        if ($list) {
            $listcount = \common\models\Experience::find()->where(['id_pulau'=>$pulau])->count();
            $data = [];
            foreach ($list as $key => $val) {
                $data[$key]['id'] = $val->id_experience;
                $data[$key]['nama'] = $val->nama;
                $data[$key]['harga'] = $val->price;
                $data[$key]['deskripsi'] = $val->deskripsi;
                $data[$key]['alamat'] = $val->alamat;
                 $data[$key]['rating'] = $val->rating;
                 $data[$key]['kategori'] = $val->id_kategori;
                $desa = \common\models\Villages::find()->where(['id' => $val->id_desa])->one();
                if ($desa) {
                    $data[$key]['desa'] = $desa->name;
                }
                $foto = \common\models\FotoExperience::find()->where(['id_experience' => $val->id_experience])->all();
                $onephoto = \common\models\FotoExperience::find()->where(['id_experience' => $val->id_experience])->one();
//                if ($foto) {
//                    $sub = [];
//                    foreach ($foto as $ft) {
//                        $sub_name['foto'] = 'http://veby.mediasumbar.com/uploads/experience/' . $ft['foto'];
//                        array_push($sub, $sub_name);
//                    }
//                    $data[$key]['gallery'] = $sub;
//                }
                if ($onephoto) {

                    $data[$key]['foto'] = 'http://veby.mediasumbar.com/uploads/experience/' . $onephoto->foto;
                }
            }
            $key++;
            return[
                'status' => 'success',
                'data' => $data,
                'lengthData' => $listcount,
            ];
        } else {

            return[
                'status' => 'error',
                'message' => 'Data tidak ditemukan'
            ];
        }
        }else if ($provinsi){
 $list = \common\models\Experience::find()->where(['id_provinsi'=>$provinsi])->all();
        if ($list) {
            $listcount = \common\models\Experience::find()->where(['id_provinsi'=>$provinsi])->count();
            $data = [];
            foreach ($list as $key => $val) {
                $data[$key]['id'] = $val->id_experience;
                $data[$key]['nama'] = $val->nama;
                $data[$key]['harga'] = $val->price;
                $data[$key]['deskripsi'] = $val->deskripsi;
                $data[$key]['alamat'] = $val->alamat;
                 $data[$key]['rating'] = $val->rating;
                 $data[$key]['kategori'] = $val->id_kategori;
                $desa = \common\models\Villages::find()->where(['id' => $val->id_desa])->one();
                if ($desa) {
                    $data[$key]['desa'] = $desa->name;
                }
                $foto = \common\models\FotoExperience::find()->where(['id_experience' => $val->id_experience])->all();
                $onephoto = \common\models\FotoExperience::find()->where(['id_experience' => $val->id_experience])->one();
//                if ($foto) {
//                    $sub = [];
//                    foreach ($foto as $ft) {
//                        $sub_name['foto'] = 'http://veby.mediasumbar.com/uploads/experience/' . $ft['foto'];
//                        array_push($sub, $sub_name);
//                    }
//                    $data[$key]['gallery'] = $sub;
//                }
                if ($onephoto) {

                    $data[$key]['foto'] = 'http://veby.mediasumbar.com/uploads/experience/' . $onephoto->foto;
                }
            }
            $key++;
            return[
                'status' => 'success',
                'data' => $data,
                'lengthData' => $listcount,
            ];
        } else {

            return[
                'status' => 'error',
                'message' => 'Data tidak ditemukan'
            ];
        }
        }else if ($kecamatan){
 $list = \common\models\Experience::find()->where(['id_kecamatan'=>$kecamatan])->all();
        if ($list) {
            $listcount = \common\models\Experience::find()->where(['id_kecamatan'=>$kecamatan])->count();
            $data = [];
            foreach ($list as $key => $val) {
                $data[$key]['id'] = $val->id_experience;
                $data[$key]['nama'] = $val->nama;
                $data[$key]['harga'] = $val->price;
                $data[$key]['deskripsi'] = $val->deskripsi;
                $data[$key]['alamat'] = $val->alamat;
                 $data[$key]['rating'] = $val->rating;
                 $data[$key]['kategori'] = $val->id_kategori;
                $desa = \common\models\Villages::find()->where(['id' => $val->id_desa])->one();
                if ($desa) {
                    $data[$key]['desa'] = $desa->name;
                }
                $foto = \common\models\FotoExperience::find()->where(['id_experience' => $val->id_experience])->all();
                $onephoto = \common\models\FotoExperience::find()->where(['id_experience' => $val->id_experience])->one();
//                if ($foto) {
//                    $sub = [];
//                    foreach ($foto as $ft) {
//                        $sub_name['foto'] = 'http://veby.mediasumbar.com/uploads/experience/' . $ft['foto'];
//                        array_push($sub, $sub_name);
//                    }
//                    $data[$key]['gallery'] = $sub;
//                }
                if ($onephoto) {

                    $data[$key]['foto'] = 'http://veby.mediasumbar.com/uploads/experience/' . $onephoto->foto;
                }
            }
            $key++;
            return[
                'status' => 'success',
                'data' => $data,
                'lengthData' => $listcount,
            ];
        } else {

            return[
                'status' => 'error',
                'message' => 'Data tidak ditemukan'
            ];
        }
        }else if ($desa){
 $list = \common\models\Experience::find()->where(['id_desa'=>$desa])->all();
        if ($list) {
            $listcount = \common\models\Experience::find()->where(['id_desa'=>$desa])->count();
            $data = [];
            foreach ($list as $key => $val) {
                $data[$key]['id'] = $val->id_experience;
                $data[$key]['nama'] = $val->nama;
                $data[$key]['harga'] = $val->price;
                $data[$key]['deskripsi'] = $val->deskripsi;
                $data[$key]['alamat'] = $val->alamat;
                 $data[$key]['rating'] = $val->rating;
                 $data[$key]['kategori'] = $val->id_kategori;
                $desa = \common\models\Villages::find()->where(['id' => $val->id_desa])->one();
                if ($desa) {
                    $data[$key]['desa'] = $desa->name;
                }
                $foto = \common\models\FotoExperience::find()->where(['id_experience' => $val->id_experience])->all();
                $onephoto = \common\models\FotoExperience::find()->where(['id_experience' => $val->id_experience])->one();
//                if ($foto) {
//                    $sub = [];
//                    foreach ($foto as $ft) {
//                        $sub_name['foto'] = 'http://veby.mediasumbar.com/uploads/experience/' . $ft['foto'];
//                        array_push($sub, $sub_name);
//                    }
//                    $data[$key]['gallery'] = $sub;
//                }
                if ($onephoto) {

                    $data[$key]['foto'] = 'http://veby.mediasumbar.com/uploads/experience/' . $onephoto->foto;
                }
            }
            $key++;
            return[
                'status' => 'success',
                'data' => $data,
                'lengthData' => $listcount,
            ];
        } else {

            return[
                'status' => 'error',
                'message' => 'Data tidak ditemukan'
            ];
        }
        }else if ($harga){
 $list = \common\models\Experience::find()->orderBy('price  '.$harga)->all();
        if ($list) {
            $listcount = \common\models\Experience::find()->orderBy('price  '.$harga)->count();
            $data = [];
            foreach ($list as $key => $val) {
                $data[$key]['id'] = $val->id_experience;
                $data[$key]['nama'] = $val->nama;
                $data[$key]['harga'] = $val->price;
                $data[$key]['deskripsi'] = $val->deskripsi;
                $data[$key]['alamat'] = $val->alamat;
                 $data[$key]['rating'] = $val->rating;
                 $data[$key]['kategori'] = $val->id_kategori;
                $desa = \common\models\Villages::find()->where(['id' => $val->id_desa])->one();
                if ($desa) {
                    $data[$key]['desa'] = $desa->name;
                }
                $foto = \common\models\FotoExperience::find()->where(['id_experience' => $val->id_experience])->all();
                $onephoto = \common\models\FotoExperience::find()->where(['id_experience' => $val->id_experience])->one();
//                if ($foto) {
//                    $sub = [];
//                    foreach ($foto as $ft) {
//                        $sub_name['foto'] = 'http://veby.mediasumbar.com/uploads/experience/' . $ft['foto'];
//                        array_push($sub, $sub_name);
//                    }
//                    $data[$key]['gallery'] = $sub;
//                }
                if ($onephoto) {

                    $data[$key]['foto'] = 'http://veby.mediasumbar.com/uploads/experience/' . $onephoto->foto;
                }
            }
            $key++;
            return[
                'status' => 'success',
                'data' => $data,
                'lengthData' => $listcount,
            ];
        } else {

            return[
                'status' => 'error',
                'message' => 'Data tidak ditemukan'
            ];
        }
        }else if ($kategori){
 $list = \common\models\Experience::find()->where(['id_kategori'=>$kategori])->all();
        if ($list) {
            $listcount = \common\models\Experience::find()->where(['id_kategori'=>$kategori])->count();
            $data = [];
            foreach ($list as $key => $val) {
                $data[$key]['id'] = $val->id_experience;
                $data[$key]['nama'] = $val->nama;
                $data[$key]['harga'] = $val->price;
                $data[$key]['deskripsi'] = $val->deskripsi;
                $data[$key]['alamat'] = $val->alamat;
                 $data[$key]['rating'] = $val->rating;
                 $data[$key]['kategori'] = $val->id_kategori;
                $desa = \common\models\Villages::find()->where(['id' => $val->id_desa])->one();
                if ($desa) {
                    $data[$key]['desa'] = $desa->name;
                }
                $foto = \common\models\FotoExperience::find()->where(['id_experience' => $val->id_experience])->all();
                $onephoto = \common\models\FotoExperience::find()->where(['id_experience' => $val->id_experience])->one();
//                if ($foto) {
//                    $sub = [];
//                    foreach ($foto as $ft) {
//                        $sub_name['foto'] = 'http://veby.mediasumbar.com/uploads/experience/' . $ft['foto'];
//                        array_push($sub, $sub_name);
//                    }
//                    $data[$key]['gallery'] = $sub;
//                }
                if ($onephoto) {

                    $data[$key]['foto'] = 'http://veby.mediasumbar.com/uploads/experience/' . $onephoto->foto;
                }
            }
            $key++;
            return[
                'status' => 'success',
                'data' => $data,
                'lengthData' => $listcount,
            ];
        } else {

            return[
                'status' => 'error',
                'message' => 'Data tidak ditemukan'
            ];
        }
        }else if ($limit){
 $list = \common\models\Experience::find()->limit($limit)->all();
        if ($list) {
            $listcount = \common\models\Experience::find()->count();
            if ($listcount>$limit){
                $total= $limit;
            }else {
                $total = $listcount;
            }
            $data = [];
            foreach ($list as $key => $val) {
                $data[$key]['id'] = $val->id_experience;
                $data[$key]['nama'] = $val->nama;
                $data[$key]['harga'] = $val->price;
                $data[$key]['deskripsi'] = $val->deskripsi;
                $data[$key]['alamat'] = $val->alamat;
                 $data[$key]['rating'] = $val->rating;
                 $data[$key]['kategori'] = $val->id_kategori;
                $desa = \common\models\Villages::find()->where(['id' => $val->id_desa])->one();
                if ($desa) {
                    $data[$key]['desa'] = $desa->name;
                }
                $foto = \common\models\FotoExperience::find()->where(['id_experience' => $val->id_experience])->all();
                $onephoto = \common\models\FotoExperience::find()->where(['id_experience' => $val->id_experience])->one();
//                if ($foto) {
//                    $sub = [];
//                    foreach ($foto as $ft) {
//                        $sub_name['foto'] = 'http://veby.mediasumbar.com/uploads/experience/' . $ft['foto'];
//                        array_push($sub, $sub_name);
//                    }
//                    $data[$key]['gallery'] = $sub;
//                }
                if ($onephoto) {

                    $data[$key]['foto'] = 'http://veby.mediasumbar.com/uploads/experience/' . $onephoto->foto;
                }
            }
            $key++;
            return[
                'status' => 'success',
                'data' => $data,
                'lengthData' => $total,
            ];
        } else {

            return[
                'status' => 'error',
                'message' => 'Data tidak ditemukan'
            ];
        }
        }else {
             $list = \common\models\Experience::find()->all();
        if ($list) {
            $listcount = \common\models\Experience::find()->count();
            $data = [];
            foreach ($list as $key => $val) {
                $data[$key]['id'] = $val->id_experience;
                $data[$key]['nama'] = $val->nama;
                $data[$key]['harga'] = $val->price;
                $data[$key]['deskripsi'] = $val->deskripsi;
                $data[$key]['alamat'] = $val->alamat;
                 $data[$key]['rating'] = $val->rating;
                  $data[$key]['kategori'] = $val->id_kategori;
                $desa = \common\models\Villages::find()->where(['id' => $val->id_desa])->one();
                if ($desa) {
                    $data[$key]['desa'] = $desa->name;
                }
                $foto = \common\models\FotoExperience::find()->where(['id_experience' => $val->id_experience])->all();
                $onephoto = \common\models\FotoExperience::find()->where(['id_experience' => $val->id_experience])->one();
//                if ($foto) {
//                    $sub = [];
//                    foreach ($foto as $ft) {
//                        $sub_name['foto'] = 'http://veby.mediasumbar.com/uploads/experience/' . $ft['foto'];
//                        array_push($sub, $sub_name);
//                    }
//                    $data[$key]['gallery'] = $sub;
//                }
                if ($onephoto) {

                    $data[$key]['foto'] = 'http://veby.mediasumbar.com/uploads/experience/' . $onephoto->foto;
                }
            }
            $key++;
            return[
                'status' => 'success',
                'data' => $data,
                'lengthData' => $listcount,
            ];
        } else {

            return[
                'status' => 'error',
                'message' => 'Data tidak ditemukan'
            ];
        }
        }
       
    }

    public function actionAllAkomodasi($pulau = NULL, $provinsi = NULL, $kecamatan = NULL , $desa=NULL, $harga=NULL, $kategori=NULL, $limit=NULL) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        if ($pulau){
 $akomodasi = \common\models\Accommodation::find()->where(['id_pulau'=>$pulau])->all();
        $data = [];
        if ($akomodasi){
             $count = \common\models\Accommodation::find()->where(['id_pulau'=>$pulau])->count();
              foreach ($akomodasi as $key => $val) {

            $desa = \common\models\Villages::find()->where(['id' => $val->id_desa])->one();
            $provinsi = \common\models\Provinces::find()->where(['id' => $val->id_provinsi])->one();
            $pulau = \common\models\Island::find()->where(['id' => $provinsi->island_id])->one();
            $foto = \common\models\FotoAccommodation::find()->where(['id_homestay' => $val->id_homestay])->all();
            $data[$key]['id'] = $val->id_homestay;
            $data[$key]['nama'] = $val->nama_homestay;
            $data[$key]['rating'] = $val->rating;
            $data[$key]['harga'] = $val->harga;
            $data[$key]['kategori'] = $val->id_kategori;
            $data[$key]['alamat'] = $val->alamat;
            if ($pulau) {
                $data[$key]['pulau'] = $pulau->name;
            }

               $onephoto = \common\models\FotoAccommodation::find()->where(['id_homestay' => $val->id_homestay])->one();
         
               if ($onephoto) {

                    $data[$key]['foto'] = 'http://veby.mediasumbar.com/uploads/akomodasi/' . $onephoto->foto;
                }
        } $key++;

        return [
            'status' => 'success',
            'data' => $data,
            'lengthData'=>$count,
        ];
        }else{
            return[
                'status'=>'error',
                'message'=>'Data tidak ditemukan'
            ];
        } 
        }else if ($provinsi){
 $akomodasi = \common\models\Accommodation::find()->where(['id_provinsi'=>$provinsi])->all();
        $data = [];
        if ($akomodasi){
             $count = \common\models\Accommodation::find()->where(['id_provinsi'=>$provinsi])->count();
              foreach ($akomodasi as $key => $val) {

            $desa = \common\models\Villages::find()->where(['id' => $val->id_desa])->one();
            $provinsi = \common\models\Provinces::find()->where(['id' => $val->id_provinsi])->one();
            $pulau = \common\models\Island::find()->where(['id' => $provinsi->island_id])->one();
            $foto = \common\models\FotoAccommodation::find()->where(['id_homestay' => $val->id_homestay])->all();
            $data[$key]['id'] = $val->id_homestay;
            $data[$key]['nama'] = $val->nama_homestay;
            $data[$key]['rating'] = $val->rating;
            $data[$key]['harga'] = $val->harga;
            $data[$key]['kategori'] = $val->id_kategori;
            $data[$key]['alamat'] = $val->alamat;
            if ($pulau) {
                $data[$key]['pulau'] = $pulau->name;
            }


               $onephoto = \common\models\FotoAccommodation::find()->where(['id_homestay' => $val->id_homestay])->one();
         
               if ($onephoto) {

                    $data[$key]['foto'] = 'http://veby.mediasumbar.com/uploads/akomodasi/' . $onephoto->foto;
                }
        } $key++;

        return [
            'status' => 'success',
            'data' => $data,
            'lengthData'=>$count,
        ];
        }else{
            return[
                'status'=>'error',
                'message'=>'Data tidak ditemukan'
            ];
        }
        }else if ($kecamatan){
 $akomodasi = \common\models\Accommodation::find()->where(['id_kecamatan'=>$kecamatan])->all();
        $data = [];
        if ($akomodasi){
             $count = \common\models\Accommodation::find()->where(['id_kecamatan'=>$kecamatan])->count();
              foreach ($akomodasi as $key => $val) {

            $desa = \common\models\Villages::find()->where(['id' => $val->id_desa])->one();
            $provinsi = \common\models\Provinces::find()->where(['id' => $val->id_provinsi])->one();
            $pulau = \common\models\Island::find()->where(['id' => $provinsi->island_id])->one();
            $foto = \common\models\FotoAccommodation::find()->where(['id_homestay' => $val->id_homestay])->all();
            $data[$key]['id'] = $val->id_homestay;
            $data[$key]['nama'] = $val->nama_homestay;
            $data[$key]['rating'] = $val->rating;
            $data[$key]['harga'] = $val->harga;
            $data[$key]['kategori'] = $val->id_kategori;
            $data[$key]['alamat'] = $val->alamat;
            if ($pulau) {
                $data[$key]['pulau'] = $pulau->name;
            }


               $onephoto = \common\models\FotoAccommodation::find()->where(['id_homestay' => $val->id_homestay])->one();
         
               if ($onephoto) {

                    $data[$key]['foto'] = 'http://veby.mediasumbar.com/uploads/akomodasi/' . $onephoto->foto;
                }
        } $key++;

        return [
            'status' => 'success',
            'data' => $data,
            'lengthData'=>$count,
        ];
        }else{
            return[
                'status'=>'error',
                'message'=>'Data tidak ditemukan'
            ];
        }
        }else if ($desa){
 $akomodasi = \common\models\Accommodation::find()->where(['id_desa'=>$desa])->all();
        $data = [];
        if ($akomodasi){
             $count = \common\models\Accommodation::find()->count();
              foreach ($akomodasi as $key => $val) {

            $desa = \common\models\Villages::find()->where(['id' => $val->id_desa])->one();
            $provinsi = \common\models\Provinces::find()->where(['id' => $val->id_provinsi])->one();
            $pulau = \common\models\Island::find()->where(['id' => $provinsi->island_id])->one();
            $foto = \common\models\FotoAccommodation::find()->where(['id_homestay' => $val->id_homestay])->all();
            $data[$key]['id'] = $val->id_homestay;
            $data[$key]['nama'] = $val->nama_homestay;
            $data[$key]['rating'] = $val->rating;
            $data[$key]['harga'] = $val->harga;
            $data[$key]['kategori'] = $val->id_kategori;
            $data[$key]['alamat'] = $val->alamat;
            if ($pulau) {
                $data[$key]['pulau'] = $pulau->name;
            }


               $onephoto = \common\models\FotoAccommodation::find()->where(['id_homestay' => $val->id_homestay])->one();
         
               if ($onephoto) {

                    $data[$key]['foto'] = 'http://veby.mediasumbar.com/uploads/akomodasi/' . $onephoto->foto;
                }
        } $key++;

        return [
            'status' => 'success',
            'data' => $data,
            'lengthData'=>$count,
        ];
        }else{
            return[
                'status'=>'error',
                'message'=>'Data tidak ditemukan'
            ];
        }
        }else if ($kategori){
 $akomodasi = \common\models\Accommodation::find()->where(['id_kategori'=>$kategori])->all();
        $data = [];
        if ($akomodasi){
             $count = \common\models\Accommodation::find()->where(['id_kategori'=>$kategori])->count();
              foreach ($akomodasi as $key => $val) {

            $desa = \common\models\Villages::find()->where(['id' => $val->id_desa])->one();
            $provinsi = \common\models\Provinces::find()->where(['id' => $val->id_provinsi])->one();
            $pulau = \common\models\Island::find()->where(['id' => $provinsi->island_id])->one();
            $foto = \common\models\FotoAccommodation::find()->where(['id_homestay' => $val->id_homestay])->all();
            $data[$key]['id'] = $val->id_homestay;
            $data[$key]['nama'] = $val->nama_homestay;
            $data[$key]['rating'] = $val->rating;
            $data[$key]['harga'] = $val->harga;
            $data[$key]['kategori'] = $val->id_kategori;
            $data[$key]['alamat'] = $val->alamat;
            if ($pulau) {
                $data[$key]['pulau'] = $pulau->name;
            }



//            if ($foto) {
//                $sub = [];
//                foreach ($foto as $ft) {
//                    $sub_name['foto'] = 'http://veby.mediasumbar.com/uploads/akomodasi/' . $ft['foto'];
//                    array_push($sub, $sub_name);
//                }
//                   $data[$key]['foto'] = $sub;
//            }
               $onephoto = \common\models\FotoAccommodation::find()->where(['id_homestay' => $val->id_homestay])->one();
         
               if ($onephoto) {

                    $data[$key]['foto'] = 'http://veby.mediasumbar.com/uploads/akomodasi/' . $onephoto->foto;
                }
        } $key++;

        return [
            'status' => 'success',
            'data' => $data,
            'lengthData'=>$count,
        ];
        }else{
            return[
                'status'=>'error',
                'message'=>'Data tidak ditemukan'
            ];
        }
        }else if ($limit){
 $akomodasi = \common\models\Accommodation::find()->limit($limit)->all();
        $data = [];
        if ($akomodasi){
             $count = \common\models\Accommodation::find()->count();
             if ($count>$limit){
                $total= $limit;
            }else {
                $total = $count;
            }
              foreach ($akomodasi as $key => $val) {

            $desa = \common\models\Villages::find()->where(['id' => $val->id_desa])->one();
            $provinsi = \common\models\Provinces::find()->where(['id' => $val->id_provinsi])->one();
            $pulau = \common\models\Island::find()->where(['id' => $provinsi->island_id])->one();
            $foto = \common\models\FotoAccommodation::find()->where(['id_homestay' => $val->id_homestay])->all();
            $data[$key]['id'] = $val->id_homestay;
            $data[$key]['nama'] = $val->nama_homestay;
            $data[$key]['rating'] = $val->rating;
            $data[$key]['harga'] = $val->harga;
            $data[$key]['kategori'] = $val->id_kategori;
            $data[$key]['alamat'] = $val->alamat;
            if ($pulau) {
                $data[$key]['pulau'] = $pulau->name;
            }



//            if ($foto) {
//                $sub = [];
//                foreach ($foto as $ft) {
//                    $sub_name['foto'] = 'http://veby.mediasumbar.com/uploads/akomodasi/' . $ft['foto'];
//                    array_push($sub, $sub_name);
//                }
//                   $data[$key]['foto'] = $sub;
//            }
               $onephoto = \common\models\FotoAccommodation::find()->where(['id_homestay' => $val->id_homestay])->one();
         
               if ($onephoto) {

                    $data[$key]['foto'] = 'http://veby.mediasumbar.com/uploads/akomodasi/' . $onephoto->foto;
                }
        } $key++;

        return [
            'status' => 'success',
            'data' => $data,
            'lengthData'=>$count,
        ];
        }else{
            return[
                'status'=>'error',
                'message'=>'Data tidak ditemukan'
            ];
        }
        }else if ($harga){
 $akomodasi = \common\models\Accommodation::find()->orderBy('harga '.$harga)->all();
        $data = [];
        if ($akomodasi){
             $count = \common\models\Accommodation::find()->orderBy('harga '.$harga)->count();
              foreach ($akomodasi as $key => $val) {

            $desa = \common\models\Villages::find()->where(['id' => $val->id_desa])->one();
            $provinsi = \common\models\Provinces::find()->where(['id' => $val->id_provinsi])->one();
            $pulau = \common\models\Island::find()->where(['id' => $provinsi->island_id])->one();
            $foto = \common\models\FotoAccommodation::find()->where(['id_homestay' => $val->id_homestay])->all();
            $data[$key]['id'] = $val->id_homestay;
            $data[$key]['nama'] = $val->nama_homestay;
            $data[$key]['rating'] = $val->rating;
            $data[$key]['harga'] = $val->harga;
            $data[$key]['kategori'] = $val->id_kategori;
            $data[$key]['alamat'] = $val->alamat;
            if ($pulau) {
                $data[$key]['pulau'] = $pulau->name;
            }



//            if ($foto) {
//                $sub = [];
//                foreach ($foto as $ft) {
//                    $sub_name['foto'] = 'http://veby.mediasumbar.com/uploads/akomodasi/' . $ft['foto'];
//                    array_push($sub, $sub_name);
//                }
//                   $data[$key]['foto'] = $sub;
//            }
               $onephoto = \common\models\FotoAccommodation::find()->where(['id_homestay' => $val->id_homestay])->one();
         
               if ($onephoto) {

                    $data[$key]['foto'] = 'http://veby.mediasumbar.com/uploads/akomodasi/' . $onephoto->foto;
                }
        } $key++;

        return [
            'status' => 'success',
            'data' => $data,
            'lengthData'=>$count,
        ];
        }else{
            return[
                'status'=>'error',
                'message'=>'Data tidak ditemukan'
            ];
        }
        }else {
             $akomodasi = \common\models\Accommodation::find()->all();
        $data = [];
        if ($akomodasi){
             $count = \common\models\Accommodation::find()->count();
              foreach ($akomodasi as $key => $val) {

            $desa = \common\models\Villages::find()->where(['id' => $val->id_desa])->one();
            $provinsi = \common\models\Provinces::find()->where(['id' => $val->id_provinsi])->one();
            $pulau = \common\models\Island::find()->where(['id' => $provinsi->island_id])->one();
            $foto = \common\models\FotoAccommodation::find()->where(['id_homestay' => $val->id_homestay])->all();
            $data[$key]['id'] = $val->id_homestay;
            $data[$key]['nama'] = $val->nama_homestay;
            $data[$key]['rating'] = $val->rating;
            $data[$key]['harga'] = $val->harga;
            $data[$key]['kategori'] = $val->id_kategori;
            $data[$key]['alamat'] = $val->alamat;
            if ($pulau) {
                $data[$key]['pulau'] = $pulau->name;
            }



//            if ($foto) {
//                $sub = [];
//                foreach ($foto as $ft) {
//                    $sub_name['foto'] = 'http://veby.mediasumbar.com/uploads/akomodasi/' . $ft['foto'];
//                    array_push($sub, $sub_name);
//                }
//                   $data[$key]['foto'] = $sub;
//            }
               $onephoto = \common\models\FotoAccommodation::find()->where(['id_homestay' => $val->id_homestay])->one();
         
               if ($onephoto) {

                    $data[$key]['foto'] = 'http://veby.mediasumbar.com/uploads/akomodasi/' . $onephoto->foto;
                }
        } $key++;

        return [
            'status' => 'success',
            'data' => $data,
            'lengthData'=>$count,
        ];
        }else{
            return[
                'status'=>'error',
                'message'=>'Data tidak ditemukan'
            ];
        }
        }
       
      
    }

    public function actionAllTransportasi($pulau = NULL, $provinsi = NULL, $kecamatan = NULL , $desa=NULL, $harga=NULL, $kategori=NULL, $limit=NULL) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        if ($pulau){
                        $transportdata = \common\models\Transportation::find()->where(['id_pulau'=>$pulau])->all();

        $data = [];
        if ($transportdata){
               $count = \common\models\Transportation::find()->where(['id_pulau'=>$pulau])->count();

                foreach ($transportdata as $key => $val) {
            $desa = \common\models\Villages::find()->where(['id' => $val->id_desa])->one();
            $provinsi = \common\models\Provinces::find()->where(['id' => $val->id_provinsi])->one();
            $pulau = \common\models\Island::find()->where(['id' => $provinsi->island_id])->one();

            $data[$key]['id'] = $val->id_transportation;
            $data[$key]['jenis_transportasi'] = $val->jenis_transportasi;
            $data[$key]['nama'] = $val->nama;
            $data[$key]['rute'] = $val->rute;
            $data[$key]['deskripsi'] = $val->deskripsi;
             $data[$key]['rating'] = $val->rating;
            $data[$key]['desa'] = $desa->name;
            $data[$key]['provinsi'] = $provinsi->name;
            if ($pulau) {
                $data[$key]['pulau'] = $pulau->name;
            }
            $data[$key]['harga'] = $val->price;
            $onephoto = \common\models\FotoTransportasi::find()->where(['id_transportasi' => $val->id_transportation])->one();
         
               if ($onephoto) {

                    $data[$key]['foto'] = 'http://veby.mediasumbar.com/uploads/transport/' . $onephoto->foto;
                }
        }$key++;

        return [
            'status' => 'success',
            'data' => $data,
            'lengthData'=>$count,
        ];
        }else{
            return[
                'status'=>'error',
                'message'=>'Data tidak ditemukan'
            ];
        }
        }else if ($provinsi){
                        $transportdata = \common\models\Transportation::find()->where(['id_provinsi'=>$provinsi])->all();

        $data = [];
        if ($transportdata){
               $count = \common\models\Transportation::find()->where(['id_provinsi'=>$provinsi])->count();

                foreach ($transportdata as $key => $val) {
            $desa = \common\models\Villages::find()->where(['id' => $val->id_desa])->one();
            $provinsi = \common\models\Provinces::find()->where(['id' => $val->id_provinsi])->one();
            $pulau = \common\models\Island::find()->where(['id' => $provinsi->island_id])->one();

            $data[$key]['id'] = $val->id_transportation;
            $data[$key]['jenis_transportasi'] = $val->jenis_transportasi;
            $data[$key]['nama'] = $val->nama;
            $data[$key]['rute'] = $val->rute;
            $data[$key]['deskripsi'] = $val->deskripsi;
             $data[$key]['rating'] = $val->rating;
            $data[$key]['desa'] = $desa->name;
            $data[$key]['provinsi'] = $provinsi->name;
            if ($pulau) {
                $data[$key]['pulau'] = $pulau->name;
            }
            $data[$key]['harga'] = $val->price;
            $onephoto = \common\models\FotoTransportasi::find()->where(['id_transportasi' => $val->id_transportation])->one();
         
               if ($onephoto) {

                    $data[$key]['foto'] = 'http://veby.mediasumbar.com/uploads/transport/' . $onephoto->foto;
                }
        }$key++;

        return [
            'status' => 'success',
            'data' => $data,
            'lengthData'=>$count,
        ];
        }else{
            return[
                'status'=>'error',
                'message'=>'Data tidak ditemukan'
            ];
        }
        }else if ($kecamatan){
                        $transportdata = \common\models\Transportation::find()->where(['id_kecamatan'=>$kecamatan])->all();

        $data = [];
        if ($transportdata){
               $count = \common\models\Transportation::find()->where(['id_kecamatan'=>$kecamatan])->count();

                foreach ($transportdata as $key => $val) {
            $desa = \common\models\Villages::find()->where(['id' => $val->id_desa])->one();
            $provinsi = \common\models\Provinces::find()->where(['id' => $val->id_provinsi])->one();
            $pulau = \common\models\Island::find()->where(['id' => $provinsi->island_id])->one();

            $data[$key]['id'] = $val->id_transportation;
            $data[$key]['jenis_transportasi'] = $val->jenis_transportasi;
            $data[$key]['nama'] = $val->nama;
            $data[$key]['rute'] = $val->rute;
            $data[$key]['deskripsi'] = $val->deskripsi;
             $data[$key]['rating'] = $val->rating;
            $data[$key]['desa'] = $desa->name;
            $data[$key]['provinsi'] = $provinsi->name;
            if ($pulau) {
                $data[$key]['pulau'] = $pulau->name;
            }
            $data[$key]['harga'] = $val->price;
            $onephoto = \common\models\FotoTransportasi::find()->where(['id_transportasi' => $val->id_transportation])->one();
         
               if ($onephoto) {

                    $data[$key]['foto'] = 'http://veby.mediasumbar.com/uploads/transport/' . $onephoto->foto;
                }
        }$key++;

        return [
            'status' => 'success',
            'data' => $data,
            'lengthData'=>$count,
        ];
        }else{
            return[
                'status'=>'error',
                'message'=>'Data tidak ditemukan'
            ];
        }
        }else if ($desa){
                        $transportdata = \common\models\Transportation::find()->where(['id_desa'=>$desa])->all();

        $data = [];
        if ($transportdata){
               $count = \common\models\Transportation::find()->where(['id_desa'=>$desa])->count();

                foreach ($transportdata as $key => $val) {
            $desa = \common\models\Villages::find()->where(['id' => $val->id_desa])->one();
            $provinsi = \common\models\Provinces::find()->where(['id' => $val->id_provinsi])->one();
            $pulau = \common\models\Island::find()->where(['id' => $provinsi->island_id])->one();

            $data[$key]['id'] = $val->id_transportation;
            $data[$key]['jenis_transportasi'] = $val->jenis_transportasi;
            $data[$key]['nama'] = $val->nama;
            $data[$key]['rute'] = $val->rute;
            $data[$key]['deskripsi'] = $val->deskripsi;
             $data[$key]['rating'] = $val->rating;
            $data[$key]['desa'] = $desa->name;
            $data[$key]['provinsi'] = $provinsi->name;
            if ($pulau) {
                $data[$key]['pulau'] = $pulau->name;
            }
            $data[$key]['harga'] = $val->price;
            $onephoto = \common\models\FotoTransportasi::find()->where(['id_transportasi' => $val->id_transportation])->one();
         
               if ($onephoto) {

                    $data[$key]['foto'] = 'http://veby.mediasumbar.com/uploads/transport/' . $onephoto->foto;
                }
        }$key++;

        return [
            'status' => 'success',
            'data' => $data,
            'lengthData'=>$count,
        ];
        }else{
            return[
                'status'=>'error',
                'message'=>'Data tidak ditemukan'
            ];
        }
        }else if ($harga){
                        $transportdata = \common\models\Transportation::find()->orderBy('price '.$harga)->all();

        $data = [];
        if ($transportdata){
               $count = \common\models\Transportation::find()->orderBy('price '.$harga)->count();

                foreach ($transportdata as $key => $val) {
            $desa = \common\models\Villages::find()->where(['id' => $val->id_desa])->one();
            $provinsi = \common\models\Provinces::find()->where(['id' => $val->id_provinsi])->one();
            $pulau = \common\models\Island::find()->where(['id' => $provinsi->island_id])->one();

            $data[$key]['id'] = $val->id_transportation;
            $data[$key]['jenis_transportasi'] = $val->jenis_transportasi;
            $data[$key]['nama'] = $val->nama;
            $data[$key]['rute'] = $val->rute;
            $data[$key]['deskripsi'] = $val->deskripsi;
             $data[$key]['rating'] = $val->rating;
            $data[$key]['desa'] = $desa->name;
            $data[$key]['provinsi'] = $provinsi->name;
            if ($pulau) {
                $data[$key]['pulau'] = $pulau->name;
            }
            $data[$key]['harga'] = $val->price;
            $onephoto = \common\models\FotoTransportasi::find()->where(['id_transportasi' => $val->id_transportation])->one();
         
               if ($onephoto) {

                    $data[$key]['foto'] = 'http://veby.mediasumbar.com/uploads/transport/' . $onephoto->foto;
                }
        }$key++;

        return [
            'status' => 'success',
            'data' => $data,
            'lengthData'=>$count,
        ];
        }else{
            return[
                'status'=>'error',
                'message'=>'Data tidak ditemukan'
            ];
        }
        }else if ($kategori){
                        $transportdata = \common\models\Transportation::find()->where(['jenis_transportasi'=>$kategori])->all();

        $data = [];
        if ($transportdata){
               $count = \common\models\Transportation::find()->where(['jenis_transportasi'=>$kategori])->count();

                foreach ($transportdata as $key => $val) {
            $desa = \common\models\Villages::find()->where(['id' => $val->id_desa])->one();
            $provinsi = \common\models\Provinces::find()->where(['id' => $val->id_provinsi])->one();
            $pulau = \common\models\Island::find()->where(['id' => $provinsi->island_id])->one();

            $data[$key]['id'] = $val->id_transportation;
            $data[$key]['jenis_transportasi'] = $val->jenis_transportasi;
            $data[$key]['nama'] = $val->nama;
            $data[$key]['rute'] = $val->rute;
            $data[$key]['deskripsi'] = $val->deskripsi;
             $data[$key]['rating'] = $val->rating;
            $data[$key]['desa'] = $desa->name;
            $data[$key]['provinsi'] = $provinsi->name;
            if ($pulau) {
                $data[$key]['pulau'] = $pulau->name;
            }
            $data[$key]['harga'] = $val->price;
            $onephoto = \common\models\FotoTransportasi::find()->where(['id_transportasi' => $val->id_transportation])->one();
         
               if ($onephoto) {

                    $data[$key]['foto'] = 'http://veby.mediasumbar.com/uploads/transport/' . $onephoto->foto;
                }
        }$key++;

        return [
            'status' => 'success',
            'data' => $data,
            'lengthData'=>$count,
        ];
        }else{
            return[
                'status'=>'error',
                'message'=>'Data tidak ditemukan'
            ];
        }
        }else if ($limit){
                        $transportdata = \common\models\Transportation::find()->limit($limit)->all();

        $data = [];
        if ($transportdata){
               $count = \common\models\Transportation::find()->count();
                if ($count>$limit){
                $total= $limit;
            }else {
                $total = $count;
            }
                foreach ($transportdata as $key => $val) {
            $desa = \common\models\Villages::find()->where(['id' => $val->id_desa])->one();
            $provinsi = \common\models\Provinces::find()->where(['id' => $val->id_provinsi])->one();
            $pulau = \common\models\Island::find()->where(['id' => $provinsi->island_id])->one();

            $data[$key]['id'] = $val->id_transportation;
            $data[$key]['jenis_transportasi'] = $val->jenis_transportasi;
            $data[$key]['nama'] = $val->nama;
            $data[$key]['rute'] = $val->rute;
            $data[$key]['deskripsi'] = $val->deskripsi;
             $data[$key]['rating'] = $val->rating;
            $data[$key]['desa'] = $desa->name;
            $data[$key]['provinsi'] = $provinsi->name;
            if ($pulau) {
                $data[$key]['pulau'] = $pulau->name;
            }
            $data[$key]['harga'] = $val->price;
            $onephoto = \common\models\FotoTransportasi::find()->where(['id_transportasi' => $val->id_transportation])->one();
         
               if ($onephoto) {

                    $data[$key]['foto'] = 'http://veby.mediasumbar.com/uploads/transport/' . $onephoto->foto;
                }
        }$key++;

        return [
            'status' => 'success',
            'data' => $data,
            'lengthData'=>$total,
        ];
        }else{
            return[
                'status'=>'error',
                'message'=>'Data tidak ditemukan'
            ];
        }
        }else {
                $transportdata = \common\models\Transportation::find()->all();

        $data = [];
        if ($transportdata){
               $count = \common\models\Transportation::find()->count();

                foreach ($transportdata as $key => $val) {
            $desa = \common\models\Villages::find()->where(['id' => $val->id_desa])->one();
            $provinsi = \common\models\Provinces::find()->where(['id' => $val->id_provinsi])->one();
            $pulau = \common\models\Island::find()->where(['id' => $provinsi->island_id])->one();

            $data[$key]['id'] = $val->id_transportation;
            $data[$key]['jenis_transportasi'] = $val->jenis_transportasi;
            $data[$key]['nama'] = $val->nama;
            $data[$key]['rute'] = $val->rute;
            $data[$key]['deskripsi'] = $val->deskripsi;
             $data[$key]['rating'] = $val->rating;
            $data[$key]['desa'] = $desa->name;
            $data[$key]['provinsi'] = $provinsi->name;
            if ($pulau) {
                $data[$key]['pulau'] = $pulau->name;
            }
            $data[$key]['harga'] = $val->price;
            $onephoto = \common\models\FotoTransportasi::find()->where(['id_transportasi' => $val->id_transportation])->one();
         
               if ($onephoto) {

                    $data[$key]['foto'] = 'http://veby.mediasumbar.com/uploads/transport/' . $onephoto->foto;
                }
        }$key++;

        return [
            'status' => 'success',
            'data' => $data,
            'lengthData'=>$count,
        ];
        }else{
            return[
                'status'=>'error',
                'message'=>'Data tidak ditemukan'
            ];
        }            
        }

    
    }

    public function actionAllMerchant() {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $merchant = \common\models\Merchandise::find()->all();

        $data = [];
        if ($merchant){
              $count = \common\models\Merchandise::find()->count();
              foreach ($merchant as $key => $val) {
            $foto = \common\models\FotoMerchandise::find()->where(['id_merchandise' => $val->id_merchandise])->orderBy('id_foto DESC')->one();
            $desa = \common\models\Villages::find()->where(['id' => $val->id_desa])->one();
            $provinsi = \common\models\Provinces::find()->where(['id' => $val->id_provinsi])->one();

            $data[$key]['id'] = $val->id_merchandise;
            $data[$key]['kategori'] = $val->kategori;
            $data[$key]['nama'] = $val->nama;
            $data[$key]['alamat'] = $val->alamat;
            $data[$key]['deskripsi'] = $val->deskripsi;
             $data[$key]['rating'] = $val->rating;
            $data[$key]['desa'] = $desa->name;
            if ($provinsi) {
                $data[$key]['provinsi'] = $provinsi->name;
                $pulau = \common\models\Island::find()->where(['id' => $provinsi->island_id])->one();
                if ($pulau) {

                    $data[$key]['pulau'] = $pulau->name;
                }
            }

            $data[$key]['harga'] = $val->harga;
//            if ($foto) {
//                $sub = [];
//                foreach ($foto as $ft) {
//                    $sub_name['foto'] = 'http://veby.mediasumbar.com/uploads/merchant/' . $ft['foto'];
//                    array_push($sub, $sub_name);
//                } $data[$key]['foto'] = $sub;
//            }
           
             $onephoto = \common\models\FotoMerchandise::find()->where(['id_merchandise' => $val->id_merchandise])->one();
         
               if ($onephoto) {

                    $data[$key]['foto'] = 'http://veby.mediasumbar.com/uploads/merchant/' . $onephoto->foto;
                }
        } $key++;

        return [
            'status' => 'success',
            'data' => $data,
            'lengthData'=>$count,
        ];
        }else{
            return [
            'status' => 'error',
            'message'=>'Data tidak ditemukan'
        ]; 
        }
      
    }

        public function actionAllDesatraveling() {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $desatraveling = \common\models\DesaTraveling::find()->all();

        $data = [];
        if ($desatraveling){
             $count = \common\models\DesaTraveling::find()->count();
            foreach ($desatraveling as $key => $val) {
            $foto = \common\models\FotoDesatraveling::find()->where(['id_desa' => $val->id_desa])->orderBy('id_foto DESC')->one();
            $desa = \common\models\Villages::find()->where(['id' => $val->id_desa])->one();
            $provinsi = \common\models\Provinces::find()->where(['id' => $val->id_provinsi])->one();

            $data[$key]['id'] = $val->id;
         
            $data[$key]['nama'] = $val->nama;
            $data[$key]['alamat'] = $val->alamat;
            $data[$key]['deskripsi'] = $val->deskripsi;
             $data[$key]['rating'] = $val->rating;
            $data[$key]['desa'] = $desa->name;
            if ($provinsi) {
                $data[$key]['provinsi'] = $provinsi->name;
                $pulau = \common\models\Island::find()->where(['id' => $provinsi->island_id])->one();
                if ($pulau) {

                    $data[$key]['pulau'] = $pulau->name;
                }
            }

//            if ($foto) {
//                $sub = [];
//                foreach ($foto as $ft) {
//                    $sub_name['foto'] = 'http://veby.mediasumbar.com/uploads/merchant/' . $ft['foto'];
//                    array_push($sub, $sub_name);
//                } $data[$key]['foto'] = $sub;
//            }
           
             $onephoto = \common\models\FotoDesatraveling::find()->where(['id_desa' => $val->id])->one();
         
               if ($onephoto) {

                    $data[$key]['foto'] = 'http://veby.mediasumbar.com/uploads/desa/' . $onephoto->foto;
                }
        } $key++;

        return [
            'status' => 'success',
            'data' => $data,
            'lengthData'=>$count,
        ]; 
        }else{
            return[
                'status'=>'error',
                'message'=>'Data tidak ditemukan',
            ];
        }
       
    }
    public function actionListPulau() {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $provinsi = \common\models\Island::find()->all();

        $data = [];

        if ($provinsi) {
            foreach ($provinsi as $key => $val) {
                $data[$key]['id'] = $val->id;
                $data[$key]['name'] = $val->name;
            }
            $key++;
            return[
                'status' => 'success',
                'data' => $data,
            ];
        } else {
            return[
                'status' => 'error',
                'message' => 'Data tidak ditemukan',
            ];
        }
    }

    public function actionListProvinsi() {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $provinsi = \common\models\Provinces::find()->all();

        $data = [];

        if ($provinsi) {
            foreach ($provinsi as $key => $val) {
                $data[$key]['id'] = $val->id;
                $data[$key]['name'] = $val->name;
                $data[$key]['id_island'] = $val->island_id;
            }
            $key++;
            return[
                'status' => 'success',
                'data' => $data,
            ];
        } else {
            return[
                'status' => 'error',
                'message' => 'Data tidak ditemukan',
            ];
        }
    }

    public function actionListKecamatan() {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $kecamatan = \common\models\Districts::find()->all();

        $data = [];

        if ($kecamatan) {
            foreach ($kecamatan as $key => $val) {
                $data[$key]['id'] = $val->id;
                $data[$key]['name'] = $val->name;
                $data[$key]['regency_id'] = $val->regency_id;
            }
            $key++;
            return[
                'status' => 'success',
                'data' => $data,
            ];
        } else {
            return[
                'status' => 'error',
                'message' => 'Data tidak ditemukan',
            ];
        }
    }

    public function actionListNamaDesa() {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $desa = \common\models\Villages::find()->all();

        $data = [];

        if ($desa) {
            foreach ($desa as $key => $val) {

                $provinsi = \common\models\Provinces::find()->where(['id' => $val->provinsi])->one();


                $data[$key]['id'] = $val->id;
                $data[$key]['name'] = $val->name;
                if ($provinsi) {
                    $pulau = \common\models\Island::find()->where(['id' => $provinsi->island_id])->one();
                    $data[$key]['pulau'] = $pulau->name;
                }
                $data[$key]['provinsi'] = $val->provinsi;
                $data[$key]['district_id'] = $val->district_id;
            }
            $key++;
            return[
                'status' => 'success',
                'data' => $data,
            ];
        } else {
            return[
                'status' => 'error',
                'message' => 'Data tidak ditemukan',
            ];
        }
    }
       public function actionListNamaDesaTraveling() {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $desa = \common\models\DesaTraveling::find()->all();

        $data = [];

        if ($desa) {
             $count = \common\models\DesaTraveling::find()->count();

            foreach ($desa as $key => $val) {

                $provinsi = \common\models\Provinces::find()->where(['id' => $val->id_provinsi])->one();
                $kecamatan = \common\models\Districts::find()->where(['id'=>$val->id_kecamatan])->one();

                $data[$key]['id'] = $val->id;
                $data[$key]['nama'] = $val->nama;
                if ($provinsi) {
                    $pulau = \common\models\Island::find()->where(['id' => $provinsi->island_id])->one();
                    $data[$key]['idPulau'] = $pulau->name;
                      $data[$key]['idProvinsi'] = $provinsi->name;
                }
                if($kecamatan){
              
                $data[$key]['idKecamatan'] = $kecamatan->name;}
            }
            $key++;
            return[
                'status' => 'success',
                'data' => $data,
                'lengthData'=>$count,
            ];
        } else {
            return[
                'status' => 'error',
                'message' => 'Data tidak ditemukan',
            ];
        }
    }

    public function actionDetailDesa($id) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $desa = \common\models\Villages::find()->where(['id' => $id])->one();
        if ($desa) {
            $data_foto = \common\models\FotoDesa::find()->where(['id_desa' => $desa->id])->all();
            $foto = [];
            if ($data_foto) {
                foreach ($data_foto as $key => $value) {
                    $foto[$key]['foto'] = $value->foto;
                    $key++;
                }

                return[
                    'status' => 'success',
                    'data' => $desa,
                    'foto' => $foto,
                ];
            } else {
                return[
                    'status' => 'success',
                    'data' => $desa,
                ];
            }
        } else {
            return[
                'status' => 'error',
                'message' => 'Data tidak ditemukan',
            ];
        }
    }

    // public function actionDetailExperience($id) {
    //     Yii::$app->response->format = Response::FORMAT_JSON;
    //     $cari = \common\models\Experience::find()->where(['id_experience' => $id])->one();
    //        if ($cari) {
    //         $pulau = \common\models\Island::find()->where(['id'=>$cari->id_pulau])->one();
    //         $provinsi = \common\models\Provinces::find()->where(['id'=>$cari->id_provinsi])->one();
    //         $kecamatan = \common\models\Districts::find()->where(['id'=>$cari->id_kecamatan])->one();
    //         $desa = \common\models\Villages::find()->where(['id'=>$cari->id_desa])->one();
    //         if ($pulau){
    //             $cari->id_pulau= $pulau->name;
    //         }
    //         if ($provinsi){
    //             $cari->id_provinsi = $provinsi->name;
    //         }
    //         if ($kecamatan){
    //              $cari->id_kecamatan= $kecamatan->name;
    //         }
    //         if ($desa){
    //             $cari->id_desa= $desa->name;
    //         }
    //        $foto = \common\models\FotoExperience::find()->where(['id_experience' => $cari->id_experience])->all();
    //                 if ($foto) {
    //                     $sub = [];
    //                     foreach ($foto as $ft) {
    //                         $sub_name['foto'] = 'http://veby.mediasumbar.com/uploads/experience/'.$ft['foto'];
    //                         array_push($sub, $sub_name);
    //                     }
    //                      $cari['gallery']= $sub;
    //                 }
    //         return[
    //             'status' => 'success',
    //             'cari' => $cari,
    //         ];
    //     } else {
    //         return[
    //             'status' => 'error',
    //             'message' => 'Data tidak ditemukan'
    //         ];
    //     }
    // }


    public function actionDetailExperience($id) {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $list = \common\models\Experience::find()->where(['id_experience' => $id])->all();
        if ($list) {

            $data = [];
            foreach ($list as $key => $val) {
                $pulau = \common\models\Island::find()->where(['id' => $val->id_pulau])->one();
                $provinsi = \common\models\Provinces::find()->where(['id' => $val->id_provinsi])->one();
                $kecamatan = \common\models\Districts::find()->where(['id' => $val->id_kecamatan])->one();
                $desa = \common\models\Villages::find()->where(['id' => $val->id_desa])->one();
                $foto = \common\models\FotoExperience::find()->where(['id_experience' => $val->id_experience])->all();
                $onephoto = \common\models\FotoExperience::find()->where(['id_experience' => $val->id_experience])->one();
                if ($onephoto) {

                    $data[$key]['fotoUtama'] = 'http://veby.mediasumbar.com/uploads/experience/' . $onephoto->foto;
                }
                if ($foto) {
                    $sub = [];
                    foreach ($foto as $ft) {
                        $sub_name['foto'] = 'http://veby.mediasumbar.com/uploads/experience/' . $ft['foto'];
                        array_push($sub, $sub_name);
                    }
                    $data[$key]['fotoList'] = $sub;
                }

                $sub1 = [];
                $coordinate = \common\models\Coordinat::find()->where(['id_data'=>$val->id_experience])
                ->where(['kategori'=>'Experience'])->all();
                foreach ($coordinate as $ls) {

                    $sub_name1['latitude'] = $ls['latitdude'];
                    $sub_name1['longitude'] = $ls['longitude'];
                    array_push($sub1, $sub_name1);
                }
                $data[$key]['coordinate'] = $sub1;


                $data[$key]['nama'] = $val->nama;
                $data[$key]['contact'] = $val->contact;
                $data[$key]['alamat'] = $val->alamat;
                $data[$key]['deskripsi'] = $val->deskripsi;

                $data[$key]['rating'] = $val->rating;
                $data[$key]['kategori'] = $val->id_kategori;
                if ($desa) {
                    $data[$key]['desa'] = $desa->name;
                }
                if ($provinsi) {
                    $data[$key]['provinsi'] = $provinsi->name;
                }

                if ($kecamatan) {
                    $data[$key]['kecamatan'] = $kecamatan->name;
                }





                $sub2 = [];
                foreach ($list as $ls2) {
                    $sub_name2['hariBuka'] = $ls2['hari'];
                    $sub_name2['jamBuka'] = $ls2['jam_mulai'];
                    $sub_name2['jamTutup'] = $ls2['jam_berakhir'];
                  
                    $sub_name2['hargaTiket'] = $ls2['price'];
                    $sub_name2['statusTersedia'] = $ls2['status'];
                    array_push($sub2, $sub_name2);
                }
                $data[$key]['operational'] = $sub2;
                $sub3 = [];
                $listaktifitas = \common\models\Aktivitas::find()->where(['id_experience' => $val->id_experience])->all();
                if ($listaktifitas) {
                    foreach ($listaktifitas as $ls3) {
                        $sub_name3['id'] = $ls3['id_aktivitas'];
                        $sub_name3['description'] = $ls3['deskripsi'];

                        array_push($sub3, $sub_name3);
                    }
                    $data[$key]['activityList'] = $sub3;
                }
            }
            $key++;
            return[
                'status' => 'success',
                'data' => $data,
            ];
        } else {

            return[
                'status' => 'error',
                'message' => 'Data tidak ditemukan'
            ];
        }
    }

    public function actionDetailAkomodasi($id) {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $list = \common\models\Accommodation::find()->where(['id_homestay' => $id])->all();
        if ($list) {

            $data = [];
            foreach ($list as $key => $val) {
                $pulau = \common\models\Island::find()->where(['id' => $val->id_pulau])->one();
                $provinsi = \common\models\Provinces::find()->where(['id' => $val->id_provinsi])->one();
                $kecamatan = \common\models\Districts::find()->where(['id' => $val->id_kecamatan])->one();
                $desa = \common\models\Villages::find()->where(['id' => $val->id_desa])->one();
                $foto = \common\models\FotoAccommodation::find()->where(['id_homestay' => $val->id_homestay])->all();
                $onephoto = \common\models\FotoAccommodation::find()->where(['id_homestay' => $val->id_homestay])->one();
                if ($onephoto) {

                    $data[$key]['fotoUtama'] = 'http://veby.mediasumbar.com/uploads/akomodasi/' . $onephoto->foto;
                }
                if ($foto) {
                    $sub = [];
                    foreach ($foto as $ft) {
                        $sub_name['foto'] = 'http://veby.mediasumbar.com/uploads/akomodasi/' . $ft['foto'];
                        array_push($sub, $sub_name);
                    }
                    $data[$key]['fotoList'] = $sub;
                }

                $sub1 = [];
                  $coordinate = \common\models\Coordinat::find()->where(['id_data'=>$val->id_homestay])
                ->where(['kategori'=>'Accommodation'])->all();
                foreach ($coordinate as $ls) {
                    $sub_name1['latitude'] = $ls['latitdude'];
                    $sub_name1['longitude'] = $ls['longitude'];
                    array_push($sub1, $sub_name1);
                }
                $data[$key]['coordinate'] = $sub1;


                $data[$key]['nama'] = $val->nama_homestay;

                $data[$key]['alamat'] = $val->alamat;
                 $data[$key]['contact'] = $val->contact;
                $data[$key]['deskripsi'] = $val->deskripsi;

                $data[$key]['rating'] = $val->rating;
                $data[$key]['kategori'] = $val->id_kategori;
                if ($desa) {
                    $data[$key]['desa'] = $desa->name;
                }
                if ($provinsi) {
                    $data[$key]['provinsi'] = $provinsi->name;
                }

                if ($kecamatan) {
                    $data[$key]['kecamatan'] = $kecamatan->name;
                }



                $sub2 = [];
                foreach ($list as $ls2) {
                    $sub_name2['total_room'] = $ls2['total_room'];
                    $sub_name2['checkin'] = $ls2['checkin'];
                    $sub_name2['checkout'] = $ls2['checkout'];
                    $sub_name2['statusTersedia'] = $ls2['status'];
                    $sub_name2['hargaTiket'] = $ls2['harga'];
            
                    array_push($sub2, $sub_name2);
                }
                $data[$key]['operational'] = $sub2;
                $sub3 = [];
                $listaktifitas = \common\models\Fasilitas::find()->where(['id_akomodasi' => $val->id_homestay])->all();
                if ($listaktifitas) {
                    foreach ($listaktifitas as $ls3) {
                        $sub_name3['id'] = $ls3['id_fasilitas'];
                       $sub_name3['wifi'] = $ls3['wifi'];
                          $sub_name3['restauran'] = $ls3['wifi'];
                            $sub_name3['hot_water'] = $ls3['wifi'];
                              $sub_name3['free_park'] = $ls3['wifi'];
                        array_push($sub3, $sub_name3);
                    }
                    $data[$key]['facility'] = $sub3;
                }
            }
            $key++;
            return[
                'status' => 'success',
                'data' => $data,
            ];
        } else {

            return[
                'status' => 'error',
                'message' => 'Data tidak ditemukan'
            ];
        }
    }

     public function actionDetailTransport($id) {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $list = \common\models\Transportation::find()->where(['id_transportation' => $id])->all();
        if ($list) {

            $data = [];
            foreach ($list as $key => $val) {
                $pulau = \common\models\Island::find()->where(['id' => $val->id_pulau])->one();
                $provinsi = \common\models\Provinces::find()->where(['id' => $val->id_provinsi])->one();
                $kecamatan = \common\models\Districts::find()->where(['id' => $val->id_kecamatan])->one();
                $desa = \common\models\Villages::find()->where(['id' => $val->id_desa])->one();
                $foto = \common\models\FotoTransportasi::find()->where(['id_transportasi' => $val->id_transportation])->all();
                $onephoto = \common\models\FotoTransportasi::find()->where(['id_transportasi' => $val->id_transportation])->one();
                if ($onephoto) {

                    $data[$key]['fotoUtama'] = 'http://veby.mediasumbar.com/uploads/transport/' . $onephoto->foto;
                }
                if ($foto) {
                    $sub = [];
                    foreach ($foto as $ft) {
                        $sub_name['foto'] = 'http://veby.mediasumbar.com/uploads/transport/' . $ft['foto'];
                        array_push($sub, $sub_name);
                    }
                    $data[$key]['fotoList'] = $sub;
                }

              
                

                $data[$key]['nama'] = $val->nama;
                 $data[$key]['contact'] = $val->contact;
                 $data[$key]['hari'] = $val->hari;
                $data[$key]['alamatMerchant'] = $val->alamat;
                $data[$key]['deskripsi'] = $val->deskripsi;

                $data[$key]['rating'] = $val->rating;
                $data[$key]['kategoriKendaraan'] = $val->jenis_transportasi;
                  $data[$key]['tipeKendaraan'] = $val->tipe_kendaraan;
                if ($desa) {
                    $data[$key]['desa'] = $desa->name;
                }
                if ($provinsi) {
                    $data[$key]['provinsi'] = $provinsi->name;
                }

                if ($kecamatan) {
                    $data[$key]['kecamatan'] = $kecamatan->name;
                }
                 $coordinate = \common\models\Coordinat::find()->where(['id_data'=>$val->id_transportation])
                ->where(['kategori'=>'Transportation'])->all();
                  $sub1 = [];
                foreach ($coordinate as $ls) {
                    $sub_name1['latitude'] = $ls['latitdude'];
                    $sub_name1['longitude'] = $ls['longitude'];
                    array_push($sub1, $sub_name1);
                }
                $data[$key]['coordinate'] = $sub1;
                $rute = \common\models\Rute::find()->where(['id_transport'=>$val->id_transportation])->all();
              
                  if ($rute) {
                    $sub3 = [];
                    foreach ($rute as $rt) {
                        $sub_name3['daerah_mulai'] = $rt['daerah_mulai'];
                         $sub_name3['daerah_akhir'] = $rt['daerah_akhir'];
                          $sub_name3['harga'] = $rt['harga'];
                          
                        array_push($sub3, $sub_name3);
                    }
                    $data[$key]['rute'] = $sub3;
                }
                $fasilitas = \common\models\FasilitasTransport::find()->where(['id_transportasi'=>$val->id_transportation])->all();
                  if ($fasilitas) {
                    $sub2 = [];
                    foreach ($fasilitas as $fs) {
                      
                       $sub_name2['wifi'] = $fs['wifi'];
                          $sub_name2['restauran'] = $fs['wifi'];
                            $sub_name2['hot_water'] = $fs['wifi'];
                              $sub_name2['free_park'] = $fs['wifi'];
                            
                        array_push($sub2, $sub_name2);
                    }
                    $data[$key]['fasilitas'] = $sub2;
                }


                
            }
            $key++;
            return[
                'status' => 'success',
                'data' => $data,
            ];
        } else {

            return[
                'status' => 'error',
                'message' => 'Data tidak ditemukan'
            ];
        }
    }


     public function actionDetailMerchant($id) {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $list = \common\models\Merchandise::find()->where(['id_merchandise' => $id])->all();
        if ($list) {

            $data = [];
            foreach ($list as $key => $val) {
                $pulau = \common\models\Island::find()->where(['id' => $val->id_pulau])->one();
                $provinsi = \common\models\Provinces::find()->where(['id' => $val->id_provinsi])->one();
                $kecamatan = \common\models\Districts::find()->where(['id' => $val->id_kecamatan])->one();
                $desa = \common\models\Villages::find()->where(['id' => $val->id_desa])->one();
                $foto = \common\models\FotoMerchandise::find()->where(['id_merchandise' => $val->id_merchandise])->all();
                $onephoto = \common\models\FotoMerchandise::find()->where(['id_merchandise' => $val->id_merchandise])->one();
                if ($onephoto) {

                    $data[$key]['fotoUtama'] = 'http://veby.mediasumbar.com/uploads/merchant/' . $onephoto->foto;
                }
             

                $sub1 = [];
                 $coordinate = \common\models\Coordinat::find()->where(['id_data'=>$val->id_merchandise])
                ->where(['kategori'=>'Merchandise'])->all();
                foreach ($coordinate as $ls) {
                    $sub_name1['latitude'] = $ls['latitdude'];
                    $sub_name1['longitude'] = $ls['longitude'];
                    array_push($sub1, $sub_name1);
                }
                $data[$key]['coordinate'] = $sub1;


                $data[$key]['nama'] = $val->nama;
                $data[$key]['harga'] = $val->harga;
                $data[$key]['alamatMerchant'] = $val->alamat;
                $data[$key]['deskripsi'] = $val->deskripsi;

           //     $data[$key]['rating'] = $val->rating;
                $data[$key]['kategoriMerchant'] = $val->kategori;
                if ($desa) {
                    $data[$key]['desa'] = $desa->name;
                }
                if ($provinsi) {
                    $data[$key]['provinsi'] = $provinsi->name;
                }

                if ($kecamatan) {
                    $data[$key]['kecamatan'] = $kecamatan->name;
                }


            }
            $key++;
            return[
                'status' => 'success',
                'data' => $data,
            ];
        } else {

            return[
                'status' => 'error',
                'message' => 'Data tidak ditemukan'
            ];
        }
    }


    public function actionKategoriAkomodasi() {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $kategori = \common\models\KategoriAccommodation::find()->all();

        $data = [];
        if ($kategori) {
            foreach ($kategori as $key => $val) {
                $data[$key]['id'] = $val->id_kategori;
                $data[$key]['nama'] = $val->nama_kategori;
            }
            $key++;
            return[
                'status' => 'success',
                'data' => $data,
            ];
        } else {
            return[
                'status' => 'error',
                'message' => 'Data tidak ditemukan'
            ];
        }
    }

    public function actionKategoriTransportasi() {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $kategori = \common\models\KategoriTransportasi::find()->all();

        $data = [];
        if ($kategori) {
            foreach ($kategori as $key => $val) {
                $data[$key]['id'] = $val->id_kategori;
                $data[$key]['nama'] = $val->nama;
            }
            $key++;
            return[
                'status' => 'success',
                'data' => $data,
            ];
        } else {
            return[
                'status' => 'error',
                'message' => 'Data tidak ditemukan'
            ];
        }
    }

    public function actionKategoriMerchant() {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $kategori = \common\models\KategoriMerchandise::find()->all();

        $data = [];
        if ($kategori) {
            foreach ($kategori as $key => $val) {
                $data[$key]['id'] = $val->id_kategori;
                $data[$key]['nama'] = $val->nama;
            }
            $key++;
            return[
                'status' => 'success',
                'data' => $data,
            ];
        } else {
            return[
                'status' => 'error',
                'message' => 'Data tidak ditemukan'
            ];
        }
    }

    public function actionLogin() {
        Yii::$app->response->format = Response::FORMAT_JSON;

        if (!Yii::$app->user->isGuest) {
            return [
                'status' => 'error',
                'message' => 'Sudah login',
            ];
        }

        $model = new \common\models\LoginApi();
        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                $data = $model->login();
                if ($data) {

                    return $data;
                }
            } else {
                return [
                    'status' => 'error-validasi',
                    'data' => $model->errors,
                ];
            }
        }
        return [
            'status' => 'error',
            'message' => 'Gagal login',
        ];
    }

    public function actionLogout() {
        Yii::$app->user->logout();

        return [
            'status' => 'success',
            'message' => 'sukses logout',
        ];
    }

    public function actionAddPlan() {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $user = Yii::$app->user->identity;
        if ($user) {
            $new = new \common\models\Plan();
            $new->created_at = date('d-M-Y');
            $new->id_user = $user->id;
            if ($new->load(Yii::$app->request->post())) {
                $new->save(false);
                return[
                    'status' => 'success',
                    'data' => $new
                ];
            } else {
                return[
                    'status' => 'error',
                    'message' => 'Data tidak bisa disimpan'
                ];
            }
        } else {
            return[
                'status' => 'error',
                'message' => 'Silahkan login terlebih dahulu'
            ];
        }
    }

    public function actionListPlan() {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $user = Yii::$app->user->identity;
        if ($user) {
            $carilist = \common\models\Plan::find()->where(['id_user' => $user->id])->all();

            if ($carilist) {
                $data = [];
                foreach ($carilist as $key => $value) {
                    $data[$key]['id'] = $value->id_plan;
                    $data[$key]['judul'] = $value->judul;
                      $data[$key]['foto'] = 'http://veby.mediasumbar.com/uploads/plan/' . $value->foto;
                }
                $key++;
                return[
                    'status' => 'success',
                    'data' => $data
                ];
            } else {
                return[
                    'status' => 'error',
                    'message' => 'Data tidak ada'
                ];
            }
        } else {
            return[
                'status' => 'error',
                'message' => 'Silahkan login terlebih dahulu'
            ];
        }
    }

    public function actionAddBookmark($id, $kategori) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $user = Yii::$app->user->identity;
        if ($user) {
            if ($kategori == 'Experience') {
                $cari = \common\models\Experience::find()->where(['id_experience' => $id])->one();
                if ($cari) {
                    $foto = \common\models\FotoExperience::find()->where(['id_experience' => $cari->id_experience])->one();
                    $datalama = \common\models\Bookmark::find()->where(['id_data' => $id])->andWhere(['kategori' => $kategori])
                            ->andWhere(['id_user'=>$user->id])->all();

                    if ($datalama) {
                        return[
                            'status' => 'error',
                            'message' => 'Data sudah ada',
                        ];
                    } else {
                        $new = new \common\models\Bookmark();
                        $new->id_data = $cari->id_experience;
                        $new->kategori = $kategori;
                        $new->nama = $cari->nama;
                        $new->harga = $cari->price;
                        $new->id_desa = $cari->id_desa;
                        $new->id_user = $user->id;
                        $new->foto = 'http://veby.mediasumbar.com/uploads/experience/' . $foto->foto;
                        $new->save(false);

                        $coba = new \common\models\BookmarkDesa;
                        $caridulu = \common\models\BookmarkDesa::find()->where(['id_user' => $new->id_user])
                                        ->andWhere(['id_desa' => $new->id_desa])->all();

                        if (!$caridulu) {
                            $coba->id_desa = $new->id_desa;
                            $coba->id_user = $new->id_user;
                            $coba->id_bookmark = $new->id_bookmark;
                            $coba->save(false);
                        }
                        return[
                            'status' => 'success',
                            'data' => $new,
                        ];
                    }
                } else {
                    return[
                        'status' => 'error',
                        'message' => 'Data experience tidak ditemukan'
                    ];
                }
            } else if ($kategori == 'Accommodation') {
                $cari = \common\models\Accommodation::find()->where(['id_homestay' => $id])->one();
                if ($cari) {
                    $foto = \common\models\FotoAccommodation::find()->where(['id_homestay' => $cari->id_homestay])->one();
                    $datalama = \common\models\Bookmark::find()->where(['id_data' => $id])->andWhere(['kategori' => $kategori])
                            ->andWhere(['id_user'=>$user->id])->all();

                    if ($datalama) {
                        return[
                            'status' => 'error',
                            'message' => 'Data sudah ada',
                        ];
                    } else {
                        $new = new \common\models\Bookmark();
                        $new->id_data = $cari->id_homestay;
                        $new->kategori = $kategori;
                        $new->nama = $cari->nama_homestay;
                        $new->harga = $cari->harga;
                        $new->id_desa = $cari->id_desa;
                        $new->id_user = $user->id;
                        $new->foto = 'http://veby.mediasumbar.com/uploads/akomodasi/' . $foto->foto;

                        $new->save(false);
                        $coba = new \common\models\BookmarkDesa;
                        $caridulu = \common\models\BookmarkDesa::find()->where(['id_user' => $new->id_user])
                                        ->andWhere(['id_desa' => $new->id_desa])->all();
                        if (!$caridulu) {
                            $coba->id_desa = $new->id_desa;
                            $coba->id_user = $new->id_user;
                            $coba->id_bookmark = $new->id_bookmark;
                            $coba->save(false);
                        }
                        return[
                            'status' => 'success',
                            'data' => $new,
                        ];
                    }
                } else {
                    return[
                        'status' => 'error',
                        'message' => 'Data accomodation tidak ditemukan'
                    ];
                }
            } else if ($kategori == 'Transportation') {
                $cari = \common\models\Transportation::find()->where(['id_transportation' => $id])->one();
                if ($cari) {
                    $datalama = \common\models\Bookmark::find()->where(['id_data' => $id])
                            ->andWhere(['id_user'=>$user->id])->andWhere(['kategori' => $kategori])->all();

                    if ($datalama) {
                        return[
                            'status' => 'error',
                            'message' => 'Data sudah ada',
                        ];
                    } else {
                        $new = new \common\models\Bookmark();
                        $new->id_data = $cari->id_transportation;
                        $new->kategori = $kategori;
                        $new->nama = $cari->nama;
                        $new->harga = $cari->price;
                        $new->id_desa = $cari->id_desa;
                        $new->id_user = $user->id;
                        $new->foto = 'http://veby.mediasumbar.com/uploads/transport/' . $cari->foto;
                        $new->save(false);
                        $coba = new \common\models\BookmarkDesa;
                        $caridulu = \common\models\BookmarkDesa::find()->where(['id_user' => $new->id_user])
                                        ->andWhere(['id_desa' => $new->id_desa])->all();
                        if (!$caridulu) {
                            $coba->id_desa = $new->id_desa;
                            $coba->id_user = $new->id_user;
                            $coba->id_bookmark = $new->id_bookmark;
                            $coba->save(false);
                        }
                        return[
                            'status' => 'success',
                            'data' => $new,
                        ];
                    }
                } else {
                    return[
                        'status' => 'error',
                        'message' => 'Data transportasi tidak ditemukan'
                    ];
                }
            } else if ($kategori == 'Merchandise') {
                $cari = \common\models\Merchandise::find()->where(['id_merchandise' => $id])->one();
                if ($cari) {
                    $foto = \common\models\FotoMerchandise::find()->where(['id_merchandise' => $cari->id_merchandise])->one();
                    $datalama = \common\models\Bookmark::find()->where(['id_data' => $id])
                            ->andWhere(['id_user'=>$user->id])->andWhere(['kategori' => $kategori])->all();

                    if ($datalama) {
                        return[
                            'status' => 'error',
                            'message' => 'Data sudah ada',
                        ];
                    } else {
                        $new = new \common\models\Bookmark();
                        $new->id_data = $cari->id_merchandise;
                        $new->kategori = $kategori;
                        $new->nama = $cari->nama;
                        $new->harga = $cari->harga;
                        $new->id_desa = $cari->id_desa;
                        $new->id_user = $user->id;
                        $new->foto = 'http://veby.mediasumbar.com/uploads/merchant/' . $foto->foto;
                        $new->save(false);
                        $coba = new \common\models\BookmarkDesa;
                        $caridulu = \common\models\BookmarkDesa::find()->where(['id_user' => $new->id_user])
                                        ->andWhere(['id_desa' => $new->id_desa])->all();

                        if (!$caridulu) {
                            $coba->id_desa = $new->id_desa;
                            $coba->id_user = $new->id_user;
                            $coba->id_bookmark = $new->id_bookmark;
                            $coba->save(false);
                        }
                        return[
                            'status' => 'success',
                            'data' => $new,
                        ];
                    }
                } else {
                    return[
                        'status' => 'error',
                        'message' => 'Data merchandise tidak ditemukan'
                    ];
                }
            } else {
                return[
                    'status' => 'error',
                    'message' => 'Data tidak ditemukan'
                ];
            }
        } else {
            return[
                'status' => 'error',
                'message' => 'Silahkan login terlebih dahulu'
            ];
        }
    }

    public function actionListBookmark() {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $user = Yii::$app->user->identity;
        if ($user) {
            $datadesa = \common\models\BookmarkDesa::find()->where(['id_user' => $user->id])->all();
            if ($datadesa) {
                $data = [];
                foreach ($datadesa as $key => $value) {
                    $desa = \common\models\Villages::find()->where(['id' => $value->id_desa])->one();
                    if ($desa) {
                        $data[$key]['id'] = $value->id_desa;
                        $data[$key]['nama_desa'] = $desa->name;
                    }

                    $bookmark = \common\models\Bookmark::find()->where(['id_desa' => $value->id_desa])
                            ->andWhere(['id_user'=>$user->id])->all();
                    $sub = [];
                    foreach ($bookmark as $ft) {
                        $sub_name['nama'] = $ft['nama'];
                        $sub_name['harga'] = $ft['harga'];
                        $sub_name['foto'] = $ft['foto'];
                        array_push($sub, $sub_name);
                    }
                    $data[$key]['bookmark'] = $sub;
                }
                $key++;
                return[
                    'status' => 'success',
                    'data' => $data,
                ];
            } else {
                return[
                    'status' => 'error',
                    'message' => 'Dat tidak ada'
                ];
            }
        } else {
            
        }
    }
    
  
    public function actionRegistrasi() {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $model = new \common\models\SignupForm();

        $data = Yii::$app->request->post();

        if ($model->load($data) && $model->validate()) {
            $data = $model->signup();

            if ($data == 1) {
                return [
                    'status' => 'success',
                    'message' => 'Berhasil registrasi',
                    'data' => $data,
                ];
            } else {
                return [
                    'status' => 'error',
                    'message' => 'Gagal registrasi',
                    'data' => $data,
                ];
            }
        }

        return [
            'status' => 'error-validasi',
            'message' => 'Gagal validasi',
            'data' => $model->getErrors(),
        ];
    }

    public function actionAkomodasiRandom() {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $akomodasi = \common\models\Accommodation::find()->orderBy(new Expression('rand()'))->limit(10)->all();
        $data = [];

        foreach ($akomodasi as $key => $val) {
            $foto = \common\models\FotoAccommodation::find()->where(['id_homestay' => $val->id_homestay])->orderBy('id_foto DESC')->one();
            $desa = \common\models\Villages::find()->where(['id' => $val->id_desa])->one();
            $provinsi = \common\models\Provinces::find()->where(['id' => $val->id_provinsi])->one();
            $pulau = \common\models\Island::find()->where(['id' => $provinsi->island_id])->one();
            $data[$key]['id'] = $val->id_homestay;

            $data[$key]['nama'] = $val->nama_homestay;
            $data[$key]['deskripsi'] = $val->deskripsi;


            $data[$key]['desa'] = $desa->name;
            $data[$key]['provinsi'] = $provinsi->name;
            if ($pulau) {
                $data[$key]['pulau'] = $pulau->name;
            }

            $data[$key]['harga'] = $val->harga;
            if ($foto) {
                $data[$key]['foto'] = $foto->foto;
            }

            $key++;
        }

        return [
            'status' => 'success',
            'data' => $data,
        ];
    }

    public function actionExperienceRandom() {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $akomodasi = \common\models\Experience::find()->orderBy(new Expression('rand()'))->limit(10)->all();
        $data = [];

        foreach ($akomodasi as $key => $val) {
            $foto = \common\models\FotoExperience::find()->where(['id_experience' => $val->id_experience])->orderBy('id_foto DESC')->one();
            $desa = \common\models\Villages::find()->where(['id' => $val->id_desa])->one();
            $provinsi = \common\models\Provinces::find()->where(['id' => $val->id_provinsi])->one();
            $pulau = \common\models\Island::find()->where(['id' => $provinsi->island_id])->one();
            $data[$key]['id'] = $val->id_experience;
            $data[$key]['kategori'] = $val->id_kategori;
            $data[$key]['nama'] = $val->nama;
            $data[$key]['rating'] = $val->rating;
            $data[$key]['deskripsi'] = $val->deskripsi;

            $data[$key]['desa'] = $desa->name;
            $data[$key]['provinsi'] = $provinsi->name;
            if ($pulau) {
                $data[$key]['pulau'] = $pulau->name;
            }

            $data[$key]['price'] = $val->price;
            if ($foto) {
                $data[$key]['foto'] = $foto->foto;
            }

            $key++;
        }

        return [
            'status' => 'success',
            'data' => $data,
        ];
    }

    public function actionTransportasiRandom() {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $akomodasi = \common\models\Transportation::find()->orderBy(new Expression('rand()'))->limit(10)->all();

        $data = [];

        foreach ($akomodasi as $key => $val) {
            $desa = \common\models\Villages::find()->where(['id' => $val->id_desa])->one();
            $provinsi = \common\models\Provinces::find()->where(['id' => $val->id_provinsi])->one();
            $pulau = \common\models\Island::find()->where(['id' => $provinsi->island_id])->one();

            $data[$key]['id'] = $val->id_transportation;
            $data[$key]['jenis_transportasi'] = $val->jenis_transportasi;
            $data[$key]['nama'] = $val->nama;
            $data[$key]['rute'] = $val->rute;
            $data[$key]['deskripsi'] = $val->deskripsi;

            $data[$key]['desa'] = $desa->name;
            $data[$key]['provinsi'] = $provinsi->name;
            if ($pulau) {
                $data[$key]['pulau'] = $pulau->name;
            }
            $data[$key]['price'] = $val->price;
            $data[$key]['foto'] = $val->foto;
            $key++;
        }

        return [
            'status' => 'success',
            'data' => $data,
        ];
    }

    public function actionMerchantRandom() {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $merchant = \common\models\Merchandise::find()->orderBy(new Expression('rand()'))->limit(10)->all();

        $data = [];

        foreach ($merchant as $key => $val) {
            $foto = \common\models\FotoMerchandise::find()->where(['id_merchandise' => $val->id_merchandise])->orderBy('id_foto DESC')->one();
            $desa = \common\models\Villages::find()->where(['id' => $val->id_desa])->one();
            $provinsi = \common\models\Provinces::find()->where(['id' => $val->id_provinsi])->one();

            $data[$key]['id'] = $val->id_merchandise;
            $data[$key]['kategori'] = $val->kategori;
            $data[$key]['nama'] = $val->nama;
            $data[$key]['alamat'] = $val->alamat;
            $data[$key]['deskripsi'] = $val->deskripsi;

            $data[$key]['desa'] = $desa->name;
            if ($provinsi) {
                $data[$key]['provinsi'] = $provinsi->name;
                $pulau = \common\models\Island::find()->where(['id' => $provinsi->island_id])->one();
                if ($pulau) {

                    $data[$key]['pulau'] = $pulau->name;
                }
            }

            $data[$key]['harga'] = $val->harga;
            if ($foto) {
                $data[$key]['foto'] = $foto->foto;
            }
            $key++;
        }

        return [
            'status' => 'success',
            'data' => $data,
        ];
    }

    public function actionDataExperience($pulau = NULL, $provinsi = NULL, $kecamatan = NULL) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        if ($provinsi) {
            $model = new \common\models\Experience();
            if ($model->load(Yii::$app->request->post())) {
                $cari_pulau = \common\models\Experience::find()->where(['like', 'nama', $model->nama])
                                ->andWhere(['id_provinsi' => $provinsi])->all();
                $data = [];
                foreach ($cari_pulau as $key => $value) {



                    $data[$key]['id'] = $value->id_experience;
                    $data[$key]['nama'] = $value->nama;
                }$key++;

                return[
                    'status' => 'success',
                    'data' => $data,
                ];
            }
        }
    }

    public function actionAddCard() {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $user = Yii::$app->user->identity;
        if ($user) {
            $model = new \common\models\CreditCard();
            if ($model->load(Yii::$app->request->post())) {
                $model->id_user = $user->id;
                $model->save();
                return[
                    'status' => 'success',
                    'data' => $model,
                ];
            } else {
                return[
                    'status' => 'error',
                    'message' => 'Data tidak bisa disimpan'
                ];
            }
        } else {
            return[
                'status' => 'error',
                'message' => 'Silahkan login terlebih dahulu'
            ];
        }
    }

    public function actionCreditcardData() {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $user = Yii::$app->user->identity;

        if ($user) {
            $cari = \common\models\CreditCard::find()->where(['id_user' => $user->id])->all();
            if ($cari) {
                $data = [];
                foreach ($cari as $key => $value) {
                    $data[$key]['id'] = $value->id_credit;
                    $data[$key]['name_card'] = $value->name_card;
                    $data[$key]['card_number'] = $value->card_number;
                    $data[$key]['expired_date'] = $value->expired_date;
                    $data[$key]['cvv'] = $value->cvv;
                    $data[$key]['user'] = $value->id_user;
                }$key++;
                return[
                    'status' => 'success',
                    'data' => $data
                ];
            } else {
                return[
                    'status' => 'error',
                    'message' => 'Data tidak ditemukan',
                ];
            }
        } else {
            return[
                'status' => 'error',
                'message' => 'Silahkan login terlebih dahulu'
            ];
        }
    }

    public function actionProfilData() {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $id = Yii::$app->user->identity;
        if ($id) {
            $model = \backend\models\User::find()->where(['id' => $id->id])->one();
            $profile = \common\models\Profile::find()->where(['user_id' => $id])->one();
            if ($model) {
                $model->photo = 'http://localhost/entra/www/uploads/foto_profil/' . $model->photo;
                return [
                    'status' => 'success',
                    'data' => $model,
                    'profile' => $profile,
                ];
            } else {
                return [
                    'status' => 'error',
                    'data' => 'Data tidak ditemukan',
                ];
            }
        } else {
            return [
                'status' => 'error',
                'data' => 'Harus login terlebih dahulu',
            ];
        }
    }

}
