<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;
use yii\helpers\Url;
AppAsset::register($this);
$about = \common\models\About::find()->one();
$contact = \common\models\Contact::find()->where(['status'=>'Aktif'])->one()?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>ENTRA INDONESIA</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,700,900|Display+Playfair:200,300,400,700"> 
    <link rel="stylesheet" href="<?= Yii::getAlias('@web/fonts/icomoon/style.css')?>">

    <link rel="stylesheet" href="<?= Yii::getAlias('@web/css/bootstrap.min.css')?>">
    <link rel="stylesheet" href="<?= Yii::getAlias('@web/css/magnific-popup.css')?>">
    <link rel="stylesheet" href="<?= Yii::getAlias('@web/css/jquery-ui.css')?>">
    <link rel="stylesheet" href="<?= Yii::getAlias('@web/css/owl.carousel.min.css')?>">
    <link rel="stylesheet" href="<?= Yii::getAlias('@web/css/owl.theme.default.min.css')?>">

    <link rel="stylesheet" href="<?= Yii::getAlias('@web/css/bootstrap-datepicker.css')?> ">

    <link rel="stylesheet" href="<?= Yii::getAlias('@web/fonts/flaticon/font/flaticon.css')?>">

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/mediaelement@4.2.7/build/mediaelementplayer.min.css">


    <link rel="stylesheet" href="<?= Yii::getAlias('@web/css/aos.css')?>">

    <link rel="stylesheet" href="<?= Yii::getAlias('@web/css/style.css')?>">
    
  </head>
  <body>
<?php $this->beginBody() ?>

 <div class="site-wrap">
       <div class="site-mobile-menu">
      <div class="site-mobile-menu-header">
        <div class="site-mobile-menu-close mt-3">
          <span class="icon-close2 js-menu-toggle"></span>
        </div>
      </div>
      <div class="site-mobile-menu-body"></div>
    </div>
    
  
        <?= $content ?>
    
</div>

    <footer class="site-footer">
      <div class="container">
        <div class="row">
          <div class="col-lg-4">
            <div class="mb-5">
              <h3 class="footer-heading mb-4"><?= $about->judul?></h3>
              <p><?= $about->deskripsi?></p>
            </div>

            
            
          </div>
          <div class="col-lg-4 mb-5 mb-lg-0">
            <div class="row mb-5">
              <div class="col-md-12">
                <h3 class="footer-heading mb-4">Navigations</h3>
              </div>
              <div class="col-md-6 col-lg-6">
                <ul class="list-unstyled">
                  <li><a href="#">Home</a></li>
                  <li><a href="#">Destination</a></li>
                  <li><a href="#">Services</a></li>
                  <li><a href="#">About</a></li>
                </ul>
              </div>
              <div class="col-md-6 col-lg-6">
                <ul class="list-unstyled">
                  <li><a href="#">About Us</a></li>
                  <li><a href="#">Privacy Policy</a></li>
                  <li><a href="#">Contact Us</a></li>
                  <li><a href="#">Discounts</a></li>
                </ul>
              </div>
            </div>

            

          </div>

        
        </div>
        <div class="row pt-5 mt-5 text-center">
          <div class="col-md-12">
            <div class="mb-5">
              <a href="https://facebook.com/<?= $contact->facebook?>"
   onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;"
   target="_blank" title="Facebook" data-social="tw"class="pl-0 pr-3"><span class="icon-facebook"></span></a>
              <a href="https://twitter.com/<?= $contact->twitter?>"
   onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;"
   target="_blank" title="Twitter" data-social="tw" class="pl-3 pr-3"><span class="icon-twitter"></span></a>
              <a href="https://instagram.com/<?= $contact->instagram?>"
   onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;"
   target="_blank" title="Facebook" data-social="tw" class="pl-3 pr-3"><span class="icon-instagram"></span></a>
             <!--  <a href="#" class="pl-3 pr-3"><span class="icon-linkedin"></span></a> -->
            </div>

          <!--   <p>
            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
         <!--    Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="icon-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank" >Colorlib</a> -->
            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
           <!--  </p> --> 
          </div>
          
        </div>
      </div>
    </footer>
      
  <script src="<?= Yii::getAlias('@web/js/jquery-3.3.1.min.js')?>"></script>
  <script src="<?= Yii::getAlias('@web/js/jquery-migrate-3.0.1.min.js')?>"></script>
  <script src="<?= Yii::getAlias('@web/js/jquery-ui.js')?>"></script>
  <script src="<?= Yii::getAlias('@web/js/popper.min.js')?>"></script>
  <script src="<?= Yii::getAlias('@web/js/bootstrap.min.js')?>"></script>
  <script src="<?= Yii::getAlias('@web/js/owl.carousel.min.js')?>"></script>
  <script src="<?= Yii::getAlias('@web/js/jquery.stellar.min.js')?>"></script>
  <script src="<?= Yii::getAlias('@web/js/jquery.countdown.min.js')?>"></script>
  <script src="<?= Yii::getAlias('@web/js/jquery.magnific-popup.min.js')?>"></script>
  <script src="<?= Yii::getAlias('@web/js/bootstrap-datepicker.min.js')?>"></script>
  <script src="<?= Yii::getAlias('@web/js/aos.js')?>"></script>

  <script src="<?= Yii::getAlias('@web/js/main.js')?>"></script>
    
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
