
<?php 
use yii\helpers\Url;
use yii\helpers\StringHelper;
?>
    <div class="site-mobile-menu">
      <div class="site-mobile-menu-header">
        <div class="site-mobile-menu-close mt-3">
          <span class="icon-close2 js-menu-toggle"></span>
        </div>
      </div>
      <div class="site-mobile-menu-body"></div>
    </div>
<header class="site-navbar py-1" role="banner">

      <div class="container">
        <div class="row align-items-center">
          
          <div class="col-6 col-xl-2">
           
           <a class="mb-0" href="<?=Url::toRoute(['/'])?>">
                  <img alt="Brand"  style="height:50px; width:100px;" src="<?=  Yii::getAlias('uploads/entra.jpg')?>">
      </a>
          </div>
          <div class="col-10 col-md-8 d-none d-xl-block">
            <nav class="site-navigation position-relative text-right text-lg-center" role="navigation">

              <ul class="site-menu js-clone-nav mx-auto d-none d-lg-block">
                <li >
                  <a href="<?= Url::toRoute(['/']) ?>">Home</a>
                </li>
                <li class="has-children">
                  <a href="#">Explore</a>
                  <ul class="dropdown">
                    <li ><a href="<?= Url::toRoute(['accommodation']) ?>">Accomodation</a></li>
                    <li><a href="<?= Url::toRoute(['experience']) ?>">Experience</a></li>
                   
                    <li><a href="<?= Url::toRoute(['merchandise']) ?>">Merchandise</a></li>
                     <li><a href="<?= Url::toRoute(['transportation']) ?>">Transportation</a></li>
                  </ul>
                </li>
              
                <li  ><a href="<?= Url::toRoute(['about']) ?>">About</a></li>
                <li class="active"><a href="<?= Url::toRoute(['blog']) ?>">Blog</a></li>
                
                <li><a href="<?= Url::toRoute(['contact']) ?>">Contact</a></li>
                <!-- <li><a href="booking.html">Book Online</a></li> -->
              </ul>
            </nav>
          </div>

          <div class="col-6 col-xl-2 text-right">
            <div class="d-none d-xl-inline-block">
              <ul class="site-menu js-clone-nav ml-auto list-unstyled d-flex text-right mb-0" data-class="social">
                <li>
                  <a href="#" class="pl-0 pr-3 text-black"><span class="icon-download"></span></a>
                </li>
                <li>
                  <a href="#" class="pl-3 pr-3 text-black"><span class="icon-twitter"></span></a>
                </li>
                <li>
                  <a href="#" class="pl-3 pr-3 text-black"><span class="icon-facebook"></span></a>
                </li>
                <li>
                  <a href="#" class="pl-3 pr-3 text-black"><span class="icon-instagram"></span></a>
                </li>
                
              </ul>
            </div>

            <div class="d-inline-block d-xl-none ml-md-0 mr-auto py-3" style="position: relative; top: 3px;"><a href="#" class="site-menu-toggle js-menu-toggle text-black"><span class="icon-menu h3"></span></a></div>

          </div>

        </div>
      </div>
      
    </header>


  

   

    <div class="site-blocks-cover inner-page-cover" style="background-image: url(images/hero_bg_2.jpg);" data-aos="fade" data-stellar-background-ratio="0.5">
        <div class="container">
          <div class="row align-items-center justify-content-center text-center">

            <div class="col-md-8" data-aos="fade-up" data-aos-delay="400">
              <h1 class="text-white font-weight-light">Blog Posts</h1>
              <div><a href="index.html">Home</a> <span class="mx-2 text-white">&bullet;</span> <span class="text-white">Blog</span></div>
              
            </div>
          </div>
        </div>
      </div>  


    
    <div class="site-section">
      <div class="container">
        
        <div class="row mb-3 align-items-stretch">
         
         <?php foreach ($blog as $value){ ?>
          <div class="col-md-6 col-lg-6 mb-4 mb-lg-4">
            <div class="h-entry">
              <img style="height:250px; width:400px;" src="<?=  Yii::getAlias('uploads/blog/thumb-'.$value->foto)?>" alt="Image" class="img-fluid">
              <h2 class="font-size-regular"><a href="<?= Url::toRoute(['detail-blog','id'=>$value->id_tips]); ?>"><?= $value->judul?></a></h2>
              <div class="meta mb-4">by <?= $value->created_by?> <span class="mx-2">&bullet;</span> <?= $value->created_at?><span class="mx-2">&bullet;</span> <a href="#"></a></div>
              <p> <?= ucfirst(StringHelper::truncate($value->deskripsi, 180))?></p>
            </div>
          </div>
          <?php } ?>
        </div>
        <div class="row">
          <div class="col-12 text-center">
           <!--  <a href="#" class="btn btn-outline-primary border-2 py-3 px-5">Load More Posts...</a> -->
          </div>
        </div>
      </div>
    </div>

  
  