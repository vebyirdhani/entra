<?php 
use yii\helpers\Url;
use yii\helpers\StringHelper;
?>
<header class="site-navbar py-1" role="banner">

      <div class="container">
        <div class="row align-items-center">
          
          <div class="col-6 col-xl-2">
           
           <a class="mb-0" href="<?=Url::toRoute(['/'])?>">
                  <img alt="Brand"  style="height:50px; width:100px;" src="<?=  Yii::getAlias('uploads/entra.jpg')?>">
      </a>
          </div>
          <div class="col-10 col-md-8 d-none d-xl-block">
            <nav class="site-navigation position-relative text-right text-lg-center" role="navigation">

              <ul class="site-menu js-clone-nav mx-auto d-none d-lg-block">
                <li class="active" >
                  <a href="<?= Url::toRoute(['/']) ?>">Home</a>
                </li>
                <li class="has-children">
                  <a href="#">Explore</a>
                  <ul class="dropdown">
                    <li><a href="<?= Url::toRoute(['accommodation']) ?>">Accomodation</a></li>
                    <li><a href="<?= Url::toRoute(['experience']) ?>">Experience</a></li>
                  
                    <li><a href="<?= Url::toRoute(['merchandise']) ?>">Merchandise</a></li>
                      <li><a href="<?= Url::toRoute(['transportation']) ?>">Transportation</a></li>
                  </ul>
                </li>
              
                <li  ><a href="<?= Url::toRoute(['about']) ?>">About</a></li>
                <li><a href="<?= Url::toRoute(['blog']) ?>">Blog</a></li>
                
                <li><a href="<?= Url::toRoute(['contact']) ?>">Contact</a></li>
                <!-- <li><a href="booking.html">Book Online</a></li> -->
              </ul>
            </nav>
          </div>

          <div class="col-6 col-xl-2 text-right">
            <div class="d-none d-xl-inline-block">
              <ul class="site-menu js-clone-nav ml-auto list-unstyled d-flex text-right mb-0" data-class="social">
                <li>
                  <a href="#" class="pl-0 pr-3 text-black"><span class="icon-download"></span></a>
                </li>
                <li>
                  <a href="#" class="pl-3 pr-3 text-black"><span class="icon-twitter"></span></a>
                </li>
                <li>
                  <a href="#" class="pl-3 pr-3 text-black"><span class="icon-facebook"></span></a>
                </li>
                <li>
                  <a href="#" class="pl-3 pr-3 text-black"><span class="icon-instagram"></span></a>
                </li>
                
              </ul>
            </div>

            <div class="d-inline-block d-xl-none ml-md-0 mr-auto py-3" style="position: relative; top: 3px;"><a href="#" class="site-menu-toggle js-menu-toggle text-black"><span class="icon-menu h3"></span></a></div>

          </div>

        </div>
      </div>
      
    </header>

<!--    <div class="slide-one-item home-slider owl-carousel">
      <?php if ($slider) {
          foreach ($slider as $vs){
          ?>
    
      <?php }} ?>
     

    </div>-->

  <div class="site-blocks-cover inner-page-cover" style="background-image: url(uploads/slider/<?=$slider->foto?>);" data-aos="fade" data-stellar-background-ratio="0.5">
        <div class="container">
          <div class="row align-items-center justify-content-center text-center">
 <div class="col-md-5" data-aos="fade-up" data-aos-delay="400">
           
            </div>
            <div class="col-md-7" data-aos="fade-up" data-aos-delay="400">
              <h1 class="text-black font-weight-light"><?= $slider->judul?></h1>
              <div> <span class="mx-2 text-black"></span> <span class="text-black"><?= $slider->deskripsi?></span></div>
              
            </div>
          </div>
        </div>
      </div>  




  
  
    <div class="site-section bg-light">
      <div class="container">
        <div class="row justify-content-center mb-5">
          <div class="col-md-7 text-center">
            <h2 class="font-weight-light text-black">Our Services</h2>
            <p class="color-black-opacity-5">We Offer The Following Services</p>
          </div>
        </div>
        <div class="row align-items-stretch">
             <?php foreach ($fitur as $ft){ ?>
          <div class="col-md-6 col-lg-6 mb-4 mb-lg-4">
            <div class="unit-4 d-flex">
              <div class="unit-4-icon mr-4"><span class="text-primary "><img style="height:48px; width:48px;" src="<?=  Yii::getAlias('uploads/fitur/'.'thumb-'.$ft->icon)?>" alt="Image" class="img-md-fluid">
             </span></div>
              <div>
                 <h3><?= $ft->judul?></h3>
                <p><?= $ft->deskripsi?></p>
<!--                <p><a href="#">Learn More</a></p>-->
              </div>
            </div>
          </div>
         
       

  <?php } ?>
        
         
        </div>
      </div>
    </div>



    
    <div class="site-section">
      
      <div >
        <div class="row">
            <?php if ($kategori){ 
                
                foreach ($kategori as $vk){
            ?>
          <div class="col-md-6 col-lg-3 mb-3 mb-lg-1">
              
            <?php if ($vk->id_kategori==1){?>
            <a href="<?= Url::toRoute(['accommodation']) ?>" >
              <?php } else if ($vk->id_kategori==2){ ?>
                <a href="<?= Url::toRoute(['transportation']) ?>">
             <?php } else if ($vk->id_kategori==3){ ?>
            <a href="<?= Url::toRoute(['merchandise']) ?>"  >
             <?php } else {?>
<a href="<?= Url::toRoute(['experience']) ?>"  >
             <?php } ?>
    <center>      <img style="height:170px; width:170px; border-radius: 50px;" src="<?=  Yii::getAlias('uploads/kategori/'.'thumb-'.$vk->foto)?>" alt="Image" class="img-md-fluid">
    </center>
            </a>
               
          </div>
            
            <?php }} ?>
       
              
        </div>
      </div>
    
    </div>

 



    <div class="site-section">
      
      <div class="container">
        <div class="row justify-content-center mb-5">
          <div class="col-md-7 text-center">
            <h2 class="font-weight-light text-black">Our Destinations</h2>
            <p class="color-black-opacity-5">Choose Your Next Destination</p>
          </div>
        </div>
        <div class="row">
            <?php if ($desa){
                foreach ($desa as $vd){
                    $carifoto = common\models\FotoDesatraveling::find()->where(['id_desa'=>$vd->id])->one();?>
          <div class="col-md-6 col-lg-4 mb-4 mb-lg-4">
            <a href="#" class="unit-1 text-center" data-toggle="modal" data-target="#data-<?= $vd->id?>">
                <?php if ($carifoto){?>
              <img style="height:400px; width:400px;" src="<?=  Yii::getAlias('uploads/desa/'.'thumb-'.$carifoto->foto)?>" alt="Image" class="img-fluid">
              
                <?php }else { ?>
              <img src="images/02-rome.jpg" alt="Image" class="img-fluid">
                <?php } ?>
              <div class="unit-1-text">
                <strong class="text-primary mb-2 d-block"></strong>
                <?php $caridesa = \common\models\Provinces::find()->where(['id'=>$vd->id_provinsi])->one();
                if ($caridesa){?>
                <h3 class="unit-1-heading"><?= $vd->nama?> , <?= $caridesa->name?></h3>
                <?php }?>
              </div>
            </a>
          </div>
            <?php } }?>
        

        </div>
      </div>
    
    </div>

   <div class="site-section block-13 bg-light">
  

    <div class="container">
        <div class="row justify-content-center mb-5">
          <div class="col-md-7">
            <h2 class="font-weight-light text-black text-center">Testimonials</h2>
          </div>
        </div>

        <div class="nonloop-block-13 owl-carousel">

          <div class="item">
            <div class="container">
              <div class="row">
                <div class="col-lg-6 mb-5">
                  <img src="images/img_1.jpg" alt="Image" class="img-md-fluid">
                </div>
                <div class="overlap-left col-lg-6 bg-white p-md-5 align-self-center">
                  <p class="text-black lead">&ldquo;Lorem ipsum dolor sit amet, consectetur adipisicing elit. Similique dolorem quisquam laudantium, incidunt id laborum, tempora aliquid labore minus. Nemo maxime, veniam! Fugiat odio nam eveniet ipsam atque, corrupti porro&rdquo;</p>
                  <p class="">&mdash; <em>James Martin</em>, <a href="#">Traveler</a></p>
                </div>
              </div>
            </div>
          </div>

          <div class="item">
            <div class="container">
              <div class="row">
                <div class="col-lg-6 mb-5">
                  <img src="images/img_2.jpg" alt="Image" class="img-md-fluid">
                </div>
                <div class="overlap-left col-lg-6 bg-white p-md-5 align-self-center">
                  <p class="text-black lead">&ldquo;Lorem ipsum dolor sit amet, consectetur adipisicing elit. Similique dolorem quisquam laudantium, incidunt id laborum, tempora aliquid labore minus. Nemo maxime, veniam! Fugiat odio nam eveniet ipsam atque, corrupti porro&rdquo;</p>
                  <p class="">&mdash; <em>Clair Augustin</em>, <a href="#">Traveler</a></p>
                </div>
              </div>
            </div>
          </div>

          <div class="item">
            <div class="container">
              <div class="row">
                <div class="col-lg-6 mb-5">
                  <img src="images/img_4.jpg" alt="Image" class="img-md-fluid">
                </div>
                <div class="overlap-left col-lg-6 bg-white p-md-5 align-self-center">
                  <p class="text-black lead">&ldquo;Lorem ipsum dolor sit amet, consectetur adipisicing elit. Similique dolorem quisquam laudantium, incidunt id laborum, tempora aliquid labore minus. Nemo maxime, veniam! Fugiat odio nam eveniet ipsam atque, corrupti porro&rdquo;</p>
                  <p class="">&mdash; <em>James Martin</em>, <a href="#">Traveler</a></p>
                </div>
              </div>
            </div>
          </div>

        </div>
      </div>
    </div>
    <!-- <div class="site-section bg-light">
      
    </div> -->



   

    <div class="site-section">
      <div class="container">
        <div class="row justify-content-center mb-5">
          <div class="col-md-7 text-center">
            <h2 class="font-weight-light text-black">Our Blog</h2>
            <p class="color-black-opacity-5">See Our Daily News &amp; Updates</p>
          </div>
        </div>
        <div class="row mb-3 align-items-stretch">
          <?php foreach ($blog as $bg){ ?>
        <div class="col-md-6 col-lg-6 mb-4 mb-lg-4">
            <div class="h-entry">
              <img style="height:250px; width:400px;" src="<?=  Yii::getAlias('uploads/blog/thumb-'.$bg->foto)?>" alt="Image" class="img-fluid">
              <h2 class="font-size-regular"><a href="<?= Url::toRoute(['detail-blog','id'=>$bg->id_tips]) ?>"><?= $bg->judul?></a></h2>
              <div class="meta mb-4">by <?= $bg->created_by?> <span class="mx-2">&bullet;</span> <?= $bg->created_at?><span class="mx-2">&bullet;</span> <a href="#"></a></div>
              <p> <?= ucfirst(StringHelper::truncate($bg->deskripsi, 180))?></p>
            </div>
          </div>
         <?php } ?>
        </div>
        <div class="row">
          <div class="col-12 text-center">
            <a href="<?= Url::toRoute(['blog']) ?>" class="btn btn-outline-primary border-2 py-3 px-5">View All Blog Posts</a>
          </div>
        </div>
      </div>
    </div>
    
   
    

<?php foreach($desa as  $vd){
     $carifoto = common\models\FotoDesatraveling::find()->where(['id_desa'=>$vd->id])->one()?>
<div class="modal fade" id="data-<?= $vd->id?>" tabindex="-1" role="dialog" aria-labelledby="Modal-label-1">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
                                            <h4 class="modal-title" id="Modal-label-1"><?= $vd->nama?></h4>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						
					</div>
					<div class="modal-body">
                                            <?php if ($carifoto){ ?>
                                            <center> <img style="height:400px; width:100%;" src="<?=  Yii::getAlias('uploads/desa/'.$carifoto->foto)?>" alt="img01" class="img-responsive" /> </center>
                                            <?php } else{ ?>
                                         <center>      <img style="height:450px; width:460px;" src="images/02-rome.jpg" alt="img01" class="img-responsive" /></center>
                                            <?php } ?>
					</div>
					
				</div>
			</div>
		</div>
<?php } ?>