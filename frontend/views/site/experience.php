<?php 
use yii\helpers\Url;
?>
    <div class="site-mobile-menu">
      <div class="site-mobile-menu-header">
        <div class="site-mobile-menu-close mt-3">
          <span class="icon-close2 js-menu-toggle"></span>
        </div>
      </div>
      <div class="site-mobile-menu-body"></div>
    </div>
<header class="site-navbar py-1" role="banner">

      <div class="container">
        <div class="row align-items-center">
          
          <div class="col-6 col-xl-2">
           
           <a class="mb-0" href="<?=Url::toRoute(['/'])?>">
                  <img alt="Brand"  style="height:50px; width:100px;" src="<?=  Yii::getAlias('uploads/entra.jpg')?>">
      </a>
          </div>
          <div class="col-10 col-md-8 d-none d-xl-block">
            <nav class="site-navigation position-relative text-right text-lg-center" role="navigation">

              <ul class="site-menu js-clone-nav mx-auto d-none d-lg-block">
                <li >
                  <a href="<?= Url::toRoute(['/']) ?>">Home</a>
                </li>
                <li class="has-children">
                  <a href="#">Explore</a>
                  <ul class="dropdown">
                      <?php $fotoakomodasi = common\models\Menu::find()->where(['kategori'=>'Accomodation'])->one();?>
                       <?php $fotoexperience = common\models\Menu::find()->where(['kategori'=>'Experience'])->one();?>
                       <?php $fotomerchant = common\models\Menu::find()->where(['kategori'=>'Merchandise'])->one();?> 
                       <?php $fototransportasi = common\models\Menu::find()->where(['kategori'=>'Transportation'])->one();?>
                    <li >  <a href="<?= Url::toRoute(['accommodation']) ?>"><img style="height:20px; width:20px;" src="<?=  Yii::getAlias('uploads/menu/thumb-'.$fotoakomodasi->icon)?>"> Accomodation</a></li>
                    <li class="active"><a href="<?= Url::toRoute(['experience']) ?>"><img style="height:20px; width:20px;" src="<?=  Yii::getAlias('uploads/menu/thumb-'.$fotoexperience->icon)?>"> Experience</a></li>
                   
                    <li><a href="<?= Url::toRoute(['merchandise']) ?>"><img style="height:20px; width:20px;" src="<?=  Yii::getAlias('uploads/menu/thumb-'.$fotomerchant->icon)?>"> Merchandise</a></li>
                     <li><a href="<?= Url::toRoute(['transportation']) ?>"><img style="height:20px; width:20px;" src="<?=  Yii::getAlias('uploads/menu/thumb-'.$fototransportasi->icon)?>"> Transportation</a></li>
                  </ul>
                </li>
              
                <li  ><a href="<?= Url::toRoute(['about']) ?>">About</a></li>
                <li><a href="<?= Url::toRoute(['blog']) ?>">Blog</a></li>
                
                <li><a href="<?= Url::toRoute(['contact']) ?>">Contact</a></li>
                <!-- <li><a href="booking.html">Book Online</a></li> -->
              </ul>
            </nav>
          </div>

          <div class="col-6 col-xl-2 text-right">
            <div class="d-none d-xl-inline-block">
              <ul class="site-menu js-clone-nav ml-auto list-unstyled d-flex text-right mb-0" data-class="social">
                <li>
                  <a href="#" class="pl-0 pr-3 text-black"><span class="icon-download"></span></a>
                </li>
                <li>
                  <a href="#" class="pl-3 pr-3 text-black"><span class="icon-twitter"></span></a>
                </li>
                <li>
                  <a href="#" class="pl-3 pr-3 text-black"><span class="icon-facebook"></span></a>
                </li>
                <li>
                  <a href="#" class="pl-3 pr-3 text-black"><span class="icon-instagram"></span></a>
                </li>
                
              </ul>
            </div>

            <div class="d-inline-block d-xl-none ml-md-0 mr-auto py-3" style="position: relative; top: 3px;"><a href="#" class="site-menu-toggle js-menu-toggle text-black"><span class="icon-menu h3"></span></a></div>

          </div>

        </div>
      </div>
      
    </header>

   

    <div class="site-blocks-cover inner-page-cover" style="background-image: url(images/hero_bg_2.jpg);" data-aos="fade" data-stellar-background-ratio="0.5">
        <div class="container">
          <div class="row align-items-center justify-content-center text-center">

            <div class="col-md-8" data-aos="fade-up" data-aos-delay="400">
              <h1 class="text-white font-weight-light">Experience</h1>
              <div><a href="index.html">Home</a> <span class="mx-2 text-white">&bullet;</span> <span class="text-white">Experience</span></div>
              
            </div>
          </div>
        </div>
      </div>  


    

    <div class="site-section">
      
      <div class="container">
        
        <div class="row">
       
            <?php foreach ($data as $value ){
                $foto  = common\models\FotoExperience::find()->where(['id_experience'=>$value->id_experience])->one();
                ?>
          <div class="col-md-6 col-lg-4 mb-4 mb-lg-4">
              <a href="#" class="unit-1 text-center" data-toggle="modal" data-target="#data-<?= $value->id_experience?>">
                <?php if ($foto){ ?>
              <img src="<?=  Yii::getAlias('uploads/experience/thumb-'.$foto->foto)?>" alt="Image" class="img-fluid">
                <?php } ?>
              <div class="unit-1-text">
                  <h5><?= Yii::$app->formatter->asCurrency($value->price) ?> &mdash;</h5>
                <h3 class="unit-1-heading"><?= $value->nama?></h3>
              </div>
            </a>
          </div>
         
            <?php } ?>

       
        
        </div>
      </div>
    
    </div>

    <div class="site-section block-13 bg-light">
  

    <div class="container">
        <div class="row justify-content-center mb-5">
          <div class="col-md-7">
            <h2 class="font-weight-light text-black text-center">What People Says</h2>
          </div>
        </div>

        <div class="nonloop-block-13 owl-carousel">

          <div class="item">
            <div class="container">
              <div class="row">
                <div class="col-lg-6 mb-5">
                  <img src="images/img_1.jpg" alt="Image" class="img-md-fluid">
                </div>
                <div class="overlap-left col-lg-6 bg-white p-md-5 align-self-center">
                  <p class="text-black lead">&ldquo;Lorem ipsum dolor sit amet, consectetur adipisicing elit. Similique dolorem quisquam laudantium, incidunt id laborum, tempora aliquid labore minus. Nemo maxime, veniam! Fugiat odio nam eveniet ipsam atque, corrupti porro&rdquo;</p>
                  <p class="">&mdash; <em>James Martin</em>, <a href="#">Traveler</a></p>
                </div>
              </div>
            </div>
          </div>

          <div class="item">
            <div class="container">
              <div class="row">
                <div class="col-lg-6 mb-5">
                  <img src="images/img_2.jpg" alt="Image" class="img-md-fluid">
                </div>
                <div class="overlap-left col-lg-6 bg-white p-md-5 align-self-center">
                  <p class="text-black lead">&ldquo;Lorem ipsum dolor sit amet, consectetur adipisicing elit. Similique dolorem quisquam laudantium, incidunt id laborum, tempora aliquid labore minus. Nemo maxime, veniam! Fugiat odio nam eveniet ipsam atque, corrupti porro&rdquo;</p>
                  <p class="">&mdash; <em>Clair Augustin</em>, <a href="#">Traveler</a></p>
                </div>
              </div>
            </div>
          </div>

          <div class="item">
            <div class="container">
              <div class="row">
                <div class="col-lg-6 mb-5">
                  <img src="images/img_4.jpg" alt="Image" class="img-md-fluid">
                </div>
                <div class="overlap-left col-lg-6 bg-white p-md-5 align-self-center">
                  <p class="text-black lead">&ldquo;Lorem ipsum dolor sit amet, consectetur adipisicing elit. Similique dolorem quisquam laudantium, incidunt id laborum, tempora aliquid labore minus. Nemo maxime, veniam! Fugiat odio nam eveniet ipsam atque, corrupti porro&rdquo;</p>
                  <p class="">&mdash; <em>James Martin</em>, <a href="#">Traveler</a></p>
                </div>
              </div>
            </div>
          </div>

        </div>
      </div>
    </div>

<?php foreach($data as  $value){
    $foto  = common\models\FotoExperience::find()->where(['id_experience'=>$value->id_experience])->one();?>
<div class="modal fade" id="data-<?= $value->id_experience?>" tabindex="-1" role="dialog" aria-labelledby="Modal-label-1">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
                                            <h4 class="modal-title" id="Modal-label-1"><?= $value->nama?></h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            
          </div>
          <div class="modal-body">
                                            <?php if ($foto){ ?>
                                            <img style="height:450px; width:100%;" src="<?=  Yii::getAlias('uploads/experience/'.$foto->foto)?>" alt="img01" class="img-responsive" />
                                            <?php } else{ ?>
                                             <img style="height:450px; width:460px;" src="images/02-rome.jpg" alt="img01" class="img-responsive" />
                                            <?php } ?>
          </div>
          
        </div>
      </div>
    </div>
<?php } ?>
  <!--   <div class="site-section">
      <div class="container">
        <div class="row justify-content-center mb-5">
          <div class="col-md-7 text-center">
            <h2 class="font-weight-light text-black">Our Services</h2>
            <p class="color-black-opacity-5">We Offer The Following Services</p>
          </div>
        </div>
        <div class="row align-items-stretch">
         
         
          <div class="col-md-6 col-lg-4 mb-4 mb-lg-4">
            <div class="unit-4 d-flex">
              <div class="unit-4-icon mr-4"><span class="text-primary flaticon-route"></span></div>
              <div>
                <h3>Tour Packages</h3>
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Perferendis quis molestiae vitae eligendi at.</p>
                <p><a href="#">Learn More</a></p>
              </div>
            </div>
          </div>


          <div class="col-md-6 col-lg-4 mb-4 mb-lg-4">
            <div class="unit-4 d-flex">
              <div class="unit-4-icon mr-4"><span class="text-primary flaticon-hotel"></span></div>
              <div>
                <h3>Hotel Accomodations</h3>
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Perferendis quis molestiae vitae eligendi at.</p>
                <p><a href="#">Learn More</a></p>
              </div>
            </div>
          </div>
        
          <div class="col-md-6 col-lg-4 mb-4 mb-lg-4">
            <div class="unit-4 d-flex">
              <div class="unit-4-icon mr-4"><span class="text-primary flaticon-ski"></span></div>
              <div>
                <h3>Ski Experiences</h3>
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Perferendis quis molestiae vitae eligendi at.</p>
                <p><a href="#">Learn More</a></p>
              </div>
            </div>
          </div>

        </div>
      </div>
    </div>
    
     -->