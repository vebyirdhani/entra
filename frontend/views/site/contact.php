  <?php
  use yii\bootstrap\ActiveForm;
 use yii\bootstrap\Alert;
         use yii\helpers\Url;     
                ?>
   <header class="site-navbar py-1" role="banner">

      <div class="container">
        <div class="row align-items-center">
          
          <div class="col-6 col-xl-2">
           
           <a class="mb-0" href="<?=Url::toRoute(['/'])?>">
                  <img alt="Brand"  style="height:50px; width:100px;" src="<?=  Yii::getAlias('uploads/entra.jpg')?>">
      </a>
          </div>
          <div class="col-10 col-md-8 d-none d-xl-block">
            <nav class="site-navigation position-relative text-right text-lg-center" role="navigation">

              <ul class="site-menu js-clone-nav mx-auto d-none d-lg-block">
                <li >
                  <a href="<?= Url::toRoute(['/']) ?>">Home</a>
                </li>
                <li class="has-children">
                  <a href="#">Explore</a>
                  <ul class="dropdown">
                   <li><a href="<?= Url::toRoute(['accommodation']) ?>">Accomodation</a></li>
                    <li><a href="<?= Url::toRoute(['experience']) ?>">Experience</a></li>
                  
                    <li><a href="<?= Url::toRoute(['merchandise']) ?>">Merchandise</a></li>
                      <li><a href="<?= Url::toRoute(['transportation']) ?>">Transportation</a></li>
                  </ul>
                </li>
              
                <li  ><a href="<?= Url::toRoute(['about']) ?>">About</a></li>
                <li><a href="<?= Url::toRoute(['blog']) ?>">Blog</a></li>
                
                <li class="active"><a href="<?= Url::toRoute(['contact']) ?>">Contact</a></li>
                <!-- <li><a href="booking.html">Book Online</a></li> -->
              </ul>
            </nav>
          </div>

          <div class="col-6 col-xl-2 text-right">
            <div class="d-none d-xl-inline-block">
              <ul class="site-menu js-clone-nav ml-auto list-unstyled d-flex text-right mb-0" data-class="social">
                <li>
                  <a href="#" class="pl-0 pr-3 text-black"><span class="icon-download"></span></a>
                </li>
                <li>
                  <a href="#" class="pl-3 pr-3 text-black"><span class="icon-twitter"></span></a>
                </li>
                <li>
                  <a href="#" class="pl-3 pr-3 text-black"><span class="icon-facebook"></span></a>
                </li>
                <li>
                  <a href="#" class="pl-3 pr-3 text-black"><span class="icon-instagram"></span></a>
                </li>
                
              </ul>
            </div>

            <div class="d-inline-block d-xl-none ml-md-0 mr-auto py-3" style="position: relative; top: 3px;"><a href="#" class="site-menu-toggle js-menu-toggle text-black"><span class="icon-menu h3"></span></a></div>

          </div>

        </div>
      </div>
      
    </header>
   

    <div class="site-blocks-cover inner-page-cover" style="background-image: url(images/hero_bg_2.jpg);" data-aos="fade" data-stellar-background-ratio="0.5">
        <div class="container">
          <div class="row align-items-center justify-content-center text-center">

            <div class="col-md-8" data-aos="fade-up" data-aos-delay="400">
              <h1 class="text-white font-weight-light">Get In Touch</h1>
              <div><a href="index.html">Home</a> <span class="mx-2 text-white">&bullet;</span> <span class="text-white">Contact</span></div>
              
            </div>
          </div>
        </div>
      </div>  
<?php if (Yii::$app->session->hasFlash('success')): ?>
    <div class="alert alert-success alert-dismissable">
         <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
         <h4><i class="icon fa fa-check"></i>Thankyou!</h4>
         <?= Yii::$app->session->getFlash('success') ?>
    </div>
<?php endif; ?>

    <div class="site-section bg-light">
      <div class="container">
        <div class="row">
          <div class="col-md-7 mb-5">

      
           <?php $form = ActiveForm::begin(); ?>

              <div class="row form-group">
                <div class="col-md-6 mb-3 mb-md-0">
                  <label class="text-black" for="fname">First Name</label>
                
                   <?=
                                                $form->field($komentar, 'first_name', ['template' => "{input}\n{error}"])
                                                ->textInput([
                                                  
                                                    'class' => 'form-control',
                                                    'type' => 'text',
                                                    'id' => 'fname',
                                                    'required' => 'required',
                                        ]);
                                        ?>
                </div>
                <div class="col-md-6">
                  <label class="text-black" for="lname">Last Name</label>
                
                    <?=
                                                $form->field($komentar, 'last_name', ['template' => "{input}\n{error}"])
                                                ->textInput([
                                                  
                                                    'class' => 'form-control',
                                                    'type' => 'text',
                                                    'id' => 'lname',
                                                    'required' => 'required',
                                        ]);
                                        ?>
                </div>
              </div>

              <div class="row form-group">
                
                <div class="col-md-12">
                  <label class="text-black" for="email">Email</label> 
                 
                   <?=
                                                $form->field($komentar, 'email', ['template' => "{input}\n{error}"])
                                                ->textInput([
                                                   
                                                    'class' => 'form-control',
                                                    'type' => 'email',
                                                    'id' => 'email',
                                                    'required' => 'required',
                                        ]);
                                        ?>
                </div>
              </div>

              <div class="row form-group">
                
                <div class="col-md-12">
                  <label class="text-black" for="subject">Subject</label> 
                
                    <?=
                                                $form->field($komentar, 'subject', ['template' => "{input}\n{error}"])
                                                ->textInput([
                                                  
                                                    'class' => 'form-control',
                                                    'type' => 'text',
                                                    'id' => 'subject',
                                                    'required' => 'required',
                                        ]);
                                        ?>
                </div>
              </div>

              <div class="row form-group">
                <div class="col-md-12">
                  <label class="text-black" for="message">Message</label> 
               
                                                        <?=
                                                $form->field($komentar, 'body', ['template' => "{input}\n{error}"])
                                                ->textArea([
                                                   
                                                    'class' => 'form-control',
                                                    'cols'=>'30',
                                                    'row' => '7',
                                                    'id' => 'message',
                                                    'required' => 'required',
                                        ]);
                                        ?>

                </div>
              </div>
               <div class="row form-group">
                
                <div class="col-md-12">
                  <label class="text-black" for="subject">Captcha</label> 
               <?= $form->field($komentar, 'verifyCode' ,['template' => "{input}\n{error}"])->widget(\yii\captcha\Captcha::classname(), ['options' => 
                                        ['class' => 'form-control', 'required' => 'required']

    // configure additional widget properties here
]) ?>
 </div>
              </div>
              <div class="row form-group">
                <div class="col-md-12">
                  <input type="submit" value="Send Message" class="btn btn-primary py-2 px-4 text-white">
                </div>
              </div>

  
            <?php $form = ActiveForm::end(); ?>
          </div>
          <div class="col-md-5">
            
            <div class="p-4 mb-3 bg-white">
              <p class="mb-0 font-weight-bold">Address</p>
              <p class="mb-4"><?= $contact->address?></p>

              <p class="mb-0 font-weight-bold">Phone</p>
              <p class="mb-4"><a href="#"><?= $contact->phone?></a></p>

              <p class="mb-0 font-weight-bold">Email Address</p>
              <p class="mb-0"><a href="#"><?= $contact->email?></a></p>

            </div>
            
            <div class="p-4 mb-3 bg-white">
              <img src="images/hero_bg_1.jpg" alt="Image" class="img-fluid mb-4 rounded">
              <h3 class="h5 text-black mb-3">More Info</h3>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsa ad iure porro mollitia architecto hic consequuntur. Distinctio nisi perferendis dolore, ipsa consectetur? Fugiat quaerat eos qui, libero neque sed nulla.</p>
              <p><a href="#" class="btn btn-primary px-4 py-2 text-white">Learn More</a></p>
            </div>

          </div>
        </div>
      </div>
    </div>

 