<?php 
use yii\helpers\Url;
?>
<header class="site-navbar py-1" role="banner">

      <div class="container">
        <div class="row align-items-center">
          
          <div class="col-6 col-xl-2">
           
           <a class="mb-0" href="<?=Url::toRoute(['/'])?>">
                  <img alt="Brand"  style="height:50px; width:100px;" src="<?=  Yii::getAlias('uploads/entra.jpg')?>">
      </a>
          </div>
          <div class="col-10 col-md-8 d-none d-xl-block">
            <nav class="site-navigation position-relative text-right text-lg-center" role="navigation">

              <ul class="site-menu js-clone-nav mx-auto d-none d-lg-block">
                <li >
                  <a href="<?= Url::toRoute(['/']) ?>">Home</a>
                </li>
                <li class="has-children">
                  <a href="#">Explore</a>
                  <ul class="dropdown">
                 <li><a href="<?= Url::toRoute(['accommodation']) ?>">Accommodation</a></li>
                    <li><a href="<?= Url::toRoute(['experience']) ?>">Experience</a></li>
                  
                    <li><a href="<?= Url::toRoute(['merchandise']) ?>">Merchandise</a></li>
                      <li><a href="<?= Url::toRoute(['transportation']) ?>">Transportation</a></li>
                  </ul>
                </li>
              
                <li class="active" ><a href="<?= Url::toRoute(['about']) ?>">About</a></li>
                <li><a href="<?= Url::toRoute(['blog']) ?>">Blog</a></li>
                
                <li><a href="<?= Url::toRoute(['contact']) ?>">Contact</a></li>
                <!-- <li><a href="booking.html">Book Online</a></li> -->
              </ul>
            </nav>
          </div>

          <div class="col-6 col-xl-2 text-right">
            <div class="d-none d-xl-inline-block">
              <ul class="site-menu js-clone-nav ml-auto list-unstyled d-flex text-right mb-0" data-class="social">
                <li>
                  <a href="#" class="pl-0 pr-3 text-black"><span class="icon-download"></span></a>
                </li>
                <li>
                  <a href="#" class="pl-3 pr-3 text-black"><span class="icon-twitter"></span></a>
                </li>
                <li>
                  <a href="#" class="pl-3 pr-3 text-black"><span class="icon-facebook"></span></a>
                </li>
                <li>
                  <a href="#" class="pl-3 pr-3 text-black"><span class="icon-instagram"></span></a>
                </li>
                
              </ul>
            </div>

            <div class="d-inline-block d-xl-none ml-md-0 mr-auto py-3" style="position: relative; top: 3px;"><a href="#" class="site-menu-toggle js-menu-toggle text-black"><span class="icon-menu h3"></span></a></div>

          </div>

        </div>
      </div>
      
    </header>
   

    <div class="site-mobile-menu">
      <div class="site-mobile-menu-header">
        <div class="site-mobile-menu-close mt-3">
          <span class="icon-close2 js-menu-toggle"></span>
        </div>
      </div>
      <div class="site-mobile-menu-body"></div>
    </div>
    

  

   

    <div class="site-blocks-cover inner-page-cover" style="background-image: url(images/hero_bg_2.jpg);" data-aos="fade" data-stellar-background-ratio="0.5">
        <div class="container">
          <div class="row align-items-center justify-content-center text-center">

            <div class="col-md-8" data-aos="fade-up" data-aos-delay="400">
              <h1 class="text-white font-weight-light"><?= $about->judul?></h1>
              <div> <span class="mx-2 text-white"></span> <span class="text-white"><?= $about->deskripsi?></span></div>
              
            </div>
          </div>
        </div>
      </div>  


    
<!--    <div class="site-section" data-aos="fade-up">
      <div class="container">
        <div class="row align-items-center">
          <div class="col-md-6 mb-5 mb-md-0">
            <img src="images/hero_bg_2.jpg" alt="Image" class="img-fluid rounded">
          </div>
          <div class="col-md-6 pl-md-5">
            <h2 class="font-weight-light text-black mb-4"><?= $about->judul?></h2>
            <p><?= $about->deskripsi?>.</p>

            <ul class="list-unstyled">
              <li class="d-flex align-items-center"><span class="icon-check2 text-primary h3 mr-2"></span><span>Vitae cumque eius modi expedita</span></li>
              <li class="d-flex align-items-center"><span class="icon-check2 text-primary h3 mr-2"></span><span>Totam at maxime Accusantium</span></li>
              <li class="d-flex align-items-center"><span class="icon-check2 text-primary h3 mr-2"></span><span>Distinctio temporibus</span></li>

            </ul>
          </div>
        </div>
      </div>
    </div>-->

    <div class="site-section">
      <div class="container">
         <div class="row justify-content-center mb-5" data-aos="fade-up">
          <div class="col-md-7">
            <h2 class="font-weight-light text-black text-center">Our Team</h2>
          </div>
        </div>
        <div class="row">
            <?php foreach ($team as $tm){ ?>
          <div class="col-md-6 col-lg-4 text-center mb-5" data-aos="fade-up">
            <img src="<?=  Yii::getAlias('uploads/team/'.'thumb-'.$tm->foto)?>" alt="Image" class="img-fluid w-50 rounded-circle mb-4">
            <h2 class="text-black font-weight-light mb-4"><?= $tm->nama?></h2>
             <h5><?= $tm->jabatan?></h5>
            <p class="mb-4"><?= $tm->deskripsi?>        <p>
              <a href="#" class="pl-0 pr-3"><span class="icon-twitter"></span></a>
              <a href="#" class="pl-3 pr-3"><span class="icon-instagram"></span></a>
              <a href="#" class="pl-3 pr-3"><span class="icon-facebook"></span></a>
            </p>
          </div>
            <?php } ?>
         
         
        </div>
      </div>
    </div>

    <div class="site-section block-13 bg-light">
  

    <div class="container" data-aos="fade">
        <div class="row justify-content-center mb-5">
          <div class="col-md-7">
            <h2 class="font-weight-light text-black text-center">What People Says</h2>
          </div>
        </div>

        <div class="nonloop-block-13 owl-carousel">

          <div class="item">
            <div class="container">
              <div class="row">
                <div class="col-lg-6 mb-5">
                  <img src="images/img_1.jpg" alt="Image" class="img-md-fluid">
                </div>
                <div class="overlap-left col-lg-6 bg-white p-md-5 align-self-center">
                  <p class="text-black lead">&ldquo;Lorem ipsum dolor sit amet, consectetur adipisicing elit. Similique dolorem quisquam laudantium, incidunt id laborum, tempora aliquid labore minus. Nemo maxime, veniam! Fugiat odio nam eveniet ipsam atque, corrupti porro&rdquo;</p>
                  <p class="">&mdash; <em>James Martin</em>, <a href="#">Traveler</a></p>
                </div>
              </div>
            </div>
          </div>

          <div class="item">
            <div class="container">
              <div class="row">
                <div class="col-lg-6 mb-5">
                  <img src="images/img_2.jpg" alt="Image" class="img-md-fluid">
                </div>
                <div class="overlap-left col-lg-6 bg-white p-md-5 align-self-center">
                  <p class="text-black lead">&ldquo;Lorem ipsum dolor sit amet, consectetur adipisicing elit. Similique dolorem quisquam laudantium, incidunt id laborum, tempora aliquid labore minus. Nemo maxime, veniam! Fugiat odio nam eveniet ipsam atque, corrupti porro&rdquo;</p>
                  <p class="">&mdash; <em>Clair Augustin</em>, <a href="#">Traveler</a></p>
                </div>
              </div>
            </div>
          </div>

          <div class="item">
            <div class="container">
              <div class="row">
                <div class="col-lg-6 mb-5">
                  <img src="images/img_4.jpg" alt="Image" class="img-md-fluid">
                </div>
                <div class="overlap-left col-lg-6 bg-white p-md-5 align-self-center">
                  <p class="text-black lead">&ldquo;Lorem ipsum dolor sit amet, consectetur adipisicing elit. Similique dolorem quisquam laudantium, incidunt id laborum, tempora aliquid labore minus. Nemo maxime, veniam! Fugiat odio nam eveniet ipsam atque, corrupti porro&rdquo;</p>
                  <p class="">&mdash; <em>James Martin</em>, <a href="#">Traveler</a></p>
                </div>
              </div>
            </div>
          </div>

        </div>
      </div>
    </div>

    
