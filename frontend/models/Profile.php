<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "profile".
 *
 * @property integer $user_id
 * @property string $name
 * @property string $location
 * @property string $bio
 * @property string $tempat_lahir
 * @property string $nik
 * @property string $no_hp
 * @property string $tinggi_badan
 * @property string $berat_badan
 * @property string $jenis_kelamin 
 * @property string $kota
 * @property integer $agama
 * @property integer $jenjang_pendidikan
 * @property integer $tahun_lulus
 *
 * @property User $user
 * @property TbAgama $agama0 
 * @property Regencies $kota0
 * @property ProfileInt $profileInt
 */
class Profile extends \yii\db\ActiveRecord {

 
    public $foto_profil;
    public $email;
    public $password;

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'profile';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
                [[ 'user_id', 'name'], 'required', 'on' => 'require-profil'],
//                ['foto_profil', 'required', 'when' => function($model) {
//                    return $model->user->photo === null;
//                }],
            ['email', 'unique', 'targetClass' => User::className(), 'filter' => function ($query) {
                    if (!$this->isNewRecord) {
                        $query->andWhere(['not', ['email' => $this->email]]);
              }
                }],
               
                [['password'], 'string', 'min' => 6, 'max' => 6],

               
                [['user_id'], 'required'],
                [['user_id'], 'integer'],
                [['bio', ], 'safe'],
                [['name', 'location', ], 'string', 'max' => 255],
               
                [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
              
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'user_id' => 'User ID',
            'name' => 'Nama',
            'location' => 'Alamat',
            'bio' => 'Tanggal Lahir',
           
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser() {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

   
}
