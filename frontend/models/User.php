<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "{{%user}}".
 *
 * @property integer $id
 * @property string $username
 * @property string $auth_key
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $photo
 * @property string $registration_ip
 * @property integer $last_login_at
 * @property integer $confirmed_at
 * @property integer $blocked_at

 
 */
class User extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username', 'auth_key', 'password_hash', 'email', 'created_at', 'updated_at'], 'required'],
            [['status', 'created_at', 'updated_at', 'last_login_at', 'confirmed_at', 'blocked_at'], 'integer'],
            [['username', 'password_hash', 'password_reset_token', 'email', 'photo', 'registration_ip'], 'string', 'max' => 255],
            [['auth_key'], 'string', 'max' => 32],
            [['username'], 'unique'],
            [['email'], 'unique'],
            [['password_reset_token'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'auth_key' => 'Auth Key',
            'password_hash' => 'Password Hash',
            'password_reset_token' => 'Password Reset Token',
            'email' => 'Email',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'photo' => 'Photo',
            'registration_ip' => 'Registration Ip',
            'last_login_at' => 'Last Login At',
            'confirmed_at' => 'Confirmed At',
            'blocked_at' => 'Blocked At',
            'jenis_user' => 'Jenis User',
            'pin' => 'Pin',
        ];
    }


   

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfile()
    {
        return $this->hasOne(Profile::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserPasswordLogs()
    {
        return $this->hasMany(UserPasswordLog::className(), ['id_user' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserPasswordSalahLogs()
    {
        return $this->hasMany(UserPasswordSalahLog::className(), ['id_user' => 'id']);
    }

   
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserRequirements()
    {
        return $this->hasMany(UserRequirement::className(), ['id_user' => 'id']);
    }
}
