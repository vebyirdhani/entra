<?php

$params = array_merge(
        require(__DIR__ . '/../../common/config/params.php'), require(__DIR__ . '/../../common/config/params-local.php'), require(__DIR__ . '/params.php'), require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-frontend',
    'name' => '',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'timeZone' => 'Asia/Jakarta',
    'controllerNamespace' => 'frontend\controllers',
     'modules' => [
        'api' => [
            'class' => 'frontend\Api',
        ],
    ],
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-frontend',
        ],
//        'user' => [
//            'identityClass' => 'common\models\User',
//            'enableAutoLogin' => false,
//            'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
//        ],
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'name' => 'advanced-frontend-7syn',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                  '/index'=>'site/index',
                  '/contact'=>'/site/contact',
                '/about'=>'/site/about',
               '/accommodation'=>'/site/accommodation',
                '/experience'=>'/site/experience',
                '/transportation'=>'/site/transportation',
                '/merchandise'=>'/site/merchandise',
                 '/blog'=>'/site/blog',
                 '/detail-blog'=>'/site/detail-blog',
             
            ],
        ],
      



    ],
    'params' => $params,
];
