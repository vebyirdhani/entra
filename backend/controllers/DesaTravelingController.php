<?php

namespace backend\controllers;

use Yii;
use common\models\DesaTraveling;
use common\models\DesaTravelingSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\imagine\Image;
use yii\filters\AccessControl;
/**
 * DesaTravelingController implements the CRUD actions for DesaTraveling model.
 */
class DesaTravelingController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
                    'access'=>[
                'class'=>AccessControl::className(),
                'rules'=>[
                    [
                      
                        'allow'=>true,
                          'roles'=>['@'],
                        'matchCallback'=>function(){
                            return (
                                Yii::$app->user->can('superAdmin')
                            );
                        }
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all DesaTraveling models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new \common\models\VillagesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    public function actionDataDesa()
    {
        $searchModel = new DesaTravelingSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('data-desa', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single DesaTraveling model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new DesaTraveling model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id)
    {
        $cari = \common\models\Villages::find()->where(['id'=>$id])->one();
        $model = new DesaTraveling();

      if ($cari){
          $model->id_desa = $cari->id;
          $model->nama = $cari->name;
          $model->id_kecamatan= $cari->district_id;
          if ($cari->provinsi){
               $caripulau = \common\models\Provinces::find()->where(['id'=>$cari->provinsi])->one();
         
          $model->id_pulau = $caripulau->island_id;
          $model->id_provinsi = $cari->provinsi;
          }else{
              $caridata = \common\models\Districts::find()->where(['id'=>$cari->district_id])->one();
              $carikota = \common\models\Regencies::find()->where(['id'=>$caridata->regency_id])->one();
              $cariprovinsi = \common\models\Provinces::find()->where(['id'=>$carikota->province_id ])->one();
              
              $model->id_provinsi = $cariprovinsi->id;
              $model->id_pulau= $cariprovinsi->island_id;
          }
         
          
        if ($model->load(Yii::$app->request->post())) {
            $foto = new \common\models\FotoDesatraveling();
            $model2 = new DesaTraveling();

            $model2->picture = UploadedFile::getInstances($model, 'picture');


            if ($model2->picture) {
                $desa = new DesaTraveling();
                $desa->id_desa = $model->id_desa;
                $desa->id_kecamatan = $model->id_kecamatan;
                $desa->id_provinsi = $model->id_provinsi;
                $desa->id_pulau = $model->id_pulau;
                $desa->rating = $model->rating;
                $desa->nama = $model->nama;
                $desa->deskripsi = $model->deskripsi;
                $desa->alamat = $model->alamat;
              
               
              
                $desa->save(false);
               
                $tampungdata = $desa->id;
              
                foreach ($model2->picture as $file) {
                    $img = new \common\models\FotoDesatraveling();
                   
                    $fileName = time() . $file->baseName . '.' . $file->getExtension();

                    
                    if ($file->saveAs(('../uploads/desa/') . $fileName)) {
                        $val = Image::thumbnail(('../uploads/desa/') . $fileName, 320, 320)
                                ->save(('../uploads/desa/') . $fileName, ['quality' => 50]);
                        if ($val) {
                          
                            $img->foto = $fileName;
                            $img->id_desa = $tampungdata;
                         
                            $img->save(false);
                        }
                    }
                }
            } else {
                $model->save(false);
            }
               Yii::$app->session->setFlash('success', 'Save!');
            return $this->redirect(['view','id'=>$model->id]);
        }else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
      }else{
             Yii::$app->session->setFlash('error', 'Data tidak ditemukan');
           return $this->redirect(['index',]);
      }
    }

    /**
     * Updates an existing Koleksi model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
           $model = $this->findModel($id);
        
        if ($model->load(Yii::$app->request->post())) {
           $foto = new \common\models\FotoDesatraveling();
            $model2 = new DesaTraveling();

            $model2->picture = UploadedFile::getInstances($model, 'picture');


            if ($model2->picture) {
                Yii::$app->db->createCommand("DELETE FROM foto_desatraveling WHERE id_desa = '$id' ")->execute();
              
                $model->save(false);
               
              $tampungdata = $model->id;
                
                foreach ($model2->picture as $file) {
                    $img = new \common\models\FotoDesatraveling();

                    $fileName = time() . $file->baseName . '.' . $file->getExtension();

                    
                    if ($file->saveAs(('../uploads/desa/') . $fileName)) {
                        $val = Image::thumbnail(('../uploads/desa/') . $fileName, 320, 320)
                                ->save(('../uploads/desa/') . 'thumb-' . $fileName, ['quality' => 50]);
                        if ($val) {
                         
                            $img->foto = $fileName;
                            $img->id_desa = $tampungdata;
                         
                            $img->save(false);
                        }
                    }
                }
            } else {
               // Yii::$app->db->createCommand("UPDATE foto_koleksi SET date = '$model->date' WHERE id_email = '$id' ")->execute();
               
               $model->save();
               
               
                
            }
              Yii::$app->session->setFlash('success', 'Save!');
             return $this->redirect(['view','id'=>$model->id]);
          
        } else {
            return $this->render('update', [
                        'model' => $model,
            ]);
        }
    }


    /**
     * Deletes an existing DesaTraveling model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the DesaTraveling model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return DesaTraveling the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = DesaTraveling::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
