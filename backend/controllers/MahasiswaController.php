<?php

namespace backend\controllers;

use Yii;
use common\models\Mahasiswa;
use common\models\MahasiswaSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use common\models\CsvForm;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\imagine\Image;
use yii\base\DynamicModel;
/**
 * MahasiswaController implements the CRUD actions for Mahasiswa model.
 */
class MahasiswaController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
              'access'=>[
                'class'=>AccessControl::className(),
                'rules'=>[
                    [
                      
                        'allow'=>true,
                          'roles'=>['@'],
                        'matchCallback'=>function(){
                            return (
                                Yii::$app->user->identity->level== 1 
                            );
                        }
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Mahasiswa models.
     * @return mixed
     */
    public function actionIndex()
    {
          $model = new DynamicModel(['nama']);
        $model->addRule('nama', 'safe');
      
     
        $mhs = Mahasiswa::find();
              

     

          if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $mhs->andFilterWhere(['like', 'nama', $model->nama])
                    ->orFilterWhere(['like', 'nim', $model->nama]);
                   
        }

     
        $dataProvider = new ActiveDataProvider([
            'query' => $mhs,
            'pagination' => [
                'pagesize' => 20
            ]
        ]);
           return $this->render('index', [
                    'dataProvider' => $dataProvider,
                  
                    'model' => $model,
        ]);
    }

    /**
     * Displays a single Mahasiswa model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Mahasiswa model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Mahasiswa();
        $mhs = Mahasiswa::find()->orderBy('id_mahasiswa DESC');
            $dataProvider = new ActiveDataProvider([
            'query' =>$mhs,
            'pagination' => [
                'pagesize' => 10
            ]
        ]);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
                Yii::$app->session->setFlash('success', 'Data berhasil di inputkan . . !');
            return $this->redirect(['create']);
        } else {
            return $this->render('create', [
                'model' => $model,
                  'dataProvider' => $dataProvider,
            ]);
        }
    }
    
      public function actionPdf($id) {
      
        $liat = Mahasiswa::find()->where(['id_mahasiswa'=>$id])->one();


        if ($id) {

        
            return $this->renderPartial('pdf', [
                        'liat' => $liat,
                       
                        'id' => $id,
                   
            ]);
        } else {

            Yii::$app->session->setFlash('warning', '<b>' . 'Data tidak ditemukan' . '</b>');

            return $this->redirect(['index']);
        }
    }

    /**
     * Updates an existing Mahasiswa model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_mahasiswa]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

        public function actionImport()
    {
        $model = new CsvForm();
        if($model->load(Yii::$app->request->post())){
            $file = UploadedFile::getInstance($model,'file');
            $filename = 'Data'.date('dmYhis').'.'.$file->extension;
            $upload = $file->saveAs(Yii::getAlias('./excel/dosen/') .$filename);
            
            if($upload){
                define('CSV_PATH','excel/dosen/');
                $csv_file = CSV_PATH . $filename;
                $filecsv = file($csv_file);
//                 print_r($filecsv);
//                 die();
                foreach($filecsv as $data){

                      $hasil = explode(",",$data);

                        $mahasiswa = new Mahasiswa();
                        $mahasiswa->nim = $hasil[0];
                        $mahasiswa->nama = $hasil[1];
                      
                         $mahasiswa->prodi =$hasil[2];
                       
                         $mahasiswa->angkatan = $hasil[3];
                        
                        $cekmahasiswa = Mahasiswa::find()->where(['nim' => $mahasiswa->nim])->one();
                        if (!$cekmahasiswa) {
                         
                            $mahasiswa->save(false);
                        }  
                }
                Yii::$app->session->setFlash('success', 'Import Berhasil . . !');
                return $this->redirect(['index']);
            }
        }else{
            return $this->render('import',[
                'model'=>$model
            ]);
        }
    }
    
    public function actionUpload($id)
    {
        $model = $this->findModel($id);
       
        if ($model->load(Yii::$app->request->post())) {

              $model->picture = UploadedFile::getInstance($model, 'picture');

            if ($model->picture){
                
                $foto = time()  .  rand() . '.' . $model->picture->extension;

//                $update = new Mahasiswa();
//             
//                $update->foto = $foto;
//               
//                $model->created_at = strtotime($update->created_at);
//                $model->foto = $update->foto;
//                $model->save();

               if ($model->picture->saveAs(Yii::getAlias('../uploads/mahasiswa/') . $foto)) {
                    $val = Image::thumbnail(Yii::getAlias('../uploads/mahasiswa/') . $foto, 320, 320)
                          ->save(Yii::getAlias('../uploads/mahasiswa/') . 'thumb-' . $foto, ['quality' => 50]);
                            
                    if ($val) {
                        $model->foto = $foto;
                        $model->save(false);                
                        }
                }
                 Yii::$app->session->setFlash('success', ' Berhasil diperbaharui');

                return $this->redirect(['index']);
            }
Yii::$app->session->setFlash('success', 'Data berita berhasil diperbaharui');
            return $this->redirect(['index']);
        } else {
            return $this->render('upload', [
                'model' => $model,
            ]);
        }
    }
    /**
     * Deletes an existing Mahasiswa model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Mahasiswa model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Mahasiswa the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Mahasiswa::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
