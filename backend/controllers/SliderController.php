<?php

namespace backend\controllers;

use Yii;
use common\models\Slider;
use common\models\SliderSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\imagine\Image;
use yii\filters\AccessControl;
/**
 * SliderController implements the CRUD actions for Slider model.
 */
class SliderController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
                              'access'=>[
                'class'=>AccessControl::className(),
                'rules'=>[
                    [
                      
                        'allow'=>true,
                          'roles'=>['@'],
                        'matchCallback'=>function(){
                            return (
                                Yii::$app->user->can('superAdmin')
                            );
                        }
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Slider models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SliderSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Slider model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Slider model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
       public function actionCreate()
    {
        $model = new Slider();
        
        
        if ($model->load(Yii::$app->request->post())) {

            $model->picture = UploadedFile::getInstance($model, 'picture');

            if ($model->picture){
             $slider =  new Slider();
               
                $foto = time()  . $model->foto .  rand() . '.' . $model->picture->extension;
                 
                 $slider->judul=$model->judul;
                 $slider->deskripsi=$model->deskripsi;
              
                 $slider->status = $model->status;
                 $slider->save();

               
                if ($model->picture->saveAs(Yii::getAlias('../uploads/slider/') . $foto)) {
                    $val = Image::thumbnail(Yii::getAlias('../uploads/slider/') . $foto, 320, 320)
                            ->save(Yii::getAlias('../uploads/slider/') . 'thumb-' . $foto, ['quality' => 50]);
                    if ($val) {
                        $slider->foto = $foto;
                        $slider->save();
                    }
                }
            Yii::$app->session->setFlash('success', 'Data berita berhasil ditambahkan');
                return $this->redirect(['index',]);
            }else{
                 $model->save();
            }
         Yii::$app->session->setFlash('success', 'Data berita berhasil ditambahkan');
       
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }


    /**
     * Updates an existing Slider model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
     public function actionUpdate($id)
    {
        $model = $this->findModel($id);
       
        if ($model->load(Yii::$app->request->post())) {

              $model->picture = UploadedFile::getInstance($model, 'picture');

            if ($model->picture){
                
                $foto = time()  .  rand() . '.' . $model->picture->extension;
//                print_r($foto);die();
                $update = new Slider();
             
             
                $update->foto = $foto;
               
                $model->foto = $update->foto;
                $model->save();

               if ($model->picture->saveAs(Yii::getAlias('../uploads/slider/') . $foto)) {
                    $val = Image::thumbnail(Yii::getAlias('../uploads/slider/') . $foto, 320, 320)
                          ->save(Yii::getAlias('../uploads/slider/') . 'thumb-' . $foto, ['quality' => 50]);
                            

                }
                 Yii::$app->session->setFlash('success', ' Berhasil diperbaharui');

                return $this->redirect(['index',]);
            }else{
             
                $model->save(false);
            }
Yii::$app->session->setFlash('success', 'Data berita berhasil diperbaharui');
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Slider model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Slider model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Slider the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Slider::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
