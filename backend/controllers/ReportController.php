<?php

namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use kartik\mpdf\Pdf;

/**
 * KaryawanController implements the CRUD actions for ExKaryawan model.
 */
class ReportController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['superAdmin', 'admin'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all ExKaryawan models.
     * @return mixed
     */
    public function actionIndex($id) {
        $project = \common\models\DaftarProject::find()->where(['id_project' => $id])->one();
        return $this->render('index', [
                    'project' => $project,
        ]);
    }

    public function actionPengeluaran($id = NULL) {
        $project = \common\models\DaftarProject::find()->where(['id_project' => $id])->one();
        return $this->render('pengeluaran', [
                    'project' => $project,
        ]);
    }

    public function actionPendapatan($id = NULL) {
        if ($id) {
            $project = \common\models\DaftarProject::find()->where(['id_project' => $id])->one();
            $all = \common\models\DataPelanggan::find()->where(['project' => $id])->andWhere(['status' => 'Sukses'])->sum('total');
            $searchModel = new \common\models\DataPelangganSearch();
            $searchModel->project = $id;
            $searchModel->status = 'Sukses';
            $params = \Yii::$app->request->queryParams;

            $tglAwal = \Yii::$app->request->get('tglAwal');
            $tglAkhir = \Yii::$app->request->get('tglAkhir');



            $params['tglAwal'] = $tglAwal;
            $params['tglAkhir'] = $tglAkhir;

            $dataProvider = $searchModel->searchReport($params);
            if ($tglAwal && $tglAkhir) {
                $project = \common\models\DaftarProject::find()->where(['id_project' => $id])->one();

                $all = \common\models\DataPelanggan::find()->where(['project' => $id])->andWhere(['status' => 'Sukses'])->andWhere(['between', 'tanggal_akad', $params['tglAwal'], $params['tglAkhir']])->sum('total');
                return $this->render('pendapataan', [
                            'searchModel' => $searchModel,
                            'dataProvider' => $dataProvider,
                            'id' => $id,
                            'project' => $project,
                            'tglAwal' => $tglAwal,
                            'tglAkhir' => $tglAkhir,
                            'all' => $all,
                ]);
            }
            return $this->render('pendapatan', [
                        'searchModel' => $searchModel,
                        'dataProvider' => $dataProvider,
                        'tglAwal' => $tglAwal,
                        'tglAkhir' => $tglAkhir,
                        'project' => $project,
                        'id' => $id,
                        'all' => $all,
            ]);
        }
    }

    public function actionDataPengeluaran($id = NULL, $nama = NULL, $tglAwal = NULL, $tglAkhir = NULL) {
        if ($id && $nama) {
            $searchModel = new \common\models\DataPengeluaranSearch();
            $searchModel->project = $id;
            $searchModel->jenis_pengeluaran = $nama;
            $all = \common\models\DataPengeluaran::find()->where(['project' => $id])->andWhere(['jenis_pengeluaran' => $nama])->sum('harga');
            $params = \Yii::$app->request->queryParams;

            $tglAwal = \Yii::$app->request->get('tglAwal');
            $tglAkhir = \Yii::$app->request->get('tglAkhir');



            $params['tglAwal'] = $tglAwal;
            $params['tglAkhir'] = $tglAkhir;

            $dataProvider = $searchModel->searchReport($params);
            if ($tglAwal && $tglAkhir) {


                $all = \common\models\DataPengeluaran::find()->where(['project' => $id])->andWhere(['jenis_pengeluaran' => $nama])->andWhere(['between', 'tanggal', $params['tglAwal'], $params['tglAkhir']])->sum('harga');
                return $this->render('data-pengeluaraan', [
                            'searchModel' => $searchModel,
                            'dataProvider' => $dataProvider,
                            'id' => $id,
                            'nama' => $nama,
                            'tglAwal' => $tglAwal,
                            'tglAkhir' => $tglAkhir,
                            'all' => $all,
                ]);
            }
            return $this->render('data-pengeluaran', [
                        'searchModel' => $searchModel,
                        'dataProvider' => $dataProvider,
                        'id' => $id,
                        'nama' => $nama,
                        'tglAwal' => $tglAwal,
                        'tglAkhir' => $tglAkhir,
                        'all' => $all,
            ]);
        }
    }

    public function actionLabarugi($id) {

        if ($id) {
            $project = \common\models\DaftarProject::find()->where(['id_project' => $id])->one();
            $searchModel = new \common\models\KeuanganSearch();
            $searchModel->project = $id;
            $params = \Yii::$app->request->queryParams;
            $pendapatan = \common\models\Keuangan::find()->where(['project' => $id])->andWhere(['keterangan' => 'Pendapatan'])->sum('harga');
            $pengeluaran = \common\models\Keuangan::find()->where(['project' => $id])->andWhere(['keterangan' => 'Pengeluaran'])->sum('harga');
            $total = $pendapatan - $pengeluaran;
            $tglAwal = \Yii::$app->request->get('tglAwal');
            $tglAkhir = \Yii::$app->request->get('tglAkhir');



            $params['tglAwal'] = $tglAwal;
            $params['tglAkhir'] = $tglAkhir;

            $dataProvider = $searchModel->searchReport($params);
            if ($tglAwal && $tglAkhir) {
                $pendapatan = \common\models\Keuangan::find()->where(['project' => $id])->andWhere(['keterangan' => 'Pendapatan'])->andWhere(['between', 'created_at', $params['tglAwal'], $params['tglAkhir']])->sum('harga');
                $pengeluaran = \common\models\Keuangan::find()->where(['project' => $id])->andWhere(['keterangan' => 'Pengeluaran'])->andWhere(['between', 'created_at', $params['tglAwal'], $params['tglAkhir']])->sum('harga');
                
                $total = $pendapatan - $pengeluaran;

                return $this->render('labarugii', [
                            'searchModel' => $searchModel,
                            'dataProvider' => $dataProvider,
                            'id' => $id,
                            'project' => $project,
                            'tglAwal' => $tglAwal,
                            'tglAkhir' => $tglAkhir,
                            'total' => $total,
                            'pengeluaran' => $pengeluaran,
                            'pendapatan' => $pendapatan,
                ]);
            }

            return $this->render('labarugi', [
                        'searchModel' => $searchModel,
                        'dataProvider' => $dataProvider,
                        'id' => $id,
                        'tglAwal' => $tglAwal,
                        'tglAkhir' => $tglAkhir,
                        'project' => $project,
                        'total' => $total,
                        'pengeluaran' => $pengeluaran,
                        'pendapatan' => $pendapatan,
            ]);
        }
    }

    public function actionPdfpendapatan($id = NULL) {
        $tglAwal = \Yii::$app->request->get('tglAwal');
        $tglAkhir = \Yii::$app->request->get('tglAkhir');

        $liat = \common\models\DataPelanggan::viewpelanggan($tglAwal, $tglAkhir, $id);


        if ($tglAwal != NULL && $tglAkhir != NULL) {
            $data = \common\models\DataPelanggan::find()->where(['between', 'tanggal_akad', $tglAwal, $tglAkhir])->one();
            $all = \common\models\DataPelanggan::find()->where(['project' => $id])->andWhere(['status' => 'Sukses'])->andWhere(['between', 'tanggal_akad', $tglAwal, $tglAkhir])->sum('total');

            return $this->renderPartial('cetaklaporan', [
                        'liat' => $liat,
                        'tglAwal' => $tglAwal,
                        'tglAkhir' => $tglAkhir,
                        'data' => $data,
                        'all' => $all,
            ]);
        } else {

            Yii::$app->session->setFlash('warning', '<b>' . 'MASUKKAN TANGGAL DAN KLIK SEARCH UNTUK MELAKUKAN CETAK LAPORAN' . '</b>');

            return $this->redirect(['pendapatan', 'id' => $id]);
        }
    }

    public function actionPdfpengeluaran($id = NULL, $nama = NULL) {
        $tglAwal = \Yii::$app->request->get('tglAwal');
        $tglAkhir = \Yii::$app->request->get('tglAkhir');

        $liat = \common\models\DataPengeluaran::viewpengeluaran($tglAwal, $tglAkhir, $id, $nama);


        if ($tglAwal != NULL && $tglAkhir != NULL) {

            $all = \common\models\DataPengeluaran::find()->where(['project' => $id])->andWhere(['jenis_pengeluaran' => $nama])->andWhere(['between', 'tanggal', $tglAwal, $tglAkhir])->sum('harga');

            return $this->renderPartial('cetakpengeluaran', [
                        'liat' => $liat,
                        'tglAwal' => $tglAwal,
                        'tglAkhir' => $tglAkhir,
                        'all' => $all,
                        'id' => $id,
                        'nama' => $nama,
            ]);
        } else {

            Yii::$app->session->setFlash('warning', '<b>' . 'MASUKKAN TANGGAL DAN KLIK SEARCH UNTUK MELAKUKAN CETAK LAPORAN' . '</b>');

            return $this->redirect(['pengeluaran', 'id' => $id, 'nama' => $nama]);
        }
    }

    function actionPdflabarugi($id = NULL) {
        $tglAwal = \Yii::$app->request->get('tglAwal');
        $tglAkhir = \Yii::$app->request->get('tglAkhir');

        $liat = \common\models\Keuangan::viewlabarugi($tglAwal, $tglAkhir, $id);


        if ($tglAwal != NULL && $tglAkhir != NULL) {

            $pendapatan = \common\models\Keuangan::find()->where(['project' => $id])->andWhere(['Keterangan' => 'Pendapatan'])->andWhere(['between', 'created_at', $tglAwal, $tglAkhir])->sum('harga');
            $pengeluaran = \common\models\Keuangan::find()->where(['project' => $id])->andWhere(['Keterangan' => 'Pengeluaran'])->andWhere(['between', 'created_at', $tglAwal, $tglAkhir])->sum('harga');
            $total = $pendapatan - $pengeluaran;
            return $this->renderPartial('cetaklabarugi', [
                        'liat' => $liat,
                        'tglAwal' => $tglAwal,
                        'tglAkhir' => $tglAkhir,
                        'id' => $id,
                        'pendapatan' => $pendapatan,
                        'pengeluaran' => $pengeluaran,
                        'total' => $total,
            ]);
        } else {

            Yii::$app->session->setFlash('warning', '<b>' . 'MASUKKAN TANGGAL DAN KLIK SEARCH UNTUK MELAKUKAN CETAK LAPORAN' . '</b>');

            return $this->redirect(['labarugi', 'id' => $id]);
        }
    }

}
