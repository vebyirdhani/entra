<?php

namespace backend\controllers;

use Yii;
use common\models\Kategori;
use common\models\KategoriSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\imagine\Image;
use yii\filters\AccessControl;
/**
 * KategoriController implements the CRUD actions for Kategori model.
 */
class KategoriController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
                           'access'=>[
                'class'=>AccessControl::className(),
                'rules'=>[
                    [
                      
                        'allow'=>true,
                          'roles'=>['@'],
                        'matchCallback'=>function(){
                            return (
                                Yii::$app->user->can('superAdmin')
                            );
                        }
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Kategori models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new KategoriSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Kategori model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Kategori model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new Kategori();
    
        if ($model->load(Yii::$app->request->post())) {

            $model->picture = UploadedFile::getInstance($model, 'picture');

            if ($model->picture) {
                $kategori = new Kategori();

                $foto = time() . $model->foto . rand() . '.' . $model->picture->extension;

                $kategori->kategori = $model->kategori;
                 $kategori->status = $model->status;
                $kategori->save();


                if ($model->picture->saveAs(Yii::getAlias('../uploads/kategori/') . $foto)) {
                    $val = Image::thumbnail(Yii::getAlias('../uploads/kategori/') . $foto, 320, 320)
                            ->save(Yii::getAlias('../uploads/kategori/') . 'thumb-' . $foto, ['quality' => 50]);
                    if ($val) {
                        $kategori->foto = $foto;
                        $kategori->save();
                    }
                }
                Yii::$app->session->setFlash('success', 'Data  berhasil ditambahkan');
                return $this->redirect(['index',]);
            } else {
               $model->save(false);
            }
            Yii::$app->session->setFlash('success', 'Data  berhasil ditambahkan');

            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Berita model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);
      
        if ($model->load(Yii::$app->request->post())) {

            $model->picture = UploadedFile::getInstance($model, 'picture');

            if ($model->picture) {

                $foto = time() . rand() . '.' . $model->picture->extension;

                $update = new Kategori();

             
                $update->foto = $foto;

                $model->foto = $update->foto;
                $model->save();

                if ($model->picture->saveAs(Yii::getAlias('../uploads/kategori/') . $foto)) {
                    $val = Image::thumbnail(Yii::getAlias('../uploads/kategori/') . $foto, 320, 320)
                            ->save(Yii::getAlias('../uploads/kategori/') . 'thumb-' . $foto, ['quality' => 50]);


                }
                Yii::$app->session->setFlash('success', ' Berhasil diperbaharui');

                return $this->redirect(['index']);
            } else {
              
                $model->save();
            }
            Yii::$app->session->setFlash('success', 'Data berhasil diperbaharui');
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Kategori model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Kategori model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Kategori the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Kategori::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
