<?php

namespace backend\controllers;

use Yii;
use common\models\Team;
use common\models\TeamSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\imagine\Image;
use yii\filters\AccessControl;
/**
 * TeamController implements the CRUD actions for Team model.
 */
class TeamController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
                           'access'=>[
                'class'=>AccessControl::className(),
                'rules'=>[
                    [
                      
                        'allow'=>true,
                          'roles'=>['@'],
                        'matchCallback'=>function(){
                            return (
                                Yii::$app->user->can('superAdmin')
                            );
                        }
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Team models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TeamSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Team model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Team model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
   public function actionCreate()
    {
        $model = new Team();
      
        if ($model->load(Yii::$app->request->post())) {

            $model->picture = UploadedFile::getInstance($model, 'picture');

            if ($model->picture){
             $team =  new Team();
               
                $foto = time()  . $model->foto .  rand() . '.' . $model->picture->extension;
                 
                 $team->nama=$model->nama;
                 $team->jabatan=$model->jabatan;
               
                 $team->deskripsi = $model->deskripsi;
                 $team->twitter=$model->twitter;
                 $team->instagram = $model->instagram;
                 $team->facebook= $model->facebook;
                 
                 $team->save();

               
                if ($model->picture->saveAs(Yii::getAlias('../uploads/team/') . $foto)) {
                    $val = Image::thumbnail(Yii::getAlias('../uploads/team/') . $foto, 320, 320)
                            ->save(Yii::getAlias('../uploads/team/') . 'thumb-' . $foto, ['quality' => 50]);
                    if ($val) {
                        $team->foto = $foto;
                        $team->save();
                    }
                }
            Yii::$app->session->setFlash('success', 'Data berita berhasil ditambahkan');
                return $this->redirect(['index',]);
            }else{
                $model->save(false);
            }
         Yii::$app->session->setFlash('success', 'Data berita berhasil ditambahkan');
       
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Berita model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
       
        if ($model->load(Yii::$app->request->post())) {

              $model->picture = UploadedFile::getInstance($model, 'picture');

            if ($model->picture){
                
                $foto = time()  .  rand() . '.' . $model->picture->extension;
//                print_r($foto);die();
                $update = new Team();
             
              
                $update->foto = $foto;
               
            
                $model->foto = $update->foto;
                $model->save();

               if ($model->picture->saveAs(Yii::getAlias('../uploads/team/') . $foto)) {
                    $val = Image::thumbnail(Yii::getAlias('../uploads/team/') . $foto, 320, 320)
                          ->save(Yii::getAlias('../uploads/team/') . 'thumb-' . $foto, ['quality' => 50]);
                            
//                    if ($val) {
//                        $model->foto = $update->foto;
//                        $model->save();                
//                        }
                }
                 Yii::$app->session->setFlash('success', ' Berhasil diperbaharui');

                return $this->redirect(['view','id'=>$model->id_team]);
            }else{
              
                $model->save();
            }
Yii::$app->session->setFlash('success', 'Data berita berhasil diperbaharui');
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }


    /**
     * Deletes an existing Team model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Team model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Team the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Team::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
