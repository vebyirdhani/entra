<?php

namespace backend\controllers;

use Yii;
use common\models\JadwalSeminar;
use common\models\JadwalSeminarSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
/**
 * JadwalSeminarController implements the CRUD actions for JadwalSeminar model.
 */
class JadwalSeminarController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
               'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function() {
                            return (
                                    Yii::$app->user->identity->level == 1
                                    );
                        }
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all JadwalSeminar models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new JadwalSeminarSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single JadwalSeminar model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new JadwalSeminar model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new JadwalSeminar();
           $jadwal = JadwalSeminar::find()->orderBy('id_jadwal DESC');
            $dataProvider = new ActiveDataProvider([
            'query' =>$jadwal,
            'pagination' => [
                'pagesize' => 10
            ]
        ]);
           $model->status =1;
        if ($model->load(Yii::$app->request->post()) && $model->save(false)) {
              Yii::$app->session->setFlash('success', 'Data berhasil di inputkan');
			  if ($model->kategori==1 || $model->kategori==3){
				    return $this->redirect(['data-seminar/tambah-data','id'=>$model->id_jadwal]);
			  }else if ($model->kategori==2 || $model->kategori==4){
				    return $this->redirect(['data-seminar/tambah-datasatu','id'=>$model->id_jadwal]);
			  }
          
        } else {
           return $this->render('create', [
                        'model' => $model,
                'dataProvider'=>$dataProvider,
            ]);
        }
    }

    /**
     * Updates an existing JadwalSeminar model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_jadwal]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing JadwalSeminar model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the JadwalSeminar model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return JadwalSeminar the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = JadwalSeminar::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
