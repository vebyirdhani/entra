<?php

namespace backend\controllers;

use Yii;
use common\models\Fitur;
use common\models\FiturSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\UploadedFile;
use yii\imagine\Image;

/**
 * FiturController implements the CRUD actions for Fitur model.
 */
class FiturController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
                              'access'=>[
                'class'=>AccessControl::className(),
                'rules'=>[
                    [
                      
                        'allow'=>true,
                          'roles'=>['@'],
                        'matchCallback'=>function(){
                            return (
                                Yii::$app->user->can('superAdmin')
                            );
                        }
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Fitur models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new FiturSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Fitur model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Fitur model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
      public function actionCreate()
    {
        $model = new Fitur();
      
        if ($model->load(Yii::$app->request->post())) {

            $model->picture = UploadedFile::getInstance($model, 'picture');

            if ($model->picture){
             $fitur =  new Berita();
               
                $foto = time()  . $model->foto .  rand() . '.' . $model->picture->extension;
                 
                 $fitur->judul=$model->judul;
                 $fitur->deskripsi=$model->deskripsi;
               
                 $fitur->save();

               
                if ($model->picture->saveAs(Yii::getAlias('../uploads/fitur/') . $foto)) {
                    $val = Image::thumbnail(Yii::getAlias('../uploads/fitur/') . $foto, 320, 320)
                            ->save(Yii::getAlias('../uploads/fitur/') . 'thumb-' . $foto, ['quality' => 50]);
                    if ($val) {
                        $fitur->icon = $foto;
                        $fitur->save();
                    }
                }
            Yii::$app->session->setFlash('success', 'Data berhasil ditambahkan');
                return $this->redirect(['index',]);
            }else{
                $model->save();
            }
         Yii::$app->session->setFlash('success', 'Data  berhasil ditambahkan');
       
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Berita model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        
        if ($model->load(Yii::$app->request->post())) {

              $model->picture = UploadedFile::getInstance($model, 'picture');

            if ($model->picture){
                
                $foto = time()  .  rand() . '.' . $model->picture->extension;
//                print_r($foto);die();
                $update = new Fitur();
             
                $update->icon = $foto;
               
           
                $model->icon = $update->icon;
                $model->save();

               if ($model->picture->saveAs(Yii::getAlias('../uploads/fitur/') . $foto)) {
                    $val = Image::thumbnail(Yii::getAlias('../uploads/fitur/') . $foto, 320, 320)
                          ->save(Yii::getAlias('../uploads/fitur/') . 'thumb-' . $foto, ['quality' => 50]);
                   
                }
                 Yii::$app->session->setFlash('success', ' Berhasil diperbaharui');

                return $this->redirect(['index']);
            }else{
             
                $model->save();
            }
Yii::$app->session->setFlash('success', 'Data  berhasil diperbaharui');
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Fitur model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Fitur model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Fitur the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Fitur::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
