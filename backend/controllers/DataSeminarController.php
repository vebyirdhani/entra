<?php

namespace backend\controllers;

use Yii;
use common\models\DataSeminar;
use common\models\DataSeminarSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\base\DynamicModel;
/**
 * DataSeminarController implements the CRUD actions for DataSeminar model.
 */
class DataSeminarController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
             'access'=>[
                'class'=>AccessControl::className(),
                'rules'=>[
                    [
                      
                        'allow'=>true,
                          'roles'=>['@'],
                        'matchCallback'=>function(){
                            return (
                                Yii::$app->user->identity->level== 1 
                            );
                        }
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all DataSeminar models.
     * @return mixed
     */
    public function actionIndex() {
             $model = new DynamicModel(['nama','kategori']);
        $model->addRule('nama', 'safe')
               ->addRule('kategori', 'safe');
      
     
        $mhs = DataSeminar::find()->orderBy('id_data DESC');
              

     

          if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $mhs ->andFilterWhere(['like', 'kategori', $model->kategori])
                    ->andFilterWhere(['like', 'nama', $model->nama])
                    ->orFilterWhere(['like', 'nim', $model->nama]);
                   
        }

     
        $dataProvider = new ActiveDataProvider([
            'query' => $mhs,
            'pagination' => [
                'pagesize' => 20
            ]
        ]);
           return $this->render('index', [
                    'dataProvider' => $dataProvider,
                  
                    'model' => $model,
        ]);
    }

    /**
     * Displays a single DataSeminar model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new DataSeminar model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new DataSeminar();

        if ($model->load(Yii::$app->request->post()) && $model->save(false)) {
            return $this->redirect(['view', 'id' => $model->id_data]);
        } else {
            return $this->render('create', [
                        'model' => $model,
            ]);
        }
    }

    public function actionTambahData($id) {
        if ($id) {
            $model = new DataSeminar();
            $cari = \common\models\JadwalSeminar::find()->where(['id_jadwal'=>$id])->one();
             $jadwal = \common\models\DataSeminar::find()->where(['id_jadwal'=>$cari->id_jadwal])->orderBy('id_data DESC');
            $dataProvider = new ActiveDataProvider([
            'query' =>$jadwal,
            'pagination' => [
                'pagesize' => 10
            ]
                ]);
            $model->id_jadwal = $id;
            $model->kategori= $cari->kategori;
            
            if ($model->load(Yii::$app->request->post())) {
                $mahasiswa = \common\models\Mahasiswa::find()->where(['id_mahasiswa'=>$model->mahasiswa])->one();
           
                $model->nama = $mahasiswa->nama;
                $model->nim= $mahasiswa->nim;
                if ($model->kategori==3){
                    $carimahasiswa = DataSeminar::find()->where(['mahasiswa'=>$model->mahasiswa])->andWhere(['kategori'=>1])->all();
                    if($carimahasiswa){
                        $model->save(false);
                           Yii::$app->session->setFlash('success', 'Data berhasil di inputkan');
                
                return $this->redirect(['tambah-data','id'=>$id]);
                    }else{
                          Yii::$app->session->setFlash('error', 'Data proposal belum ada');
                        return $this->render('tambah-data', [
                            'model' => $model,
                       'dataProvider'=>$dataProvider,
                       'cari'=>$cari,
                ]);
                    }
                }else{
                      $model->save(false);
                     Yii::$app->session->setFlash('success', 'Data berhasil di inputkan');
                
                return $this->redirect(['tambah-data','id'=>$id]);
                }
               
            } else {
                return $this->render('tambah-data', [
                            'model' => $model,
                       'dataProvider'=>$dataProvider,
                       'cari'=>$cari,
                ]);
            }
        } else {
             Yii::$app->session->setFlash('error', 'Data tidak ada');
                return $this->redirect(['index']);
           
        }
    }

       public function actionTambahDatasatu($id) {
        if ($id) {
            $model = new DataSeminar();
            $cari = \common\models\JadwalSeminar::find()->where(['id_jadwal'=>$id])->one();
             $jadwal = \common\models\DataSeminar::find()->where(['id_jadwal'=>$cari->id_jadwal])->orderBy('id_data DESC');
            $dataProvider = new ActiveDataProvider([
            'query' =>$jadwal,
            'pagination' => [
                'pagesize' => 10
            ]
                ]);
            $model->id_jadwal = $id;
            $model->kategori= $cari->kategori;
             
            if ($model->load(Yii::$app->request->post()) && $model->save(false)) {

                    $mahasiswa = \common\models\Mahasiswa::find()->where(['id_mahasiswa'=>$model->mahasiswa])->one();
           
                $model->nama = $mahasiswa->nama;
                $model->nim= $mahasiswa->nim;
                if ($model->kategori==3){
                    $carimahasiswa = DataSeminar::find()->where(['mahasiswa'=>$model->mahasiswa])->andWhere(['kategori'=>1])->all();
                    if($carimahasiswa){
                        $model->save(false);
                           Yii::$app->session->setFlash('success', 'Data berhasil di inputkan');
                
                return $this->redirect(['tambah-datasatu','id'=>$id]);
                    }else{
                          Yii::$app->session->setFlash('error', 'Data proposal belum ada');
                        return $this->render('tambah-datasatu', [
                            'model' => $model,
                       'dataProvider'=>$dataProvider,
                       'cari'=>$cari,
                ]);
                    }
                }else{
                      $model->save(false);
                     Yii::$app->session->setFlash('success', 'Data berhasil di inputkan');
                
                return $this->redirect(['tambah-datasatu','id'=>$id]);
                }
               
            } else {
                return $this->render('tambah-datasatu', [
                            'model' => $model,
                       'dataProvider'=>$dataProvider,
                       'cari'=>$cari,
                ]);
            }
        } else {
             Yii::$app->session->setFlash('error', 'Data tidak ada');
                return $this->redirect(['index']);
           
        }
    }
    /**
     * Updates an existing DataSeminar model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save(false)) {
            return $this->redirect(['view', 'id' => $model->id_data]);
        } else {
            return $this->render('update', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing DataSeminar model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the DataSeminar model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return DataSeminar the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = DataSeminar::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
