<?php

namespace backend\controllers;

use Yii;
use common\models\Transportation;
use common\models\TransportationSearch;
use common\models\Villages;
use common\models\VillagesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\imagine\Image;
use yii\filters\AccessControl;
/**
 * TransportationController implements the CRUD actions for Transportation model.
 */
class TransportationController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
                           'access'=>[
                'class'=>AccessControl::className(),
                'rules'=>[
                    [
                      
                        'allow'=>true,
                          'roles'=>['@'],
                        'matchCallback'=>function(){
                            return (
                                Yii::$app->user->can('superAdmin')
                            );
                        }
                    ],
                ],
            ],

        ];
    }

    /**
     * Lists all Transportation models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new VillagesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

   public function actionDataTransportasi()
    {
        $searchModel = new TransportationSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('data-transportasi', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    } 

    /**
     * Displays a single Transportation model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Transportation model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
     public function actionCreate($id=NULL)
    {
        $desa= Villages::find()->where(['id'=>$id])->one();
        
        if ($desa){
           
              $model = new Transportation();
         $user = Yii::$app->user->identity->id;
         $model->id_desa = $desa->id;
         $model->id_provinsi = $desa->provinsi;
         $model->id_kecamatan= $desa->district_id;
        if ($model->load(Yii::$app->request->post())) {
            $foto = new \common\models\FotoTransportasi();
            $model2 = new Transportation();

            $model2->picture = UploadedFile::getInstances($model, 'picture');


            if ($model2->picture) {
                $transport = new Transportation();
               $transport->nama=$model->nama;
                 $transport->rute=$model->rute;
               
                 $transport->jenis_transportasi = $model->jenis_transportasi;
                 $transport->price= $model->price;
                 $transport->id_desa = $model->id_desa;
                 $transport->id_provinsi = $model->id_provinsi;
                 $transport->id_kecamatan = $model->id_kecamatan;
                 $transport->deskripsi = $model->deskripsi;
                $transport->id_user = $user;
                 $transport->save(false);
                $tampungdata = $transport->id_transportation;
               
                foreach ($model2->picture as $file) {
                    $img = new \common\models\FotoTransportasi();
                   
                    $fileName = time() . $file->baseName . '.' . $file->getExtension();

                    
                    if ($file->saveAs(('../uploads/transport/') . $fileName)) {
                        $val = Image::thumbnail(('../uploads/transport/') . $fileName, 320, 320)
                                ->save(('../uploads/transport/') . $fileName, ['quality' => 50]);
                        if ($val) {
                           
                            $img->id_transportasi = $tampungdata;
                             $img->foto = $fileName;
                            $img->save(false);
                        }
                    }
                }
            } else {
                $model->save(false);

                
            }
            return $this->redirect(['data-transportasi',]);
        }else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
        }else{
                Yii::$app->session->setFlash('error', 'Data tidak ditemukan');
                 return $this->redirect(['index',]);
        }
       
    }

    /**
     * Updates an existing Merchandise model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
            $model = $this->findModel($id);
        
        if ($model->load(Yii::$app->request->post())) {
           $foto = new \common\models\FotoTransportasi();
            $model2 = new Transportation();

            $model2->picture = UploadedFile::getInstances($model, 'picture');


            if ($model2->picture) {
                Yii::$app->db->createCommand("DELETE FROM foto_transportasi WHERE id_transportasi = '$id' ")->execute();
               $update = new Transportation();
              
               
                if ($model->id_kecamatan==NULL){
                    $cari = Villages::find()->where(['id'=>$model->id_desa])->one();
                    $model->id_kecamatan= $cari->district_id;
                }
                 $model->save();
              $tampungdata = $model->id_transportation;
                 
                foreach ($model2->picture as $file) {
                    $img = new \common\models\FotoTransportasi();

                    $fileName = time() . $file->baseName . '.' . $file->getExtension();

                    
                    if ($file->saveAs(('../uploads/transport/') . $fileName)) {
                        $val = Image::thumbnail(('../uploads/transport/') . $fileName, 320, 320)
                                ->save(('../uploads/transport/') . 'thumb-' . $fileName, ['quality' => 50]);
                        if ($val) {
                            $img->id_transportasi = $tampungdata;
                            $img->foto = $fileName;
                           
                            $img->save(false);
                        }
                    }
                }
               
            } else {
               // Yii::$app->db->createCommand("UPDATE foto_koleksi SET date = '$model->date' WHERE id_email = '$id' ")->execute();
                  if ($model->id_kecamatan==NULL){
                    $cari = Villages::find()->where(['id'=>$model->id_desa])->one();
                    $model->id_kecamatan= $cari->district_id;
                }
               $model->save();
               
                
                
            }
           
           return $this->redirect(['data-transportasi']);
        } else {
            return $this->render('update', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Transportation model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Transportation model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Transportation the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Transportation::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
