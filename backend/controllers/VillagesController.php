<?php

namespace backend\controllers;

use Yii;
use common\models\Villages;
use common\models\VillagesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\imagine\Image;
use yii\filters\AccessControl;
/**
 * VillagesController implements the CRUD actions for Villages model.
 */
class VillagesController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
             'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Villages models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new VillagesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Villages model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
          $foto= \common\models\FotoDesa::find()->where(['id_desa'=>$id])->all();
        return $this->render('view', [
            'model' => $this->findModel($id),
            'foto'=>$foto,
        ]);
    }

    /**
     * Creates a new Villages model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
 public function actionCreate()
    {
        $model = new Villages();
        $cek = Villages::find()->orderBy('id DESC')->one();
        $model->id = $cek->id+1;
      
        if ($model->load(Yii::$app->request->post())) {
            $foto = new \common\models\FotoDesa();
            $model2 = new Villages();

            $model2->picture = UploadedFile::getInstances($model, 'picture');
            $daerah = \common\models\Districts::find()->where(['id'=>$model->district_id])->one();
           $kota = \common\models\Regencies::find()->where(['id'=>$daerah->regency_id])->one();
           $provinsi = \common\models\Provinces::find()->where(['id'=>$kota->province_id])->one();
            $model->provinsi = $provinsi->id;
            if ($model2->picture) {
                $desa = new Villages();
                $desa->id = $model->id;
                $desa->distric_id = $model->distric_id;
                $desa->name = $model->name;
                $desa->address = $model->address;
                $desa->description = $model->discription;
                $desa->provinsi = $model->provinsi;
                $desa->save(false);
               
                $tampungdata = $desa->id;
               
                foreach ($model2->picture as $file) {
                    $img = new \common\models\FotoDesa();
                   
                    $fileName = time() . $file->baseName . '.' . $file->getExtension();

                    
                    if ($file->saveAs(('../uploads/desa/') . $fileName)) {
                        $val = Image::thumbnail(('../uploads/desa/') . $fileName, 320, 320)
                                ->save(('../uploads/desa/') . $fileName, ['quality' => 50]);
                        if ($val) {
                           
                            $img->id_desa = $tampungdata;
                             $img->foto = $fileName;
                            $img->save(false);
                        }
                    }
                }
            } else {
                $model->save(false);

                
            }
            return $this->redirect(['index',]);
        }else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Villages model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
      public function actionUpdate($id)
    {
           $model = $this->findModel($id);
           $daerah = \common\models\Districts::find()->where(['id'=>$model->district_id])->one();
           $kota = \common\models\Regencies::find()->where(['id'=>$daerah->regency_id])->one();
           $provinsi = \common\models\Provinces::find()->where(['id'=>$kota->province_id])->one();
           $model->provinsi = $provinsi->id;
        if ($model->load(Yii::$app->request->post())) {
           //$foto = new \common\models\FotoKoleksi();
            $model2 = new Villages();

            $model2->picture = UploadedFile::getInstances($model, 'picture');


            if ($model2->picture) {
                Yii::$app->db->createCommand("DELETE FROM foto_desa WHERE id_desa = '$id' ")->execute();
             
             
                $model->save(false);
               
              $tampungdata = $model->id;
                 
                foreach ($model2->picture as $file) {
                    $img = new \common\models\FotoDesa();

                    $fileName = time() . $file->baseName . '.' . $file->getExtension();

                    
                    if ($file->saveAs(('../uploads/desa/') . $fileName)) {
                        $val = Image::thumbnail(('../uploads/desa/') . $fileName, 320, 320)
                                ->save(('../uploads/desa/') . 'thumb-' . $fileName, ['quality' => 50]);
                        if ($val) {
                           
                            $img->id_desa = $tampungdata;
                              $img->foto = $fileName;
                            $img->save(false);
                        }
                    }
                }
            } else {
              
               $model->save(false);
               
               
                
            }
           
            return $this->redirect(['view','id'=>$model->id]);
        } else {
            return $this->render('update', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Villages model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Villages model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Villages the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Villages::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
