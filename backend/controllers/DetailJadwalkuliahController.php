<?php

namespace backend\controllers;

use Yii;
use common\models\DetailJadwalkuliah;
use common\models\DetailJadwalkuliahSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\base\DynamicModel;
use common\models\Mahasiswa;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
/**
 * DetailJadwalkuliahController implements the CRUD actions for DetailJadwalkuliah model.
 */
class DetailJadwalkuliahController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
              'access'=>[
                'class'=>AccessControl::className(),
                'rules'=>[
                    [
                      
                        'allow'=>true,
                          'roles'=>['@'],
                        'matchCallback'=>function(){
                            return (
                                Yii::$app->user->identity->level== 1 
                            );
                        }
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all DetailJadwalkuliah models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new DetailJadwalkuliahSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
      public function actionDataMahasiswa($id)
    {
          $cari = \common\models\JadwalKuliah::find()->where(['id_jadwal'=>$id])->one();
        $mhs = DetailJadwalkuliah::find()
             
                ->where(['id_jadwal'=>$id]);
               
        
     
        
    
        
        
        $dataProvider = new ActiveDataProvider([
            'query' =>$mhs,
            'pagination' => [
                'pagesize' => 20
            ]
        ]);

        return $this->render('data-mahasiswa', [
           // 'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'cari'=>$cari,
        ]);
    }

    /**
     * Displays a single DetailJadwalkuliah model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new DetailJadwalkuliah model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id =NULL )
    {
       
        if ($id){
               $searchModel = new \common\models\MahasiswaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            $data = \common\models\JadwalKuliah::find()->where(['id_jadwal'=>$id])->one();
             $model = new DetailJadwalkuliah();
             $model->nama_matkul = $data->mata_kuliah;
             $model->jam_mulai= $data->jadwal_awal;
             $model->jam_akhir = $data->jadwal_akhir;
             $model->dosen = $data->dosen;
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_detail]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'data'=>$data,
                  'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
                'id'=>$id,
            ]);
        }
        }else{
             Yii::$app->session->setFlash('error', 'Pilih pilih jadwal kuliah terlebih dahulu');
                return $this->redirect(['index',]);
        }
       
    }
  public function actionTambahMahasiswa($d){
        
        if($this->tambahMhsAjax($d)){
              return ;
             
        }        
         $data = \common\models\JadwalKuliah::find()->where(['id_jadwal'=>$d])->one();
        $model = new DynamicModel(['angkatan','prodi','jumlah_data']);
     
        $model->addRule('angkatan', 'safe');
        $model->addRule('prodi', 'safe');
        $model->addRule('jumlah_data', 'safe');
        $model->addRule('jumlah_data', 'integer');
        
     $subQuery= DetailJadwalkuliah::find()->where(['id_jadwal'=>$d])->all();
     
         $idM = [];
        foreach($subQuery as $val){
            array_push($idM, $val->mahasiswa);
        }
        
        $mhs = Mahasiswa::find()
             
                ->andWhere(['not in','id_mahasiswa',$idM]);
               
        
        $model->jumlah_data = 10;
        
        if($model->load(Yii::$app->request->post()) && $model->validate()){
            $mhs
                ->andFilterWhere(['like','angkatan',$model->angkatan])
                 ->andFilterWhere(['like','prodi',$model->prodi]);
   
        }
        
        
        $dataProvider = new ActiveDataProvider([
            'query' =>$mhs,
            'pagination' => [
                'pagesize' => $model->jumlah_data
            ]
        ]);
        
        return $this->render('tambah-mahasiswa',[
            'dataProvider' => $dataProvider,
            'model' =>$model,
            'data'=>$data
            
        ]);
    }
    
    private function tambahMhsAjax($d){
        if(Yii::$app->request->isAjax && Yii::$app->request->post('item')){
            $jadwal = \common\models\JadwalKuliah::find()->where(['id_jadwal'=>$d])->one();
            $dataToInsert = [];
            foreach (Yii::$app->request->post('item') as $value) {
			
             $angkatan = Mahasiswa::find()->where(['id_mahasiswa'=>$value])->one();
			 $date = date('Y');
			 
			 if ($jadwal->semester=='Ganjil-'.$date){
				 $tahun = date('Y');
				 if ($angkatan->angkatan==$tahun){
					 $semestermhs = 1;
				 }else{
					 $coba = $angkatan->angkatan;
					 $totaltahun = $tahun - $coba;
					 $semestermhs =$totaltahun*3;
				 }
				 
				 
				 
			 }else if ($jadwal->semester=='Genap-'.$date){
				 $tahun = date('Y');
				 $tambah = $tahun+1;
				 if ($angkatan->angkatan==$tambah){
					 $semestermhs = 2;
				 }else{
					  $coba = $angkatan->angkatan;
					 $totaltahun = $tahun - $coba;
					 $semestermhs =$totaltahun*2;
				 }
			 }
				$angkatan->semester = $semestermhs;
				$angkatan->save(false);
                $dataToInsert[]=[
                    'nama_matkul'=>$jadwal->mata_kuliah,
                     'jam_mulai'=>$jadwal->jadwal_awal,
                     'jam_akhir'=>$jadwal->jadwal_akhir,
                    'mahasiswa' => $value,
                    'dosen' => $jadwal->dosen,
                    'dosen2'=>$jadwal->dosen2,
                    'angkatan'=>$angkatan->angkatan,
                    'id_jadwal'=>$jadwal->id_jadwal,
                    'nama'=>$angkatan->nama,
                    'nim'=>$angkatan->nim,
					'semester'=>$semestermhs,
                ];
            }
            if(!empty($dataToInsert)){
                $rest = Yii::$app->db->createCommand()
                    ->batchInsert(DetailJadwalkuliah::tableName(), ['nama_matkul','jam_mulai','jam_akhir','mahasiswa','dosen','dosen2','angkatan','id_jadwal','nama','nim','semester'], $dataToInsert)
                    ->execute();
				 $countjadwal =  DetailJadwalkuliah::find()->where(['id_jadwal'=>$d])->count();
				 $jadwal->jumlah_mhs = $countjadwal;
				 $jadwal->save(false);
                echo json_encode($rest);
            }   Yii::$app->session->setFlash('success', 'Berhasil menambahkan mahasiswa !');
            return true;
        }
        return false;
    }

    /**
     * Updates an existing DetailJadwalkuliah model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_detail]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing DetailJadwalkuliah model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the DetailJadwalkuliah model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return DetailJadwalkuliah the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = DetailJadwalkuliah::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
