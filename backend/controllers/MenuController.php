<?php

namespace backend\controllers;

use Yii;
use common\models\Menu;
use common\models\MenuSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\imagine\Image;
/**
 * MenuController implements the CRUD actions for Menu model.
 */
class MenuController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Menu models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MenuSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Menu model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Menu model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
      public function actionCreate() {
        $model = new Menu();
    
        if ($model->load(Yii::$app->request->post())) {

            $model->picture = UploadedFile::getInstance($model, 'picture');

            if ($model->picture) {
                $menu = new Menu();

                $foto = time() . $model->icon . rand() . '.' . $model->picture->extension;

                $menu->kategori = $model->kategori;
              
                $menu->save();


                if ($model->picture->saveAs(Yii::getAlias('../uploads/menu/') . $foto)) {
                    $val = Image::thumbnail(Yii::getAlias('../uploads/menu/') . $foto, 320, 320)
                            ->save(Yii::getAlias('../uploads/menu/') . 'thumb-' . $foto, ['quality' => 50]);
                    if ($val) {
                        $menu->icon = $foto;
                        $menu->save();
                    }
                }
                Yii::$app->session->setFlash('success', 'Data  berhasil ditambahkan');
                return $this->redirect(['index',]);
            } else {
               $model->save(false);
            }
            Yii::$app->session->setFlash('success', 'Data  berhasil ditambahkan');

            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Berita model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);
      
        if ($model->load(Yii::$app->request->post())) {

            $model->picture = UploadedFile::getInstance($model, 'picture');

            if ($model->picture) {

                $foto = time() . rand() . '.' . $model->picture->extension;

                $update = new Menu();

             
                $update->icon = $foto;

                $model->icon = $update->icon;
                $model->save();

                if ($model->picture->saveAs(Yii::getAlias('../uploads/menu/') . $foto)) {
                    $val = Image::thumbnail(Yii::getAlias('../uploads/menu/') . $foto, 320, 320)
                            ->save(Yii::getAlias('../uploads/menu/') . 'thumb-' . $foto, ['quality' => 50]);


                }
                Yii::$app->session->setFlash('success', ' Berhasil diperbaharui');

                return $this->redirect(['index']);
            } else {
              
                $model->save();
            }
            Yii::$app->session->setFlash('success', 'Data berhasil diperbaharui');
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                        'model' => $model,
            ]);
        }
    }


    /**
     * Deletes an existing Menu model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Menu model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Menu the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Menu::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
