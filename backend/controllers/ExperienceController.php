<?php

namespace backend\controllers;

use Yii;
use common\models\Experience;
use common\models\ExperienceSearch;
use common\models\Villages;
use common\models\VillagesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\imagine\Image;
use yii\filters\AccessControl;
/**
 * ExperienceController implements the CRUD actions for Experience model.
 */
class ExperienceController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
                       'access'=>[
                'class'=>AccessControl::className(),
                'rules'=>[
                    [
                      
                        'allow'=>true,
                          'roles'=>['@'],
                        'matchCallback'=>function(){
                            return (
                                Yii::$app->user->can('superAdmin')
                            );
                        }
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Experience models.
     * @return mixed
     */
     public function actionIndex()
    {
        $searchModel = new VillagesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    public function actionDataExperience()
    {
        $searchModel = new ExperienceSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('data-experience', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Experience model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Experience model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id)
    {
         $desa= Villages::find()->where(['id'=>$id])->one();
        
        if ($desa){
           
              $model = new Experience();
         $user = Yii::$app->user->identity->id;
         $model->id_desa = $desa->id;
         $model->id_user = $user;
          $model->id_desa = $desa->id;
         $model->id_provinsi = $desa->provinsi;
          $model->id_kecamatan= $desa->district_id;
        if ($model->load(Yii::$app->request->post())) {
            $foto = new \common\models\FotoExperience();
            $model2 = new Experience();

            $model2->picture = UploadedFile::getInstances($model, 'picture');


            if ($model2->picture) {
                $experience = new Experience();
                $experience->id_kategori = $model->id_kategori;
                $experience->nama = $model->nama;
                $experience->jam_mulai = $model->jam_mulai;
                $experience->jam_berakhir = $model->jam_berakhir;
                $experience->price = $model->price;
                $experience->id_user = $user;
                $experience->id_desa = $model->id_desa;
               $experience->deskripsi = $model->deskripsi;
                $experience->rating = $model->rating;
                 $experience->latitude = $model->latitude;
                  $experience->longitude = $model->longitude;
                   $experience->contact = $model->contact;
                    $experience->alamat = $model->alamat;
                    $experience->id_kecamatan = $model->id_kecamatan;
                $experience->save(false);
               
                $tampungdata = $experience->id_experience;
               
                foreach ($model2->picture as $file) {
                    $img = new \common\models\FotoExperience();
                   
                    $fileName = time() . $file->baseName . '.' . $file->getExtension();

                    
                    if ($file->saveAs(('../uploads/experience/') . $fileName)) {
                        $val = Image::thumbnail(('../uploads/experience/') . $fileName, 320, 320)
                                ->save(('../uploads/experience/') . $fileName, ['quality' => 50]);
                        if ($val) {
                           
                            $img->id_experience = $tampungdata;
                             $img->foto = $fileName;
                            $img->save(false);
                        }
                    }
                }
            } else {
                $model->save(false);

                
            }
            return $this->redirect(['data-experience',]);
        }else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
        }else{
                Yii::$app->session->setFlash('error', 'Data tidak ditemukan');
                 return $this->redirect(['index',]);
        }
       
       
    }

    /**
     * Updates an existing Experience model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
            $model = $this->findModel($id);
        
        if ($model->load(Yii::$app->request->post())) {
           $foto = new \common\models\FotoExperience();
            $model2 = new Experience();

            $model2->picture = UploadedFile::getInstances($model, 'picture');


            if ($model2->picture) {
                Yii::$app->db->createCommand("DELETE FROM foto_experience WHERE id_experience = '$id' ")->execute();
               $update = new Experience();
              
               
               if ($model->id_kecamatan==NULL){
                    $cari = Villages::find()->where(['id'=>$model->id_desa])->one();
                    $model->id_kecamatan= $cari->district_id;
                }
                 $model->save();
              $tampungdata = $model->id_experience;
                 
                foreach ($model2->picture as $file) {
                    $img = new \common\models\FotoExperience();

                    $fileName = time() . $file->baseName . '.' . $file->getExtension();

                    
                    if ($file->saveAs(('../uploads/experience/') . $fileName)) {
                        $val = Image::thumbnail(('../uploads/experience/') . $fileName, 320, 320)
                                ->save(('../uploads/experience/') . 'thumb-' . $fileName, ['quality' => 50]);
                        if ($val) {
                            $img->id_experience = $tampungdata;
                            $img->foto = $fileName;
                           
                            $img->save(false);
                        }
                    }
                }
               
            } else {
               // Yii::$app->db->createCommand("UPDATE foto_koleksi SET date = '$model->date' WHERE id_email = '$id' ")->execute();
                  if ($model->id_kecamatan==NULL){
                    $cari = Villages::find()->where(['id'=>$model->id_desa])->one();
                    $model->id_kecamatan= $cari->district_id;
                }
               $model->save();
               
                
                
            }
           
           return $this->redirect(['view','id'=>$model->id_experience]);
        } else {
            return $this->render('update', [
                        'model' => $model,
            ]);
        }
    }


    /**
     * Deletes an existing Experience model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Experience model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Experience the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Experience::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
