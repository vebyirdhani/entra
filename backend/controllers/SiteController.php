<?php

namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\Profile;
use common\models\LoginForm;
use yii\web\UploadedFile;
use yii\imagine\Image;

/**
 * Site controller
 */
class SiteController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index', 'profil', 'foto-profil', 'change-password','menu'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions() {
        return [
//            'error' => [
//                'class' => 'yii\web\ErrorAction',
//            ],
        ];
    }

    public function actionError() {

        $exception = Yii::$app->errorHandler->exception;
        if ($exception !== null) {
            if (Yii::$app->user->id) {
                $this->layout = 'main';
            } else {
                $this->layout = 'error';
            }
            return $this->render('error', ['exception' => $exception]);
        }
    }

    public function actionChangePassword() {
        $user = \common\models\User::findOne(Yii::$app->user->id);
        $user->scenario = 'change_password';

        $model = Profile::findOne(['user_id' => $user->id]);

        if (!$model) {
            return false;
        }

        $model->born_date = date("d M Y", $model->born_date);

        // var_dump($user);die;
        $loadedPost = $user->load(Yii::$app->request->post());

        if ($loadedPost && $user->validate()) {

            $user->password = $user->newPassword;

            if ($user->save(false)) {
                Yii::$app->session->setFlash('success', 'Password Berhasil Diperbarui');
                return $this->refresh();
            }
        }

        return $this->render("change-password", [
                    'user' => $user,
                    'model' => $model,
        ]);
    }

    public function actionProfil() {
        $user = Yii::$app->user->identity;
        $model = Profile::findOne(['user_id' => $user->id]);
        $project = Profile::find()->where(['user_id'=>$user->id])->one();
        if (!$model) {
            return false;
        }

       $model->born_date = date("d-M-Y", $model->born_date);
        $fotoLama = $user->photo;

        if ($model->load(Yii::$app->request->post()) && $user->load(Yii::$app->request->post())) {
            $model->photo = UploadedFile::getInstance($model, 'photo');

            if ($model->photo) {
                $foto = time() . $user->username . $model->photo->baseName . '.' . $model->photo->extension;
              $model->born_date = strtotime($model->born_date);
                $model->save();
//          
                $profile = \common\models\Profile::findOne($model->user_id);
                $profile->name = $model->name;
                $profile->public_email = $model->public_email;
                //$profile->museum = $model->museum;
                $profile->save(false);

                if ($model->photo->saveAs(Yii::getAlias('@home/uploads/foto_profil/') . $foto)) {
                    $val = Image::thumbnail(Yii::getAlias('@home/uploads/foto_profil/') . $foto, 320, 320)
                            ->save(Yii::getAlias('@home/uploads/foto_profil/') . 'thumb-' . $foto, ['quality' => 50]);
                    if ($val) {
                        $user->photo = $foto;
                        $user->save();
                    }
                }
            } else {
                $model->born_date = strtotime($model->born_date);
                if ($model->save()) {
                      $data_pelanggan = new \common\models\DataPelanggan();
            $data_pelanggan->nama = $model->name;
            $data_pelanggan->no_ktp = $model->no_ktp;
            $data_pelanggan->kelamin = $model->kelamin;
            $data_pelanggan->alamat=$model->address;
            $data_pelanggan->no_hp = $model->no_hp;
            $data_pelanggan->project= $project->project;
            $data_pelanggan->save(false);
                    if ($user->save()) {
                        $profile = \common\models\Profile::findOne($model->user_id);
                        $profile->name = $model->name;
                        $profile->public_email = $model->public_email;
                     //    $profile->museum = $model->museum;
                        $profile->save(false);
                    } else {
                        print_r($user->getErrors());
                        die();
                    }
                } else {
                    print_r($model->getErrors());
                    die();
                }
            }

            return $this->redirect('profil');
        }
        return $this->render('profil', [
                    'user' => $user,
                    'model' => $model,
        ]);
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex() {
    return $this->render('index',[
            
        ]);
        
    }
    
     public function actionMenu($id) {
     $project = \common\models\DaftarProject::find()->where(['id_project'=>$id])->one();
        return $this->render('menu',[
            'project'=>$project,
        ]);
        
    }

    

    
    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin() {
        $this->layout = 'login';
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout() {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionFotoProfil($inline = false) {
        $model = Yii::$app->user->identity;

        if ($model) {
            $path = \Yii::getAlias('@home') . '/uploads/foto_profil/thumb-';
            $file_download = $path . $model->photo;
            $fake_filename = $model->photo;
            $file_default = $path . 'default.png';
            $fake_filename_default = 'default.png';
            $response = Yii::$app->getResponse();

            if ($file_download && $fake_filename) {
                $response->sendFile($file_download, $fake_filename, ['inline' => $inline]);
            } else {
                $response->sendFile($file_default, $fake_filename_default, ['inline' => $inline]);
            }
        } else {
            throw new \yii\web\NotFoundHttpException('File not found');
        }
    }

}
