<?php

namespace backend\controllers;

use Yii;
use common\models\About;
use common\models\AboutSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\imagine\Image;
use yii\filters\AccessControl;
/**
 * AboutController implements the CRUD actions for About model.
 */
class AboutController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
               'access'=>[
                'class'=>AccessControl::className(),
                'rules'=>[
                    [
                      
                        'allow'=>true,
                          'roles'=>['@'],
                        'matchCallback'=>function(){
                            return (
                                Yii::$app->user->can('superAdmin')
                            );
                        }
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all About models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AboutSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single About model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new About model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
     public function actionCreate()
    {
        $model = new About();
     
        if ($model->load(Yii::$app->request->post())) {

            $model->picture = UploadedFile::getInstance($model, 'picture');

            if ($model->picture){
             $about =  new About();
               
                $foto = time()  . $model->foto .  rand() . '.' . $model->picture->extension;
                 
                 $about->judul=$model->judul;
                 $about->deskripsi=$model->deskripsi;
                
                 $about->save();

               
                if ($model->picture->saveAs(Yii::getAlias('../uploads/about/') . $foto)) {
                    $val = Image::thumbnail(Yii::getAlias('../uploads/about/') . $foto, 320, 320)
                            ->save(Yii::getAlias('../uploads/about/') . 'thumb-' . $foto, ['quality' => 50]);
                    if ($val) {
                        $about->foto = $foto;
                        $about->save();
                    }
                }
            Yii::$app->session->setFlash('success', 'Data berita berhasil ditambahkan');
                return $this->redirect(['index',]);
            }else{
               $model->save(false);
            }
         Yii::$app->session->setFlash('success', 'Data berita berhasil ditambahkan');
       
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Berita model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
     //    $model->created_by = Yii::$app->user->identity->username;
//        $model->created_at = date('d-M-Y H:i:s');
        if ($model->load(Yii::$app->request->post())) {

              $model->picture = UploadedFile::getInstance($model, 'picture');

            if ($model->picture){
                
                $foto = time()  .  rand() . '.' . $model->picture->extension;
//                print_r($foto);die();
                $update = new About();
             
             //   $update->created_at = date('d-M-Y H:i:s');
                $update->foto = $foto;
               
              //  $model->created_at = strtotime($update->created_at);
                $model->foto = $update->foto;
                $model->save();

               if ($model->picture->saveAs(Yii::getAlias('../uploads/about/') . $foto)) {
                    $val = Image::thumbnail(Yii::getAlias('../uploads/about/') . $foto, 320, 320)
                          ->save(Yii::getAlias('../uploads/about/') . 'thumb-' . $foto, ['quality' => 50]);
                            
//                    if ($val) {
//                        $model->foto = $update->foto;
//                        $model->save();                
//                        }
                }
                 Yii::$app->session->setFlash('success', ' Berhasil diperbaharui');

                return $this->redirect(['index']);
            }else{
               
                $model->save(false);
            }
Yii::$app->session->setFlash('success', 'Data berhasil diperbaharui');
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }



    /**
     * Deletes an existing About model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the About model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return About the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = About::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
