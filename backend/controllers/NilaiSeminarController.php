<?php

namespace backend\controllers;

use Yii;
use common\models\NilaiSeminar;
use common\models\NilaiSeminarSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
/**
 * NilaiSeminarController implements the CRUD actions for NilaiSeminar model.
 */
class NilaiSeminarController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
              'access'=>[
                'class'=>AccessControl::className(),
                'rules'=>[
                    [
                      
                        'allow'=>true,
                          'roles'=>['@'],
                        'matchCallback'=>function(){
                            return (
                                Yii::$app->user->identity->level== 1 
                            );
                        }
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all NilaiSeminar models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new NilaiSeminarSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single NilaiSeminar model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new NilaiSeminar model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new NilaiSeminar();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_nilai]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing NilaiSeminar model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
		
        if ($model->load(Yii::$app->request->post()) ) {
			 if ($model->kategori == 1) {
                          $model->save(false);
                            $nilaipembimbing = $model->nilai_pembimbing1 + $model->nilai_pembimbing2;
                            $totalpembimbing = $nilaipembimbing / 2;
                            $totalpenguji = $model->nilai_penguji1 + $model->nilai_penguji2 + $model->nilai_penguji3 + $model->nilai_penguji4;
                            if ($model->nilai_penguji1 != NULL && $model->nilai_penguji2 != NULL && $model->nilai_penguji3 != NULL && $model->nilai_penguji4 != NULL) {
                                $bagi = $totalpenguji / 4;
                            } else if ($model->nilai_penguji1 != NULL && $model->nilai_penguji2 != NULL && $model->nilai_penguji3 != NULL) {
                                $bagi = $totalpenguji / 3;
                            } else if ($model->nilai_penguji1 != NULL && $model->nilai_penguji2 != NULL && $model->nilai_penguji4 != NULL) {
                                $bagi = $totalpenguji / 3;
                            } else if ($model->nilai_penguji1 != NULL && $model->nilai_penguji3 != NULL && $model->nilai_penguji4 != NULL) {
                                $bagi = $totalpenguji / 3;
                            } else if ($model->nilai_penguji2 != NULL && $model->nilai_penguji3 != NULL && $model->nilai_penguji4 != NULL) {
                                $bagi = $totalpenguji / 3;
                            } else if ($model->nilai_penguji1 != NULL && $model->nilai_penguji2 != NULL) {
                                $bagi = $totalpenguji / 2;
                            } else if ($model->nilai_penguji1 != NULL && $model->nilai_penguji3 != NULL) {
                                $bagi = $totalpenguji / 2;
                            } else if ($model->nilai_penguji1 != NULL && $model->nilai_penguji4 != NULL) {
                                $bagi = $totalpenguji / 2;
                            } else if ($model->nilai_penguji2 != NULL && $model->nilai_penguji3 != NULL) {
                                $bagi = $totalpenguji / 2;
                            } else if ($model->nilai_penguji2 != NULL && $model->nilai_penguji4 != NULL) {
                                $bagi = $totalpenguji / 2;
                            } else if ($model->nilai_penguji3 != NULL && $model->nilai_penguji4 != NULL) {
                                $bagi = $totalpenguji / 2;
                            } else if ($model->nilai_penguji1) {
                                $bagi = $totalpenguji;
                            } else if ($model->nilai_penguji2) {
                                $bagi = $totalpenguji;
                            } else if ($model->nilai_penguji3) {
                                $bagi = $totalpenguji;
                            } else if ($model->nilai_penguji4) {
                                $bagi = $totalpenguji;
                            }
                            $persen60 = 60 / 100;
                            $persen40 = 40 / 100;

                            $pembimbing60 = $totalpembimbing * $persen60;
                            $penguji40 = $bagi * $persen40;
                            $totalsemua = $pembimbing60 + $penguji40;
                            $model->total_nilaipembimbing = $totalpembimbing;
                            $model->total_nilaipenguji = $bagi;
                            $model->pembimbing_persen = $pembimbing60;
                            $model->penguji_persen = $penguji40;
                            $model->total_nilaiseminar = $totalsemua;

                            if ($totalsemua > 85) {
                                $model->akumulasi = "A";
                            } else if ($totalsemua >= 80 && $totalsemua < 85) {
                                $model->akumulasi = "A-";
                            } else if ($totalsemua >= 75 && $totalsemua < 80) {
                                $model->akumulasi = "B+";
                            } else if ($totalsemua >= 70 && $totalsemua < 75) {
                                $model->akumulasi = "B";
                            } else if ($totalsemua >= 65 && $totalsemua < 70) {
                                $model->akumulasi = "B-";
                            } else if ($totalsemua >= 60 && $totalsemua < 65) {
                                $model->akumulasi = "C+";
                            } else if ($totalsemua >= 55 && $totalsemua < 60) {
                                $model->akumulasi = "C";
                            } else if ($totalsemua >= 40 && $totalsemua < 55) {
                                $model->akumulasi = "D";
                            } else if ($totalsemua <= 40) {
                                $model->akumulasi = "E";
                            }
                            $model->save(false);
							return $this->redirect(['view', 'id' => $model->id_nilai]);
                        } else if ($model->kategori == 2) {
                          
                            $model->save(false);
                            $persen60 = 60 / 100;
                            $persen40 = 40 / 100;
                            $persen50 = 50 / 100;
                            $pembimbinglap = $persen60 * $model->nilai_pembimbing2;
                            $pembimbingaka = $persen40 * $model->nilai_pembimbing1;
                            $total_nilaipembimbing = $pembimbinglap + $pembimbingaka;
                            $nilai_penguji1 = $persen50 * $model->nilai_penguji1;
                            $nilai_penguji2 = $persen50 * $model->nilai_penguji2;
                            $total_nilaipenguji = $nilai_penguji1 + $nilai_penguji2;
                            $total_pembimbing60 = $persen60 * $total_nilaipembimbing;
                            $total_penguji40 = $persen40 * $total_nilaipenguji;
                            $nilai_akhir = $total_pembimbing60 + $total_penguji40;

                            $model->total_nilaipembimbing = $total_nilaipembimbing;
                            $model->total_nilaipenguji = $total_nilaipenguji;
                            $model->pembimbing_persen = $total_pembimbing60;
                            $model->penguji_persen = $total_penguji40;
                            $model->total_nilaiakhir = $nilai_akhir;

                            if ($nilai_akhir > 85) {
                                $model->akumulasi = "A";
                            } else if ($nilai_akhir >= 80 && $nilai_akhir < 85) {
                                $model->akumulasi = "A-";
                            } else if ($nilai_akhir >= 75 && $nilai_akhir < 80) {
                                $model->akumulasi = "B+";
                            } else if ($nilai_akhir >= 70 && $nilai_akhir < 75) {
                                $model->akumulasi = "B";
                            } else if ($nilai_akhir >= 65 && $nilai_akhir < 70) {
                                $model->akumulasi = "B-";
                            } else if ($nilai_akhir >= 60 && $nilai_akhir < 65) {
                                $model->akumulasi = "C+";
                            } else if ($nilai_akhir >= 55 && $nilai_akhir < 60) {
                                $model->akumulasi = "C";
                            } else if ($nilai_akhir >= 40 && $nilai_akhir < 55) {
                                $model->akumulasi = "D";
                            } else if ($nilai_akhir <= 40) {
                                $model->akumulasi = "E";
                            }
                            $model->save(false);
							return $this->redirect(['view', 'id' => $model->id_nilai]);
                        } else if ($model->kategori == 3) {
                         
                            $model->save(false);
							 $carimahasiswa = \common\models\NilaiSeminar::find()->where(['mahasiswa' => $model->mahasiswa])
                                            ->andWhere(['kategori' => 1])->orderBy('id_nilai DESC')->one();
							IF ($carimahasiswa){	
                            $nilaipembimbing = $model->nilai_pembimbing1 + $model->nilai_pembimbing2;
                            $totalpembimbing = $nilaipembimbing / 2;
                            $totalpenguji = $model->nilai_penguji1 + $model->nilai_penguji2 + $model->nilai_penguji3 + $model->nilai_penguji4;
                            if ($model->nilai_penguji1 != NULL && $model->nilai_penguji2 != NULL && $model->nilai_penguji3 != NULL && $model->nilai_penguji4 != NULL) {
                                $bagi = $totalpenguji / 4;
                            } else if ($model->nilai_penguji1 != NULL && $model->nilai_penguji2 != NULL && $model->nilai_penguji3 != NULL) {
                                $bagi = $totalpenguji / 3;
                            } else if ($model->nilai_penguji1 != NULL && $model->nilai_penguji2 != NULL && $model->nilai_penguji4 != NULL) {
                                $bagi = $totalpenguji / 3;
                            } else if ($model->nilai_penguji1 != NULL && $model->nilai_penguji3 != NULL && $model->nilai_penguji4 != NULL) {
                                $bagi = $totalpenguji / 3;
                            } else if ($model->nilai_penguji2 != NULL && $model->nilai_penguji3 != NULL && $model->nilai_penguji4 != NULL) {
                                $bagi = $totalpenguji / 3;
                            } else if ($model->nilai_penguji1 != NULL && $model->nilai_penguji2 != NULL) {
                                $bagi = $totalpenguji / 2;
                            } else if ($model->nilai_penguji1 != NULL && $model->nilai_penguji3 != NULL) {
                                $bagi = $totalpenguji / 2;
                            } else if ($model->nilai_penguji1 != NULL && $model->nilai_penguji4 != NULL) {
                                $bagi = $totalpenguji / 2;
                            } else if ($model->nilai_penguji2 != NULL && $model->nilai_penguji3 != NULL) {
                                $bagi = $totalpenguji / 2;
                            } else if ($model->nilai_penguji2 != NULL && $model->nilai_penguji4 != NULL) {
                                $bagi = $totalpenguji / 2;
                            } else if ($model->nilai_penguji3 != NULL && $model->nilai_penguji4 != NULL) {
                                $bagi = $totalpenguji / 2;
                            } else if ($model->nilai_penguji1) {
                                $bagi = $totalpenguji;
                            } else if ($model->nilai_penguji2) {
                                $bagi = $totalpenguji;
                            } else if ($model->nilai_penguji3) {
                                $bagi = $totalpenguji;
                            } else if ($model->nilai_penguji4) {
                                $bagi = $totalpenguji;
                            }
                            $persen60 = 60 / 100;
                            $persen40 = 40 / 100;
                            $persen50 = 50 / 100;
                            $pembimbing60 = $totalpembimbing * $persen60;
                            $penguji40 = $bagi * $persen40;
                            $totalsemua = $pembimbing60 + $penguji40;
                            $model->total_nilaipembimbing = $totalpembimbing;
                            $model->total_nilaipenguji = $bagi;
                            $model->pembimbing_persen = $pembimbing60;
                            $model->penguji_persen = $penguji40;
							  
                            $model->total_nilaiseminar = $carimahasiswa->total_nilaiseminar;
                            $proposal50 = $persen50 * $model->total_nilaiseminar;
                            $seminar50 = $persen50 * $totalsemua;
                            $model->total_nilaiakhir = $proposal50 + $seminar50;

                            if ($totalsemua > 85) {
                                $model->akumulasi = "A";
                            } else if ($totalsemua >= 80 && $totalsemua < 85) {
                                $model->akumulasi = "A-";
                            } else if ($totalsemua >= 75 && $totalsemua < 80) {
                                $model->akumulasi = "B+";
                            } else if ($totalsemua >= 70 && $totalsemua < 75) {
                                $model->akumulasi = "B";
                            } else if ($totalsemua >= 65 && $totalsemua < 70) {
                                $model->akumulasi = "B-";
                            } else if ($totalsemua >= 60 && $totalsemua < 65) {
                                $model->akumulasi = "C+";
                            } else if ($totalsemua >= 55 && $totalsemua < 60) {
                                $model->akumulasi = "C";
                            } else if ($totalsemua >= 40 && $totalsemua < 55) {
                                $model->akumulasi = "D";
                            } else if ($totalsemua <= 40) {
                                $model->akumulasi = "E";
                            }
                            $model->save(false);
				return $this->redirect(['view', 'id' => $model->id_nilai]);
							}else{
								return $this->render('update', [
                'model' => $model,
            ]);
							}
                        }else if ($model->kategori == 4) {
                          
                            $model->save(false);
                            $persen60 = 60 / 100;
                            $persen40 = 40 / 100;
                            $persen50 = 50 / 100;
                            $pembimbinglap = $persen60 * $model->nilai_pembimbing2;
                            $pembimbingaka = $persen40 * $model->nilai_pembimbing1;
                            $total_nilaipembimbing = $pembimbinglap + $pembimbingaka;
                            $nilai_penguji1 = $persen50 * $model->nilai_penguji1;
                            $nilai_penguji2 = $persen50 * $model->nilai_penguji2;
                            $total_nilaipenguji = $nilai_penguji1 + $nilai_penguji2;
                            $total_pembimbing60 = $persen60 * $total_nilaipembimbing;
                            $total_penguji40 = $persen40 * $total_nilaipenguji;
                            $nilai_akhir = $total_pembimbing60 + $total_penguji40;

                            $model->total_nilaipembimbing = $total_nilaipembimbing;
                            $model->total_nilaipenguji = $total_nilaipenguji;
                            $model->pembimbing_persen = $total_pembimbing60;
                            $model->penguji_persen = $total_penguji40;
                            $model->total_nilaiakhir = $nilai_akhir;

                            if ($nilai_akhir > 85) {
                                $model->akumulasi = "A";
                            } else if ($nilai_akhir >= 80 && $nilai_akhir < 85) {
                                $model->akumulasi = "A-";
                            } else if ($nilai_akhir >= 75 && $nilai_akhir < 80) {
                                $model->akumulasi = "B+";
                            } else if ($nilai_akhir >= 70 && $nilai_akhir < 75) {
                                $model->akumulasi = "B";
                            } else if ($nilai_akhir >= 65 && $nilai_akhir < 70) {
                                $model->akumulasi = "B-";
                            } else if ($nilai_akhir >= 60 && $nilai_akhir < 65) {
                                $model->akumulasi = "C+";
                            } else if ($nilai_akhir >= 55 && $nilai_akhir < 60) {
                                $model->akumulasi = "C";
                            } else if ($nilai_akhir >= 40 && $nilai_akhir < 55) {
                                $model->akumulasi = "D";
                            } else if ($nilai_akhir <= 40) {
                                $model->akumulasi = "E";
                            }
                            $model->save(false);
							return $this->redirect(['view', 'id' => $model->id_nilai]);
                        }
            return $this->redirect(['view', 'id' => $model->id_nilai]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing NilaiSeminar model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the NilaiSeminar model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return NilaiSeminar the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = NilaiSeminar::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
