<?php

namespace backend\controllers;

use Yii;
use common\models\Merchandise;
use common\models\MerchandiseSearch;
use common\models\Villages;
use common\models\VillagesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\imagine\Image;
use yii\filters\AccessControl;
/**
 * MerchandiseController implements the CRUD actions for Merchandise model.
 */
class MerchandiseController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
                       'access'=>[
                'class'=>AccessControl::className(),
                'rules'=>[
                    [
                      
                        'allow'=>true,
                          'roles'=>['@'],
                        'matchCallback'=>function(){
                            return (
                                Yii::$app->user->can('superAdmin')
                            );
                        }
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Merchandise models.
     * @return mixed
     */
    
      public function actionIndex()
    {
        $searchModel = new VillagesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionDataMerchant()
    {
        $searchModel = new MerchandiseSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('data-merchant', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Merchandise model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $foto= \common\models\FotoMerchandise::find()->where(['id_merchandise'=>$id])->all();
        return $this->render('view', [
            'model' => $this->findModel($id),

            'foto'=>$foto,
        ]);
    }

    /**
     * Creates a new Merchandise model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id=NULL)
    {
        $desa= Villages::find()->where(['id'=>$id])->one();
        if ($desa){
           
              $model = new Merchandise();
         $user = Yii::$app->user->identity->id;
         $model->id_desa = $desa->id;
         $model->id_provinsi = $desa->provinsi;
        if ($model->load(Yii::$app->request->post())) {
            $foto = new \common\models\FotoMerchandise();
            $model2 = new Merchandise();

            $model2->picture = UploadedFile::getInstances($model, 'picture');


            if ($model2->picture) {
                $merchant = new Merchandise();
                $merchant->id_merchandise = $model->id_merchandise;
                $merchant->nama = $model->nama;
                $merchant->deskripsi = $model->deskripsi;
                $merchant->alamat = $model->alamat;
                $merchant->harga = $model->harga;
                $merchant->id_user = $user;
                $merchant->id_desa = $model->id_desa;
                $merchant->id_provinsi = $model->id_provinsi;
                $merchant->kategori = $model->kategori;
                $merchant->save(false);
               
                $tampungdata = $merchant->id_merchandise;
               
                foreach ($model2->picture as $file) {
                    $img = new \common\models\FotoMerchandise();
                   
                    $fileName = time() . $file->baseName . '.' . $file->getExtension();

                    
                    if ($file->saveAs(('../uploads/merchant/') . $fileName)) {
                        $val = Image::thumbnail(('../uploads/merchant/') . $fileName, 320, 320)
                                ->save(('../uploads/merchant/') . $fileName, ['quality' => 50]);
                        if ($val) {
                           
                            $img->id_merchandise = $tampungdata;
                             $img->foto = $fileName;
                            $img->save(false);
                        }
                    }
                }
            } else {
                $model->save(false);

                
            }
            return $this->redirect(['data-merchant',]);
        }else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
        }else{
                Yii::$app->session->setFlash('error', 'Data tidak ditemukan');
                 return $this->redirect(['index',]);
        }
       
    }

    /**
     * Updates an existing Merchandise model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
            $model = $this->findModel($id);
        
        if ($model->load(Yii::$app->request->post())) {
           $foto = new \common\models\FotoMerchandise();
            $model2 = new Merchandise();

            $model2->picture = UploadedFile::getInstances($model, 'picture');


            if ($model2->picture) {
                Yii::$app->db->createCommand("DELETE FROM foto_merchandise WHERE id_merchandise = '$id' ")->execute();
               $update = new Merchandise();
              
                $model->save();
               
              $tampungdata = $model->id_merchandise;
                 
                foreach ($model2->picture as $file) {
                    $img = new \common\models\FotoMerchandise();

                    $fileName = time() . $file->baseName . '.' . $file->getExtension();

                    
                    if ($file->saveAs(('../uploads/merchant/') . $fileName)) {
                        $val = Image::thumbnail(('../uploads/merchant/') . $fileName, 320, 320)
                                ->save(('../uploads/merchant/') . 'thumb-' . $fileName, ['quality' => 50]);
                        if ($val) {
                            $img->id_merchandise = $tampungdata;
                            $img->foto = $fileName;
                           
                            $img->save(false);
                        }
                    }
                }
               
            } else {
               // Yii::$app->db->createCommand("UPDATE foto_koleksi SET date = '$model->date' WHERE id_email = '$id' ")->execute();
               
               $model->save();
               
                
                
            }
           
           return $this->redirect(['view','id'=>$model->id_merchandise]);
        } else {
            return $this->render('update', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Merchandise model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['data-merchant']);
    }

    /**
     * Finds the Merchandise model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Merchandise the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Merchandise::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
