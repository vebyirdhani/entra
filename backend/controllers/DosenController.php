<?php

namespace backend\controllers;

use Yii;
use common\models\Dosen;
use common\models\DosenSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use common\models\CsvForm;
use yii\filters\AccessControl;
use yii\data\ActiveDataProvider;
/**
 * DosenController implements the CRUD actions for Dosen model.
 */
class DosenController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function() {
                            return (
                                    Yii::$app->user->identity->level == 1
                                    );
                        }
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Dosen models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new DosenSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Dosen model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Dosen model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new Dosen();
        $dsn = Dosen::find()->orderBy('id_dosen DESC');
            $dataProvider = new ActiveDataProvider([
            'query' =>$dsn,
            'pagination' => [
                'pagesize' => 10
            ]
        ]);
        if ($model->load(Yii::$app->request->post())) {
            $cari = Dosen::find()->where(['nidn'=>$model->nidn])->all();
              $email = dosen::find()->where(['email'=>$model->email])->all();
            if ($cari){
                   Yii::$app->session->setFlash('error', 'NIDN sudah ada!');
                     return $this->render('create', [
                        'model' => $model,
                'dataProvider'=>$dataProvider,
            ]);
            }
			else if ($email){
                  Yii::$app->session->setFlash('error', 'email sudah ada!');
                     return $this->render('create', [
                        'model' => $model,
                'dataProvider'=>$dataProvider,
            ]);
            }else{
                 $model->save(false);
            $user = new \common\models\User();

            $user->username = $model->nidn;
            $user->email = $model->email;
            $user->level = 2;
            $user->setPassword($model->nidn);
            $user->generateAuthKey();
            $user->save(false);


            $model->id_user = $user->id;
            $model->save(false);
            $profil = new \common\models\Profile();
            $profil->user_id = $user->id;
            $profil->name = $model->nama;
            $profil->save(false);
                Yii::$app->session->setFlash('success', 'Data berhasil di inputkan');
            return $this->redirect(['create']);
            }
           
        } else {
            return $this->render('create', [
                        'model' => $model,
                'dataProvider'=>$dataProvider,
            ]);
        }
    }

    public function actionImport() {
        $model = new CsvForm();
        if ($model->load(Yii::$app->request->post())) {
            $file = UploadedFile::getInstance($model, 'file');
            $filename = 'Data' . date('dmYhis') . '.' . $file->extension;
            $upload = $file->saveAs(Yii::getAlias('./excel/dosen/') . $filename);

            if ($upload) {
                define('CSV_PATH', 'excel/dosen/');
                $csv_file = CSV_PATH . $filename;
                $filecsv = file($csv_file);
//                 print_r($filecsv);
//                 die();
                foreach ($filecsv as $data) {

                    $hasil = explode(",", $data);

                    $dosen = new Dosen();
                    $dosen->nama = $hasil[0];
                    $dosen->nidn = $hasil[1];
                   
                    $dosen->email = $hasil[2];

                    $dosen->jenis_kelamin = $hasil[3];
                    $dosen->agama = $hasil[4];
                
                    $dosen->jabatan = $hasil[5];
                  
                    $cekdosen = Dosen::find()->where(['nidn' => $dosen->nidn])->one();
                    if (!$cekdosen) {
                        $user = new \common\models\User();

                        $user->username = $dosen->nidn;
                        $user->email = $dosen->email;
                        $user->level = 2;
                        $user->setPassword($dosen->nidn);
                        $user->generateAuthKey();
                        $user->save(false);


                        $dosen->id_user = $user->id;
                        $dosen->save(false);
                        $profil = new \common\models\Profile();
                        $profil->user_id = $user->id;
                        $profil->name = $dosen->nama;
                        $profil->save(false);
                    }
                }
                Yii::$app->session->setFlash('success', 'Import Berhasil . . !');
                return $this->redirect(['index']);
            }
        } else {
            return $this->render('import', [
                        'model' => $model
            ]);
        }
    }

    public function actionUpload() {
        $model = new CsvForm();

        if ($model->load(Yii::$app->request->post())) {
            $file = UploadedFile::getInstance($model, 'file');
            $filename = 'Data.' . $file->extension;
            $upload = $file->saveAs(Yii::getAlias('@home/www/akasys/excel/dosen/') . $filename);

            if ($upload) {
                define('CSV_PATH', 'excel/dosen/');
                $csv_file = CSV_PATH . $filename;
                $filecsv = file($csv_file);

                foreach ($filecsv as $data) {
                    $modelnew = new Dosen;
                    $hasil = explode(",", $data);

                    $modelnew->nip = $hasil[0];
                    $modelnew->nama = $hasil[1];
                    $modelnew->prodi = $hasil[2];
                    $modelnew->save(false);
                }
                // unlink('uploads/'.$filename);
                return $this->redirect(['index']);
            }
        } else {
            return $this->render('upload', ['model' => $model]);
        }
    }

    /**
     * Updates an existing Dosen model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save(false)) {
			$caridata = \common\models\Dosen::find()->where(['id_dosen'=>$model->id_dosen])->one();
			
			$user =\common\models\User::find()->where(['username'=>$caridata->nidn])->one();
			$user->email = $caridata->email;
			//$user->username = $caridata->nidn;
			$user->save(false);
            return $this->redirect(['view', 'id' => $model->id_dosen]);
        } else {
            return $this->render('update', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Dosen model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
		$caridata = \common\models\Dosen::find()->where(['id_dosen'=>$id])->one();
		$carinidn = \common\models\User::find()->where(['username'=>$caridata->nidn])->one();
			if ($carinidn){
		$userid= $carinidn->id;
	
		Yii::$app->db->createCommand("DELETE FROM user  WHERE id = '$userid'")->execute(); }
        $this->findModel($id)->delete();
		 Yii::$app->session->setFlash('success', 'Data berhasil dihapus');
        return $this->redirect(['index']);
    }

    /**
     * Finds the Dosen model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Dosen the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Dosen::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
