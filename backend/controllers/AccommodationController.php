<?php

namespace backend\controllers;

use Yii;
use common\models\Accommodation;
use common\models\AccommodationSearch;
use common\models\Villages;
use common\models\VillagesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\imagine\Image;
use yii\filters\AccessControl;
/**
 * AccommodationController implements the CRUD actions for Accommodation model.
 */
class AccommodationController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
                    'access'=>[
                'class'=>AccessControl::className(),
                'rules'=>[
                    [
                      
                        'allow'=>true,
                          'roles'=>['@'],
                        'matchCallback'=>function(){
                            return (
                                Yii::$app->user->can('superAdmin')
                            );
                        }
                    ],
                ],
            ],
            ],
        ];
    }

    /**
     * Lists all Accommodation models.
     * @return mixed
     */
      public function actionIndex()
    {
        $searchModel = new VillagesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    public function actionDataAkomodasi()
    {
        $searchModel = new AccommodationSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('data-akomodasi', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Accommodation model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Accommodation model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
   public function actionCreate($id=NULL)
    {
        $desa= Villages::find()->where(['id'=>$id])->one();
        
        if ($desa){
           
              $model = new Accommodation();
         $user = Yii::$app->user->identity->id;
         $model->id_desa = $desa->id;
         $model->id_provinsi = $desa->provinsi;
         $model->id_kecamatan= $desa->district_id;
        if ($model->load(Yii::$app->request->post())) {
            $foto = new \common\models\FotoAccommodation();
            $model2 = new Accommodation();

            $model2->picture = UploadedFile::getInstances($model, 'picture');


            if ($model2->picture) {
                $akomodasi = new Accommodation();
                $akomodasi->id_homestay = $model->id_homestay;
                $akomodasi->id_kategori = $model->id_kategori;
                $akomodasi->nama_homestay = $model->nama_homestay;
               
                $akomodasi->harga = $model->harga;
                $akomodasi->id_user = $user;
                $akomodasi->id_desa = $model->id_desa;
                $akomodasi->id_provinsi = $model->id_provinsi;
                $akomodasi->id_kecamatan = $model->id_kecamatan;
                $akomodasi->rating = $model->rating;
                $akomodasi->save(false);
               
                $tampungdata = $akomodasi->id_homestay;
               
                foreach ($model2->picture as $file) {
                    $img = new \common\models\FotoAccommodation();
                   
                    $fileName = time() . $file->baseName . '.' . $file->getExtension();

                    
                    if ($file->saveAs(('../uploads/akomodasi/') . $fileName)) {
                        $val = Image::thumbnail(('../uploads/akomodasi/') . $fileName, 320, 320)
                                ->save(('../uploads/akomodasi/') . $fileName, ['quality' => 50]);
                        if ($val) {
                           
                            $img->id_homestay = $tampungdata;
                             $img->foto = $fileName;
                            $img->save(false);
                        }
                    }
                }
            } else {
                $model->save(false);

                
            }
            return $this->redirect(['data-akomodasi',]);
        }else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
        }else{
                Yii::$app->session->setFlash('error', 'Data tidak ditemukan');
                 return $this->redirect(['index',]);
        }
       
    }

    /**
     * Updates an existing Merchandise model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
            $model = $this->findModel($id);
        
        if ($model->load(Yii::$app->request->post())) {
           $foto = new \common\models\FotoAccommodation();
            $model2 = new Accommodation();

            $model2->picture = UploadedFile::getInstances($model, 'picture');


            if ($model2->picture) {
                Yii::$app->db->createCommand("DELETE FROM foto_accommodation WHERE id_homestay = '$id' ")->execute();
               $update = new Accommodation();
              
               
                if ($model->id_kecamatan==NULL){
                    $cari = Villages::find()->where(['id'=>$model->id_desa])->one();
                    $model->id_kecamatan= $cari->district_id;
                }
                 $model->save();
              $tampungdata = $model->id_homestay;
                 
                foreach ($model2->picture as $file) {
                    $img = new \common\models\FotoAccommodation();

                    $fileName = time() . $file->baseName . '.' . $file->getExtension();

                    
                    if ($file->saveAs(('../uploads/akomodasi/') . $fileName)) {
                        $val = Image::thumbnail(('../uploads/akomodasi/') . $fileName, 320, 320)
                                ->save(('../uploads/akomodasi/') . 'thumb-' . $fileName, ['quality' => 50]);
                        if ($val) {
                            $img->id_homestay = $tampungdata;
                            $img->foto = $fileName;
                           
                            $img->save(false);
                        }
                    }
                }
               
            } else {
               // Yii::$app->db->createCommand("UPDATE foto_koleksi SET date = '$model->date' WHERE id_email = '$id' ")->execute();
                  if ($model->id_kecamatan==NULL){
                    $cari = Villages::find()->where(['id'=>$model->id_desa])->one();
                    $model->id_kecamatan= $cari->district_id;
                }
               $model->save();
               
                
                
            }
           
           return $this->redirect(['view','id'=>$model->id_homestay]);
        } else {
            return $this->render('update', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Accommodation model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Accommodation model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Accommodation the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Accommodation::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
