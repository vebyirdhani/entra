<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $model common\models\JadwalKuliah */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="jadwal-kuliah-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'kelas_nama')->textInput(['maxlength' => true]) ?>
  <?= $form->field($model, 'kode_matakuliah')->textInput(['maxlength' => true]) ?>

    
      <?=
                    $form->field($model, 'mata_kuliah', [
                      
                    ])->widget(Select2::className(), [
                        'data' => ArrayHelper::map(common\models\MataKuliah::find()->all(), 'id_matkul', 'nama'),
                        'language' => 'en',
                        'options' => [
                            'placeholder' => 'Pilih Mata Kuliah ...',
                           
                        ],
                        'pluginOptions' => [
                            'allowClear' => true
                        ]
                    ])
                    ?> 
 <?= $form->field($model, 'prodi')->dropDownList(ArrayHelper::map(\common\models\Prodi::find()->all(), 'id_prodi', 'nama_prodi'),[
                        'prompt' => 'Silahkan Pilih Prodi'
                    ]); ?>
   <?=
                    $form->field($model, 'dosen', [
                      
                    ])->widget(Select2::className(), [
                        'data' => ArrayHelper::map(common\models\Dosen::find()->all(), 'id_dosen', 'nama'),
                        'language' => 'en',
                        'options' => [
                            'placeholder' => 'Pilih Dosen ...',
                           
                        ],
                        'pluginOptions' => [
                            'allowClear' => true
                        ]
                    ])
                    ?> 

      <?=
                    $form->field($model, 'dosen2', [
                      
                    ])->widget(Select2::className(), [
                        'data' => ArrayHelper::map(common\models\Dosen::find()->all(), 'id_dosen', 'nama'),
                        'language' => 'en',
                        'options' => [
                            'placeholder' => 'Pilih Dosen ...',
                           
                        ],
                        'pluginOptions' => [
                            'allowClear' => true
                        ]
                    ])
                    ?> 

    <?= $form->field($model, 'semester')->dropDownList(ArrayHelper::map(\common\models\Semester::find()->all(), 'keterangan', 'keterangan'),[
                        'prompt' => 'Silahkan Pilih Periode'
                    ]); ?>
        
   <?= $form->field($model, 'hari')->dropDownList(
            ['prompt'=>'Pilih Hari','Senin'=>'Senin','Selasa'=>'Selasa','Rabu'=>'Rabu','Kamis'=>'Kamis','Jumat'=>'Jumat','Sabtu'=>'Sabtu','Minggu'=>'Minggu']) ?>
      <?= $form->field($model, 'jadwal_awal')->dropDownList(
            ArrayHelper::map(common\models\JamKuliah::find()->all(),'jam_kuliah','jam_kuliah'),
            ['prompt'=>'Pilih Jam']) ?>
   
        <?= $form->field($model, 'jadwal_akhir')->dropDownList(
            ArrayHelper::map(common\models\JamKuliah::find()->all(),'jam_kuliah','jam_kuliah'),
            ['prompt'=>'Pilih Jam']) ?>
    
    <?= $form->field($model, 'sks')->textInput(['maxlength' => true]) ?>
  <?= $form->field($model, 'jumlah_pertemuan')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
