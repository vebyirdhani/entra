<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\JadwalKuliahSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Jadwal Kuliah';
$this->params['breadcrumbs'][] = $this->title;
$tombol='{tambah}{view}{update}{delete}';
?>

<div class="row">
    <div class="col-lg-3 col-md-3 admin-menu-right-line">
        <div class="box-header with-border">
            <h3 class="box-title">Menu</h3>
        </div>
        <div class="box-body no-padding">
            <ul class="nav nav-pills nav-stacked">
                <li class="active" style="background-color: #f4f4f4"><a href="<?= Url::toRoute(['index']) ?>"><i class="fa fa-list-alt"></i> Data <?= Html::encode($this->title) ?> </a></li>
                <li ><a href="<?=  Url::toRoute(['create']) ?>"><i class="fa fa-pencil-square-o"></i> Input Data <?= Html::encode($this->title) ?></a></li>
            </ul>
        </div>
    </div>
    <div class="col-lg-9 col-md-9">
        <div class="jadwal-kuliah-index">

            <h1><?= Html::encode($this->title) ?></h1>
                                <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
            
            <div class="table-responsive">
                                                    <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                 //   'filterModel' => $searchModel,
        'columns' => [
                    [
                    'class' => 'yii\grid\SerialColumn',
                    'header' => 'No.'
                    ],

                    //            'id_jadwal',
            'kelas_nama',
            'kode_matakuliah',
            [
                            'attribute'=> 'mata_kuliah',
                  
                            'value' => 'idMatakuliah.nama',
                        ],
 [
                            'attribute'=> 'dosen',
                  
                            'value' => 'idDosen1.nama',
                        ],
//           [
//                            'attribute'=> 'dosen2',
//                  
//                            'value' => 'idDosen2.nama',
//                        ],
            'hari',
            'jadwal_awal',
             'jadwal_akhir',
            'semester',
          
            // 'jumlah_mhs',
            // 'pertemuan',

                        [
                    'class' => 'yii\grid\ActionColumn','header'=>'Action',
                    'template' => $tombol,
                    'buttons' => [
                        'tambah' => function ($url, $model, $key){
                           
                             return  Html::a('<i class=""></i>',['detail-jadwalkuliah/data-mahasiswa', 'id' => $model->id_jadwal], ['class' => 'glyphicon glyphicon-list']); 
                        }, 
                         
                                
                        'view' => function ($url, $model, $key){
                           
                             return  Html::a('<i class=""></i>',['view', 'id' => $model->id_jadwal], ['class' => 'glyphicon glyphicon-eye-open']); 
                        },
                        'update' => function ($url, $model, $key){
               
                            return  Html::a('<i class=""></i>',['update', 'id' => $model->id_jadwal], ['class' => 'glyphicon glyphicon-pencil']); 
                        },
                        'delete' => function ($url, $model, $key){
                         
                            return Html::a('<i class="glyphicon glyphicon-trash"></i> <?= Html::encode($modelName) ?>', ['delete', 'id' => $model->id_jadwal], [
                        'data' => [
                            'confirm' => 'Hapus?',
                            'method' => 'post',
                        ],
                    ]); 
                        
                            
                            
                            },

                    ]
                ],
                    ],
                    ]); ?>
                                
            </div>
        </div>

    </div>
</div>