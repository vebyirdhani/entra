<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\JadwalKuliahSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="jadwal-kuliah-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id_jadwal') ?>

    <?= $form->field($model, 'kelas_nama') ?>

    <?= $form->field($model, 'mata_kuliah') ?>

    <?= $form->field($model, 'dosen') ?>

    <?= $form->field($model, 'jadwal_awal') ?>

    <?php // echo $form->field($model, 'jadwal_akhir') ?>

    <?php // echo $form->field($model, 'jumlah_mhs') ?>

    <?php // echo $form->field($model, 'pertemuan') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
