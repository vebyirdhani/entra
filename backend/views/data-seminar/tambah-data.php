<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;


/* @var $this yii\web\View */
/* @var $model common\models\DataSeminar */
$kategori = \common\models\KategoriSeminar::find()->where(['id_kategori'=>$cari->kategori])->one();
$modelName = 'Data Seminar';
$this->title = 'Input  '.$kategori->kategori;
$this->params['breadcrumbs'][] = ['label' => 'Data Seminar', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <div class="col-lg-3 col-md-3 admin-menu-right-line">
        <div class="box-header with-border">
            <h3 class="box-title">Menu</h3>
        </div>
        <div class="box-body no-padding">
            <ul class="nav nav-pills nav-stacked">
                <li><a href="<?= Url::toRoute(['index']) ?>"><i class="fa fa-list-alt"></i>  <?= Html::encode($modelName) ?> </a></li>
<!--                <li class="active" style="background-color: #f4f4f4"><a href="#"><i class="fa fa-pencil-square-o"></i> Input  <?= Html::encode($modelName) ?></a></li>-->
                <?php  $date = date('Y-m-d');

        $programs = \common\models\JadwalSeminar::find()->where(['<=', 'jadwal_awal', $date])
                        ->andWhere(['>=', 'jadwal_akhir', $date])
                        ->andWhere(['status' => 1])->orderBy('id_jadwal DESC')->all();
                        if ($programs){
                            foreach ($programs as  $value) {
                             
                            $kategori = \common\models\KategoriSeminar::find()->where(['id_kategori'=>$value->kategori])->one();
?>
                <?php if ($value->kategori==1){ ?>
                <li ><a href="<?=  Url::toRoute(['tambah-data','id'=>$value->id_jadwal]) ?>"><i class="fa fa-pencil-square-o"></i> Input  <?= $kategori->kategori ?></a></li>
                <?php } ?>
                 <?php if ($value->kategori==2){ ?>
                <li ><a href="<?=  Url::toRoute(['tambah-datasatu','id'=>$value->id_jadwal]) ?>"><i class="fa fa-pencil-square-o"></i> Input  <?= $kategori->kategori ?></a></li>
                <?php } ?>
                 <?php if ($value->kategori==3){ ?>
                <li ><a href="<?=  Url::toRoute(['tambah-data','id'=>$value->id_jadwal]) ?>"><i class="fa fa-pencil-square-o"></i> Input  <?= $kategori->kategori ?></a></li>
                <?php } ?>
                 <?php if ($value->kategori==4){ ?>
                <li ><a href="<?=  Url::toRoute(['tambah-datasatu','id'=>$value->id_jadwal]) ?>"><i class="fa fa-pencil-square-o"></i> Input  <?= $kategori->kategori ?></a></li>
                <?php } ?>
 <?php } }?>
            </ul>
        </div>
    </div>
    <div class="col-lg-9 col-md-9">

        <div class="admin-create">

            <div class="data-seminar-create">

              
                <h3>Mulai tanggal <?= $cari->jadwal_awal?> hingga <?= $cari->jadwal_akhir?></h3>

                <?= $this->render('_form', [
                'model' => $model,
                ]) ?>
			
					<div class="col-lg-12">
                  <div class="table-responsive">
 <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                  //  'filterModel' => $searchModel,
        'columns' => [
                    [
                    'class' => 'yii\grid\SerialColumn',
                    'header' => 'No.'
                    ],

                               // 'id_data',
        

        //    'judul',
           
  [
                            'attribute'=> 'dosen_pembimbing1',
                  
                            'value' => 'idDosen1.nama',
                        ],

        
       
            'tanggal_seminar',
             [
                            'attribute'=> 'mahasiswa',
                  
                            'value' => 'idMahasiswa.nama',
                        ],
            // 'tanggal_seminar',

//                    ['class' => 'yii\grid\ActionColumn'],
                    ],
                    ]); ?></div>
           </div>
</div>

        </div>
    </div>
</div>