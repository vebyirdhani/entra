<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $searchModel common\models\DataSeminarSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Data Seminar';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <div class="col-lg-3 col-md-3 admin-menu-right-line">
        <div class="box-header with-border">
            <h3 class="box-title">Menu</h3>
        </div>
        <div class="box-body no-padding">
            <ul class="nav nav-pills nav-stacked">
                <li class="active" style="background-color: #f4f4f4"><a href="<?= Url::toRoute(['index']) ?>"><i class="fa fa-list-alt"></i>  <?= Html::encode($this->title) ?> </a></li>
                <?php  $date = date('Y-m-d');

        $programs = \common\models\JadwalSeminar::find()->where(['<=', 'jadwal_awal', $date])
                        ->andWhere(['>=', 'jadwal_akhir', $date])
                        ->andWhere(['status' => 1])->orderBy('id_jadwal DESC')->all();
						
                        if ($programs){
                            foreach ($programs as  $value) {
                             
                            $kategori = \common\models\KategoriSeminar::find()->where(['id_kategori'=>$value->kategori])->one();
?>
                <?php if ($value->kategori==1){ ?>
                <li ><a href="<?=  Url::toRoute(['tambah-data','id'=>$value->id_jadwal]) ?>"><i class="fa fa-pencil-square-o"></i> Input  <?= $kategori->kategori ?></a></li>
                <?php } ?>
                 <?php if ($value->kategori==2){ ?>
                <li ><a href="<?=  Url::toRoute(['tambah-datasatu','id'=>$value->id_jadwal]) ?>"><i class="fa fa-pencil-square-o"></i> Input  <?= $kategori->kategori ?></a></li>
                <?php } ?>
                 <?php if ($value->kategori==3){ ?>
                <li ><a href="<?=  Url::toRoute(['tambah-data','id'=>$value->id_jadwal]) ?>"><i class="fa fa-pencil-square-o"></i> Input  <?= $kategori->kategori ?></a></li>
                <?php } ?>
                 <?php if ($value->kategori==4){ ?>
                <li ><a href="<?=  Url::toRoute(['tambah-datasatu','id'=>$value->id_jadwal]) ?>"><i class="fa fa-pencil-square-o"></i> Input  <?= $kategori->kategori ?></a></li>
                <?php } ?>
 <?php } }?>
            </ul>
        </div>
    </div>
    <div class="col-lg-9 col-md-9">
        <div class="data-seminar-index">

            <h2>Data seminar</h2>
         
                           <?php $form = ActiveForm::begin([]);?>
  <?= $form->field($model, 'kategori')->dropDownList(ArrayHelper::map(common\models\KategoriSeminar::find()->all(), 'id_kategori', 'kategori'),[
                        'prompt' => 'Silahkan Pilih Kategori Seminar'
                    ]); ?>
                    <?= $form->field($model, 'nama')->textInput(['style'=>'width:300px', 'placeholder' => 'Ketikan Nama / NIM Mahasiswa']) ?>
                    
                  
                    
                    <?= Html::submitButton('Search',['class' => 'btn btn-primary']); ?>
                       <?= Html::a('<i class=""></i>Reset', ['data-seminar/'], ['class' => 'btn btn-default']) ?>
                    <?php ActiveForm::end(); ?>
            
            <div class="table-responsive">
                                                    <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                   // 'filterModel' => $searchModel,
        'columns' => [
                    [
                    'class' => 'yii\grid\SerialColumn',
                    'header' => 'No.'
                    ],
//
//                                'id_data',
               'nama',
            'nim',
             'tanggal_seminar',
            [
                            'attribute'=> 'kategori',
                            'value' => 'idKategori.kategori',
                        ],
         //   'judul',
                [
                            'attribute'=> 'dosen_pembimbing1',
                            'value' => 'idDosen1.nama',
                        ],
           [
                            'attribute'=> 'dosen_pembimbing2',
                            'value' => 'idDosen2.nama',
                        ],
            [
                            'attribute'=> 'penguji1',
                            'value' => 'idPenguji1.nama',
                        ],
              [
                            'attribute'=> 'penguji2',
                  
                            'value' => 'idPenguji2.nama',
                        ],
       

                    ['class' => 'yii\grid\ActionColumn'],
                    ],
                    ]); ?>
                                
            </div>
        </div>

    </div>
</div>