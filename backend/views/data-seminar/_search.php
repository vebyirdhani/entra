<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $model common\models\DataSeminarSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="data-seminar-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?php // $form->field($model, 'id_data') ?>

     <?= $form->field($model, 'kategori')->dropDownList(ArrayHelper::map(common\models\KategoriSeminar::find()->all(), 'id_kategori', 'kategori'),[
                        'prompt' => 'Silahkan Pilih Kategori Seminar'
                    ]); ?>

    <?php // $form->field($model, 'judul') ?>

    <?php // $form->field($model, 'dosen_pembimbing1') ?>

    <?php // $form->field($model, 'dosen_pembimbing2') ?>

    <?php // echo $form->field($model, 'mahasiswa') ?>

    <?php // echo $form->field($model, 'tanggal_seminar') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
