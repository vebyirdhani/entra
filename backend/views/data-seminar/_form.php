<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use kartik\date\DatePicker

/* @var $this yii\web\View */
/* @var $model common\models\DataSeminar */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="data-seminar-form">

    <?php $form = ActiveForm::begin(); ?>

  <div class="col-lg-3">

    <?= $form->field($model, 'judul')->textInput(['maxlength' => true]) ?>
</div>
  <div class="col-lg-3">
        <?=
                    $form->field($model, 'mahasiswa', [
                      
                    ])->widget(Select2::className(), [
                        'data' => ArrayHelper::map(common\models\Mahasiswa::find()->all(), 'id_mahasiswa', 'nama'),
                        'language' => 'en',
                        'options' => [
                            'placeholder' => 'Pilih Mahasiswa ...',
                           
                        ],
                        'pluginOptions' => [
                            'allowClear' => true
                        ]
                    ])
                    ?> </div>
					  <div class="col-lg-3">
     <?=
                    $form->field($model, 'dosen_pembimbing1', [
                      
                    ])->widget(Select2::className(), [
                        'data' => ArrayHelper::map(common\models\Dosen::find()->all(), 'id_dosen', 'nama'),
                        'language' => 'en',
                        'options' => [
                            'placeholder' => 'Pilih Dosen ...',
                           
                        ],
                        'pluginOptions' => [
                            'allowClear' => true
                        ]
                    ])
                    ?> </div>
					  <div class="col-lg-3">
      <?=
                    $form->field($model, 'dosen_pembimbing2', [
                      
                    ])->widget(Select2::className(), [
                        'data' => ArrayHelper::map(common\models\Dosen::find()->all(), 'id_dosen', 'nama'),
                        'language' => 'en',
                        'options' => [
                            'placeholder' => 'Pilih Dosen ...',
                           
                        ],
                        'pluginOptions' => [
                            'allowClear' => true
                        ]
                    ])
                    ?> </div>
  <div class="col-lg-3">
      <?=
                    $form->field($model, 'penguji1', [
                      
                    ])->widget(Select2::className(), [
                        'data' => ArrayHelper::map(common\models\Dosen::find()->all(), 'id_dosen', 'nama'),
                        'language' => 'en',
                        'options' => [
                            'placeholder' => 'Pilih Dosen ...',
                           
                        ],
                        'pluginOptions' => [
                            'allowClear' => true
                        ]
                    ])
                    ?> </div>
					  <div class="col-lg-3">
      <?=
                    $form->field($model, 'penguji2', [
                      
                    ])->widget(Select2::className(), [
                        'data' => ArrayHelper::map(common\models\Dosen::find()->all(), 'id_dosen', 'nama'),
                        'language' => 'en',
                        'options' => [
                            'placeholder' => 'Pilih Dosen ...',
                           
                        ],
                        'pluginOptions' => [
                            'allowClear' => true
                        ]
                    ])
                    ?> </div>
					  <div class="col-lg-3">
      <?=
                    $form->field($model, 'penguji3', [
                      
                    ])->widget(Select2::className(), [
                        'data' => ArrayHelper::map(common\models\Dosen::find()->all(), 'id_dosen', 'nama'),
                        'language' => 'en',
                        'options' => [
                            'placeholder' => 'Pilih Dosen ...',
                           
                        ],
                        'pluginOptions' => [
                            'allowClear' => true
                        ]
                    ])
                    ?> </div>
					  <div class="col-lg-3">
      <?=
                    $form->field($model, 'penguji4', [
                      
                    ])->widget(Select2::className(), [
                        'data' => ArrayHelper::map(common\models\Dosen::find()->all(), 'id_dosen', 'nama'),
                        'language' => 'en',
                        'options' => [
                            'placeholder' => 'Pilih Dosen ...',
                           
                        ],
                        'pluginOptions' => [
                            'allowClear' => true
                        ]
                    ])
                    ?> 
 </div>
   <div class="col-lg-3">

   <?=
    $form->field($model, 'tanggal_seminar')->widget(
            DatePicker::className(), [
            'name' => 'tgl_lahir',
            'type' => DatePicker::TYPE_INPUT,
            'pluginOptions' => [
            'autoclose' => true,
            'format' => 'yyyy-mm-dd',
        ],
    ])
    ?></div>
	  <div class="col-lg-3">
     <?= $form->field($model, 'jadwal_mulai')->textInput(['maxlength' => true]) ?></div>
	   <div class="col-lg-3">
 <?= $form->field($model, 'jadwal_selesai')->textInput(['maxlength' => true]) ?></div>
 
    <div class="col-lg-12">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
