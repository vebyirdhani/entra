<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\RuteSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="rute-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id_rute') ?>

    <?= $form->field($model, 'daerah_mulai') ?>

    <?= $form->field($model, 'daerah_akhir') ?>

    <?= $form->field($model, 'harga') ?>

    <?= $form->field($model, 'id_transport') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
