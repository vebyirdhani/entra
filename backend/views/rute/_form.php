<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Rute */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="rute-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'daerah_mulai')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'daerah_akhir')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'harga')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'id_transport')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
