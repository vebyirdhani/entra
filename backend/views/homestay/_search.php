<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\HomestaySearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="homestay-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id_homestay') ?>

    <?= $form->field($model, 'id_kategori') ?>

    <?= $form->field($model, 'nama_homestay') ?>

    <?= $form->field($model, 'id_user') ?>

    <?= $form->field($model, 'id_desa') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
