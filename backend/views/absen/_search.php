<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\AbsenSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="absen-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id_absen') ?>

    <?= $form->field($model, 'dosen') ?>

    <?= $form->field($model, 'mahasiswa') ?>

    <?= $form->field($model, 'jam_mulai') ?>

    <?= $form->field($model, 'jam_akhir') ?>

    <?php // echo $form->field($model, 'tanggal') ?>

    <?php // echo $form->field($model, 'materi_kuliah') ?>

    <?php // echo $form->field($model, 'pertemuan') ?>

    <?php // echo $form->field($model, 'mata_kuliah') ?>

    <?php // echo $form->field($model, 'status_hadir') ?>

    <?php // echo $form->field($model, 'keterangan') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
