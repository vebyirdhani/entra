<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $model common\models\SubKategoriExperience */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="sub-kategori-experience-form">

    <?php $form = ActiveForm::begin(); ?>
      <?= $form->field($model, 'id_kategori')->dropDownList(
            ArrayHelper::map(common\models\KategoriExperience::find()->all(),'id_kategori','nama'),
            ['prompt'=>'Pilih Kategori']) ?>

    <?= $form->field($model, 'nama')->textInput(['maxlength' => true]) ?>

  

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
