<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\DesaTravelingSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="desa-traveling-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'id_desa') ?>

    <?= $form->field($model, 'id_kecamatan') ?>

    <?= $form->field($model, 'id_provinsi') ?>

    <?= $form->field($model, 'id_pulau') ?>

    <?php // echo $form->field($model, 'rating') ?>

    <?php // echo $form->field($model, 'harga') ?>

    <?php // echo $form->field($model, 'nama') ?>

    <?php // echo $form->field($model, 'deskripsi') ?>

    <?php // echo $form->field($model, 'foto') ?>

    <?php // echo $form->field($model, 'alamat') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
