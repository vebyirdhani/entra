<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\DesaTravelingSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Desa Traveling';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <div class="col-lg-3 col-md-3 admin-menu-right-line">
        <div class="box-header with-border">
            <h3 class="box-title">Menu</h3>
        </div>
        <div class="box-body no-padding">
            <ul class="nav nav-pills nav-stacked">
                <li class="active" style="background-color: #f4f4f4"><a href="<?= Url::toRoute(['index']) ?>"><i class="fa fa-list-alt"></i> Data <?= Html::encode($this->title) ?> </a></li>
                <li ><a href="<?=  Url::toRoute(['index']) ?>"><i class="fa fa-pencil-square-o"></i> Input Data <?= Html::encode($this->title) ?></a></li>
            </ul>
        </div>
    </div>
    <div class="col-lg-9 col-md-9">
        <div class="desa-traveling-index">

            <h1><?= Html::encode($this->title) ?></h1>
                                <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
            
            <div class="table-responsive">
                                                    <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
        'columns' => [
                    [
                    'class' => 'yii\grid\SerialColumn',
                    'header' => 'No.'
                    ],

                            //    'id',
         //   'id_desa',
           // 'id_kecamatan',
            //'id_provinsi',
            //'id_pulau',
            // 'rating',
            // 'harga',
             'nama',
            // 'deskripsi:ntext',
            // 'foto',
            // 'alamat:ntext',

                    ['class' => 'yii\grid\ActionColumn'],
                    ],
                    ]); ?>
                                
            </div>
        </div>

    </div>
</div>