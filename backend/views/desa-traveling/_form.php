<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\DesaTraveling */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="desa-traveling-form">

    <?php $form = ActiveForm::begin(); ?>

  

    <?= $form->field($model, 'rating')->textInput(['maxlength' => true]) ?>



    <?= $form->field($model, 'deskripsi')->textarea(['rows' => 6]) ?>

   <?= $form->field($model, 'picture[]')->fileInput(['multiple'=>TRUE])->label('Upload Foto ') .'*Maksimal ukuran foto 2MB' ?>

    <?= $form->field($model, 'alamat')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
