<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\CoordinatSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="coordinat-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id_coordinat') ?>

    <?= $form->field($model, 'latitdude') ?>

    <?= $form->field($model, 'longitude') ?>

    <?= $form->field($model, 'nama') ?>

    <?= $form->field($model, 'kategori') ?>

    <?php // echo $form->field($model, 'id_data') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
