<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\CreditCardSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="credit-card-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id_credit') ?>

    <?= $form->field($model, 'name_card') ?>

    <?= $form->field($model, 'card_number') ?>

    <?= $form->field($model, 'expired_date') ?>

    <?= $form->field($model, 'cvv') ?>

    <?php // echo $form->field($model, 'id_user') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
