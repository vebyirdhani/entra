<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\NilaiSeminar */

$modelName = 'Nilai Seminar';
$this->title = 'Nilai Seminar | ' . $model->id_nilai;
$this->params['breadcrumbs'][] = ['label' => 'Nilai Seminar', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <div class="col-lg-3 col-md-3 admin-menu-right-line">
        <div class="box-header with-border">
            <h3 class="box-title">Menu</h3>
        </div>
        <div class="box-body no-padding">
            <ul class="nav nav-pills nav-stacked">
                <li ><a href="<?= Url::toRoute(['index']) ?>"><i class="fa fa-list-alt"></i> Data <?= Html::encode($modelName) ?></a></li>
                <li ><a href="<?= Url::toRoute(['create']) ?>"><i class="fa fa-pencil-square-o"></i> Input Data <?= Html::encode($modelName) ?></a></li>
                <li class="active" style="background-color: #f4f4f4"><a href="#"><i class="fa fa-eye"></i> Lihat Data <?= Html::encode($modelName) ?></a></li>
                <li ><a href="<?= Url::toRoute(['update', 'id' => $model->id_nilai]) ?>"><i class="fa fa-pencil"></i> Update Data <?= Html::encode($modelName) ?></a></li>
                <li >
                    <?=                     Html::a('<i class="fa fa-trash"></i> Hapus Data <?= Html::encode($modelName) ?>', ['delete', 'id' => $model->id_nilai], [
                        'data' => [
                            'confirm' => 'Hapus?',
                            'method' => 'post',
                        ],
                    ])
                    ?>
                </li>
            </ul>
        </div>
    </div>
    <div class="col-lg-9 col-md-9">
        <div class="nilai-seminar-view">

            <h1><?= Html::encode($this->title) ?></h1>

            <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                   //     'id_nilai',
          //  'id_dataseminar',
            'judul',
			   'mahasiswaid.nama',
                'dosenpemb1.nama',
             'dosenpemb2.nama',
               'dosenpeng1.nama',
                 'dosenpeng2.nama',
                   'dosenpeng3.nama',
                     'dosenpeng4.nama',
 'kategoriid.kategori',
			'nilai_pembimbing1',
			'nilai_pembimbing2',
			'nilai_penguji1',
			'nilai_penguji2',
			'nilai_penguji3',
			'nilai_penguji4',
            'total_nilaipembimbing',
            'total_nilaipenguji',
            'total_nilaiseminar',
            'total_nilaiakhir',
            'akumulasi',
            ],
            ]) ?>

        </div>


    </div>
</div> 