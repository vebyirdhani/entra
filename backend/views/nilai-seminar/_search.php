<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\NilaiSeminarSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="nilai-seminar-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id_nilai') ?>

    <?= $form->field($model, 'id_dataseminar') ?>

    <?= $form->field($model, 'judul') ?>

    <?= $form->field($model, 'mahasiswa') ?>

    <?= $form->field($model, 'dosen_pembimbing1') ?>

    <?php // echo $form->field($model, 'dosen_pembimbing2') ?>

    <?php // echo $form->field($model, 'penguji1') ?>

    <?php // echo $form->field($model, 'penguji2') ?>

    <?php // echo $form->field($model, 'penguji3') ?>

    <?php // echo $form->field($model, 'penguji4') ?>

    <?php // echo $form->field($model, 'kategori') ?>

    <?php // echo $form->field($model, 'total_nilaipembimbing') ?>

    <?php // echo $form->field($model, 'total_nilaipenguji') ?>

    <?php // echo $form->field($model, 'total_nilai') ?>

    <?php // echo $form->field($model, 'total_nilaiakhir') ?>

    <?php // echo $form->field($model, 'akumulasi') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
