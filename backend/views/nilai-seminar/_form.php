<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\NilaiSeminar */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="nilai-seminar-form">

    <?php $form = ActiveForm::begin(); ?>

  
   
    <?= $form->field($model, 'nilai_pembimbing1')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'nilai_pembimbing2')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'nilai_penguji1')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'nilai_penguji2')->textInput(['maxlength' => true]) ?>
	<?php if ($model->kategori==1 || $model->kategori==3){?>
    <?= $form->field($model, 'nilai_penguji3')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'nilai_penguji4')->textInput(['maxlength' => true]) ?>
	<?php } ?>
  
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
