<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\date\DatePicker;
/* @var $this yii\web\View */
/* @var $model common\models\JadwalSeminar */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="jadwal-seminar-form">

    <?php $form = ActiveForm::begin(); ?>

    <?=
    $form->field($model, 'jadwal_awal')->widget(
            DatePicker::className(), [
            'name' => 'tgl_lahir',
            'type' => DatePicker::TYPE_INPUT,
            'pluginOptions' => [
            'autoclose' => true,
            'format' => 'yyyy-mm-dd',
        ],
    ])
    ?>
    <?=
    $form->field($model, 'jadwal_akhir')->widget(
            DatePicker::className(), [
            'name' => 'tgl_lahir',
            'type' => DatePicker::TYPE_INPUT,
            'pluginOptions' => [
            'autoclose' => true,
            'format' => 'yyyy-mm-dd',
        ],
    ])
    ?>



     <?= $form->field($model, 'kategori')->dropDownList(
            ArrayHelper::map(common\models\KategoriSeminar::find()->all(),'id_kategori','kategori'),
            ['prompt'=>'Pilih Kategori']) ?>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
