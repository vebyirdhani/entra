<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\DataAbsenSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="data-absen-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id_dataabsen') ?>

    <?= $form->field($model, 'dosen') ?>

    <?= $form->field($model, 'mahasiswa') ?>

    <?= $form->field($model, 'keterangan') ?>

    <?= $form->field($model, 'id_absen') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
