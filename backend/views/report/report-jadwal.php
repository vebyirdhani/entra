<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use dosamigos\datepicker\DateRangePicker;
$tombol='{view}';
/* @var $this yii\web\View */
/* @var $searchModel common\models\KeuanganSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Laporan Absen';
$this->params['breadcrumbs'][] = ['label' => 'Back', 'url' => ['site']];

?>


      
    <div class="col-lg-12 col-md-12">
        <div class="keuangan-index">

        
                                <?php  echo $this->render('_searchjadwal', ['model' => $searchModel]); ?>
            
            <div class="table-responsive">
                                                     <?= GridView::widget([
                    'dataProvider' => $dataProvider,
               //     'filterModel' => $searchModel,
        'columns' => [
                    [
                    'class' => 'yii\grid\SerialColumn',
                    'header' => 'No.'
                    ],
      'kelas_nama',
            
            [
                            'attribute'=> 'mata_kuliah',
                  
                            'value' => 'idMatakuliah.nama',
                        ],
              [
                            'attribute'=> 'prodi',
                  
                            'value' => 'idProdi.nama_prodi',
                        ],
 [
                            'attribute'=> 'dosen',
                  
                            'value' => 'idDosen1.nama',
                        ],
           [
                            'attribute'=> 'dosen2',
                  
                            'value' => 'idDosen2.nama',
                        ],
            'jadwal_awal',
             'jadwal_akhir',
            'semester',
            'periode',

                           [
                    'class' => 'yii\grid\ActionColumn','header'=>'Action',
                    'template' => $tombol,
                    'buttons' => [
                       
                         
                                
                        'view' => function ($url, $model, $key){
                           
                             return  Html::a('Cek data',['report-dosen', 'id' => $model->id_jadwal], ['class' => 'btn btn-primary']); 
                        },
                       

                    ]
                ],
                    ],
                    ]); ?>
                                
            </div>
          </div>
</div>