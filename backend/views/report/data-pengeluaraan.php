<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use dosamigos\datepicker\DateRangePicker;
/* @var $this yii\web\View */
/* @var $searchModel common\models\DataPengeluaranSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Data '. $nama;
$this->params['breadcrumbs'][] = ['label' => 'Back ', 'url' => ['pengeluaran','id'=>$id]];
$datapengeluaran = common\models\DetailPengeluaran::find()->where(['jenis_pengeluaran' => $nama])->one();

?>

<div class="row">
    
    <?php $form = ActiveForm::begin([
        'action' => ['data-pengeluaran','id'=>$id,'nama'=>$nama],
        'method' => 'get',
    ]); ?>
 
                  <p>   &ensp;   &ensp;<b>Pencarian Data Berdasarkan Tanggal</b></p>   
                          <div class="form-group">
        
        <div class="col-md-5"><?php
            echo DateRangePicker::widget([
                'name' => 'tglAwal',
                'value' => $tglAwal,
                'nameTo' => 'tglAkhir',
                'valueTo' => $tglAkhir,
                'clientOptions' => [
                    'autoclose' => true,
                     'format' => 'dd-M-yyyy']
            ]);
            ?>

        </div>
                           
                                    <div class="col-md-5"><h4><B>TOTAL PENGELUARAN:<br> <?= Yii::$app->formatter->asCurrency($all) ?> </b></h4></div>
         <br>
    </div> 
                         
   <div class="col-md-5">
                          
     <?php // $form->field($model, 'detail_pemasukan')->dropDownList(
//            ArrayHelper::map(common\models\DetailPemasukan::find()->where(['project'=>$data->project])->all(),'nama','nama'),
//            ['prompt'=>'Detail Pemasukan']) ?>
                   </div> 
         <div class="col-md-12">
            <?php echo Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
           
             <?= Html::a('<i class=""></i>Reset', ['report/data-pengeluaran','id'=>$id,'nama'=>$nama], ['class' => 'btn btn-default']) ?>
            <?= Html::a('<i></i>Cetak Laporan', ['pdfpengeluaran','tglAwal' => $tglAwal, 'tglAkhir' => $tglAkhir,'id'=>$id,'nama'=>$nama], [
 
                 'class' => 'btn btn-primary',
                 'target' => '_blank',
                 'data-toggle' => 'tooltip',
                 'title' => 'liat']);?>
    <?php ActiveForm::end(); ?>
    <div class="col-lg-12 col-md-12">
        <div class="data-pengeluaran-index">

           
                     <?PHP IF ($datapengeluaran->detail1 && $datapengeluaran->detail2==NULL && $datapengeluaran->detail3 == NULL && $datapengeluaran->detail4 == NULL && $datapengeluaran->detail5 == NULL) { ?>
                <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

                <div class="table-responsive">
                    <?=
                    GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'columns' => [
                            [
                                'class' => 'yii\grid\SerialColumn',
                                'header' => 'No.'
                            ],
                         
                            [
                                'attribute' => 'jumlah_detail1',
                                'label' => $datapengeluaran->detail1,
                                'contentOptions' => ['style' => 'width:250px;  min-width:50px;  '],
                                'content' => function($row) {

                                    return $row->jumlah_detail1;
                                },
                            ],
                                        'harga',
                                         'bukti',
                                        'tanggal',
                           
                           
                        ],
                    ]);
                    ?>

                </div>
<?php } ?>
            <?PHP IF ($datapengeluaran->detail1 && $datapengeluaran->detail2 && $datapengeluaran->detail3 == NULL && $datapengeluaran->detail4 == NULL && $datapengeluaran->detail5 == NULL) { ?>
                <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

                <div class="table-responsive">
                    <?=
                    GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'columns' => [
                            [
                                'class' => 'yii\grid\SerialColumn',
                                'header' => 'No.'
                            ],
                         
                            [
                                'attribute' => 'jumlah_detail1',
                                'label' => $datapengeluaran->detail1,
                                'contentOptions' => ['style' => 'width:250px;  min-width:50px;  '],
                                'content' => function($row) {

                                    return $row->jumlah_detail1;
                                },
                            ],
                            [
                                'attribute' => 'jumlah_detail2',
                                'label' => $datapengeluaran->detail2,
                                'contentOptions' => ['style' => 'width:250px;  min-width:50px;  '],
                                'content' => function($row) {

                                    return $row->jumlah_detail2;
                                },
                            ],
                                         'harga',
                                         'bukti',
                                            'tanggal',
                          
                        ],
                    ]);
                    ?>

                </div>
<?php } ?>

            <?PHP IF ($datapengeluaran->detail1 && $datapengeluaran->detail2 && $datapengeluaran->detail3 && $datapengeluaran->detail4 == NULL && $datapengeluaran->detail5 == NULL) { ?>
                <?php // echo $this->render('_search', ['model' => $searchModel]);  ?>

                <div class="table-responsive">
    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'class' => 'yii\grid\SerialColumn',
                'header' => 'No.'
            ],
           
            [
                'attribute' => 'jumlah_detail1',
                'label' => $datapengeluaran->detail1,
                'contentOptions' => ['style' => 'width:250px;  min-width:50px;  '],
                'content' => function($row) {

                    return $row->jumlah_detail1;
                },
            ],
            [
                'attribute' => 'jumlah_detail2',
                'label' => $datapengeluaran->detail2,
                'contentOptions' => ['style' => 'width:250px;  min-width:50px;  '],
                'content' => function($row) {

                    return $row->jumlah_detail2;
                },
            ],
            [
                'attribute' => 'jumlah_detail3',
                'label' => $datapengeluaran->detail3,
                'contentOptions' => ['style' => 'width:250px;  min-width:50px;  '],
                'content' => function($row) {

                    return $row->jumlah_detail3;
                },
            ],
                         'harga',
                         'bukti',
                            'tanggal',
           
        ],
    ]);
    ?>

                </div>
                <?php } ?>

                <?PHP IF ($datapengeluaran->detail1 && $datapengeluaran->detail2 && $datapengeluaran->detail3 && $datapengeluaran->detail4 && $datapengeluaran->detail5 == NULL) { ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]);  ?>

                <div class="table-responsive">
                <?=
                GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        [
                            'class' => 'yii\grid\SerialColumn',
                            'header' => 'No.'
                        ],
                    
                       
                        [
                            'attribute' => 'jumlah_detail1',
                            'label' => $datapengeluaran->detail1,
                            'contentOptions' => ['style' => 'width:250px;  min-width:50px;  '],
                            'content' => function($row) {

                                return $row->jumlah_detail1;
                            },
                        ],
                        [
                            'attribute' => 'jumlah_detail2',
                            'label' => $datapengeluaran->detail2,
                            'contentOptions' => ['style' => 'width:250px;  min-width:50px;  '],
                            'content' => function($row) {

                                return $row->jumlah_detail2;
                            },
                        ],
                        [
                            'attribute' => 'jumlah_detail3',
                            'label' => $datapengeluaran->detail3,
                            'contentOptions' => ['style' => 'width:250px;  min-width:50px;  '],
                            'content' => function($row) {

                                return $row->jumlah_detail3;
                            },
                        ],
                        [
                            'attribute' => 'jumlah_detail4',
                            'label' => $datapengeluaran->detail4,
                            'contentOptions' => ['style' => 'width:250px;  min-width:50px;  '],
                            'content' => function($row) {

                                return $row->jumlah_detail4;
                            },
                        ],
                                     'harga',
                                     'bukti',
                                        'tanggal',
                     
                    ],
                ]);
                ?>

                </div>
                <?php } ?>
                <?PHP IF ($datapengeluaran->detail1 && $datapengeluaran->detail2 && $datapengeluaran->detail3 && $datapengeluaran->detail4 && $datapengeluaran->detail5) { ?>
                    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

                <div class="table-responsive">
                    <?=
                    GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'columns' => [
                            [
                                'class' => 'yii\grid\SerialColumn',
                                'header' => 'No.'
                            ],
                         
                            [
                                'attribute' => 'jumlah_detail1',
                                'label' => $datapengeluaran->detail1,
                                'contentOptions' => ['style' => 'width:250px;  min-width:50px;  '],
                                'content' => function($row) {

                                    return $row->jumlah_detail1;
                                },
                            ],
                            [
                                'attribute' => 'jumlah_detail2',
                                'label' => $datapengeluaran->detail2,
                                'contentOptions' => ['style' => 'width:250px;  min-width:50px;  '],
                                'content' => function($row) {

                                    return $row->jumlah_detail2;
                                },
                            ],
                            [
                                'attribute' => 'jumlah_detail3',
                                'label' => $datapengeluaran->detail3,
                                'contentOptions' => ['style' => 'width:250px;  min-width:50px;  '],
                                'content' => function($row) {

                                    return $row->jumlah_detail3;
                                },
                            ],
                            [
                                'attribute' => 'jumlah_detail4',
                                'label' => $datapengeluaran->detail4,
                                'contentOptions' => ['style' => 'width:250px;  min-width:50px;  '],
                                'content' => function($row) {

                                    return $row->jumlah_detail4;
                                },
                            ],
                            [
                                'attribute' => 'jumlah_detail5',
                                'label' => $datapengeluaran->detail5,
                                'contentOptions' => ['style' => 'width:250px;  min-width:50px;  '],
                                'content' => function($row) {

                                    return $row->jumlah_detail5;
                                },
                            ],
                                         'harga',
                                        'bukti',
                                            'tanggal',
                                                ],
                    ]);
                    ?>

                </div>
                <?php } ?>
        </div>

    </div>
</div>