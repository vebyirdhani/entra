<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use dosamigos\datepicker\DateRangePicker;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\KunjunganSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Report';
$this->params['breadcrumbs'][] = $this->title;
?>
<?= Html::a('Back',['report/'],[
                            'class'=>'btn btn-success pull-right',
                           
     ]); ?><BR>
<div class="kunjungan-index">

    <div class="box-header with-border">
              <h3 > Cetak Laporan berdasarkan Tanggal </h3>
               <?php echo Html::beginForm([''],'get', ['class' => 'form-horizontal']) ?>
            </div>
   
     <div class="box-body">
    
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
   
    <div class="form-group">
        
        <div class="col-md-5"><?php
            echo DateRangePicker::widget([
                'name' => 'tglAwal',
                'value' => $tglAwal,
                'nameTo' => 'tglAkhir',
                'valueTo' => $tglAkhir,
                'clientOptions' => [
                    'autoclose' => true,
                    'format' => 'yyyy-mm-dd']
            ]);
            ?>

        </div>
       <div class="form-group">
            <?php echo Html::submitButton('Cari', ['class' => 'btn btn-primary']) ?>
            <?= Html::a('<i class=""></i>Reset', ['report/report-proposal'], ['class' => 'btn btn-default']) ?>
          
              <?= Html::a('<i></i>Cetak Laporan', ['pdf-proposal','tglAwal' => $tglAwal, 'tglAkhir' => $tglAkhir,'prodi'=>$prodi], [
 
                 'class' => 'btn btn-primary',
               //  'target' => '_blank',
                 'data-toggle' => 'tooltip',
                 'title' => 'liat']);?>
        </div>
        
    </div>
    </div>
       <?php  echo $this->render('_searchproposal', ['model' => $searchModel]); ?>
      
</div>
            
      
            <div class="box">
                
        <div class="box-body table-responsive">
          
     <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                  
        'columns' => [
                    [
                    'class' => 'yii\grid\SerialColumn',
                    'header' => 'No.'
                    ],

//                 'id_dataseminar',
       //     'judul',
             [
                            'attribute'=> 'prodi',
                  
                            'value' => 'idProdi.nama_prodi',
                        ],
            'tanggal_seminar',
              [
                            'attribute'=> 'mahasiswa',
                  
                            'value' => 'idMahasiswa.nama',
                        ],
 [
                            'attribute'=> 'dosen_pembimbing1',
                  
                            'value' => 'idDosen1.nama',
                        ],
           [
                            'attribute'=> 'dosen_pembimbing2',
                  
                            'value' => 'idDosen2.nama',
                        ],
             [
                            'attribute'=> 'penguji1',
                  
                            'value' => 'idPenguji1.nama',
                        ],
               [
                            'attribute'=> 'penguji2',
                  
                            'value' => 'idPenguji2.nama',
                        ],
          
            // 'penguji3',
            // 'penguji4',
            // 'kategori',
            // 'total_nilaipembimbing',
            // 'total_nilaipenguji',
            // 'total_nilai',
            // 'total_nilaiseminar',
            // 'akumulasi',

             
                    ],
                    ]); ?>
        </div>
    </div>
</div>
