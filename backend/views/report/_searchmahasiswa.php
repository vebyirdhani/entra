<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $model common\models\DonasiSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="donasi-search">

    <?php $form = ActiveForm::begin([
        'action' => ['report-mahasiswa'],
        'method' => 'get',
    ]); ?>

    <div class="col-md-3"
     <?=
                    $form->field($model, 'prodi')->dropDownList(
                            ArrayHelper::map(common\models\Prodi::find()->all(),'id_prodi', 'nama_prodi' ), [
                        'prompt' => 'Cari berdasarkan Prodi',
                        'style'=>'width:300px',
                            ]
                    )
                    ?>
       <div class="col-md-3"
     <?= $form->field($model, 'periode')->textInput(['style'=>'width:300px']) ?>
             
            
     <div class="col-md-3"
     <?=
                    $form->field($model, 'mata_kuliah')->dropDownList(
                            ArrayHelper::map(common\models\MataKuliah::find()->all(),'id_matkul', 'nama' ), [
                        'prompt' => 'Cari berdasarkan Mata Kuliah',
                        'style'=>'width:300px',
                            ]
                    )
                    ?>
          <div class="col-md-2"
     <?= $form->field($model, 'kelas_nama')->textInput(['style'=>'width:300px']) ?>
             
                  

    <?php // echo $form->field($model, 'file') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'status') ?>

    <div class="col-md-12">
      
    </div>

    <?php ActiveForm::end(); ?>
            </div></div></div></div>
</div>
