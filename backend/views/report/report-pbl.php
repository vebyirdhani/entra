<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use dosamigos\datepicker\DateRangePicker;
/* @var $this yii\web\View */
/* @var $searchModel common\models\KeuanganSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Laporan PBL';
$this->params['breadcrumbs'][] = ['label' => 'Back', 'url' => ['site']];

?>
<?= Html::a('Back',['report/'],[
                            'class'=>'btn btn-success pull-right',
                           
     ]); ?><BR>
<div class="row">
          
    <?php $form = ActiveForm::begin([
        'action' => ['report-pbl'],
        'method' => 'get',
    ]); ?>
 
                  <p>   &ensp;   &ensp;<b>Pencarian Data Berdasarkan Tanggal</b></p>   
                          <div class="form-group">
        
        <div class="col-md-5"><?php
            echo DateRangePicker::widget([
                'name' => 'tglAwal',
                'value' => $tglAwal,
                'nameTo' => 'tglAkhir',
                'valueTo' => $tglAkhir,
                'clientOptions' => [
                    'autoclose' => true,
                     'format' => 'yyyy-mm-dd']
            ]);
            ?>

        </div>
                           
                                   
    </div> 
                         
   <div class="col-md-5">
                          
     <?php // $form->field($model, 'detail_pemasukan')->dropDownList(
//            ArrayHelper::map(common\models\DetailPemasukan::find()->where(['project'=>$data->project])->all(),'nama','nama'),
//            ['prompt'=>'Detail Pemasukan']) ?>
                   </div> 
         <div class="col-md-12">   <br>
                   <?php echo $this->render('_searchmagang', ['model' => $searchModel]); ?>
            
            <?php echo Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
           
             <?= Html::a('<i class=""></i>Reset', ['report/report-pbl'], ['class' => 'btn btn-default']) ?>
           <?= Html::a('<i></i>Cetak Laporan', ['pdf-pbl','tglAwal' => $tglAwal, 'tglAkhir' => $tglAkhir,'prodi'=>$prodi], [
 
                 'class' => 'btn btn-primary',
                // 'target' => '_blank',
                 'data-toggle' => 'tooltip',
                 'title' => 'liat']);?>
    <?php ActiveForm::end(); ?>
                       
   <br>   <br>
    <div class="col-lg-12 col-md-12">
        <div class="keuangan-index">

        
                          
            <div class="table-responsive">
                                                    <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                  
        'columns' => [
                    [
                    'class' => 'yii\grid\SerialColumn',
                    'header' => 'No.'
                    ],

//                 'id_dataseminar',
       //     'judul',
            
            'tanggal_seminar',
            [
                            'attribute'=> 'prodi',
                  
                            'value' => 'idProdi.nama_prodi',
                        ],
             [
                            'attribute'=> 'mahasiswa',
                  
                            'value' => 'idMahasiswa.nama',
                        ],
 [
                            'attribute'=> 'dosen_pembimbing1',
                  
                            'value' => 'idDosen1.nama',
                        ],
           [
                            'attribute'=> 'dosen_pembimbing2',
                  
                            'value' => 'idDosen2.nama',
                        ],
             [
                            'attribute'=> 'penguji1',
                  
                            'value' => 'idPenguji1.nama',
                        ],
               [
                            'attribute'=> 'penguji2',
                  
                            'value' => 'idPenguji2.nama',
                        ],
             
                    ],
                    ]); ?>
                                
            </div>
        </div>

    </div>
</div>