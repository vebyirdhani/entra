<?php

/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use dosamigos\datepicker\DateRangePicker;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\KunjunganSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Laporan ';
$this->params['breadcrumbs'][] = $this->title;
$this->title = 'Admin Panel';
?>


<div class="kunjungan-index">


    <div class="section services-section">
        <div class="container">
     <div class="row">
           <div class="col-6 col-sm-3 col-md-4">
            <div class="info-box">
              <span class="info-box-icon bg-info elevation-1"><i class="fa fa-list"></i></span>

              <div class="info-box-content">
               
                  <span class="info-box-text"><b> <BR>
                  <span class="info-box-text">Rekap Kehadiran Pembimbing</span>
                  <span class="info-box-text">dan Penguji (MAGANG)</b></span>
                </span>
                  
                   <a href="<?= Url::toRoute(['report-magang']) ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
         
         
           <div class="col-6 col-sm-3 col-md-4">
            <div class="info-box">
              <span class="info-box-icon bg-info elevation-1"><i class="fa fa-list"></i></span>

              <div class="info-box-content">
               
                  <span class="info-box-text"><b> <BR>
                  <span class="info-box-text">Rekap Kehadiran Pembimbing</span>
                  <span class="info-box-text">dan Penguji (PBL)</b></span>
                </span>
                  
                   <a href="<?= Url::toRoute(['report-pbl']) ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
           <div class="col-6 col-sm-3 col-md-4">
            <div class="info-box">
              <span class="info-box-icon bg-info elevation-1"><i class="fa fa-list"></i></span>

              <div class="info-box-content">
               
                  <span class="info-box-text"><b>Rekap Kehadiran dosen  </span>
                  <span class="info-box-text">pembimbing dan penguji  <BR>
                  <span class="info-box-text">(Skripsi)</b></span>
                </span>
                  
                   <a href="<?= Url::toRoute(['report-skripsi']) ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
           <div class="col-6 col-sm-3 col-md-4">
            <div class="info-box">
              <span class="info-box-icon bg-info elevation-1"><i class="fa fa-list"></i></span>

              <div class="info-box-content">
               
                  <span class="info-box-text"><b>Rekap Kehadiran dosen  </span>
                  <span class="info-box-text">pembimbing dan penguji  <BR>
                  <span class="info-box-text">(Proposal)</b></span>
                </span>
                  
                   <a href="<?= Url::toRoute(['report-proposal']) ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>

           <div class="col-6 col-sm-3 col-md-4">
            <div class="info-box">
              <span class="info-box-icon bg-info elevation-1"><i class="fa fa-list"></i></span>

              <div class="info-box-content">
               
                  <span class="info-box-text"><b> <BR>
                  <span class="info-box-text">Laporan jumlah kehadiran </span>
                  <span class="info-box-text">berdasarkan ket kuliah</b></span>
                </span>
                  
                   <a href="<?= Url::toRoute(['report-pertemuan']) ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          
          <!-- /.col -->
       
          <div class="col-6 col-sm-3 col-md-4">
            <div class="info-box">
              <span class="info-box-icon bg-info elevation-1"><i class="fa fa-list"></i></span>

              <div class="info-box-content">
               
                  <span class="info-box-text"><b><BR>
                  <span class="info-box-text">Rekap Kehadiran Dosen</b></span>
                </span>
                  
                   <a href="<?= Url::toRoute(['report-jadwal']) ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->
          
  <div class="col-6 col-sm-3 col-md-4">
            <div class="info-box">
              <span class="info-box-icon bg-info elevation-1"><i class="fa fa-list"></i></span>

              <div class="info-box-content">
               
                  <span class="info-box-text"><b>Rekap Kehadiran Dosen<BR>
                  <span class="info-box-text"> berdasarkan nama dosen</b></span>
                </span>
                  
                   <a href="<?= Url::toRoute(['report-coba']) ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
           
          
            <div class="col-6 col-sm-3 col-md-4">
            <div class="info-box">
              <span class="info-box-icon bg-info elevation-1"><i class="fa fa-list"></i></span>

              <div class="info-box-content">
               
                  <span class="info-box-text"><BR>
                  <span class="info-box-text"><b>Rekap Kehadiran Mahasiswa</b></span>
                </span>
                  
                   <a href="<?= Url::toRoute(['report-mahasiswaall']) ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->
        

         
         
        </div>
    
</div>
