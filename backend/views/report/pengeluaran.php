<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use dosamigos\datepicker\DateRangePicker;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\KunjunganSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Laporan Pengeluaran '.$project->nama_project;
$this->params['breadcrumbs'][] = ['label' => 'Back', 'url' => ['site/menu','id'=>$project->id_project]];
$this->params['breadcrumbs'][] = $this->title;

$jenis_pengeluaran = \common\models\JenisPengeluaran::find()->all();
?>
<div class="kunjungan-index">

   
             
              <div class="row">
                     <?php if ($jenis_pengeluaran){ 
      
      foreach ($jenis_pengeluaran as $value){
          $data_pengeluaran = common\models\DataPengeluaran::find()->where(['jenis_pengeluaran'=>$value->nama])->andWhere(['project'=>$project->id_project])->sum('harga');
         
      ?>
          <div class="col-lg-3 col-6">
            <div class="small-box bg-info">
             
              <div class="inner">
                     <?php if ($data_pengeluaran){?>
                <h3><?= Yii::$app->formatter->asCurrency($data_pengeluaran) ?></h3>
                     <?php } else{?>
                    <h3>0</h3>      
                <?php     }?>

                <p><?=$value->nama?></p>
              </div>
              <div class="icon">
                <i class="ion ion-bag"></i>
              </div>
                <a href= <?= Url::toRoute(['report/data-pengeluaran/','id'=>$project->id_project,'nama'=>$value->nama]) ?>    <center>More info <i class="fa fa-arrow-circle-right"></i></center></a>
            </div>
          </div>  
                     <?php }} ?>
        </div>
      
            <div class="box">
                
        <div class="box-body table-responsive">
        
        </div>
    </div>
</div>
