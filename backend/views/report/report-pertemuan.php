<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use dosamigos\datepicker\DateRangePicker;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\KunjunganSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Report';
$this->params['breadcrumbs'][] = $this->title;
$tombol = '{view}{delete}{confirm}';
?><?= Html::a('Back',['report/'],[
                            'class'=>'btn btn-success pull-right',
                           
     ]); ?><BR>
<div class="row">
   
    <div class="col-lg-12 col-md-12">
<div class="kunjungan-index">

    <div class="box-header with-border">
              <h3 > Cetak Laporan berdasarkan Kategori </h3>
               <?php echo Html::beginForm([''],'get', ['class' => 'form-horizontal']) ?>
            </div>
   
       <?php  echo $this->render('_searchpertemuan', ['model' => $searchModel,'semester'=>$semester,'prodi'=>$prodi]); ?>
      
</div>
          <br>
      
            <div class="box">
                
        <div class="box-body table-responsive">
          
                                     <?= GridView::widget([
                    'dataProvider' => $dataProvider,
               //     'filterModel' => $searchModel,
        'columns' => [
                    [
                    'class' => 'yii\grid\SerialColumn',
                    'header' => 'No.'
                    ],
      'kelas_nama',
            
            [
                            'attribute'=> 'mata_kuliah',
                  
                            'value' => 'idMatakuliah.nama',
                        ],
              [
                            'attribute'=> 'prodi',
                  
                            'value' => 'idProdi.nama_prodi',
                        ],
 [
                            'attribute'=> 'dosen',
                  
                            'value' => 'idDosen1.nama',
                        ],
           [
                            'attribute'=> 'dosen2',
                  
                            'value' => 'idDosen2.nama',
                        ],
            'jadwal_awal',
             'jadwal_akhir',
          
            'semester',

                       
                    ],
                    ]); ?>
        </div>
            </div>
    </div>
</div>
</div>
