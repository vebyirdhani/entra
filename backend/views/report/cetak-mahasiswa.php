
<head>

    <title></title>

    <link href="<?= Yii::getAlias('@web/../css/bootstrap.css') ?>" rel="stylesheet" media="print">

   <?php $semuadata = common\models\JadwalKuliah::find()->where(['id_jadwal'=>$id])->one()?>

</head>







<h3 align="center">Laporan Kehadiran Mahasiswa</h3>
<table border="0" width="100%">
    
    <tr>
        <td width="20%">Tanggal</td>
        <td width="2%">:</td>
        <td width="78%"><?php echo $tglAwal ?>&nbsp; s/d &nbsp;<?php echo $tglAkhir ?></td>
        
    </tr>
     <tr>
        <td width="20%">Prodi</td>
        <td width="2%">:</td>
        <td width="20%"> <?php if ($semuadata){ $cariprodi= common\models\Prodi::find()->where(['id_prodi'=>$semuadata->prodi])->one();
        echo   $cariprodi->nama_prodi;}?>
        </td>
    </tr>
    <tr>
        <td width="20%">Semester</td>
        <td width="2%">:</td>
       <td width="20%"> <?= $semuadata->semester ?></td>
    </tr>
    <tr>
        <td width="20%">Mata Kuliah</td>
        <td width="2%">:</td>
        <td width="20%"> <?php if ($semuadata){ $matkul = \common\models\MataKuliah::find()->where(['id_matkul'=>$semuadata->mata_kuliah])->one();
      echo  $matkul->nama ;} ?></td>
    </tr>
      <tr>
        <td width="20%">Kelas</td>
        <td width="2%">:</td>
       <td width="20%"> <?= $semuadata->kelas_nama ?></td>
    </tr>
    
  
</table>

<p></p>
<?php $no=1;

?>
<table border="1" cellpadding="2" cellspacing="0" width="100%">
        <tr>
            <th align="center">No</th>
            <th align="center">No BP</th>
            <th align="center">Nama Mahasiswa</th>
            <?php foreach ($liat as $value){?>
            <th align="center"><?= $value->tanggal?></th>
            <?php } ?>
            <th align="center">Jumlah_kehadiran</th>
             <th align="center">% Kehadiran</th>
           
       
          </tr>
     
      </tr>
        <?php
        $coba = common\models\DetailJadwalkuliah::find()->where(['id_jadwal'=>$id])->all();
        foreach ($coba as $values)
        {
           $mhs = \common\models\Mahasiswa::find()->where(['id_mahasiswa'=>$values->mahasiswa])->one();
        ?>
        <tr>
             <td align="center"><?php echo $no++; ?></td>
            <td align="center"><?php echo $mhs->nim; ?></td>
             <td align="center"><?php echo $mhs->nama; ?></td>
             <?php $absen = \common\models\DataAbsen::find()->where(['mahasiswa'=>$values->mahasiswa])->andWhere(['id_jadwal'=>$id])
                     ->andWhere(['between','tanggal', $tglAwal,$tglAkhir])->all();
            
           
             foreach ($absen as $ab){
                 
                 $countmhs = \common\models\DataAbsen::find()->where(['mahasiswa'=>$ab->mahasiswa])->andWhere(['hadir'=>1])
                         ->andWhere(['id_jadwal'=>$id])
                     ->andWhere(['between','tanggal', $tglAwal,$tglAkhir])->count();
                $absencount = \common\models\Absen::find()->andWhere(['id_jadwal'=>$id])
                     ->andWhere(['between','tanggal', $tglAwal,$tglAkhir])->andWhere(['status'=>1])->count();
                $coba = $countmhs / $absencount;
                $total = $coba*100;
                
             ?>
            
            <td align="center"><?php echo $ab->hadir ?> </td>
          
             <?php } ?>
            <?php if ($countmhs){?>
             <td align="center"><?php echo $countmhs ?></td>
            <?php }else{ ?>
				<td align="center"><?php echo '0';?></td>
			<?php } ?>
			<?php if ($total){?>
              <td align="center"><?php  echo Yii::$app->formatter->asInteger($total); ?>% </td>
            <?php }else{ ?>
            <td align="center"><?php  echo '0 %' ;?> </td>
			<?php } ?>
            </tr>
        <?php
        }
        ?>
  
      
           
</table>


<p>&nbsp;</p>


  <script src="<?= Yii::getAlias('@web/../js/jquery.js') ?>"></script>

<script type="text/javascript">

    $(document).ready(function () {

        window.print();

       

       

    });

 

        </script>
