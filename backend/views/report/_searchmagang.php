<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $model common\models\ProdiSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="prodi-search">

    <?php $form = ActiveForm::begin([
        'action' => ['report-magang'],
        'method' => 'get',
    ]); ?>

     <?=
                    $form->field($model, 'prodi')->dropDownList(
                            ArrayHelper::map(common\models\Prodi::find()->all(),'id_prodi', 'nama_prodi' ), [
                        'prompt' => 'Cari berdasarkan Periode',
                        'style'=>'width:300px',
                            ]
                    )
                    ?>

    <div class="form-group">
    
    </div>

    <?php ActiveForm::end(); ?>

</div>
