<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $model common\models\DetailJadwalkuliahSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="detail-jadwalkuliah-search">

    <?php $form = ActiveForm::begin([
        'action' => ['report-jadwal'],
        'method' => 'get',
    ]); ?>


                     <?=
                    $form->field($model, 'mata_kuliah')->dropDownList(
                            ArrayHelper::map(common\models\MataKuliah::find()->all(),'id_matkul', 'nama' ), [
                        'prompt' => 'Pilih Mata Kuliah',
                        'style'=>'width:300px',
                            ]
                    )
                    ?>
    <?php // echo $form->field($model, 'dosen') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
 <?= Html::a('<i class=""></i>Reset', ['report/report-jadwal'], ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
