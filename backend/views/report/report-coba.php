<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use dosamigos\datepicker\DateRangePicker;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\KunjunganSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Report';
$this->params['breadcrumbs'][] = $this->title;
$tombol = '{view}{delete}{confirm}';
?>
<div class="row">
   
    <div class="col-lg-12 col-md-12">
<div class="kunjungan-index">

    <div class="box-header with-border">
              <h3 > Cetak Laporan berdasarkan Tanggal </h3>
               <?php echo Html::beginForm([''],'get', ['class' => 'form-horizontal']) ?>
            </div>
   
     <div class="box-body">
    
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
   
    <div class="form-group">
        
        <div class="col-md-5"><?php
            echo DateRangePicker::widget([
                'name' => 'tglAwal',
                'value' => $tglAwal,
                'nameTo' => 'tglAkhir',
                'valueTo' => $tglAkhir,
                'clientOptions' => [
                    'autoclose' => true,
                   'format' => 'dd-mm-yyyy']
            ]);
            ?>

        </div>
       <div class="form-group">
            <?php echo Html::submitButton('Cari', ['class' => 'btn btn-primary']) ?>
            <?= Html::a('<i class=""></i>Reset', ['report/report-coba'], ['class' => 'btn btn-default']) ?>
          
              <?= Html::a('<i></i>Cetak Laporan', ['pdf-coba','tglAwal' => $tglAwal, 'tglAkhir' => $tglAkhir,'prodi'=>$prodi,'semester'=>$semester], [
 
                 'class' => 'btn btn-primary',
                // 'target' => '_blank',
                 'data-toggle' => 'tooltip',
                 'title' => 'liat']);?>
        </div>
        
    </div>
    </div>
       <?php  echo $this->render('_searchcoba', ['model' => $searchModel]); ?>
      
</div>
            
      
            <div class="box">
                
        <div class="box-body table-responsive">
          
    <?= GridView::widget([
                    'dataProvider' => $dataProvider,
//                    'filterModel' => $searchModel,
        'columns' => [
                    [
                    'class' => 'yii\grid\SerialColumn',
                    'header' => 'No.'
                    ],

    [
                            'attribute'=> 'prodi',
                  
                            'value' => 'idProdi.nama_prodi',
                        ],
            'semester',
            
            'kelas_nama',
            [
                            'attribute'=> 'mata_kuliah',
                  
                            'value' => 'idMatakuliah.nama',
                        ],
 [
                            'attribute'=> 'dosen',
                  
                            'value' => 'idDosen1.nama',
                        ],
        
         
        
                    ],
                    ]); ?>
        </div>
            </div>
    </div>
</div>
</div>
