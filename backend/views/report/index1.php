<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $searchModel common\models\DosenSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Dosen';
$this->params['breadcrumbs'][] = $this->title;
$tombol='{view}';
?>

<div class="row">
   <div class="col-lg-3 col-md-3 admin-menu-right-line">
        <div class="box-header with-border">
            <h3 class="box-title">Menu</h3>
        </div>
        <div class="box-body no-padding">
            <ul class="nav nav-pills nav-stacked">
                <li class="active" style="background-color: #f4f4f4"><a href="<?= Url::toRoute(['index']) ?>"><i class="fa fa-list-alt"></i> Data <?= Html::encode($this->title) ?> </a></li>
              
            </ul>
        </div>
    </div>
    <div class="col-lg-9 col-md-9">
        <div class="dosen-index">

            <h1>Data Dosen</h1>
                                 <?php $form = ActiveForm::begin([]);?>

                    <?= $form->field($model, 'nama')->textInput(['style'=>'width:300px', 'placeholder' => 'Ketikan Nama / NIDN Dosen']) ?>
                    
                  
                    
                    <?= Html::submitButton('Search',['class' => 'btn btn-primary']); ?>
                       <?= Html::a('<i class=""></i>Reset', ['report/'], ['class' => 'btn btn-default']) ?>
                    <?php ActiveForm::end(); ?>
            <div class="table-responsive">
                                                    <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                  
        'columns' => [
                    [
                    'class' => 'yii\grid\SerialColumn',
                    'header' => 'No.'
                    ],

                  'nidn',              
            'nama',
         
           
                    [
                    'class' => 'yii\grid\ActionColumn','header'=>'Action',
                    'template' => $tombol,
                    'buttons' => [
                       
                                
                        'view' => function ($url, $model, $key){
                           
                             return  Html::a('Lihat Data',['view', 'id' => $model->id_dosen], ['class' => 'btn btn-primary']); 
                        },
                       

                    ]
                ],
                    ],
                    ]); ?>
                                
            </div>
        </div>

    </div>
</div>