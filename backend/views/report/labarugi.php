<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use dosamigos\datepicker\DateRangePicker;
/* @var $this yii\web\View */
/* @var $searchModel common\models\KeuanganSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Laporan Laba Rugi';
$this->params['breadcrumbs'][] = ['label' => 'Back', 'url' => ['site/menu/','id'=>$project->id_project]];
$pemasukan = common\models\Keuangan::find()->where(['project'=>$project->id_project])->andWhere(['keterangan'=>'Pendapatan'])->sum('harga');
$pengeluaran = common\models\Keuangan::find()->where(['project'=>$project->id_project])->andWhere(['keterangan'=>'Pengeluaran'])->sum('harga');

$total= $pemasukan-$pengeluaran;
?>

<div class="row">
        
    <?php $form = ActiveForm::begin([
        'action' => ['labarugi','id'=>$id,],
        'method' => 'get',
    ]); ?>
 
                  <p>   &ensp;   &ensp;<b>Pencarian Data Berdasarkan Tanggal</b></p>   
                          <div class="form-group">
        
        <div class="col-md-5"><?php
            echo DateRangePicker::widget([
                'name' => 'tglAwal',
                'value' => $tglAwal,
                'nameTo' => 'tglAkhir',
                'valueTo' => $tglAkhir,
                'clientOptions' => [
                    'autoclose' => true,
                     'format' => 'dd-M-yyyy']
            ]);
            ?>

        </div>
                           
                                    <div class="col-md-2"><h4><B>TOTAL :<br> <?= Yii::$app->formatter->asCurrency($total) ?> </b></h4></div>
                                     <div class="col-md-2"><h4><B>Pemasukan :<br> <?= Yii::$app->formatter->asCurrency($pendapatan) ?> </b></h4></div>
                                      <div class="col-md-2"><h4><B>Pengeluaran :<br> <?= Yii::$app->formatter->asCurrency($pengeluaran) ?> </b></h4></div>
         <br>
    </div> 
                         
   <div class="col-md-5">
                          
     <?php // $form->field($model, 'detail_pemasukan')->dropDownList(
//            ArrayHelper::map(common\models\DetailPemasukan::find()->where(['project'=>$data->project])->all(),'nama','nama'),
//            ['prompt'=>'Detail Pemasukan']) ?>
                   </div> 
         <div class="col-md-12">
            <?php echo Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
           
             <?= Html::a('<i class=""></i>Reset', ['report/labarugi','id'=>$id], ['class' => 'btn btn-default']) ?>
             <?= Html::a('<i></i>Cetak Laporan', ['pdflabarugi','tglAwal' => $tglAwal, 'tglAkhir' => $tglAkhir,'id'=>$project->id_project], [
 
                 'class' => 'btn btn-primary',
                 'target' => '_blank',
                 'data-toggle' => 'tooltip',
                 'title' => 'liat']);?>
    <?php ActiveForm::end(); ?>
    <div class="col-lg-12 col-md-12">
        <div class="keuangan-index">

            
                                <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
            
            <div class="table-responsive">
                                                    <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                  
        'columns' => [
                    [
                    'class' => 'yii\grid\SerialColumn',
                    'header' => 'No.'
                    ],

                           
            'keterangan',
            'nama',
            'harga',
            'created_at',
            // 'project',

             
                    ],
                    ]); ?>
                                
            </div>
        </div>

    </div>
</div>