<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use dosamigos\datepicker\DateRangePicker;
/* @var $this yii\web\View */
/* @var $searchModel common\models\DataPelangganSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Data Pendapatan '.$project->nama_project;
$this->params['breadcrumbs'][] = ['label' => 'Back', 'url' => ['site/menu/','id'=>$project->id_project]];
$tombol = '{ceklis}{view}{update}{delete}';

?>

<div class="row">
       <?php $form = ActiveForm::begin([
        'action' => ['pendapatan','id'=>$id],
        'method' => 'get',
    ]); ?>
 
                  <p>   &ensp;   &ensp;<b>Cetak Data Berdasarkan Tanggal</b></p>   
                   <?php echo Html::beginForm([''],'get', ['class' => 'form-horizontal']) ?>
                          <div class="form-group">
        
        <div class="col-md-5"><?php
            echo DateRangePicker::widget([
                'name' => 'tglAwal',
                'value' => $tglAwal,
                'nameTo' => 'tglAkhir',
                'valueTo' => $tglAkhir,
                'clientOptions' => [
                    'autoclose' => true,
                     'format' => 'dd-M-yyyy']
            ]);
            ?>

        </div> 
                           
                                                          <div class="col-md-5"><h4><B>TOTAL PENDAPATAN:<br> <?= Yii::$app->formatter->asCurrency($all) ?> </b></h4></div>
         <br>
         
    </div> 
                         
   <div class="col-md-5">
                          
     <?php // $form->field($model, 'detail_pemasukan')->dropDownList(
//            ArrayHelper::map(common\models\DetailPemasukan::find()->where(['project'=>$data->project])->all(),'nama','nama'),
//            ['prompt'=>'Detail Pemasukan']) ?>
                   </div> 
         <div class="col-md-12">
            <?php echo Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
           
             <?= Html::a('<i class=""></i>Reset', ['report/pendapatan','id'=>$id], ['class' => 'btn btn-default']) ?>
           <?= Html::a('<i></i>Cetak Laporan', ['pdfpendapatan','tglAwal' => $tglAwal, 'tglAkhir' => $tglAkhir,'id'=>$project->id_project], [
 
                 'class' => 'btn btn-primary',
                 'target' => '_blank',
                 'data-toggle' => 'tooltip',
                 'title' => 'liat']);?>
    <?php ActiveForm::end(); ?>
    <div class="col-lg-12 col-md-12">
        <div class="data-pelanggan-index">
             
          
                                <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
            
            <div class="table-responsive">
                                                    <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                  
        'columns' => [
                    [
                    'class' => 'yii\grid\SerialColumn',
                    'header' => 'No.'
                    ],

                          //      'id_pelanggan',
            'nama',
            'nik',
            'no_hp',
            'type',
            'blok',
             'harga',
            'total',
             'biaya_notaris',
             'biaya_kelebihan_tanah',
             'total',
             'dp',
             'status',
          'tanggal_akad',
            'tipe_pembayaran',
            'keterangan',

                 
                    ],
                    ]); ?>
                                
            </div>
        </div>

    </div>
</div>