<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $searchModel common\models\DosenSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Dosen';
$this->params['breadcrumbs'][] = $this->title;
$tombol='{view}';
?>

<div class="row">
   
    <div>
        <div class="dosen-index">

            <h1><?= Html::encode($this->title) ?></h1>
            <div class="col-lg-12 ">
                  <?php $form = ActiveForm::begin([]);?>

                <div class="col-md-3">
                   <?= $form->field($model, 'semester')->dropDownList(ArrayHelper::map(\common\models\Semester::find()->all(), 'semester', 'semester'),[
                        'prompt' => 'Silahkan Pilih Semester',
                      
                    ]); ?>
            </div>
                <div class="col-md-3">
              <?= $form->field($model, 'mata_kuliah')->dropDownList(ArrayHelper::map(\common\models\MataKuliah::find()->all(), 'id_matkul', 'nama'),[
                        'prompt' => 'Silahkan Pilih Mata Kuliah',
                      
                    ]); ?>
            </div>
                   
                
            </div>
              <div class="col-lg-12 ">  
                  <div class="col-md-3">
             <?= Html::submitButton('Search',['class' => 'btn btn-primary']); ?>
                       <?= Html::a('<i class=""></i>Reset', ['report/view','id'=>$id], ['class' => 'btn btn-default']) ?>
                    <?php ActiveForm::end(); ?>
                  </div>
              </div>
                        <div class="col-lg-12 ">           
            <div class="table-responsive">
                                                    <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                  
        'columns' => [
                    [
                    'class' => 'yii\grid\SerialColumn',
                    'header' => 'No.'
                    ],
  [
                                'attribute'=> 'dosen',
                                'value' => 'idDosen1.nama',
                            ],
            [
                                'attribute'=> 'dosen2',
                                'value' => 'idDosen2.nama',
                            ],
            'jam_mulai',
            'jam_akhir',
             'tanggal',
             'materi_kuliah',
             'pertemuan',
              [
                                'attribute'=> 'mata_kuliah',
                                'value' => 'idMatakuliah.nama',
                            ],
            'semester',
            'total_hadir',
            // 'status_hadir',
            // 'keterangan',
           
                    [
                    'class' => 'yii\grid\ActionColumn','header'=>'Action',
                    'template' => $tombol,
                    'buttons' => [
                       
                                
                        'view' => function ($url, $model, $key){
                           
                             return  Html::a('Lihat Data',['data-mahasiswa', 'id' => $model->id_absen], ['class' => 'btn btn-primary']); 
                        },
                       

                    ]
                ],
                    ],
                    ]); ?>
                                
            </div>
                        </div>
        </div>

    </div>
</div>