
<head>

    <title></title>

    <link href="<?= Yii::getAlias('@web/../css/bootstrap.css') ?>" rel="stylesheet" media="print">

   

</head>





<br>

<h3 align="center">Laporan jumlah kehadiran dosen berdasarkan keterangan kuliah</h3>
<table border="0" width="50%">
    
   <tr>
        <td width="30%">Prodi</td>
        <td width="2%">:</td>
        <td width="78%"><?php $cariprodi = \common\models\Prodi::find()->where(['id_prodi'=>$prodi])->one();
    if ($cariprodi){  echo $cariprodi->nama_prodi;}?></td>
        
    </tr>
     <tr>
        <td width="20%">Semester</td>
        <td width="2%">:</td>
       <td width="20%"><?= $semester?></td>
    </tr>
 
     <tr>
        <td width="20%">Periode</td>
        <td width="2%">:</td>
       <td width="20%"> <?= $tglAwal?> s/d <?= $tglAkhir?></td>
  
</table>

<p></p>
<?php $no=1;

?>
<table border="1" cellpadding="1" cellspacing="0" width="99%">
      <br>     <br>    
        <tr>
    
         <th align="center"><center>No</center></th>
<th align="center"><center>Nama Dosen</center></th>
<th align="center"><center>Mata Kuliah</center></th>
<th align="center"><center>Kelas</center> </th>
<th align="center"><center>Beban Mengajar</center></th>
      
          
            
          </tr>
        <?php
       if ($prodi && $semester){
           
       $caridata = \common\models\JadwalKuliah::find()->where(['prodi'=>$prodi])->andWhere(['semester'=>$semester])->all();
       if ($caridata){
        foreach ($caridata as $values)
        {
            $coba = \common\models\Absen::find() ->where(['between','tanggal', $tglAwal,$tglAkhir])->andWhere(['id_jadwal'=>$values->id_jadwal])
                   ->count();
        ?>
        <tr>
             <td align="center"><?php echo $values->id_jadwal ?></td>
            <td align="center"><?php
            $caridosen = common\models\Dosen::find()->where(['id_dosen'=>$values->dosen])->one();
            if ($caridosen){
            echo $caridosen->nama; }?></td>
             <td align="center"><?php 
             $carimatkul = \common\models\MataKuliah::find()->where(['id_matkul'=>$values->mata_kuliah])->one();
             if ($carimatkul){
             echo $carimatkul->nama; }?></td>
            <td align="center"><?php echo $values->kelas_nama; ?> </td>
         
              <td align="center"><?php 
              $hrspertemuan = $values->jumlah_pertemuan;
              
              $hitungtotal = $coba;
              if ($hitungtotal>0){
                    $bagi = $hrspertemuan/$hitungtotal;
              $kali = $bagi * $values->sks;
              echo Yii::$app->formatter->asInteger($kali);
              }else{
                  $zero= 0;
                   echo $zero;
              }
          
              
              ?></td>
           
            </tr>
        <?php
       }}}
        ?>
                 <?php
       if ($prodi && !$semester){
           
       $caridata = \common\models\JadwalKuliah::find()->where(['prodi'=>$prodi])->all();
       if ($caridata){
        foreach ($caridata as $values)
        {
            $coba = \common\models\Absen::find() ->where(['between','tanggal', $tglAwal,$tglAkhir])->andWhere(['id_jadwal'=>$values->id_jadwal])
                   ->count();
        ?>
        <tr>
             <td align="center"><?php echo $values->id_jadwal ?></td>
            <td align="center"><?php
            $caridosen = common\models\Dosen::find()->where(['id_dosen'=>$values->dosen])->one();
            if ($caridosen){
            echo $caridosen->nama; }?></td>
             <td align="center"><?php 
             $carimatkul = \common\models\MataKuliah::find()->where(['id_matkul'=>$values->mata_kuliah])->one();
             if ($carimatkul){
             echo $carimatkul->nama; }?></td>
            <td align="center"><?php echo $values->kelas_nama; ?> </td>
         
              <td align="center"><?php 
              $hrspertemuan = $values->jumlah_pertemuan;
              
              $hitungtotal = $coba;
              if ($hitungtotal>0){
                    $bagi = $hrspertemuan/$hitungtotal;
              $kali = $bagi * $values->sks;
             echo Yii::$app->formatter->asInteger($kali);
              }else{
                  $zero= 0;
                   echo $zero;
              }
          
              
              ?></td>
           
            </tr>
        <?php
       }}}
        ?>
                 <?php
       if ( $semester && !$prodi){
           
       $caridata = \common\models\JadwalKuliah::find()->where(['semester'=>$semester])->all();
       if ($caridata){
        foreach ($caridata as $values)
        {
            $coba = \common\models\Absen::find()->where(['between','tanggal', $tglAwal,$tglAkhir])->andWhere(['id_jadwal'=>$values->id_jadwal])
                    ->count();
					
        ?>
        <tr>
             <td align="center"><?php echo $values->id_jadwal ?></td>
            <td align="center"><?php
            $caridosen = common\models\Dosen::find()->where(['id_dosen'=>$values->dosen])->one();
            if ($caridosen){
            echo $caridosen->nama; }?></td>
             <td align="center"><?php 
             $carimatkul = \common\models\MataKuliah::find()->where(['id_matkul'=>$values->mata_kuliah])->one();
             if ($carimatkul){
             echo $carimatkul->nama; }?></td>
            <td align="center"><?php echo $values->kelas_nama; ?> </td>
         
              <td align="center"><?php 
              $hrspertemuan = $values->jumlah_pertemuan;
              
              $hitungtotal = $coba;
              if ($hitungtotal>0){
                    $bagi = $hrspertemuan/$hitungtotal;
              $kali = $bagi * $values->sks;
              echo Yii::$app->formatter->asInteger($kali);
              }else{
                  $zero= 0;
                   echo $zero;
              }
          
              
              ?></td>
           
            </tr>
        <?php
       }}}
        ?>     <?php
       if (!$prodi && !$semester){
           
       $caridata = \common\models\JadwalKuliah::find()->all();
       if ($caridata){
        foreach ($caridata as $values)
        {
            $coba = \common\models\Absen::find()->where(['between','tanggal', $tglAwal,$tglAkhir])->andWhere(['id_jadwal'=>$values->id_jadwal])
                    ->count();
        ?>
        <tr>
             <td align="center"><?php echo $values->id_jadwal ?></td>
            <td align="center"><?php
            $caridosen = common\models\Dosen::find()->where(['id_dosen'=>$values->dosen])->one();
            if ($caridosen){
            echo $caridosen->nama; }?></td>
             <td align="center"><?php 
             $carimatkul = \common\models\MataKuliah::find()->where(['id_matkul'=>$values->mata_kuliah])->one();
             if ($carimatkul){
             echo $carimatkul->nama; }?></td>
            <td align="center"><?php echo $values->kelas_nama; ?> </td>
         
              <td align="center"><?php 
              $hrspertemuan = $values->jumlah_pertemuan;
              
              $hitungtotal = $coba;
              if ($hitungtotal>0){
                    $bagi = $hrspertemuan/$hitungtotal;
              $kali = $bagi * $values->sks;
              echo Yii::$app->formatter->asInteger($kali);
              }else{
                  $zero= 0;
                   echo $zero;
              }
          
              
              ?></td>
           
            </tr>
        <?php
       }}}
        ?>
           
</table>



<p>&nbsp;</p>


  <script src="<?= Yii::getAlias('@web/../js/jquery.js') ?>"></script>

<script type="text/javascript">

    $(document).ready(function () {

        window.print();

      
       

    });

 

        </script>
