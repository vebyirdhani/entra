<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use dosamigos\datepicker\DateRangePicker;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\KunjunganSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Report';
$this->params['breadcrumbs'][] = $this->title;
$tombol = '{view}';

?>
<div class="row">
   
    <div class="col-lg-12 col-md-12">
<div class="kunjungan-index">

    <div class="box-header with-border">
        <h4>Cari data berdasarkan kategori</h4>
               <?php echo Html::beginForm([''],'get', ['class' => 'form-horizontal']) ?>
            </div>
   
     <div class="box-body">
    
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
   
    <div class="form-group">
        
      
       <div class="form-group">
          
        </div>
        
    </div>
    </div>
       <?php  echo $this->render('_searchbebanajar', ['model' => $searchModel]); ?>
   
</div>
            
      
            <div class="box">
                
        <div class="box-body table-responsive">
          
    <?= GridView::widget([
                    'dataProvider' => $dataProvider,
//                    'filterModel' => $searchModel,
        'columns' => [
                    [
                    'class' => 'yii\grid\SerialColumn',
                    'header' => 'No.'
                    ],
            'kelas_nama',
            
            [
                            'attribute'=> 'mata_kuliah',
                  
                            'value' => 'idMatakuliah.nama',
                        ],
              [
                            'attribute'=> 'prodi',
                  
                            'value' => 'idProdi.nama_prodi',
                        ],
 [
                            'attribute'=> 'dosen',
                  
                            'value' => 'idDosen1.nama',
                        ],
        
         
            'semester',
            'periode',
                   [
                    'class' => 'yii\grid\ActionColumn','header'=>'Action',
                    'template' => $tombol,
                    'buttons' => [
                       
                         
                                
                        'view' => function ($url, $model, $key){
                           
                             return  Html::a('Cek data',['report-beban-ajar', 'id' => $model->id_jadwal], ['class' => 'btn btn-primary']); 
                        },
                       

                    ]
                ],
                    ],
                    ]); ?>
        </div>
            </div>
    </div>
</div>
</div>

    