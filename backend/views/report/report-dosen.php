<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use dosamigos\datepicker\DateRangePicker;
/* @var $this yii\web\View */
/* @var $searchModel common\models\KeuanganSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Laporan Dosen';
$this->params['breadcrumbs'][] = ['label' => 'Back', 'url' => ['site']];

?>

<div class="row">
          
    <?php $form = ActiveForm::begin([
        'action' => ['report-dosen','id'=>$id],
        'method' => 'get',
    ]); ?>
 
                  <p>   &ensp;   &ensp;<b>Pencarian Data Berdasarkan Tanggal</b></p>   
                          <div class="form-group">
        
        <div class="col-md-5"><?php
            echo DateRangePicker::widget([
                'name' => 'tglAwal',
                'value' => $tglAwal,
                'nameTo' => 'tglAkhir',
                'valueTo' => $tglAkhir,
                'clientOptions' => [
                    'autoclose' => true,
                     'format' => 'dd-mm-yyyy']
            ]);
            ?>

        </div>
                           
                                   
    </div> 
                         
   <div class="col-md-5">
                          
     <?php // $form->field($model, 'detail_pemasukan')->dropDownList(
//            ArrayHelper::map(common\models\DetailPemasukan::find()->where(['project'=>$data->project])->all(),'nama','nama'),
//            ['prompt'=>'Detail Pemasukan']) ?>
                   </div> 
         <div class="col-md-12">   <br>
            <?php echo Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
           
             <?= Html::a('<i class=""></i>Reset', ['report/report-dosen','id'=>$id], ['class' => 'btn btn-default']) ?>
           <?= Html::a('<i></i>Cetak Laporan', ['pdf-dosen','tglAwal' => $tglAwal, 'tglAkhir' => $tglAkhir,'id'=>$id], [
 
                 'class' => 'btn btn-primary',
             //    'target' => '_blank',
                 'data-toggle' => 'tooltip',
                 'title' => 'liat']);?>
    <?php ActiveForm::end(); ?>
                       
   <br>   <br>
    <div class="col-lg-12 col-md-12">
        <div class="keuangan-index">

        
                                <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
            
            <div class="table-responsive">
                                                    <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                  
        'columns' => [
                    [
                    'class' => 'yii\grid\SerialColumn',
                    'header' => 'No.'
                    ],
    [
                            'attribute'=> 'mata_kuliah',
                  
                            'value' => 'idMatakuliah.nama',
                        ],
 [
                            'attribute'=> 'dosen',
                  
                            'value' => 'idDosen1.nama',
                        ],
           [
                            'attribute'=> 'dosen2',
                  
                            'value' => 'idDosen2.nama',
                        ],
            'jam_mulai',
            'jam_akhir',

             'tanggal',
            'materi_kuliah',
            'pertemuan',
                    ],
                    ]); ?>
                                
            </div>
        </div>

    </div>
</div>