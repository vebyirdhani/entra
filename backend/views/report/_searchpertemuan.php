<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $model common\models\DonasiSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="donasi-search">

    <?php $form = ActiveForm::begin([
        'action' => ['report-pertemuan'],
        'method' => 'get',
    ]); ?>

    <div class="col-md-3"
     <?=
                    $form->field($model, 'prodi')->dropDownList(
                            ArrayHelper::map(common\models\Prodi::find()->all(),'id_prodi', 'nama_prodi' ), [
                        'prompt' => 'Cari berdasarkan Periode',
                        'style'=>'width:300px',
                            ]
                    )
                    ?>
       <div class="col-md-3"
     <?=
                    $form->field($model, 'semester')->dropDownList(
                            ArrayHelper::map(common\models\Semester::find()->all(),'keterangan', 'keterangan' ), [
                        'prompt' => 'Cari berdasarkan Periode',
                        'style'=>'width:300px',
                            ]
                    )
                    ?>
      

    <?php // echo $form->field($model, 'email') ?>

    <?php // echo $form->field($model, 'file') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'status') ?>

    <div class="col-md-12">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
         <?= Html::a('Reset', ['report/report-pertemuan'], ['class' => 'btn btn-default']) ?>
          <?= Html::a('<i></i>Cetak Laporan', ['pdf-pertemuan','semester'=>$semester,'prodi'=>$prodi], [
 
                 'class' => 'btn btn-primary',
               //  'target' => '_blank',
                 'data-toggle' => 'tooltip',
                 'title' => 'liat']);?>
    </div>

    <?php ActiveForm::end(); ?>
        </div></div>
</div>
