<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use dosamigos\datepicker\DateRangePicker;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $searchModel common\models\KeuanganSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Laporan Mahasiswa';
$this->params['breadcrumbs'][] = ['label' => 'Back', 'url' => ['site']];

?>
<div class="row margin-bottom">
    <div class="col-md-12 ">
         <?= Html::a('Back',['report/'],[
                            'class'=>'btn btn-success pull-left',
     ]); ?>
    </<div>
</div>
    </div>
   
<div class="row">
    
 <?php $form = ActiveForm::begin([
        'action' => ['report-beban-ajar'],
        'method' => 'get',
    ]); ?>

    <div class="col-md-4">
         <?php
            echo DateRangePicker::widget([
                'name' => 'tglAwal',
                'value' => $tglAwal,
                'nameTo' => 'tglAkhir',
                'valueTo' => $tglAkhir,
                'clientOptions' => [
                    'autoclose' => true,
                   'format' => 'dd-mm-yyyy']
            ]);
            ?>
       
   
    </div>
    <div class="col-md-8">
        <div class="col-md-6">
     <?=
                    $form->field($model, 'prodi')->label(false)->dropDownList(
                            ArrayHelper::map(common\models\Prodi::find()->all(),'id_prodi', 'nama_prodi' ), [
                        'prompt' => 'Cari berdasarkan Prodi',
                        'style'=>'width:300px',
                            ]
                    )
                    ?>
        </div>
       
            <div class="col-md-6">
     <?= $form->field($model, 'periode')->label(false)->textInput(['style'=>'width:300px']) ?>
            </div>
    </div>
                  

    <?php // echo $form->field($model, 'file') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'status') ?>
</div>
    <div class="row">
    <div class="col-md-12">
    <?php echo Html::submitButton('Search', ['class' => 'btn btn-primary','name'=>'cari','value'=>'cari']) ?>
        
             <?= Html::a('<i class=""></i>Reset', ['report/report-beban-ajar',], ['class' => 'btn btn-default']) ?>
           <?= Html::a('<i></i>Cetak Laporan', ['pdf-beban-ajar','tglAwal' => $tglAwal, 'tglAkhir' => $tglAkhir,'prodi'=>$prodi, 'periode'=>$periode], [
 
                 'class' => 'btn btn-primary',
                 'target' => '_blank',
                 'data-toggle' => 'tooltip',
                 'title' => 'liat']);?>
   
               
              
   
    </div>
   
  
                    
                       
        
   
      <?php ActiveForm::end(); ?>
</div>
    <div class="col-lg-12 col-md-12">
        <div class="keuangan-index">

        
            
            <div class="table-responsive">
                                                    <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                  
        'columns' => [
                    [
                    'class' => 'yii\grid\SerialColumn',
                    'header' => 'No.'
                    ],
      'prodi',
            'periode',
            
            'kelas_nama',
            [
                            'attribute'=> 'mata_kuliah',
                  
                            'value' => 'idMatakuliah.nama',
                        ],
 [
                            'attribute'=> 'dosen',
                  
                            'value' => 'idDosen1.nama',
                        ],
        
         
             'tanggal',
                    ],
                    ]); ?>
                                
            </div>
        </div>

    </div>
