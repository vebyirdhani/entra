<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $model common\models\DonasiSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="donasi-search">

    <?php $form = ActiveForm::begin([
        'action' => ['report-mahasiswa'],
        'method' => 'get',
    ]); ?>
    <div class="row">
    <div class="col-md-6">
     <?=
                    $form->field($model, 'prodi')->dropDownList(
                            ArrayHelper::map(common\models\Prodi::find()->all(),'id_prodi', 'nama_prodi' ), [
                        'prompt' => 'Cari berdasarkan Prodi',
                        'style'=>'width:300px',
                            ]
                    )
                    ?>
    </div>   <div class="col-md-6">
     <?= $form->field($model, 'periode')->textInput(['style'=>'width:300px']) ?>
    </div>     
            
   
                  

    <?php // echo $form->field($model, 'file') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php echo Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        
             <?= Html::a('<i class=""></i>Reset', ['report/report-beban-ajar',], ['class' => 'btn btn-default']) ?>
           <?= Html::a('<i></i>Cetak Laporan', ['pdf-beban-ajar','tglAwal' => $tglAwal, 'tglAkhir' => $tglAkhir,'prodi'=>$prodi, 'periode'=>$periode], [
 
                 'class' => 'btn btn-primary',
                 'target' => '_blank',
                 'data-toggle' => 'tooltip',
                 'title' => 'liat']);?>

    <?php ActiveForm::end(); ?>
    </div>
</div>
