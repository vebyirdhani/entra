<?php
/* @var $this \yii\web\View */
/* @var $content string */

use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;
use yii\helpers\StringHelper;
use yii\bootstrap\Modal;

AppAsset::register($this);

$user = Yii::$app->user->identity;
$id = Yii::$app->user->identity->id;
$profile = \common\models\Profile::find()->Where(['user_id' => $id])->one();
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="<?= Yii::$app->name ?>">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
        <link rel="shortcut icon" type="image/x-icon" href="<?= yii\helpers\Url::toRoute(['/favicon.ico']) ?>" />
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    </head>
    <body class="hold-transition skin-yellow sidebar-mini fixed">
        <?php $this->beginBody() ?>

        <div class="wrapper">

            <header class="main-header">
                <!-- Logo -->
                <a href="<?= Url::toRoute(['/']) ?>" class="logo">
                    <!-- mini logo for sidebar mini 50x50 pixels -->
                    <span class="logo-mini"> <i class="fa fa-home"></i></span>
                    <!-- logo for regular state and mobile devices -->
                    <span class="logo-sm" style="font-size:16px;"><?= Yii::$app->name ?></span>
                </a>
                <!-- Header Navbar: style can be found in header.less -->
                <nav class="navbar navbar-static-top">
                    <!-- Sidebar toggle button-->
                    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </a>

                    <div class="navbar-custom-menu">
                        <ul class="nav navbar-nav">


                            <li class="dropdown messages-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="fa fa-bell-o"></i>

                                    <span class="label label-success"></span>

                                    <span class="label label-success"></span>

                                </a>

                                <ul class="dropdown-menu">

                                    <li class="header">Anda memiliki notifikasi baru</li>

                                    <li class="header">Anda tidak memiliki  notifikasi baru</li>

                                    <li>
                                        <!-- inner menu: contains the actual data -->
                                        <ul class="menu">
                                            <li><!-- start message -->
                                                <a href="">
                                                    <div class="pull-left">
                                                        <img src="<?= Url::toRoute(['/dist/img/default.png']) ?>" class="img-circle" alt="User Image">
                                                    </div>


                                                    <h4>
                                                        <b>  </b>
                                                    </h4>

                                                    <p></p>
                                                    <p><i class="fa fa-clock-o"></i> </p>


                                                </a>
                                            </li>
                                            <li><!-- start message -->

                                                <a href="">
                                                    <div class="pull-left">
                                                        <img src="<?= Url::toRoute(['/dist/img/default.png']) ?>" class="img-circle" alt="User Image">
                                                    </div>


                                                    <h4>

                                                    </h4>

                                                    <p></p>
                                                    <p><i class="fa fa-clock-o"></i> </p>

                                                </a>
                                            </li>
                                        </ul>

                                    </li>
                                    <li class="footer"><a href="<?= Url::toRoute(['/komentar']) ?>">Lihat semua notifikasi</a></li>
                                </ul>
                            </li>

                            <li class="dropdown messages-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="fa fa-envelope-o"></i>

                                    <span class="label label-success"></span>

                                    <span class="label label-success"></span>

                                </a>
                                <ul class="dropdown-menu">

                                    <li class="header">Anda memiliki  pesan baru</li>

                                    <li class="header">Anda memiliki tidak memiliki pesan baru</li>

                                    <li>
                                        <!-- inner menu: contains the actual data -->
                                        <ul class="menu">
                                            <li><!-- start message -->

                                                <a href="">
                                                    <div class="pull-left">
                                                        <img src="<?= Url::toRoute(['/dist/img/default.png']) ?>" class="img-circle" alt="User Image">
                                                    </div>
                                                    <h4>
                                                        <b></b>

                                                    </h4>
                                                    <p></p>
                                                    <p><i class="fa fa-clock-o"></i> </p>
                                                </a>

                                            </li>
                                            <li><!-- start message -->

                                                <a href="ssss">
                                                    <div class="pull-left">
                                                        <img src="<?= Url::toRoute(['/dist/img/default.png']) ?>" class="img-circle" alt="User Image">
                                                    </div>
                                                    <h4>


                                                    </h4>
                                                    <p></p>
                                                    <p><i class="fa fa-clock-o"></i></p>
                                                </a>

                                            </li>
                                        </ul>
                                    </li>
                                    <li>
                                        <!-- inner menu: contains the actual data -->

                                    </li>
                                    <li class="footer"><a href="<?= Url::toRoute(['/donasi/']) ?>">Lihat semua pesan</a></li>
                                </ul>
                            </li>
                            <li class="dropdown user user-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <img src="<?= Url::toRoute(['/site/foto-profil', 'inline' => true]) ?>" class="user-image" alt="User Image">
                                    <span class="hidden-xs"><?= ucwords($user->profile->name) ?></span>
                                </a>
                                <ul class="dropdown-menu">
                                    <!-- User image -->
                                    <li class="user-header">
                                        <img src="<?= Url::toRoute(['/site/foto-profil', 'inline' => true]) ?>" class="img-circle" alt="User Image">

                                        <p>
                                            <?= ucwords($user->profile->name) ?> <br>  <?php // Yii::$app->name  ?>
                                            <small>Member since <?= date("Y M d", $user->created_at) ?></small>
                                        </p>
                                    </li>
                                    <!-- Menu Footer-->
                                    <li class="user-footer">
                                        <div class="pull-left">
                                            <a href="<?= Url::toRoute(['/site/profil']) ?>" class="btn btn-default btn-flat">Profil</a>
                                        </div>
                                        <div class="pull-right">
                                            <a href="<?= Url::to(['/site/logout']) ?>" data-method="post" class="btn btn-default btn-flat">Log Out</a>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </nav>
            </header>

            <!-- =============================================== -->

            <!-- Left side column. contains the sidebar -->
            <aside class="main-sidebar">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->
                    <div class="user-panel">
                        <div class="pull-left image">
                            <img src="<?= Url::toRoute(['/site/foto-profil', 'inline' => true]) ?>" class="img-circle" alt="User Image">
                        </div>
                        <div class="pull-left info">
                            <p><?= ucwords($user->profile->name) ?></p>
                            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                        </div>
                    </div>
                    <br>
                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu">
                           <li <?= Yii::$app->controller->id == 'site' ? 'class=active' : '' ?>>
                            <a href="<?= Url::toRoute(['/']) ?>">
                                <i class="fa fa-home "></i> <span>Dashboard</span>
                            </a>
                        </li>
                        <?php if (Yii::$app->user->can('superAdmin') || Yii::$app->user->can('admin')) { ?>
                            <li class="header">WEB NAVIGATION</li>
                              <?php if (Yii::$app->user->can('superAdmin')|| Yii::$app->user->can('admin') ) { ?>
                                <li <?= Yii::$app->controller->id == 'about' ? 'class=active' : '' ?>>
                                    <a href="<?= Url::toRoute(['/about']) ?>">
                                        <i class="fa   fa-sticky-note-o"></i> <span>About</span>
                                    </a>
                                </li>

                            <?php } ?>
                            <?php if (Yii::$app->user->can('superAdmin')|| Yii::$app->user->can('admin') ) { ?>
                                <li <?= Yii::$app->controller->id == 'tips-berwisata' ? 'class=active' : '' ?>>
                                    <a href="<?= Url::toRoute(['/tips-berwisata']) ?>">
                                        <i class="fa   fa-pencil"></i> <span>Blog</span>
                                    </a>
                                </li>

                            <?php } ?>

                                  <?php if (Yii::$app->user->can('superAdmin')|| Yii::$app->user->can('admin') ) { ?>
                                <li <?= Yii::$app->controller->id == 'contact' ? 'class=active' : '' ?>>
                                    <a href="<?= Url::toRoute(['/contact']) ?>">
                                        <i class="fa  fa-phone"></i> <span>Contact</span>
                                    </a>
                                </li>

                            <?php } ?>
                              <?php if (Yii::$app->user->can('superAdmin')|| Yii::$app->user->can('admin') ) { ?>
                                <li <?= Yii::$app->controller->id == 'desa-traveling' ? 'class=active' : '' ?>>
                                    <a href="<?= Url::toRoute(['/desa-traveling']) ?>">
                                        <i class="fa   fa-plane"></i> <span>Desa Traveling</span>
                                    </a>
                                </li>

                            <?php } ?>

                             <?php if (Yii::$app->user->can('superAdmin')|| Yii::$app->user->can('admin') ) { ?>
                                <li <?= Yii::$app->controller->id == 'kategori' ? 'class=active' : '' ?>>
                                    <a href="<?= Url::toRoute(['/kategori']) ?>">
                                        <i class="fa  fa-list"></i> <span>Kategori</span>
                                    </a>
                                </li>

                            <?php } ?>
                                
                                 <?php if (Yii::$app->user->can('superAdmin')|| Yii::$app->user->can('admin') ) { ?>
                                <li <?= Yii::$app->controller->id == 'menu' ? 'class=active' : '' ?>>
                                    <a href="<?= Url::toRoute(['/menu']) ?>">
                                        <i class="fa  fa-edit"></i> <span>Menu</span>
                                    </a>
                                </li>

                            <?php } ?>
                                
                            <?php if (Yii::$app->user->can('superAdmin')|| Yii::$app->user->can('admin') ) { ?>
                                <li <?= Yii::$app->controller->id == 'slider' ? 'class=active' : '' ?>>
                                    <a href="<?= Url::toRoute(['/slider']) ?>">
                                        <i class="fa  fa-image"></i> <span>Slider</span>
                                    </a>
                                </li>

                            <?php } ?>
                                 <?php if (Yii::$app->user->can('superAdmin')|| Yii::$app->user->can('admin') ) { ?>
                                <li <?= Yii::$app->controller->id == 'team' ? 'class=active' : '' ?>>
                                    <a href="<?= Url::toRoute(['/team']) ?>">
                                        <i class="fa  fa-group"></i> <span>Team</span>
                                    </a>
                                </li>

                            <?php } ?>
                                   <?php if (Yii::$app->user->can('superAdmin')|| Yii::$app->user->can('admin') ) { ?>
                                <li <?= Yii::$app->controller->id == 'fitur' ? 'class=active' : '' ?>>
                                    <a href="<?= Url::toRoute(['/fitur']) ?>">
                                        <i class="fa  fa-check"></i> <span>Fitur</span>
                                    </a>
                                </li>

                            <?php } ?>
                                  
                                
                        <?php } ?>
                        <li class="header">MAIN NAVIGATION</li>

                     
                        <?php if (Yii::$app->user->can('superAdmin')) { ?>
                            <li <?= Yii::$app->controller->id == 'villages' ? 'class=active' : '' ?>>
                                <a href="<?= Url::toRoute(['/villages']) ?>">
                                    <i class="fa  fa-edit"></i> <span>Desa</span>
                                </a>
                            </li>

                        <?php } ?>

                        <?php if (Yii::$app->user->can('superAdmin')) { ?>
                            <li <?= Yii::$app->controller->id == 'districts' ? 'class=active' : '' ?>>
                                <a href="<?= Url::toRoute(['/districts']) ?>">
                                    <i class="fa  fa-edit"></i> <span>Districts</span>
                                </a>
                            </li>

                        <?php } ?>
                        <?php if (Yii::$app->user->can('superAdmin')) { ?>
                            <li <?= Yii::$app->controller->id == 'kategori-experience' ? 'class=active' : '' ?>>
                                <a href="<?= Url::toRoute(['/kategori-experience']) ?>">
                                    <i class="fa  fa-edit"></i> <span>Kategori Experience</span>
                                </a>
                            </li>

                        <?php } ?>
                        <?php if (Yii::$app->user->can('superAdmin')) { ?>
                            <li <?= Yii::$app->controller->id == 'sub-kategori-experience' ? 'class=active' : '' ?>>
                                <a href="<?= Url::toRoute(['/sub-kategori-experience']) ?>">
                                    <i class="fa  fa-edit"></i> <span>Sub Kategori Experience</span>
                                </a>
                            </li>

                        <?php } ?>
                        <?php if (Yii::$app->user->can('superAdmin')) { ?>
                            <li <?= Yii::$app->controller->id == 'experience' ? 'class=active' : '' ?>>
                                <a href="<?= Url::toRoute(['/experience']) ?>">
                                    <i class="fa  fa-edit"></i> <span>Experience</span>
                                </a>
                            </li>

                        <?php } ?>
                        <?php if (Yii::$app->user->can('superAdmin') || Yii::$app->user->can('admin')) { ?>

                            <li <?= Yii::$app->controller->id == 'kategori-accommodation' ? 'class=active' : '' ?>>
                                <a href="<?= Url::toRoute(['/kategori-accommodation']) ?>">
                                    <i class="fa  fa-edit"></i> <span>Kategori Accommodation</span>
                                </a>
                            </li>

                        <?php } ?>
                        <?php if (Yii::$app->user->can('superAdmin') || Yii::$app->user->can('admin')) { ?>

                            <li <?= Yii::$app->controller->id == 'accommodation' ? 'class=active' : '' ?>>
                                <a href="<?= Url::toRoute(['/accommodation']) ?>">
                                    <i class="fa  fa-edit"></i> <span>Accommodation</span>
                                </a>
                            </li>

                        <?php } ?>
                        <?php if (Yii::$app->user->can('superAdmin') || Yii::$app->user->can('admin')) { ?>

                            <li <?= Yii::$app->controller->id == 'kategori-merchandise' ? 'class=active' : '' ?>>
                                <a href="<?= Url::toRoute(['/kategori-merchandise']) ?>">
                                    <i class="fa  fa-edit"></i> <span>Kategori Merchandise</span>
                                </a>
                            </li>

                        <?php } ?>
                        <?php if (Yii::$app->user->can('superAdmin') || Yii::$app->user->can('admin')) { ?>

                            <li <?= Yii::$app->controller->id == 'merchandise' ? 'class=active' : '' ?>>
                                <a href="<?= Url::toRoute(['/merchandise']) ?>">
                                    <i class="fa  fa-edit"></i> <span>Merchandise</span>
                                </a>
                            </li>

                        <?php } ?>
                        <?php if (Yii::$app->user->can('superAdmin') || Yii::$app->user->can('admin')) { ?>
                            <li <?= Yii::$app->controller->id == 'kategori-transportation' ? 'class=active' : '' ?>>
                                <a href="<?= Url::toRoute(['/kategori-transportation']) ?>">
                                    <i class="fa  fa-gift"></i> <span>Kategori Transportation</span>
                                </a>
                            </li>
                        <?php } ?>
                        <?php if (Yii::$app->user->can('superAdmin') || Yii::$app->user->can('admin')) { ?>
                            <li <?= Yii::$app->controller->id == 'transportation' ? 'class=active' : '' ?>>
                                <a href="<?= Url::toRoute(['/transportation']) ?>">
                                    <i class="fa  fa-gift"></i> <span>Transportation</span>
                                </a>
                            </li>
                        <?php } ?>

                        <?php if (Yii::$app->user->can('superAdmin') || Yii::$app->user->can('admin')) { ?>
                            <li <?= Yii::$app->controller->id == 'order' ? 'class=active' : '' ?>>
                                <a href="<?= Url::toRoute(['/order']) ?>">
                                    <i class="fa  fa-gift"></i> <span>Order</span>
                                </a>
                            </li>
                        <?php } ?>



                        <?php if (Yii::$app->user->can('superAdmin')) { ?>
                            <li class="header">USER NAVIGATION</li>
                        <?php } ?>
                        <?php if (Yii::$app->user->can('superAdmin')) { ?> 
                            <li <?= Yii::$app->controller->module->id == 'user' || Yii::$app->controller->module->id == 'rbac' ? 'active' : '' ?>>
                                <a href="<?= Url::toRoute(['/mimin/user/']) ?>">
                                    <i class="fa fa-user "></i> <span>User</span>
                                </a>
                            </li>
                        <?php } ?>



                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>

            <!-- =============================================== -->

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        <?= $this->title ?>
                    </h1>
                    <?=
                    Breadcrumbs::widget([
                        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                    ])
                    ?>
                </section>

                <!-- Main content -->
                <section class="content">

                    <div class="row">
                        <div class="col-lg-12">
                            <?php if (Yii::$app->session->getFlash('success')) : ?>
                                <div id="w0-success-0" class="alert alert-success" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    <?= Yii::$app->session->getFlash('success') ?>
                                </div>
                            <?php elseif (Yii::$app->session->getFlash('error')) : ?>
                                <div id="w0-error-0" class="alert alert-danger" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    <?= Yii::$app->session->getFlash('error') ?>
                                </div>
                            <?php elseif (Yii::$app->session->getFlash('warning')) : ?>
                                <div id="w0-error-0" class="alert alert-warning" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    <?= Yii::$app->session->getFlash('warning') ?>
                                </div>
                            <?php endif ?>
                        </div>
                    </div>

                    <!-- Default box -->
                    <div class="box">
                        <div class="box-body">
                            <?= $content ?>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->

                </section>
                <!-- /.content -->
            </div>
            <!-- /.content-wrapper -->

            <footer class="main-footer">
                <div class="pull-right hidden-xs">
                    <b>Version</b> 1.0.0
                </div>
                <strong>Copyright &copy; 2017 <a href="<?= Url::toRoute(['/']) ?>"><?= Yii::$app->name ?></a> 085222324246 - harisabintang115@gmail.com </strong>
            </footer>

        </div>

        <!--jangan diganggu-->
        <?php
        Modal::begin([
            'header' => 'janganDiganggu',
            'id' => 'modalJanganDiganggu',
            'size' => 'modal-lg',
        ]);

        echo "<div id='janganDiganggu'></div>";

        Modal::end();
        ?>
        <!--jangan diganggu-->

        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>
