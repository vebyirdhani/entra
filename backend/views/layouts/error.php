<?php
/* @var $this \yii\web\View */
/* @var $content string */

use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;
use backend\models\Admin;
use backend\models\Cp;

AppAsset::register($this);

$user = Yii::$app->user->identity;
$cp = Cp::findOne(['status' => 1]);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="<?= Yii::$app->name ?>">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
        <link rel="shortcut icon" type="image/x-icon" href="<?= Yii::$app->getHomeUrl(); ?>favicon.png" />
    </head>

    <body class="hold-transition skin-yellow layout-top-nav">
        <?php $this->beginBody() ?>

        <div class="wrapper">

            <header class="main-header">
                <nav class="navbar navbar-static-top">
                    <div class="container">
                        <div class="navbar-header" style="margin-left: -91px;">
                            <a href="<?= Url::toRoute(['/admin/site/']) ?>" class="navbar-brand"><?= Yii::$app->name ?></a>

                        </div>

                    </div>
                    <!-- /.container-fluid -->
                </nav>
            </header>
            <!-- Full Width Column -->
            <div class="content-wrapper">
                <div class="container">
                    <section class="content-header">
                        <h1>
                            <?= $this->title ?>
                        </h1>
                        <?=
                        Breadcrumbs::widget([
                            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                        ])
                        ?>
                    </section>
                    <!-- Main content -->
                    <section class="content">

                        <div class="box">
                            <div class="box-body">
                                <?= $content ?>
                            </div>
                            <!-- /.box-body -->
                        </div>

                    </section>
                    <!-- /.content -->
                </div>
                <!-- /.container -->
            </div>
            <!-- /.content-wrapper -->
            <footer class="main-footer">
                <div class="pull-right hidden-xs">
                    <b>Version</b> 1.0.0
                </div>
                <strong>Copyright &copy; 2017 <a href="<?= Url::toRoute(['/']) ?>"><?= Yii::$app->name ?></a> <?= $cp->nomor_telepon ?> - <?= $cp->alamat_email ?> </strong>
            </footer>
        </div>

        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>
