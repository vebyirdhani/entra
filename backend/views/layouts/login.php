<?php
/* @var $this \yii\web\View */
/* @var $content string */

use backend\assets\LoginAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;
use yii\helpers\Url;

LoginAsset::register($this);
?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="<?= Yii::$app->name ?>">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
        <link rel="shortcut icon" type="image/x-icon" href="<?= Yii::$app->getHomeUrl(); ?>" />
    </head>
    <body class="hold-transition login-page">
        <?php $this->beginBody() ?>

        <div class="login-box">
            <div class="login-logo">
                <a href="<?= Url::toRoute(['/admin/site/']) ?>">
<!--                    <img src="<?= Yii::$app->getHomeUrl(); ?>" height="100">-->
                    <br>
                    <b><?= Yii::$app->name ?></b>
                </a>
            </div>
            <!-- /.login-logo -->
            <div class="login-box-body">
                <p class="login-box-msg">Sign in to start your session</p>

                <?= $content ?>


                <div class="row">
                    <div class="col-lg-12">
                        
                            <?= Alert::widget() ?>
                        
                    </div>
                </div>
            </div>
            <!-- /.login-box-body -->
        </div>

        <?php $this->endBody() ?>
        <script>
            $(function () {
                $('input').iCheck({
                    checkboxClass: 'icheckbox_square-blue',
                    radioClass: 'iradio_square-blue',
                    increaseArea: '20%' // optional
                });
            });
        </script>
    </body>
</html>
<?php $this->endPage() ?>
