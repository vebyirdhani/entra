<?php
/* @var $this \yii\web\View */
/* @var $content string */

use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;
use yii\helpers\StringHelper;
use yii\bootstrap\Modal;

AppAsset::register($this);

$user = Yii::$app->user->identity;
 $id =Yii::$app->user->identity->id;
$profile= \common\models\Profile::find()->Where(['user_id'=>$id])->one();
$data = common\models\Pemasukan::find()->where(['project'=>$profile->project])->all();
$biaya  = common\models\Biaya::find()->where(['project'=>$profile->project])->all();
$project = \common\models\DaftarProject::find()->all();
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="<?= Yii::$app->name ?>">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
        <link rel="shortcut icon" type="image/x-icon" href="<?= yii\helpers\Url::toRoute(['/favicon.ico']) ?>" />
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    </head>
    <body class="hold-transition skin-yellow sidebar-mini fixed">
        <?php $this->beginBody() ?>

        <div class="wrapper">

            <header class="main-header">
                <!-- Logo -->
                <a href="<?= Url::toRoute(['/']) ?>" class="logo">
                    <!-- mini logo for sidebar mini 50x50 pixels -->
                    <span class="logo-mini"> <i class="fa fa-home"></i></span>
                    <!-- logo for regular state and mobile devices -->
                    <span class="logo-sm" style="font-size:16px;"><?= Yii::$app->name ?></span>
                </a>
                <!-- Header Navbar: style can be found in header.less -->
                <nav class="navbar navbar-static-top">
                    <!-- Sidebar toggle button-->
                    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </a>

                    <div class="navbar-custom-menu">
                        <ul class="nav navbar-nav">
                           
                            
                            <li class="dropdown messages-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="fa fa-bell-o"></i>
                                  
                                    <span class="label label-success"></span>
                                  
                                        <span class="label label-success"></span>
                             
                                </a>
                                 
                                <ul class="dropdown-menu">
                                    
                                    <li class="header">Anda memiliki notifikasi baru</li>
                                    
                                    <li class="header">Anda tidak memiliki  notifikasi baru</li>
                                  
                                    <li>
                                        <!-- inner menu: contains the actual data -->
                                        <ul class="menu">
                                            <li><!-- start message -->
                               <a href="">
                                                    <div class="pull-left">
                                                        <img src="<?= Url::toRoute(['/dist/img/default.png']) ?>" class="img-circle" alt="User Image">
                                                    </div>
                                                    
                                                    
                                                    <h4>
                                                        <b>  </b>
                                                    </h4>
                                                    
                                                    <p></p>
                                                    <p><i class="fa fa-clock-o"></i> </p>
                                                    
                                                 
                                                </a>
                                            </li>
                                             <li><!-- start message -->
                                              
                                                <a href="">
                                                    <div class="pull-left">
                                                        <img src="<?= Url::toRoute(['/dist/img/default.png']) ?>" class="img-circle" alt="User Image">
                                                    </div>
                                                    
                                                    
                                                    <h4>
                                                      
                                                    </h4>
                                                    
                                                    <p></p>
                                                    <p><i class="fa fa-clock-o"></i> </p>
                                                
                                                </a>
                                            </li>
                                        </ul>
                                         
                                    </li>
                                    <li class="footer"><a href="<?= Url::toRoute(['/komentar']) ?>">Lihat semua notifikasi</a></li>
                                </ul>
                            </li>
                            
                            <li class="dropdown messages-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="fa fa-envelope-o"></i>
                                   
                                    <span class="label label-success"></span>
                                 
                                    <span class="label label-success"></span>
                                 
                                </a>
                                <ul class="dropdown-menu">
                                  
                                    <li class="header">Anda memiliki  pesan baru</li>
                                   
                                     <li class="header">Anda memiliki tidak memiliki pesan baru</li>
                                
                                    <li>
                                        <!-- inner menu: contains the actual data -->
                                        <ul class="menu">
                                            <li><!-- start message -->
                                             
                                                <a href="">
                                                    <div class="pull-left">
                                                        <img src="<?= Url::toRoute(['/dist/img/default.png']) ?>" class="img-circle" alt="User Image">
                                                    </div>
                                                    <h4>
                                                        <b></b>

                                                    </h4>
                                                    <p></p>
                                                    <p><i class="fa fa-clock-o"></i> </p>
                                                </a>
                                          
                                            </li>
                                               <li><!-- start message -->
                                     
                                                <a href="ssss">
                                                    <div class="pull-left">
                                                        <img src="<?= Url::toRoute(['/dist/img/default.png']) ?>" class="img-circle" alt="User Image">
                                                    </div>
                                                    <h4>
                                                     

                                                    </h4>
                                                    <p></p>
                                                    <p><i class="fa fa-clock-o"></i></p>
                                                </a>
                                               
                                            </li>
                                        </ul>
                                    </li>
                                      <li>
                                        <!-- inner menu: contains the actual data -->
                                        
                                    </li>
                                    <li class="footer"><a href="<?= Url::toRoute(['/donasi/']) ?>">Lihat semua pesan</a></li>
                                </ul>
                            </li>
                            <li class="dropdown user user-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <img src="<?= Url::toRoute(['/site/foto-profil', 'inline' => true]) ?>" class="user-image" alt="User Image">
                                    <span class="hidden-xs"><?= ucwords($user->profile->name) ?></span>
                                </a>
                                <ul class="dropdown-menu">
                                    <!-- User image -->
                                    <li class="user-header">
                                        <img src="<?= Url::toRoute(['/site/foto-profil', 'inline' => true]) ?>" class="img-circle" alt="User Image">

                                        <p>
                                            <?= ucwords($user->profile->name) ?> <br>  <?php // Yii::$app->name ?>
                                            <small>Member since <?= date("Y M d", $user->created_at) ?></small>
                                        </p>
                                    </li>
                                    <!-- Menu Footer-->
                                    <li class="user-footer">
                                        <div class="pull-left">
                                            <a href="<?= Url::toRoute(['/site/profil']) ?>" class="btn btn-default btn-flat">Profil</a>
                                        </div>
                                        <div class="pull-right">
                                            <a href="<?= Url::to(['/site/logout']) ?>" data-method="post" class="btn btn-default btn-flat">Log Out</a>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </nav>
            </header>

            <!-- =============================================== -->

            <!-- Left side column. contains the sidebar -->
            <aside class="main-sidebar">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->
                    <div class="user-panel">
                        <div class="pull-left image">
                            <img src="<?= Url::toRoute(['/site/foto-profil', 'inline' => true]) ?>" class="img-circle" alt="User Image">
                        </div>
                        <div class="pull-left info">
                            <p><?= ucwords($user->profile->name) ?></p>
                            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                        </div>
                    </div>
                    <br>
                    <!-- sidebar menu: : style can be found in sidebar.less -->
                     <ul class="sidebar-menu">
                        <li class="header">MAIN NAVIGATION</li>
                         
                        <li <?= Yii::$app->controller->id == 'site' ? 'class=active' : '' ?>>
                            <a href="<?= Url::toRoute(['/']) ?>">
                                <i class="fa fa-home "></i> <span>Dashboard</span>
                            </a>
                        </li>
                       <?php  if (Yii::$app->user->can('superAdmin')) { ?>
                        <li <?= Yii::$app->controller->id == 'daftar-project' ? 'class=active' : '' ?>>
                            <a href="<?= Url::toRoute(['/daftar-project']) ?>">
                                <i class="fa  fa-edit"></i> <span>Daftar Project</span>
                            </a>
                        </li>
                    
                       <?php } ?>
                        <?php if (Yii::$app->user->can('superAdmin')|| Yii::$app->user->can('admin')){?>
                          <li <?= Yii::$app->controller->id == 'data-pelanggan' ? 'class=active' : '' ?>>
                            <a href="<?= Url::toRoute(['/data-pelanggan']) ?>">
                                <i class="fa  fa-edit"></i> <span>Data Pelanggan</span>
                            </a>
                        </li>
                        <?php } ?>

                         <?php if (Yii::$app->user->can('superAdmin')||Yii::$app->user->can('admin')) { ?>
                        
                         <li <?= Yii::$app->controller->id == 'biaya' ? 'class=active' : '' ?>>
                            <a href="<?= Url::toRoute(['/biaya']) ?>">
                                <i class="fa  fa-edit"></i> <span>Biaya</span>
                            </a>
                        </li>
                        
                        <?php } ?>

                         <?php  if (Yii::$app->user->can('superAdmin')|| Yii::$app->user->can('admin')) { ?>
                        <li <?= Yii::$app->controller->id == 'detail' ? 'class=active' : '' ?>>
                            <a href="<?= Url::toRoute(['/detail']) ?>">
                                <i class="fa  fa-list-ul"></i> <span>Detail Biaya</span>
                            </a>
                        </li>

                    <?php } ?>
                       <?php  if (Yii::$app->user->can('superAdmin')|| Yii::$app->user->can('admin')) { ?>
                                         <ul class="sidebar-menu" data-widget="tree">
  
        <li class="treeview menu-open">
            <a href="#">
                <i class="fa fa-search"></i><span>Data Biaya</span>
                <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                </span>
            </a>
               <?php if ($biaya){ 
                    foreach ($biaya as $value1){?>
            <ul class="treeview-menu menu-open treeview-menu-visible">
                 
                <li class="treeview menu-open">
            <a href="#">
               
               
                <i class="fa fa-search"></i><span><?= $value1->nama?></span><br>
             
                <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                </span>
            </a>
            <ul class="treeview-menu menu-open treeview-menu-visible">
                
            
               
                <li><a href="<?=  Url::toRoute(['/jumlah-detail/tambah','id'=>$value1->id_biaya]) ?>"><i class="fa fa-lock"></i> Input <?= $value1->nama?></a></li>
                <li><a href="<?=  Url::toRoute(['/jumlah-detail/index','id'=>$value1->id_biaya]) ?>"><i class="fa fa-lock"></i> Data <?= $value1->nama?></a></li>
                   
            </ul>
        </li>
            </ul>
                 <?php } } ?>
        </li>
    </li>
</ul> 
                       <?php } ?>
                        <?php  if (Yii::$app->user->can('superAdmin')|| Yii::$app->user->can('admin')) { ?>
                        <li <?= Yii::$app->controller->id == 'pemasukan' ? 'class=active' : '' ?>>
                            <a href="<?= Url::toRoute(['/pemasukan']) ?>">
                                <i class="fa  fa-gift"></i> <span>Pemasukan</span>
                            </a>
                        </li>
                       <?php } ?>
                        
                          <?php  if (Yii::$app->user->can('superAdmin')|| Yii::$app->user->can('admin')){ ?>
                        
                         <li <?= Yii::$app->controller->id == 'detail-pemasukan' ? 'class=active' : '' ?>>
                            <a href="<?= Url::toRoute(['/detail-pemasukan']) ?>">
                                <i class="fa  fa-edit"></i> <span>Detail Pemasukan</span>
                            </a>
                        </li>
                   
                      <?php }?>   <?php  if (Yii::$app->user->can('pelanggan')) { ?>
                         <li <?= Yii::$app->controller->id == 'da-pemasukan' ? 'class=active' : '' ?>>
                            <a href="<?= Url::toRoute(['/da-pemasukan']) ?>">
                                <i class="fa  fa-pencil"></i> <span>Transaksi</span>
                            </a>
                        </li>
                   
                      <?php }?>
                           <?php  //if (Yii::$app->user->can('/data-pemasukan/index')|| Yii::$app->user->can('/data-pemasukan/*') || Yii::$app->user->can('/*')) { ?>
                        
<!--                         <li <?php // Yii::$app->controller->id == 'data-pemasukan' ? 'class=active' : '' ?>>
                            <a href="<?php //Url::toRoute(['/data-pemasukan']) ?>">
                                <i class="fa  fa-download"></i> <span>Data Pemasukan</span>
                            </a>
                        </li>
                   -->
                      <?php //}?>
                        
                             <?php  if (Yii::$app->user->can('superAdmin')|| Yii::$app->user->can('admin')){?>
                           <ul class="sidebar-menu" data-widget="tree">
  
        <li class="treeview menu-open">
            <a href="#">
                <i class="fa fa-search"></i><span>Data Pemasukan</span>
                <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                </span>
            </a>
               <?php if ($data){ 
                    foreach ($data as $value){?>
            <ul class="treeview-menu menu-open treeview-menu-visible">
                 
                <li class="treeview menu-open">
            <a href="#">
               
               
                <i class="fa fa-search"></i><span><?= $value->nama?></span><br>
             
                <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                </span>
            </a>
            <ul class="treeview-menu menu-open treeview-menu-visible">
                
                 <?PHP       $detail = \common\models\DetailPemasukan::find()->where(['pemasukan'=>$value->nama])->all();
                       foreach ($detail as $values){
                        ?>
               
                <li><a href="<?=  Url::toRoute(['/data-pemasukan/tambah','id'=>$values->nama]) ?>"><i class="fa fa-pencil"></i> <?= $values->nama?></a></li>
                
                  <?php } ?>
                
            </ul>
        </li>
            </ul>
                 <?php } } ?>
        </li>
        
        
    </li>
</ul>       
                       <?php } ?>
                        
<!--                  
                  
                        <?php  if (Yii::$app->user->can('superAdmin')|| Yii::$app->user->can('admin')){  ?>
                                        <ul class="sidebar-menu" data-widget="tree">
  
        <li class="treeview menu-open">
            <a href="#">
                <i class="fa fa-search"></i><span>Data Transaksi</span>
                <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                </span>
            </a>
               <?php if ($data){ 
                    foreach ($data as $value){?>
            <ul class="treeview-menu menu-open treeview-menu-visible">
                 
                <li class="treeview menu-open">
            <a href="#">
               
               
                <i class="fa fa-search"></i><span><?= $value->nama?></span><br>
             
                <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                </span>
            </a>
            <ul class="treeview-menu menu-open treeview-menu-visible">
                
                 <?PHP       $detail = \common\models\DetailPemasukan::find()->where(['pemasukan'=>$value->nama])->all();
                       foreach ($detail as $values){
                        ?>
               
                <li><a href="<?=  Url::toRoute(['/data-transaksi/index','id'=>$values->nama]) ?>"><i class="fa fa-lock"></i> <?= $values->nama?></a></li>
                  <?php } ?>
                
            </ul>
        </li>
            </ul>
                 <?php } } ?>
        </li>
        
        
    </li>
</ul>         -->
                        <?php } ?>
                      
                           
                        
                        
                            <?php   if (Yii::$app->user->can('superAdmin')|| Yii::$app->user->can('admin') || Yii::$app->user->can('pelanggan')) { ?> 
                         <li <?= Yii::$app->controller->id == 'pembayaran' ? 'class=active' : '' ?>>
                            <a href="<?= Url::toRoute(['/pembayaran']) ?>">
                                <i class="fa  fa-money"></i> <span>Pembayaran</span>
                            </a>
                        </li>
                   
                      <?php }?>
                        
                        
                   <?php   if (Yii::$app->user->can('superAdmin')|| Yii::$app->user->can('admin')) { ?>        
                         <li <?= Yii::$app->controller->id == 'detail-pembayaran' ? 'class=active' : '' ?>>
                            <a href="<?= Url::toRoute(['/detail-pembayaran']) ?>">
                                <i class="fa  fa-money"></i> <span>Detail Pembayaran</span>
                            </a>
                        </li>
                   
                      <?php }?>
                        
                          <?php   if (Yii::$app->user->can('superAdmin')|| Yii::$app->user->can('admin') || Yii::$app->user->can('pelanggan')) { ?>     
                         <li <?= Yii::$app->controller->id == 'data-pembayaran' ? 'class=active' : '' ?>>
                            <a href="<?= Url::toRoute(['/data-pembayaran']) ?>">
                                <i class="fa  fa-money"></i> <span>Data Pembayaran</span>
                            </a>
                        </li>
                   
                      <?php }?>
                       <?php   if (Yii::$app->user->can('superAdmin')|| Yii::$app->user->can('admin')) { ?>     
                         <li <?= Yii::$app->controller->id == 'data-supplyer' ? 'class=active' : '' ?>>
                            <a href="<?= Url::toRoute(['/data-supplyer']) ?>">
                                <i class="fa  fa-pencil"></i> <span>Data Supplyer</span>
                            </a>
                        </li>
                   
                      <?php }?>
                   
                   
                   
                          <?php   if (Yii::$app->user->can('superAdmin')|| Yii::$app->user->can('admin')) { ?>     
                         <li <?= Yii::$app->controller->id == 'data-sales' ? 'class=active' : '' ?>>
                            <a href="<?= Url::toRoute(['/data-sales']) ?>">
                                <i class="fa  fa-pencil"></i> <span>Data Sales</span>
                            </a>
                        </li>
                   
                      <?php }?>
                   
                        
                       
                        <!--     <li class="header">MASTER NAVIGATION</li>
                       -->
                  <?php   if (Yii::$app->user->can('superAdmin')) { ?>
                   
                         <li class="header">REPORT NAVIGATION</li>
                     <?php 
                     if ($project){
                         
                     foreach ($project as $db){ 
                     ?>
                       <?php   if (Yii::$app->user->can('superAdmin')) { ?>
                         <li <?= Yii::$app->controller->id == 'report' ? 'class=active' : '' ?>>
                            <a href="<?= Url::toRoute(['/report/project','id'=>$db->id_project]) ?>">
                                <i class="fa  fa-file-text-o"></i> <span>Biaya <?= $db->nama_project?></span>
                            </a>
                        </li>
                        
                        <?php } ?>
                     <?php }} ?> 
                  <?php } ?>  
                          <?php   if (Yii::$app->user->can('superAdmin')) { ?>
                        <li class="header">USER NAVIGATION</li>
                        <?php } ?>
                           <?php   if (Yii::$app->user->can('superAdmin')) { ?> 
                        <li <?= Yii::$app->controller->module->id == 'user' || Yii::$app->controller->module->id == 'rbac' ? 'active' : ''  ?>>
                            <a href="<?= Url::toRoute(['/mimin/user/']) ?>">
                                <i class="fa fa-user "></i> <span>User</span>
                            </a>
                        </li>
                         <?php } ?>
                       
                        
                          
                       
                      <?php  //if (Yii::$app->user->can('/mimin/user/index')|| Yii::$app->user->can('/mimin/user/*')) { ?>
                        
                        
                      <?php  // } ?>
                       
                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>

            <!-- =============================================== -->

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        <?= $this->title ?>
                    </h1>
                    <?=
                    Breadcrumbs::widget([
                        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                    ])
                    ?>
                </section>

                <!-- Main content -->
                <section class="content">

                    <div class="row">
                        <div class="col-lg-12">
                            <?php if (Yii::$app->session->getFlash('success')) : ?>
                                <div id="w0-success-0" class="alert alert-success" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    <?= Yii::$app->session->getFlash('success') ?>
                                </div>
                            <?php elseif (Yii::$app->session->getFlash('error')) : ?>
                                <div id="w0-error-0" class="alert alert-danger" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    <?= Yii::$app->session->getFlash('error') ?>
                                </div>
                            <?php elseif (Yii::$app->session->getFlash('warning')) : ?>
                                <div id="w0-error-0" class="alert alert-warning" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    <?= Yii::$app->session->getFlash('warning') ?>
                                </div>
                            <?php endif ?>
                        </div>
                    </div>

                    <!-- Default box -->
                    <div class="box">
                        <div class="box-body">
                            <?= $content ?>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->

                </section>
                <!-- /.content -->
            </div>
            <!-- /.content-wrapper -->

<!--            <footer class="main-footer">
                <div class="pull-right hidden-xs">
                    <b>Version</b> 1.0.0
                </div>
                <strong>Copyright &copy; 2017 <a href="<?= Url::toRoute(['/']) ?>"><?= Yii::$app->name ?></a> 085222324246 - harisabintang115@gmail.com </strong>
            </footer>-->

        </div>

        <!--jangan diganggu-->
        <?php
        Modal::begin([
            'header' => 'janganDiganggu',
            'id' => 'modalJanganDiganggu',
            'size' => 'modal-lg',
        ]);

        echo "<div id='janganDiganggu'></div>";

        Modal::end();
        ?>
        <!--jangan diganggu-->

        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>
