<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\KunjunganSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Contacts';
$this->params['breadcrumbs'][] = $this->title;
$tombol = '{view} ';
?>

<div class="kunjungan-index">
    
    <div class="box box-solid box-primary">
        <div class="box-header">
               <h3 class="box-title"></h3>
           
            <div class="box-tools pull-right">
               
              
            </div>
        </div>
         <div class="box-body">
   
 
   
    </div>
        <div class="box-body table-responsive">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
       // 'filterModel' => $searchModel,
        'tableOptions' => ['class'=> 'table table-condensed'],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn','header'=>'No'],
          //  'tanggal',
           
            'name',
            'email',
            'subject',
            'body',
          
        ],
    ]); ?>
          
        </div>
        </div>
    </div>

