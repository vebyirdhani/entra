<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Kunjungan */

$this->title = $model->id_contact;
$this->params['breadcrumbs'][] = ['label' => 'Contact', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kunjungan-view">
<div class="box box-solid box-primary">
        <div class="box-header">
            <h3 class="box-title">Detail Contact</h3>
            <div class="box-tools pull-right">
                <?= Html::a('<i class="fa fa-arrow-circle-left"></i> Back', ['index'], ['class' => 'btn btn-primary']) ?>
                <?= Html::a('<i class="fa fa-fw fa-pencil-square"></i>Update', ['update', 'id' => $model->id_contact], ['class' => 'btn btn-primary']) ?>
                
            </div>
        </div>
        <div class="box-body">
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
           'name',
            'email',
            'subject',
            'body',
             [
                    'attribute' => 'created_at',
                    'value' => function($row) {
                        return date('d-m-Y', $row->created_at);
                    }
                ],
        ],
    ]) ?>
        </div>
</div>
               

</div>
