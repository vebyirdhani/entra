<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\DetailView;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $model common\models\DetailJadwalkuliah */

$modelName = 'Detail Jadwalkuliah';
$this->title = 'Input Detail Jadwalkuliah';
$this->params['breadcrumbs'][] = ['label' => 'Detail Jadwalkuliah', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>


<div class="row">
    <div class="col-lg-3 col-md-3 admin-menu-right-line">
        <div class="box-header with-border">
            <h3 class="box-title">Menu</h3>
        </div>
        <div class="box-body no-padding">
            <ul class="nav nav-pills nav-stacked">
                <li><a href="<?= Url::toRoute(['index']) ?>"><i class="fa fa-list-alt"></i> Data <?= Html::encode($modelName) ?> </a></li>
                <li class="active" style="background-color: #f4f4f4"><a href="<?=  Url::toRoute(['create']) ?>"><i class="fa fa-pencil-square-o"></i> Input Data <?= Html::encode($modelName) ?></a></li>
            </ul>
        </div>
    </div>
    <div class="col-lg-9 col-md-9">

        <div class="admin-create">

            <div class="detail-jadwalkuliah-create">

                <h1><?= Html::encode($this->title) ?></h1>
  <?= DetailView::widget([
            'model' => $data,
            'attributes' => [
                     
            'kelas_nama',
            'mata_kuliah',
            'dosen',
            'jadwal_awal',
            'jadwal_akhir',
          
            ],
            ]) ?>
                    <?php $form = ActiveForm::begin([
        'action' => ['create','id'=>$id,'angkatan'=>$model->angkatan],
        'method' => 'get',
    ]); ?>
                    
                 <?= $form->field($model, 'angkatan') ?>
                    
                  
                   <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
                    
                    <?php ActiveForm::end(); ?>
                   <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
        'columns' => [
              [
                                'class' => 'yii\grid\CheckboxColumn',
                            ],
                    [
                    'class' => 'yii\grid\SerialColumn',
                    'header' => 'No.'
                    ],

                      //          'id_mahasiswa',
            'nim',
            'nama',
            'jurusan',
            'prodi',
            'angkatan',

                    ],
                    ]); ?>
              
            </div>


        </div>
    </div>
</div>