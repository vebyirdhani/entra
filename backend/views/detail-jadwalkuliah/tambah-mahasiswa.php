<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\DetailView;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\Angkatan;
/* @var $this yii\web\View */
/* @var $model common\models\DetailJadwalkuliah */

$modelName = 'Detail Jadwal kuliah';
$this->title = 'Input Detail Jadwal kuliah';
$this->params['breadcrumbs'][] = ['label' => 'Detail Jadwal kuliah', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <div class="col-lg-3 col-md-3 admin-menu-right-line">
        <div class="box-header with-border">
            <h3 class="box-title">Menu</h3>
        </div>
        <div class="box-body no-padding">
            <ul class="nav nav-pills nav-stacked">
               
                <li class="active" style="background-color: #f4f4f4"><a href="<?=  Url::toRoute(['tambah-mahasiswa','d'=>$data->id_jadwal]) ?>"><i class="fa fa-pencil-square-o"></i> Input Data <?= Html::encode($modelName) ?></a></li>
                 <li ><a href="<?=  Url::toRoute(['data-mahasiswa','id'=>$data->id_jadwal]) ?>"><i class="fa  fa-list-alt"></i> Data Mahasiswa per Jadwal Kuliah </a></li>
            </ul>
        </div>
    </div>
    <div class="col-lg-9 col-md-9">

        <div class="admin-create">

            <div class="detail-jadwalkuliah-create">
 <?= Html::a('Tambah Jadwal',['jadwal-kuliah/create'],[
                            'class'=>'btn btn-success pull-right',
                           
     ]); ?><BR>
               <h3><b>Data Mata Kuliah</b></h3>
  <?= DetailView::widget([
            'model' => $data,
            'attributes' => [
                     
            'kelas_nama',
            'matkulid.nama',
					'prodiid.nama_prodi',
                    'dosen1id.nama',
					'dosen2id.nama',
            'jadwal_awal',
            'jadwal_akhir',
                'hari',
          
            ],
            ]) ?>
                      <?php $form = ActiveForm::begin([]);?>
                    
                
                    
                    <?= $form->field($model, 'angkatan')->dropDownList(ArrayHelper::map(Angkatan::find()->all(), 'angkatan', 'angkatan'),[
                        'prompt' => 'Cari berdasarkan angkatan'
                    ]); ?>
                 <?= $form->field($model, 'prodi')->dropDownList(ArrayHelper::map(common\models\Prodi::find()->all(), 'id_prodi', 'nama_prodi'),[
                        'prompt' => 'Cari berdasarkan prodi'
                    ]); ?>
                    
                   
                    
                    <?= Html::submitButton('Cari',['class' => 'btn btn-primary']); ?>
                    
                    <?php ActiveForm::end(); ?>
                
                    <h3><b>Data Mahasiswa</b></h3>
                   <?=
                    GridView::widget([
                        'dataProvider' => $dataProvider,
                        'id'=>'list-mahasiswa-pa',
                        'tableOptions' => [
                            'class' => 'table table-striped',
                        ],
                        'columns' => [
                            [
                                'class' => 'yii\grid\CheckboxColumn',
                            ],
                            [
                                'class' => 'yii\grid\SerialColumn',
                                'header' => 'No'
                            ],
                            'nim',
                            'nama',
                          'angkatan',
                               [
                            'attribute'=> 'prodi',
                  
                            'value' => 'idProdi.nama_prodi',
                        ],
//                           
                        ]
                    ]);
                            ?>
                      <p>

                <br>
                        <?= Html::a('Tambah Mahasiswa',[''],[
                            'class'=>'btn btn-success pull-right',
                            'onclick' => "event.preventDefault();
                                        var keys = $('#list-mahasiswa-pa').yiiGridView('getSelectedRows');
                                        if(keys.length && 
                                        confirm('Apakah anda ingin menambahkan '+keys.length+' orang Mahasiswa ?')){
                                            ;
                                            $.post('" . Url::current() . "',{item:keys},function(data){
                                                document.location = '".Url::previous('list-mhs-pa')."';
                                            },'json');
                                        }"
                            ]); ?>
                    </p>
                  
            </div>


        </div>
    </div>
</div>