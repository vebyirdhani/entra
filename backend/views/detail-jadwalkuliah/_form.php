<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\DetailJadwalkuliah */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="detail-jadwalkuliah-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nama_matkul')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'jam_mulai')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'jam_akhir')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'mahasiswa')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'dosen')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
