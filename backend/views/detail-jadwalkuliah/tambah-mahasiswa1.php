<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use dmstr\widgets\Alert;
use yii\bootstrap\ActiveForm;
use common\models\Angkatan;
use backend\models\MasterProdi;
use yii\helpers\ArrayHelper;
use yii\widgets\DetailView;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\MasterFakultasSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Tambah Dosen PA';
$this->params['breadcrumbs'][] = $this->title;
?>
 
<?= DetailView::widget([
            'model' => $data,
            'attributes' => [
                     
            'kelas_nama',
            'mata_kuliah',
            'dosen',
            'jadwal_awal',
            'jadwal_akhir',
          
            ],
            ]) ?>

<div class="data-kurikulim-index">
    <div class="box box-primary">
        <div class="box-body">
            <div class="row">
                <div class="col-lg-12 ">
                  
  
                    <?php $form = ActiveForm::begin(['layout'=>'inline']);?>
                    
                
                    
                    <?= $form->field($model, 'angkatan')->dropDownList(ArrayHelper::map(Angkatan::find()->all(), 'angkatan', 'angkatan'),[
                        'prompt' => 'Silahkan Pilih Tahun Angkatan'
                    ]); ?>
                    
                   
                    
                    <?= Html::submitButton('Cari',['class' => 'btn btn-primary']); ?>
                    
                    <?php ActiveForm::end(); ?>
                    <p>

                <br>
                        <?= Html::a('Tambah Mahasiswa',[''],[
                            'class'=>'btn btn-success pull-right',
                            'onclick' => "event.preventDefault();
                                        var keys = $('#list-mahasiswa-pa').yiiGridView('getSelectedRows');
                                        if(keys.length && 
                                        confirm('Apakah anda ingin menambahkan '+keys.length+' orang Mahasiswa ?')){
                                            ;
                                            $.post('" . Url::current() . "',{item:keys},function(data){
                                                document.location = '".Url::previous('list-mhs-pa')."';
                                            },'json');
                                        }"
                            ]); ?>
                    </p>
                    <?=
                    GridView::widget([
                        'dataProvider' => $dataProvider,
                        'id'=>'list-mahasiswa-pa',
                        'tableOptions' => [
                            'class' => 'table table-striped',
                        ],
                        'columns' => [
                            [
                                'class' => 'yii\grid\CheckboxColumn',
                            ],
                            [
                                'class' => 'yii\grid\SerialColumn',
                                'header' => 'No'
                            ],
                            'nim',
                            'nama',
                          'angkatan',
//                            [
//                                'class' => 'yii\grid\ActionColumn',
//                                'template' => '{hapus-mahasiswa}',
//                                'buttons' => [
//                                    'hapus-mahasiswa' => function($url,$model,$key){
//                                        return Html::a('Hapus',['hapus-mahasiswa','mhs'=>$model->mahasiswa_id,'dosen'=>$this->params['dosen_id'] ],[
//                                            'class' => 'btn btn-primary btn-sm'
//                                        ]);
//                                    }
//                                ]
//                            ]
                        ]
                    ]);
                            ?>
                    <p>
                        <?= Html::a('Back',  Url::previous('list-mhs-pa'),['class'=>'btn btn-warning'])?>
                    </p>
                </div>
            </div>   
        </div>
    </div>
</div>
