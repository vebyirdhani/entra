<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
/* @var $this yii\web\View */
/* @var $model common\models\Villages */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="villages-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'provinsi')->dropDownList(
            ArrayHelper::map(common\models\Provinces::find()->all(),'id','name'),
            ['prompt'=>'Pilih Provinsi']) ?>
  <?=
                    $form->field($model, 'district_id', [
                      
                    ])->widget(Select2::className(), [
                        'data' => ArrayHelper::map( common\models\Districts::find()->all(),'id', 'name'),
                        'language' => 'en',
                        'options' => ['placeholder' => 'Pilih Kecamatan'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ]
                    ])
                    ?> 

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

      <?= $form->field($model, 'picture[]')->fileInput(['multiple'=>TRUE])->label('Upload Foto ') .'*Maksimal ukuran foto 2MB' ?>

  
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
