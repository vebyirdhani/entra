<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\BookmarkDesa */

$modelName = 'Bookmark Desa';
$this->title = 'Bookmark Desa | ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Bookmark Desa', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <div class="col-lg-3 col-md-3 admin-menu-right-line">
        <div class="box-header with-border">
            <h3 class="box-title">Menu</h3>
        </div>
        <div class="box-body no-padding">
            <ul class="nav nav-pills nav-stacked">
                <li ><a href="<?= Url::toRoute(['index']) ?>"><i class="fa fa-list-alt"></i> Data <?= Html::encode($modelName) ?></a></li>
                <li ><a href="<?= Url::toRoute(['create']) ?>"><i class="fa fa-pencil-square-o"></i> Input Data <?= Html::encode($modelName) ?></a></li>
                <li class="active" style="background-color: #f4f4f4"><a href="#"><i class="fa fa-eye"></i> Lihat Data <?= Html::encode($modelName) ?></a></li>
                <li ><a href="<?= Url::toRoute(['update', 'id' => $model->id]) ?>"><i class="fa fa-pencil"></i> Update Data <?= Html::encode($modelName) ?></a></li>
                <li >
                    <?=                     Html::a('<i class="fa fa-trash"></i> Hapus Data <?= Html::encode($modelName) ?>', ['delete', 'id' => $model->id], [
                        'data' => [
                            'confirm' => 'Hapus?',
                            'method' => 'post',
                        ],
                    ])
                    ?>
                </li>
            </ul>
        </div>
    </div>
    <div class="col-lg-9 col-md-9">
        <div class="bookmark-desa-view">

            <h1><?= Html::encode($this->title) ?></h1>

            <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                        'id',
            'id_desa',
            'id_user',
            'id_bookmark',
            ],
            ]) ?>

        </div>


    </div>
</div> 