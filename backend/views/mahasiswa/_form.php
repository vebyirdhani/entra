<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $model common\models\Mahasiswa */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="mahasiswa-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nim')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'nama')->textInput(['maxlength' => true]) ?>

      <?php // $form->field($model, 'jurusan')->dropDownList(
          //  ArrayHelper::map(common\models\Jurusan::find()->all(),'nama','nama'),
          //  ['prompt'=>'Pilih Jurusan']) ?>
      <?= $form->field($model, 'prodi')->dropDownList(
            ArrayHelper::map(common\models\Prodi::find()->all(),'id_prodi','nama_prodi'),
            ['prompt'=>'Pilih Prodi']) ?>
   
     <?= $form->field($model, 'angkatan')->dropDownList(
            ArrayHelper::map(common\models\Angkatan::find()->all(),'angkatan','angkatan'),
            ['prompt'=>'Pilih Angkatan']) ?>
   
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
