<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $searchModel common\models\MahasiswaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Mahasiswa';
$this->params['breadcrumbs'][] = $this->title;
$tombol='{pdf}{upload}{view}{update}{delete}';
?>

<div class="row">
    <div class="col-lg-3 col-md-3 admin-menu-right-line">
        <div class="box-header with-border">
            <h3 class="box-title">Menu</h3>
        </div>
        <div class="box-body no-padding">
            <ul class="nav nav-pills nav-stacked">
                <li class="active" style="background-color: #f4f4f4"><a href="<?= Url::toRoute(['index']) ?>"><i class="fa fa-list-alt"></i> Data <?= Html::encode($this->title) ?> </a></li>
                <li ><a href="<?=  Url::toRoute(['create']) ?>"><i class="fa fa-pencil-square-o"></i> Input Data <?= Html::encode($this->title) ?></a></li>
                             <li ><a href="<?=  Url::toRoute(['import']) ?>"><i class="fa fa-download"></i> Import Data <?= Html::encode($this->title) ?></a></li>
            </ul>
        </div>
    </div>
    <div class="col-lg-9 col-md-9">
        <div class="mahasiswa-index">

            <h1><?= Html::encode($this->title) ?></h1>
                                <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
               <?php $form = ActiveForm::begin([]);?>

                    <?= $form->field($model, 'nama')->textInput(['style'=>'width:300px', 'placeholder' => 'Ketikan Nama / NIM Mahasiswa']) ?>
                    
                  
                    
                    <?= Html::submitButton('Search',['class' => 'btn btn-primary']); ?>
                       <?= Html::a('<i class=""></i>Reset', ['mahasiswa/'], ['class' => 'btn btn-default']) ?>
                    <?php ActiveForm::end(); ?>
            <div class="table-responsive">
                                                    <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                 //   'filterModel' => $searchModel,
        'columns' => [
                    [
                    'class' => 'yii\grid\SerialColumn',
                    'header' => 'No.'
                    ],

                      //          'id_mahasiswa',
            'nim',
            'nama',
            'jurusan',
           [
                            'attribute'=> 'prodi',
                  
                            'value' => 'idProdi.nama_prodi',
                        ],
//                           
            'angkatan',
 [   
                'attribute' => 'foto',
                'format' => 'html',
                'label' =>'Foto',
                'value'=>  function($data) { 
                return Html::img(\Yii::getAlias('@web/../uploads/mahasiswa/thumb-').$data->foto,['width'=>'100','height'=>'100']); },
            ],
                    [
                    'class' => 'yii\grid\ActionColumn','header'=>'Action',
                    'template' => $tombol,
                    'buttons' => [
                        'pdf' => function ($url, $model, $key){
                           
                             return  Html::a('<i class=""></i>',['pdf', 'id' => $model->id_mahasiswa], ['class' => 'glyphicon glyphicon-print',  'target' => '_blank',]); 
                        }, 
                          'upload' => function ($url, $model, $key){
                           
                             return  Html::a('<i class=""></i>',['upload', 'id' => $model->id_mahasiswa], ['class' => 'glyphicon  glyphicon-picture']); 
                        },
                                
                        'view' => function ($url, $model, $key){
                           
                             return  Html::a('<i class=""></i>',['view', 'id' => $model->id_mahasiswa], ['class' => 'glyphicon glyphicon-eye-open']); 
                        },
                        'update' => function ($url, $model, $key){
               
                            return  Html::a('<i class=""></i>',['update', 'id' => $model->id_mahasiswa], ['class' => 'glyphicon glyphicon-pencil']); 
                        },
                        'delete' => function ($url, $model, $key){
                         
                            return Html::a('<i class="glyphicon glyphicon-trash"></i> <?= Html::encode($modelName) ?>', ['delete', 'id' => $model->id_mahasiswa], [
                        'data' => [
                            'confirm' => 'Hapus?',
                            'method' => 'post',
                        ],
                    ]); 
                        
                            
                            
                            },

                    ]
                ],
                    ],
                    ]); ?>
                                
            </div>
        </div>

    </div>
</div>