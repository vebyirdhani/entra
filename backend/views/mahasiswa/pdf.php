<?php

use Da\QrCode\QrCode;
use yii\widgets\DetailView;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$qrCode = (new QrCode($liat->nim))
        ->setSize(250)
        ->setMargin(5)
        ->useForegroundColor(1, 1, 1);


$this->title = 'Registration Card';
?>
<style>  
    body { 
        display: none;
    }
    .bg {
        background: url(<?= yii\helpers\Url::toRoute(['/uploads/svgdepan.svg']) ?>) top left no-repeat;
        width:100%;
        height:100%;
        position: fixed;
    }
    .bg.print {
        display: list-item;
       
            list-style-image: url(<?= yii\helpers\Url::toRoute(['/uploads/svgdepan.svg']) ?>);
       
        list-style-position: inside;
    }

    .bgback {
        background: url(<?= yii\helpers\Url::toRoute(['/uploads/svgbelakang.svg']) ?>) top left no-repeat;
        width:100%;
        height: 100%;
        position: fixed;
    }
    .bgback.printback {
        display: list-item;
        background: url(<?= yii\helpers\Url::toRoute(['/uploads/svgbelakang.svg']) ?>);
        list-style-image: url(<?= yii\helpers\Url::toRoute(['/uploads/svgbelakang.svg']) ?>);
        list-style-position: inside;
    }

    .centered {
        position: fixed;
        top: 49%;
        left: 13%;
        transform: translate(-50%, 0%);
        height: 157px;
        width: 132px;
        border: 1px solid #660099;
    }
    .containerr {
        width : 100%;
        height : 100%; 
        /*background: url(<?= yii\helpers\Url::toRoute(['/uploads/fkm-absen.jpg']) ?>);*/
    }
    .nama {
        /*color : #4f4f4f;*/
        color : #660099;
        font-family: Arial, "Helvetica Neue", Helvetica, sans-serif;
        position: fixed;
        top: 73%;
        left: 13%;
        transform: translate(-50%, 0%);
        /*background-color : white;*/
        width:261px;
        text-align: center;
        margin-top : 3px;
        font-weight : 600;
        font-size : 20px;
    }
    @media print {
        .page-break { display: block; page-break-before: always; }

        @page {
            display : block;
            size: 380px 280px ;
            margin: 0px;
        }
        body {
            display : block;
            margin: 0px; 
        }

        .centered {
            display : block;
            position: absolute;
            top: 52%;
            left: 27%;
            transform: translate(-50%, 0%);
            height: 182px;
            width: 136px;
            border: 2px solid #660099;
        }
        .centeredback {
            display : block;
            position: absolute;
            top: 23.6%;
            left: 73.5%;
            transform: translate(-50%, 0%);
            height: 111px;
            width: 111px
            /*border: 2px solid #660099;*/
        }
        .nama {
            display : block;
            color : #8206b6;
            font-family: Arial, "Helvetica Neue", Helvetica, sans-serif;
            position: absolute;
            top: 23%;
            left: 26%;
            transform: translate(-50%, 0%);
            width:45%;
            text-align: left;
            margin-top : 3px;
            font-weight : 600;
            font-size : 16px;
            z-index:999;
            text-transform:capitalize;
        }
          .nim {
            display : block;
            color : #4f4f4f;
            font-family: Arial, "Helvetica Neue", Helvetica, sans-serif;
            position: absolute;
            top: 37%;
            left: 53.4%;
            transform: translate(-50%, 0%);
            width:100%;
            text-align: left;
            margin-top : 3px;
            font-weight : 600;
            font-size : 12px;
            z-index:999;
        }
          .prodi {
            display : block;
            color : #4f4f4f;
            font-family: Arial, "Helvetica Neue", Helvetica, sans-serif;
            position: absolute;
            top: 41%;
            left: 53.4%;
            transform: translate(-50%, 0%);
            width:100%;
            text-align: left;
            margin-top : 3px;
            font-weight : 600;
            font-size : 12px;
            z-index:999;
        }
        .status {
            display : block;
            color : #4ec09f;
            font-family: Arial, "Helvetica Neue", Helvetica, sans-serif;
            position: absolute;
            top: 95%;
            left: 50%;
            transform: translate(-50%, 0%);
            background-color : white;
            width:181px;
            text-align: center;
            margin-top : 3px;
            font-weight : 600;
            font-size : 17px;
        }
        
        .ground {
            display : block;
            height: 140px;
            width: 140px;
        }   
    }
</style>
<script type="text/javascript">
//    window.print();
</script>
  <body onload="window.print()">
<div class="containerr">
  
    <div class="bg print">
        <center>
            Jika gambar latar tidak keluar harap periksa jaringan anda dan refresh halaman
        </center>
    </div>
   
    <div class="centeredback">
         
        <?php echo '<img src="' . $qrCode->writeDataUri() . '" width=115>'; ?>
    </div>
    <div class="nama">
        <span>
     <?= $liat->nama?>
        </span>
    </div> 
    <div class="nim">
        <span><?php echo $liat->nim?></span>
    </div>
    <div class="prodi">
        <span><?php $cariprodi= \common\models\Prodi::find()->where(['id_prodi'=>$liat->prodi])->one(); if ($cariprodi){ echo $cariprodi->nama_prodi;}?></span>
    </div>
</div>
<div class="page-break">
    <div class="bgback printback"></div>
    <div class="ground">
        
    </div>
</div>
</body>