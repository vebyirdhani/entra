<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Semester */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="semester-form">

    <?php $form = ActiveForm::begin(); ?>
  <div class="col-lg-3">
    <?= $form->field($model, 'semester')->dropDownList(
            ['prompt'=>'Pilih Semester','Ganjil'=>'Ganjil','Genap'=>'Genap']) ?>
			</div>
			  <div class="col-lg-3">
     <?= $form->field($model, 'tahun')->textInput(['maxlength' => true]) ?>
	 </div>
	 
    <div class="col-lg-12">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
