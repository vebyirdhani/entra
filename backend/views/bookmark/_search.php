<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\BookmarkSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="bookmark-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id_bookmark') ?>

    <?= $form->field($model, 'kategori') ?>

    <?= $form->field($model, 'id_data') ?>

    <?= $form->field($model, 'id_desa') ?>

    <?= $form->field($model, 'nama') ?>

    <?php // echo $form->field($model, 'harga') ?>

    <?php // echo $form->field($model, 'foto') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
