<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\RencanaKegiatan */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="rencana-kegiatan-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id_plan')->textInput() ?>

    <?= $form->field($model, 'id_desa')->textInput() ?>

    <?= $form->field($model, 'kategori_pemesanan')->textInput() ?>

    <?= $form->field($model, 'id_pemesanan')->textInput() ?>

    <?= $form->field($model, 'jumlah_orang')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
