<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\RencanaKegiatanSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="rencana-kegiatan-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id_rencanakegiatan') ?>

    <?= $form->field($model, 'id_plan') ?>

    <?= $form->field($model, 'id_desa') ?>

    <?= $form->field($model, 'kategori_pemesanan') ?>

    <?= $form->field($model, 'id_pemesanan') ?>

    <?php // echo $form->field($model, 'jumlah_orang') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
