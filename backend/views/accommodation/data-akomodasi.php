<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\AccommodationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Accommodation';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <div class="col-lg-3 col-md-3 admin-menu-right-line">
        <div class="box-header with-border">
            <h3 class="box-title">Menu</h3>
        </div>
        <div class="box-body no-padding">
            <ul class="nav nav-pills nav-stacked">
                <li class="active" style="background-color: #f4f4f4"><a href="<?= Url::toRoute(['index']) ?>"><i class="fa fa-list-alt"></i> Data <?= Html::encode($this->title) ?> </a></li>
                <li ><a href="<?=  Url::toRoute(['create']) ?>"><i class="fa fa-pencil-square-o"></i> Input Data <?= Html::encode($this->title) ?></a></li>
            </ul>
        </div>
    </div>
    <div class="col-lg-9 col-md-9">
        <div class="accommodation-index">

            <h1><?= Html::encode($this->title) ?></h1>
                                <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
            
            <div class="table-responsive">
                                                    <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
        'columns' => [
                    [
                    'class' => 'yii\grid\SerialColumn',
                    'header' => 'No.'
                    ],

                              //  'id_homestay',
            'id_kategori',
            'nama_homestay',
            'harga',
         //   'id_user',
             [
                                    'attribute' => 'id_desa',
                                    'content' => function($row) {
                                                        if ($row->Desa){
                                                        return $row->Desa->name;}
                                    },
                                ],
 [
                                    'attribute' => 'id_provinsi',
                                    'content' => function($row) {
                                                        if ($row->Provinsii){
                                                        return $row->Provinsii->name;}
                                    },
                                ],
                    ['class' => 'yii\grid\ActionColumn'],
                    ],
                    ]); ?>
                                
            </div>
        </div>

    </div>
</div>