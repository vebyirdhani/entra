<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model common\models\Accommodation */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="accommodation-form">

    <?php $form = ActiveForm::begin(); ?>

  
   <?= $form->field($model, 'id_kategori')->dropDownList(
            ArrayHelper::map(common\models\KategoriAccommodation::find()->all(),'id_kategori','nama_kategori'),
            ['prompt'=>'Kategori']) ?>
    <?= $form->field($model, 'nama_homestay')->textInput(['maxlength' => true]) ?>

   

    <?= $form->field($model, 'rating')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'harga')->textInput(['maxlength' => true]) ?>
 <?= $form->field($model, 'picture[]')->fileInput(['multiple'=>TRUE])->label('Upload Foto ') .'*Maksimal ukuran foto 2MB' ?>
<div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
