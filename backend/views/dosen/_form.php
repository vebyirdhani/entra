<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $model common\models\Dosen */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="dosen-form">

    <?php $form = ActiveForm::begin(); ?>
     <div class="col-lg-4">
    <?= $form->field($model, 'nidn')->textInput(['maxlength' => true,'required' => 'required',]) ?>
    </div>
 <div class="col-lg-4">
     
    <?= $form->field($model, 'nama')->textInput(['maxlength' => true]) ?>
 </div>
   
    <div class="col-lg-4">
    <?= $form->field($model, 'nip')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-lg-4">
    <?= $form->field($model, 'agama')->dropDownList(
            ArrayHelper::map(common\models\Agama::find()->all(),'agama','agama'),
            ['prompt'=>'Pilih Agama']) ?>
    </div> 
   
    <div class="col-lg-4">
    <?= $form->field($model, 'tempat_lahir')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-lg-4">
   <?=
    $form->field($model, 'tanggal_lahir')->widget(
            DatePicker::className(), [
            'name' => 'tgl_lahir',
            'type' => DatePicker::TYPE_INPUT,
            'pluginOptions' => [
            'autoclose' => true,
            'format' => 'dd-M-yyyy',
        ],
    ])
    ?>
    </div>
    <div class="col-lg-4">
   <?= $form->field($model, 'jabatan')->dropDownList(
            ArrayHelper::map(common\models\Jabatan::find()->all(),'jabatan','jabatan'),
            ['prompt'=>'Pilih Jabatan']) ?>
    </div>
    
    <div class="col-lg-4">
   <?= $form->field($model, 'status_nikah')->dropDownList(
            ['prompt'=>'Pilih Status','Menikah'=>'Menikah','Belum Menikah'=>'Belum Menikah']) ?>
        
    </div>
	<div class="col-lg-4">
     
    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
 </div>
    <div class="col-lg-4">
    <?= $form->field($model, 'jenis_kelamin')->radioList(array('L'=>'Laki-laki','P'=>'Perempuan')); ?>
    </div>
    
         <div class="col-lg-12">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
