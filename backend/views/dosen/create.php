<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\Dosen */

$modelName = 'Dosen';
$this->title = 'Input Dosen';
$this->params['breadcrumbs'][] = ['label' => 'Dosen', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <div class="col-lg-3 col-md-3 admin-menu-right-line">
        <div class="box-header with-border">
            <h3 class="box-title">Menu</h3>
        </div>
        <div class="box-body no-padding">
            <ul class="nav nav-pills nav-stacked">
                <li><a href="<?= Url::toRoute(['index']) ?>"><i class="fa fa-list-alt"></i> Data <?= Html::encode($modelName) ?> </a></li>
                <li class="active" style="background-color: #f4f4f4"><a href="<?=  Url::toRoute(['create']) ?>"><i class="fa fa-pencil-square-o"></i> Input Data <?= Html::encode($modelName) ?></a></li>
            </ul>
        </div>
    </div>
    <div class="col-lg-9 col-md-9">

        <div class="admin-create">

            <div class="dosen-create">

                <h1><?= Html::encode($this->title) ?></h1>

                <?= $this->render('_form', [
                'model' => $model,
                ]) ?>

            </div>


        </div>
         <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    
        'columns' => [
                    [
                    'class' => 'yii\grid\SerialColumn',
                    'header' => 'No.'
                    ],

                  'nidn',              
            'nama',
         
           
            'jabatan',
             [
                    'attribute' => 'jenis_kelamin',
                    'content' => function($row) {
                        return $row->jenis_kelamin == 'p' ? 'Perempuan' : 'Laki-Laki';
                    },
                ],
                
           
                    ],
                    ]); ?>
                                
    </div>
</div>