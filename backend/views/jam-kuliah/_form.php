<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\JamKuliah */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="jam-kuliah-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'jam_kuliah')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
