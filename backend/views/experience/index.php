<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\VillagesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Villages';
$this->params['breadcrumbs'][] = $this->title;
$tombol='{update}';
?>

<div class="row">
    <div class="col-lg-3 col-md-3 admin-menu-right-line">
        <div class="box-header with-border">
            <h3 class="box-title">Menu</h3>
        </div>
        <div class="box-body no-padding">
            <ul class="nav nav-pills nav-stacked">
                <li class="active" style="background-color: #f4f4f4"><a href="#"><i class="fa fa-list-alt"></i> Data <?= Html::encode($this->title) ?> </a></li>
                   <li><a href="<?= Url::toRoute(['data-experience']) ?>"><i class="fa fa-list-alt"></i> Data Experience </a></li>
                </ul>
        </div>
    </div>
    <div class="col-lg-9 col-md-9">
        <div class="villages-index">

            <h1><?= Html::encode($this->title) ?></h1>
                                <?php  echo $this->render('_searchdesa', ['model' => $searchModel]); ?>
            
            <div class="table-responsive">
                                                    <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                 
        'columns' => [
                    [
                    'class' => 'yii\grid\SerialColumn',
                    'header' => 'No.'
                    ],

                            
            // 'district_id',
            
              [
                                    'attribute' => 'district_id',
                                    'content' => function($row) {
                                        return $row->Kecamatann->name;
                                    },
                                ],
                                          
            'name',
         

                        [
                    'class' => 'yii\grid\ActionColumn','header'=>'Action',
                    'template' => $tombol,
                    'buttons' => [
                       
                        'update' => function ($url, $model, $key){
                         
                            return  Html::a('<i class=""></i>',['create', 'id' => $model->id], ['class' => 'glyphicon glyphicon-pencil']); }
                        ,
                       

                    ]
                ],
                    ],
                    ]); ?>
                                
            </div>
        </div>

    </div>
</div>