<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use dosamigos\datepicker\DateRangePicker;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\KunjunganSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Data '.$project->nama_project;
$this->params['breadcrumbs'][] = $this->title;
$total = \common\models\DataPelanggan::find()->where(['project'=>$project->id_project])->andWhere(['status'=>'Sukses'])->sum('harga');
?>
<div class="kunjungan-index">


    <div class="section services-section">
        <div class="container">
     <div class="row">
         
         
          <div class="col-12 col-sm-6 col-md-4">
            <div class="info-box">
              <span class="info-box-icon bg-info elevation-1"><i class="fa fa-money"></i></span>

              <div class="info-box-content">
               
                  <span class="info-box-number"><BR>
                  <span class="info-box-text">LAPORAN PENDAPATAN</span>
                </span>
                  
                   <a href="<?= Url::toRoute(['report/pendapatan/','id'=>$project->id_project]) ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->
          <div class="col-12 col-sm-6 col-md-4">
            <div class="info-box mb-3">
              <span class="info-box-icon bg-danger elevation-1"><i class="fa fa-shopping-cart"></i></span>

               <div class="info-box-content">
               
                  <span class="info-box-number"><BR>
                  <span class="info-box-text">LAPORAN PENGELUARAN</span>
                </span>
                    <a href="<?= Url::toRoute(['report/pengeluaran/','id'=>$project->id_project]) ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->

          <!-- fix for small devices only -->
      

          <div class="col-12 col-sm-6 col-md-4">
            <div class="info-box mb-3">
              <span class="info-box-icon bg-success elevation-1"><i class="fa fa-bar-chart"></i></span>

                <div class="info-box-content">
               
                  <span class="info-box-number"><BR>
                  <span class="info-box-text">LAPORAN LABA RUGI</span>
                </span>
                     <a href="<?= Url::toRoute(['report/labarugi/','id'=>$project->id_project]) ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->
         
     </div><br><br>
            <div class="row">
                 <div class="col-12 col-sm-6 col-md-4">
            <div class="info-box">
              <span class="info-box-icon bg-info elevation-1"><i class="fa fa-pencil"></i></span>

              <div class="info-box-content">
               
                  <span class="info-box-number"><BR>
                  <span class="info-box-text">DATA PELANGGAN</span>
                </span>
                  
                   <a href="<?= Url::toRoute(['data-pelanggan/index/','id'=>$project->id_project]) ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <div class="col-12 col-sm-6 col-md-4">
            <div class="info-box">
              <span class="info-box-icon bg-info elevation-1"><i class="fa fa-pencil"></i></span>

              <div class="info-box-content">
               
                  <span class="info-box-number"><BR>
                  <span class="info-box-text">DATA PENGELUARAN</span>
                </span>
                  
                   <a href="<?= Url::toRoute(['data-pengeluaran/menu/','id'=>$project->id_project]) ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
                
                
            </div>
        <!-- /.row
        </div>
    </div> <!-- .se
</div>
         <div class="row">
      <div class="col-lg-3 col-6">
    <!-- small box -->


</div>
