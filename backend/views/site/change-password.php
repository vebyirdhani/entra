<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

$this->title = 'Ganti Password';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <div class="col-md-3">

        <!-- Profile Image -->
        <div class="box box-primary">
            <div class="box-body box-profile">
                <img class="profile-user-img img-responsive" src="<?= Url::toRoute(['site/foto-profil', 'inline' => true]) ?>" alt="User profile picture">

                <h3 class="profile-username text-center"></h3>

                <p class="text-muted text-center"></p>

                <ul class="list-group list-group-unbordered">
                    <li class="list-group-item text-center">
                        <b><?= $model->name ?></b>
                    </li>
                    <li class="list-group-item text-center">
                        <b><?= $user->email ?></b>
                    </li>
                </ul>
            </div>
            <!-- /.box-body -->
        </div>
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">About Me</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">

                <div class="row">
                    <div class="col-lg-6">
                        <i class="fa fa-map-marker margin-r-5"></i> Tanggal Lahir
                    </div>
                    <div class="col-lg-6">
                        <?= $model->born_date ?> 
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6">
                        <i class="fa fa-map-marker margin-r-5"></i> Alamat
                    </div>
                    <div class="col-lg-6">
                        <?= $model->address ?> 
                    </div>
                </div>
            </div>
            <!-- /.box-body -->
        </div>
    </div>
    <div class="col-md-9">
        <div class="site-index">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li><?=
                        Html::a('Profil', Url::toRoute(['site/profil']), [
                            'class' => 'btn btn-transparent'
                        ])
                        ?></li>
                    <li class="active"><a href="#set" data-toggle="tab">Ubah Password</a></li>
                </ul>
                <div class="tab-content">

                    <!--/.tab-pane -->
                    <div class="active tab-pane" id="set">

                        <div class="user-form">
                            <div class="box-body">
                                <div class="col-lg-4 admin-menu-left-line">
                                    <?php $form = ActiveForm::begin(); ?>

                                    <?= $form->field($user, 'currentPassword')->passwordInput(['placeholder' => 'Masukkan Password Lama']) ?>
                                    <?= $form->field($user, 'newPassword')->passwordInput(['placeholder' => 'Masukkan Password Baru']) ?>
                                    <?= $form->field($user, 'newPasswordConfirm')->passwordInput(['placeholder' => 'Ketik Ulang Password Baru']) ?>


                                    <div class="form-group">
                                        <?= Html::submitButton($user->isNewRecord ? 'Tambah' : 'Ganti Password', ['class' => $user->isNewRecord ? 'btn btn-success' : 'btn btn-success']) ?>
                                        <a href="<?php echo Url::toRoute('/site'); ?>" class="btn btn-primary"> Cancel</a>
                                    </div>

                                    <?php ActiveForm::end(); ?>


                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--/.tab-content -->
            </div>
            <!--/.nav-tabs-custom -->
        </div>
    </div>
</div>