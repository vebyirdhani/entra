<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */

$this->title = $model->name;
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <div class="col-md-3">

        <!-- Profile Image -->
        <div class="box box-primary">
            <div class="box-body box-profile">
                <img class="profile-user-img img-responsive" src="<?= Url::toRoute(['site/foto-profil', 'inline' => true]) ?>" alt="User profile picture">

                <h3 class="profile-username text-center"></h3>

                <p class="text-muted text-center"></p>

                <ul class="list-group list-group-unbordered">
                    <li class="list-group-item text-center">
                        <b><?= $model->name?></b>
                    </li>
                    <li class="list-group-item text-center">
                        <b><?= $user->email ?></b>
                    </li>
                </ul>
            </div>
            <!-- /.box-body -->
        </div>
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">About Me</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">

                <div class="row">
                    <div class="col-lg-6">
                        <i class="fa fa-map-marker margin-r-5"></i> Tanggal Lahir
                    </div>
                    <div class="col-lg-6">
                        <?= $model->born_date ?> 
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6">
                        <i class="fa fa-map-marker margin-r-5"></i> Alamat
                    </div>
                    <div class="col-lg-6">
                        <?= $model->address ?> 
                    </div>
                </div>
            </div>
            <!-- /.box-body -->
        </div>
    </div>
    <div class="col-md-9">
        <div class="site-index">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#set" data-toggle="tab">Akun</a></li>
                    <li><?=
                        Html::a('Ubah Password', Url::toRoute(['site/change-password']), [
                            'class' => 'btn btn-transparent'
                        ])
                        ?></li>
                </ul>
                <div class="tab-content">

                    <!--/.tab-pane -->
                    <div class="active tab-pane" id="set">
                        <div class="kasi-form">

                            <div class="box-body">
                                <div class="col-lg-4 admin-menu-left-line">
                                    <?php
                                    $form = ActiveForm::begin([
                                                'fieldConfig' => [
//                                'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
                                                    'options' => [
                                                        'label' => 'col-sm-4',
                                                        'offset' => 'col-sm-offset-4',
                                                        'wrapper' => 'col-sm-8',
                                                        'error' => '',
                                                        'hint' => '',
                                                    ],
                                                ],
                                    ]);
                                    ?>

                                    <div class="form-group">
                                        <?= $form->field($model, 'name')->textInput(['maxlength' => true, 'class' => 'form-control', 'placeholder' => 'Nama Bagian']) ?>
                                    </div>
                                    <div class="form-group">
                                        <?= $form->field($user, 'username')->textInput(['class' => 'form-control', 'placeholder' => 'Username']) ?>
                                    </div>
                                    
                                      <div class="form-group">
                                        <?= $form->field($model, 'no_ktp')->textInput(['class' => 'form-control', 'placeholder' => 'No KTP']) ?>
                                    </div>
                                    <div class="form-group">
                                        <?= $form->field($user, 'email')->textInput(['class' => 'form-control', 'placeholder' => 'Email']) ?>
                                    </div>

                                    <div class="form-group">
                                        <?=
                                        $form->field($model, 'born_date')->widget(
                                                DatePicker::className(), [
                                            'name' => 'born_date',
                                            'type' => DatePicker::TYPE_INPUT,
                                            'pluginOptions' => [
                                                'autoclose' => true,
                                                'format' => 'dd-M-yyyy',
                                            ],
                                        ])
                                        ?>
                                    </div>
                                    
                                     <div class="form-group">
                                        <?= $form->field($model, 'kelamin')->radioList(array('L'=>'Laki-laki','P'=>'Perempuan')) ?>
                                    </div>
                                    <div class="form-group">
                                        <?= $form->field($model, 'address')->textInput(['maxlength' => true, 'class' => 'form-control', 'placeholder' => 'Alamat']) ?>
                                    </div>
                                     <div class="form-group">
                                        <?= $form->field($model, 'no_hp')->textInput(['class' => 'form-control', 'placeholder' => 'No HP']) ?>
                                    </div>
                                    <div class="form-group">
                                        <?= $form->field($model, 'photo')->fileInput(['accept' => 'image/*']) ?>
                                    </div>
                                    <div class="form-group">
                                        <?= $form->field($model, 'project')->dropDownList(
            ArrayHelper::map(common\models\DaftarProject::find()->all(),'id_project','nama_project'),
            ['prompt'=>'Pilih Project']) ?>
                                    </div>
                                     
                                    <div class="form-group">
                                        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                                    </div>



                                    <?php ActiveForm::end(); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--/.tab-content -->
            </div>
            <!--/.nav-tabs-custom -->
        </div>
    </div>
</div>