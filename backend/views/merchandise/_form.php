<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $model common\models\Merchandise */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="merchandise-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nama')->textInput(['maxlength' => true]) ?>

  <?= $form->field($model, 'kategori')->dropDownList(
            ArrayHelper::map(common\models\KategoriMerchandise::find()->all(),'id_kategori','nama'),
            ['prompt'=>'Kategori']) ?>
    <?= $form->field($model, 'deskripsi')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'alamat')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'harga')->textInput(['maxlength' => true]) ?>


    <?= $form->field($model, 'picture[]')->fileInput(['multiple'=>TRUE])->label('Upload Foto ') .'*Maksimal ukuran foto 2MB' ?>
    <?= $form->field($model, 'rating')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
