<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\TransportationSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="transportation-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id_transportation') ?>

    <?= $form->field($model, 'nama') ?>

    <?= $form->field($model, 'rute') ?>

    <?= $form->field($model, 'jenis_transportasi') ?>

    <?= $form->field($model, 'price') ?>

    <?php // echo $form->field($model, 'id_user') ?>

    <?php // echo $form->field($model, 'id_desa') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
