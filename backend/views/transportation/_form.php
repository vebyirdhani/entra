<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\tinymce\TinyMce;
use kartik\money\MaskMoney;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
/* @var $this yii\web\View */
/* @var $model common\models\Transportation */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="transportation-form">

    <?php $form = ActiveForm::begin(); ?>
  
     <?= $form->field($model, 'jenis_transportasi')->dropDownList(
            ArrayHelper::map(common\models\KategoriTransportasi::find()->all(),'nama','nama'),
            ['prompt'=>'Pilih Jenis Transportasi']) ?>

    <?= $form->field($model, 'nama')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'rute')->textarea(['rows' => 3]) ?>
   <?= $form->field($model, 'deskripsi')->textarea(['rows' => 6]) ?>

   
     <?=
                            $form->field($model, 'price')
                         
                        ->widget(MaskMoney::classname(), [
                        'pluginOptions' => [
                            'prefix' => 'Rp. ',
                            'suffix' => '',
                            'affixesStay' => TRUE,
                            'precision' => 0,
                            'allowZero' => false,
                            'allowNegative' => false,
                        ],
                    ]);
                    ?> 
     <?= $form->field($model, 'picture[]')->fileInput(['multiple'=>TRUE])->label('Upload Foto ') .'*Maksimal ukuran foto 2MB' ?>
    <?php
                    // $form->field($model, 'id_desa', [
                       
                    // ])->widget(Select2::className(), [
                    //     'data' => ArrayHelper::map(common\models\Villages::find()->all(), 'id', 'name'),
                    //     'language' => 'en',
                    //     'options' => [
                    //         'placeholder' => 'Pilih Desa',
                           
                    //     ],
                    //     'pluginOptions' => [
                    //         'allowClear' => true
                    //     ]
                    // ])
                    ?> 

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
