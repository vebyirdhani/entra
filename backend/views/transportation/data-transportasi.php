<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\TransportationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Transportation';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <div class="col-lg-3 col-md-3 admin-menu-right-line">
        <div class="box-header with-border">
            <h3 class="box-title">Menu</h3>
        </div>
        <div class="box-body no-padding">
            <ul class="nav nav-pills nav-stacked">
                <li class="active" style="background-color: #f4f4f4"><a href="<?= Url::toRoute(['index']) ?>"><i class="fa fa-list-alt"></i> Data Transportasi </a></li>
                <li ><a href="<?=  Url::toRoute(['index']) ?>"><i class="fa fa-pencil-square-o"></i> Data Desa</a></li>
            </ul>
        </div>
    </div>
    <div class="col-lg-9 col-md-9">
        <div class="transportation-index">

            <h1><?= Html::encode($this->title) ?></h1>
                                <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
            
            <div class="table-responsive">
                                                    <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
        'columns' => [
                    [
                    'class' => 'yii\grid\SerialColumn',
                    'header' => 'No.'
                    ],

                                //'id_transportation',
                    'id_desa',
            'nama',
            'rute:ntext',
            'jenis_transportasi',
            'price',
              [   
                'attribute' => 'foto',
                'format' => 'html',
                'label' =>'File',
                'value'=>  function($data) { 
                return Html::img(\Yii::getAlias('@web/../uploads/transport/').$data->foto,['width'=>'100','height'=>'100']); },
            ],
            // 'id_user',
            // 'id_desa',

                    ['class' => 'yii\grid\ActionColumn'],
                    ],
                    ]); ?>
                                
            </div>
        </div>

    </div>
</div>