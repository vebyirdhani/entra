<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "cara_didapat".
 *
 * @property string $cara_didapat_id
 * @property string $nama
 * @property integer $hibah
 * @property integer $beli
 * @property integer $order
 * @property integer $default
 * @property integer $aktif
 */
class CaraDidapat extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cara_didapat';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cara_didapat_id'], 'required'],
            [['hibah', 'beli', 'order', 'default', 'aktif'], 'integer'],
            [['cara_didapat_id'], 'string', 'max' => 20],
            [['nama'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'cara_didapat_id' => 'Cara Didapat ID',
            'nama' => 'Nama',
            'hibah' => 'Hibah',
            'beli' => 'Beli',
            'order' => 'Order',
            'default' => 'Default',
            'aktif' => 'Aktif',
        ];
    }
}
