<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "provinces".
 *
 * @property string $id
 * @property string $island_id
 * @property string $name
 */
class Provinces extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'provinces';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'island_id', 'name'], 'required'],
            [['id'], 'string', 'max' => 2],
            [['island_id'], 'string', 'max' => 7],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'island_id' => 'Island ID',
            'name' => 'Name',
        ];
    }
}
