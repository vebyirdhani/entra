<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Bookmark;

/**
 * BookmarkSearch represents the model behind the search form about `\common\models\Bookmark`.
 */
class BookmarkSearch extends Bookmark
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_bookmark', 'id_data'], 'integer'],
            [['kategori', 'id_desa', 'nama', 'harga', 'foto'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Bookmark::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_bookmark' => $this->id_bookmark,
            'id_data' => $this->id_data,
        ]);

        $query->andFilterWhere(['like', 'kategori', $this->kategori])
            ->andFilterWhere(['like', 'id_desa', $this->id_desa])
            ->andFilterWhere(['like', 'nama', $this->nama])
            ->andFilterWhere(['like', 'harga', $this->harga])
            ->andFilterWhere(['like', 'foto', $this->foto]);

        return $dataProvider;
    }
}
