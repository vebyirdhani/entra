<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "sub_kategori_experience".
 *
 * @property integer $id_sub
 * @property string $nama
 * @property integer $id_kategori
 */
class SubKategoriExperience extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sub_kategori_experience';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nama', 'id_kategori'], 'required'],
            [['id_kategori'], 'integer'],
            [['nama'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_sub' => 'Id Sub',
            'nama' => 'Nama',
            'id_kategori' => 'Id Kategori',
        ];
    }
}
