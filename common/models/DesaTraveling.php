<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "desa_traveling".
 *
 * @property integer $id
 * @property string $id_desa
 * @property string $id_kecamatan
 * @property integer $id_provinsi
 * @property integer $id_pulau
 * @property string $rating
 * @property string $harga
 * @property string $nama
 * @property string $deskripsi
 * @property string $foto
 * @property string $alamat
 */
class DesaTraveling extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'desa_traveling';
    }

    /**
     * @inheritdoc
     */
    public $picture;
    public function rules()
    {
        return [
            [['id_provinsi', 'id_pulau'], 'integer'],
            [['deskripsi', 'alamat'], 'string'],
            [['id_desa', 'id_kecamatan', 'rating', 'nama', 'foto','fotolist','coordinat'], 'string', 'max' => 255],
          [['picture'],'file','maxFiles'=>5,'maxSize'=>2048*1024],
            ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_desa' => 'Id Desa',
            'id_kecamatan' => 'Id Kecamatan',
            'id_provinsi' => 'Id Provinsi',
            'id_pulau' => 'Id Pulau',
            'rating' => 'Rating',
            'harga' => 'Harga',
            'nama' => 'Nama',
            'deskripsi' => 'Deskripsi',
            'foto' => 'Foto',
            'alamat' => 'Alamat',
        ];
    }
}
