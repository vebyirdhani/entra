<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "fitur".
 *
 * @property integer $id_fitur
 * @property string $judul
 * @property string $deskripsi
 * @property string $icon
 */
class Fitur extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'fitur';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['judul', 'icon'], 'required'],
            [['judul', 'deskripsi', 'icon'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_fitur' => 'Id Fitur',
            'judul' => 'Judul',
            'deskripsi' => 'Deskripsi',
            'icon' => 'Icon',
        ];
    }
}
