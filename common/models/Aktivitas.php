<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "aktivitas".
 *
 * @property integer $id_aktivitas
 * @property integer $id_experience
 * @property string $deskripsi
 */
class Aktivitas extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'aktivitas';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_experience', 'deskripsi'], 'required'],
            [['id_experience'], 'integer'],
            [['deskripsi'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_aktivitas' => 'Id Aktivitas',
            'id_experience' => 'Id Experience',
            'deskripsi' => 'Deskripsi',
        ];
    }
}
