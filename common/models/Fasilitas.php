<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "fasilitas".
 *
 * @property integer $id_fasilitas
 * @property integer $id_akomodasi
 * @property string $nama
 */
class Fasilitas extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'fasilitas';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_akomodasi', 'nama'], 'required'],
            [['id_akomodasi'], 'integer'],
            [['nama'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_fasilitas' => 'Id Fasilitas',
            'id_akomodasi' => 'Id Akomodasi',
            'nama' => 'Nama',
        ];
    }
}
