<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "detail_order".
 *
 * @property integer $id_detail
 * @property string $id_order
 * @property string $id_user
 * @property string $id_data
 * @property string $kategori
 * @property integer $harga
 * @property string $jumlah_orang
 * @property string $tanggal_awal
 * @property string $tanggal_akhir
 */
class DetailOrder extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'detail_order';
    }

    /**
     * @inheritdoc
     */
	  public $picture;
    public function rules()
    {
        return [
            [['harga'], 'integer'],
            [['id_card','foto','id_order', 'id_user', 'id_data', 'kategori', 'jumlah_orang', 'tanggal_awal', 'tanggal_akhir','status','total_bayar','nama','alamat','tipe_pembayaran','id_plan','catatan'], 'string', 'max' => 255],
			 [['picture'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpeg,jpg','maxSize'=>2048*1024],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_detail' => 'Id Detail',
            'id_order' => 'Id Order',
            'id_user' => 'Id User',
            'id_plan'=>'Id Plan',
            'id_data' => 'Id Data',
            'kategori' => 'Kategori',
            'harga' => 'Harga',
            'jumlah_orang' => 'Jumlah Orang',
            'tanggal_awal' => 'Tanggal Awal',
            'tanggal_akhir' => 'Tanggal Akhir',
            'catatan' =>'Catatan',
        ];
    }
}
