<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "foto_experience".
 *
 * @property integer $id_foto
 * @property string $foto
 * @property integer $id_experience
 */
class FotoExperience extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'foto_experience';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_experience'], 'required'],
            [['id_experience'], 'integer'],
            [['foto'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_foto' => 'Id Foto',
            'foto' => 'Foto',
            'id_experience' => 'Id Experience',
        ];
    }
}
