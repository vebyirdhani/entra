<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "data_absen".
 *
 * @property integer $id_dataabsen
 * @property string $dosen
 * @property string $mahasiswa
 * @property string $keterangan
 * @property integer $id_absen
 */
class DataAbsen extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'data_absen';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['dosen', 'mahasiswa', 'id_absen'], 'required'],
            [['id_absen','hadir'], 'integer'],
            [['nama','nim','dosen','dosen2', 'mahasiswa', 'keterangan','semester','matkul','id_jadwal','periode','tanggal'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_dataabsen' => 'Id Dataabsen',
            'dosen' => 'Dosen 1',
            'dosen2'=>'Dosen 2',
            'mahasiswa' => 'Mahasiswa',
            'keterangan' => 'Keterangan',
            'id_absen' => 'Id Absen',
            'hadir'=>'Hadir',
            'semester'=>'Periode',
            'matkul'=>'Mata Kuliah',
            'id_jadwal'=>'Id jadwal',
            'periode'=>'Periode',
            'nim'=>'NIM',
            'nama'=>'Nama Mahasiswa',
            'tanggal'=>'Tanggal'
        ];
    }
}
