<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "rute".
 *
 * @property integer $id_rute
 * @property string $daerah_mulai
 * @property string $daerah_akhir
 * @property string $harga
 * @property integer $id_transport
 */
class Rute extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'rute';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_transport'], 'required'],
            [['id_transport'], 'integer'],
            [['daerah_mulai', 'daerah_akhir', 'harga'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_rute' => 'Id Rute',
            'daerah_mulai' => 'Daerah Mulai',
            'daerah_akhir' => 'Daerah Akhir',
            'harga' => 'Harga',
            'id_transport' => 'Id Transport',
        ];
    }
}
