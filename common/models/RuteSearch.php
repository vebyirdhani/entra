<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Rute;

/**
 * RuteSearch represents the model behind the search form about `common\models\Rute`.
 */
class RuteSearch extends Rute
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_rute', 'id_transport'], 'integer'],
            [['daerah_mulai', 'daerah_akhir', 'harga'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Rute::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_rute' => $this->id_rute,
            'id_transport' => $this->id_transport,
        ]);

        $query->andFilterWhere(['like', 'daerah_mulai', $this->daerah_mulai])
            ->andFilterWhere(['like', 'daerah_akhir', $this->daerah_akhir])
            ->andFilterWhere(['like', 'harga', $this->harga]);

        return $dataProvider;
    }
}
