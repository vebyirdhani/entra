<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "bookmark_desa".
 *
 * @property integer $id
 * @property string $id_desa
 * @property string $id_user
 * @property string $id_bookmark
 */
class BookmarkDesa extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bookmark_desa';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_desa', 'id_user', 'id_bookmark'], 'required'],
            [['id_desa', 'id_user', 'id_bookmark'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_desa' => 'Id Desa',
            'id_user' => 'Id User',
            'id_bookmark' => 'Id Bookmark',
        ];
    }
}
