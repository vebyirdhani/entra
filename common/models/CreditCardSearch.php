<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\CreditCard;

/**
 * CreditCardSearch represents the model behind the search form about `\common\models\CreditCard`.
 */
class CreditCardSearch extends CreditCard
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_credit', 'cvv', 'id_user'], 'integer'],
            [['name_card', 'card_number', 'expired_date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CreditCard::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_credit' => $this->id_credit,
            'cvv' => $this->cvv,
            'id_user' => $this->id_user,
        ]);

        $query->andFilterWhere(['like', 'name_card', $this->name_card])
            ->andFilterWhere(['like', 'card_number', $this->card_number])
            ->andFilterWhere(['like', 'expired_date', $this->expired_date]);

        return $dataProvider;
    }
}
