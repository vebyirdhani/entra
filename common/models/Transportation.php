<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "transportation".
 *
 * @property integer $id_transportation
 * @property string $nama
 * @property string $rute
 * @property string $jenis_transportasi
 * @property string $price
 * @property integer $id_user
 * @property integer $id_desa
 */
class Transportation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'transportation';
    }

    /**
     * @inheritdoc
     */
    public $picture;
    public function rules()
    {
        return [
            [['nama', 'price', 'id_user', 'id_desa'], 'required'],
            [['rute','deskripsi'], 'string'],
            [['id_user', 'id_desa','id_provinsi','id_pulau','price'], 'integer'],
            [['nama', 'rating','hari','alamat','jenis_transportasi','id_kecamatan','latitude','longitude','jenis_transportasi','contact'], 'string', 'max' => 255],
               [['picture'],'file','maxFiles'=>5,'maxSize'=>2048*1024],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_transportation' => 'Id Transportation',
            'nama' => 'Nama',
            'rute' => 'Rute',
            'jenis_transportasi' => 'Jenis Transportasi',
            'price' => 'Price',
            'id_user' => 'Id User',
            'id_desa' => 'Desa',
            'id_provinsi'=>'Provinsi',
            'id_kecamatan'=>'Kecamatan'
        ];
    }
    public function getDesa(){
        return Villages::find()
                ->where(['id' => $this->id_desa])
                ->one();
    }
}
