<?php
namespace common\models;

use Yii;
use yii\base\Model;

/**
 * Login form
 */
class LoginApi extends Model
{
    public $username;
    public $password;
    public $rememberMe = true;
    public $email;
    private $_user;
    private $_email;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['email', 'password'], 'required'],
            // rememberMe must be a boolean value
            ['rememberMe', 'boolean'],
            // password is validated by validatePassword()
            ['password', 'validatePassword'],
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getEmail();
            if (!$user ) {
                $this->addError('email', 'Incorrect email ');
            }else if (!$user->validatePassword($this->password)) {
                  $this->addError($attribute, 'Incorrect  password.');
            }
        }
    }

    /**
     * Logs in a user using the provided username and password.
     *
     * @return bool whether the user is logged in successfully
     */
   public function login()
    {
//        if ($this->validate()) {
//            return Yii::$app->user->login($this->getUser(), $this->rememberMe ? 3600 * 24 * 30 : 0);
//        } else {
//            return false;
//        }
         $user = $this->getEmail();
        if (!$user) {
            return [
                'status' => 'error',
                'message' => 'Username tidak terdaftar',
            ];
        } else {
          
                if ($user->validatePassword($this->password)) {
                   
                        $data = Yii::$app->user->login($this->getEmail(), $this->rememberMe ? 3600 * 24 * 30 : 0);
                        if ($data) {
                          //   Yii::$app->db->createCommand("UPDATE user SET aktif = 1 WHERE id = '$user->id' ")->execute();
                            return [
                                'status' => 'success',
                                'message' => 'Berhasil login',
                            ];
                        }
                  
                } else {
                    return [
                        'status' => 'error',
                        'message' => 'Password salah',
                    ];
                }
          
        }
    }

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    protected function getUser()
    {
        if ($this->_user === null) {
            $this->_user = User::findByUsername($this->username);
        }

        return $this->_user;
    }
     protected function getEmail()
    {
        if ($this->_email === null) {
            $this->_email = User::findByEmail($this->email);
        }

        return $this->_email;
    }
}
