<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\NilaiSeminar;

/**
 * NilaiSeminarSearch represents the model behind the search form about `\common\models\NilaiSeminar`.
 */
class NilaiSeminarSearch extends NilaiSeminar
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_nilai', 'id_dataseminar'], 'integer'],
            [['prodi','tanggal_seminar','judul', 'mahasiswa', 'dosen_pembimbing1', 'dosen_pembimbing2', 'penguji1', 'penguji2', 'penguji3', 'penguji4', 'kategori', 'total_nilaipembimbing', 'total_nilaipenguji', 'total_nilaiakhir', 'akumulasi','nama_mahasiswa','nim'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = NilaiSeminar::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_nilai' => $this->id_nilai,
            'id_dataseminar' => $this->id_dataseminar,
        ]);

        $query->andFilterWhere(['like', 'judul', $this->judul])
            ->andFilterWhere(['like', 'mahasiswa', $this->mahasiswa])
            ->andFilterWhere(['like', 'dosen_pembimbing1', $this->dosen_pembimbing1])
            ->andFilterWhere(['like', 'dosen_pembimbing2', $this->dosen_pembimbing2])
            ->andFilterWhere(['like', 'penguji1', $this->penguji1])
            ->andFilterWhere(['like', 'penguji2', $this->penguji2])
            ->andFilterWhere(['like', 'penguji3', $this->penguji3])
            ->andFilterWhere(['like', 'penguji4', $this->penguji4])
            ->andFilterWhere(['like', 'kategori', $this->kategori])
            ->andFilterWhere(['like', 'total_nilaipembimbing', $this->total_nilaipembimbing])
            ->andFilterWhere(['like', 'total_nilaipenguji', $this->total_nilaipenguji])
          //  ->andFilterWhere(['like', 'total_nilai', $this->total_nilai])
            ->andFilterWhere(['like', 'total_nilaiakhir', $this->total_nilaiakhir])
            ->andFilterWhere(['like', 'akumulasi', $this->akumulasi])
             ->andFilterWhere(['like', 'nim', $this->nim])
              ->andFilterWhere(['like', 'nama_mahasiswa', $this->nama_mahasiswa]);
        
        return $dataProvider;
    }
    
     public function searchPbl($params)
    {
        $query = NilaiSeminar::find()->where(['kategori'=>4]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_nilai' => $this->id_nilai,
            'id_dataseminar' => $this->id_dataseminar,
        ]);

        $query->andFilterWhere(['like', 'judul', $this->judul])
            ->andFilterWhere(['like', 'mahasiswa', $this->mahasiswa])
            ->andFilterWhere(['like', 'dosen_pembimbing1', $this->dosen_pembimbing1])
            ->andFilterWhere(['like', 'dosen_pembimbing2', $this->dosen_pembimbing2])
            ->andFilterWhere(['like', 'penguji1', $this->penguji1])
            ->andFilterWhere(['like', 'penguji2', $this->penguji2])
            ->andFilterWhere(['like', 'penguji3', $this->penguji3])
            ->andFilterWhere(['like', 'penguji4', $this->penguji4])
            ->andFilterWhere(['like', 'kategori', $this->kategori])
            ->andFilterWhere(['like', 'total_nilaipembimbing', $this->total_nilaipembimbing])
            ->andFilterWhere(['like', 'total_nilaipenguji', $this->total_nilaipenguji])
         //   ->andFilterWhere(['like', 'total_nilai', $this->total_nilai])
            ->andFilterWhere(['like', 'total_nilaiakhir', $this->total_nilaiakhir])
            ->andFilterWhere(['like', 'tanggal_seminar', $this->tanggal_seminar])
                 ->andFilterWhere(['like', 'prodi', $this->prodi])
            ->andFilterWhere(['like', 'akumulasi', $this->akumulasi]);
         if (($params['tglAwal'] && $params['tglAkhir'])){
            $query->andWhere(['between', 'tanggal_seminar', $params['tglAwal'],$params['tglAkhir']]);
        }
        return $dataProvider;
    }
    
     public function searchMagang($params)
    {
        $query = NilaiSeminar::find()->where(['kategori'=>2]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_nilai' => $this->id_nilai,
            'id_dataseminar' => $this->id_dataseminar,
        ]);

        $query->andFilterWhere(['like', 'judul', $this->judul])
            ->andFilterWhere(['like', 'mahasiswa', $this->mahasiswa])
            ->andFilterWhere(['like', 'dosen_pembimbing1', $this->dosen_pembimbing1])
            ->andFilterWhere(['like', 'dosen_pembimbing2', $this->dosen_pembimbing2])
            ->andFilterWhere(['like', 'penguji1', $this->penguji1])
            ->andFilterWhere(['like', 'penguji2', $this->penguji2])
            ->andFilterWhere(['like', 'penguji3', $this->penguji3])
            ->andFilterWhere(['like', 'penguji4', $this->penguji4])
            ->andFilterWhere(['like', 'kategori', $this->kategori])
            ->andFilterWhere(['like', 'total_nilaipembimbing', $this->total_nilaipembimbing])
            ->andFilterWhere(['like', 'total_nilaipenguji', $this->total_nilaipenguji])
         //   ->andFilterWhere(['like', 'total_nilai', $this->total_nilai])
            ->andFilterWhere(['like', 'total_nilaiakhir', $this->total_nilaiakhir])
            ->andFilterWhere(['like', 'tanggal_seminar', $this->tanggal_seminar])
            ->andFilterWhere(['like', 'prodi', $this->prodi])
            ->andFilterWhere(['like', 'akumulasi', $this->akumulasi]);
         if (($params['tglAwal'] && $params['tglAkhir'])){
            $query->andWhere(['between', 'tanggal_seminar', $params['tglAwal'],$params['tglAkhir']]);
        }
        return $dataProvider;
    }

     public function searchSkripsi($params)
    {
        $query = NilaiSeminar::find()->where(['kategori'=>3]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_nilai' => $this->id_nilai,
            'id_dataseminar' => $this->id_dataseminar,
        ]);

        $query->andFilterWhere(['like', 'judul', $this->judul])
            ->andFilterWhere(['like', 'mahasiswa', $this->mahasiswa])
            ->andFilterWhere(['like', 'dosen_pembimbing1', $this->dosen_pembimbing1])
            ->andFilterWhere(['like', 'dosen_pembimbing2', $this->dosen_pembimbing2])
            ->andFilterWhere(['like', 'penguji1', $this->penguji1])
            ->andFilterWhere(['like', 'penguji2', $this->penguji2])
            ->andFilterWhere(['like', 'penguji3', $this->penguji3])
            ->andFilterWhere(['like', 'penguji4', $this->penguji4])
            ->andFilterWhere(['like', 'kategori', $this->kategori])
            ->andFilterWhere(['like', 'total_nilaipembimbing', $this->total_nilaipembimbing])
            ->andFilterWhere(['like', 'total_nilaipenguji', $this->total_nilaipenguji])
         //   ->andFilterWhere(['like', 'total_nilai', $this->total_nilai])
            ->andFilterWhere(['like', 'total_nilaiakhir', $this->total_nilaiakhir])
            ->andFilterWhere(['like', 'tanggal_seminar', $this->tanggal_seminar])
    ->andFilterWhere(['like', 'prodi', $this->prodi])
            ->andFilterWhere(['like', 'akumulasi', $this->akumulasi]);
         if (($params['tglAwal'] && $params['tglAkhir'])){
            $query->andWhere(['between', 'tanggal_seminar', $params['tglAwal'],$params['tglAkhir']]);
        }
        return $dataProvider;
    }
    
     public function searchProposal($params)
    {
        $query = NilaiSeminar::find()->where(['kategori'=>1]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_nilai' => $this->id_nilai,
            'id_dataseminar' => $this->id_dataseminar,
        ]);

        $query->andFilterWhere(['like', 'judul', $this->judul])
            ->andFilterWhere(['like', 'mahasiswa', $this->mahasiswa])
            ->andFilterWhere(['like', 'dosen_pembimbing1', $this->dosen_pembimbing1])
            ->andFilterWhere(['like', 'dosen_pembimbing2', $this->dosen_pembimbing2])
            ->andFilterWhere(['like', 'penguji1', $this->penguji1])
            ->andFilterWhere(['like', 'penguji2', $this->penguji2])
            ->andFilterWhere(['like', 'penguji3', $this->penguji3])
            ->andFilterWhere(['like', 'penguji4', $this->penguji4])
            ->andFilterWhere(['like', 'kategori', $this->kategori])
            ->andFilterWhere(['like', 'total_nilaipembimbing', $this->total_nilaipembimbing])
            ->andFilterWhere(['like', 'total_nilaipenguji', $this->total_nilaipenguji])
         //   ->andFilterWhere(['like', 'total_nilai', $this->total_nilai])
            ->andFilterWhere(['like', 'total_nilaiakhir', $this->total_nilaiakhir])
            ->andFilterWhere(['like', 'tanggal_seminar', $this->tanggal_seminar])
 ->andFilterWhere(['like', 'prodi', $this->prodi])
            ->andFilterWhere(['like', 'akumulasi', $this->akumulasi]);
         if (($params['tglAwal'] && $params['tglAkhir'])){
            $query->andWhere(['between', 'tanggal_seminar', $params['tglAwal'],$params['tglAkhir']]);
        }
        return $dataProvider;
    }


    
}
