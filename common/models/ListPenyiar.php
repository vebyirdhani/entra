<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "list_penyiar".
 *
 * @property integer $id_penyiar
 * @property string $nama_penyiar
 * @property string $foto_penyiar
 * @property string $tanggal_lahir
 * @property string $fb
 * @property string $twitter
 * @property string $ig
 * @property string $bio
 */
class ListPenyiar extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'list_penyiar';
    }

    /**
     * @inheritdoc
     */
    public $picture;
    public function rules()
    {
        return [
            [['nama_penyiar', 'foto_penyiar', 'tanggal_lahir', 'fb', 'twitter', 'ig', 'bio','rating'], 'string', 'max' => 255],
               [['picture'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpeg,jpg','maxSize'=>2048*1024],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_penyiar' => 'Id Penyiar',
            'nama_penyiar' => 'Nama Penyiar',
            'foto_penyiar' => 'Foto Penyiar',
            'tanggal_lahir' => 'Tanggal Lahir',
            'fb' => 'Fb',
            'twitter' => 'Twitter',
            'ig' => 'Ig',
            'bio' => 'Bio',
            'rating'=>'Rating',
        ];
    }
}
