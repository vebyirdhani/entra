<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "contacts".
 *
 * @property integer $id_contact
 * @property string $name
 * @property string $subject
 * @property string $email
 * @property string $body
 * @property integer $status
 * @property integer $created_at
 */
class Contacts extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'contacts';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['body'], 'string'],
            [['status', 'created_at'], 'integer'],
            [['name', 'subject', 'email'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_contact' => 'Id Contact',
            'name' => 'Name',
            'subject' => 'Subject',
            'email' => 'Email',
            'body' => 'Body',
            'status' => 'Status',
            'created_at' => 'Created At',
        ];
    }
}
