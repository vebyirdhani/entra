<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "kategori".
 *
 * @property integer $id_kategori
 * @property string $kategori
 * @property string $foto
 * @property string $status
 */
class Kategori extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'kategori';
    }

    /**
     * @inheritdoc
     */
    public $picture;
    public function rules()
    {
        return [
            [['kategori', 'status'], 'required'],
            [['kategori', 'foto', 'status'], 'string', 'max' => 255],
              [['picture'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpeg,jpg','maxSize'=>2048*1024],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_kategori' => 'Id Kategori',
            'kategori' => 'Kategori',
            'foto' => 'Foto',
            'status' => 'Status',
        ];
    }
}
