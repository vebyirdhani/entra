<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "nilai_seminar".
 *
 * @property integer $id_nilai
 * @property integer $id_dataseminar
 * @property string $judul
 * @property string $mahasiswa
 * @property string $dosen_pembimbing1
 * @property string $dosen_pembimbing2
 * @property string $penguji1
 * @property string $penguji2
 * @property string $penguji3
 * @property string $penguji4
 * @property string $kategori
 * @property string $total_nilaipembimbing
 * @property string $total_nilaipenguji
 * @property string $total_nilaiakhir
 * @property string $akumulasi
 * @property string $pembimbing_persen
 * @property string $penguji_persen
 * @property string $nilai_pembimbing1
 * @property string $nilai_pembimbing2
 * @property string $nilai_penguji1
 * @property string $nilai_penguji2
 * @property string $nilai_penguji3
 * @property string $nilai_penguji4
 */
class NilaiSeminar extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'nilai_seminar';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_dataseminar', 'judul', 'mahasiswa', 'dosen_pembimbing1', 'dosen_pembimbing2', 'penguji1', 'penguji2', 'penguji3', 'penguji4', 'kategori', 'total_nilaipembimbing', 'total_nilaipenguji'], 'required'],
            [['id_dataseminar'], 'integer'],
            [['nama_mahasiswa','nim','prodi','dosen_input','tanggal_seminar','created_at','judul', 'mahasiswa', 'dosen_pembimbing1', 'dosen_pembimbing2', 'penguji1', 'penguji2', 'penguji3', 'penguji4', 'kategori', 'total_nilaipembimbing', 'total_nilaipenguji', 'total_nilaiakhir', 'akumulasi', 'pembimbing_persen', 'penguji_persen', 'nilai_pembimbing1', 'nilai_pembimbing2', 'nilai_penguji1', 'nilai_penguji2', 'nilai_penguji3', 'nilai_penguji4'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_nilai' => 'Id Nilai',
            'id_dataseminar' => 'Id Dataseminar',
            'judul' => 'Judul',
            'mahasiswa' => 'Mahasiswa',
            'dosen_pembimbing1' => 'Dosen Pembimbing1',
            'dosen_pembimbing2' => 'Dosen Pembimbing2',
            'penguji1' => 'Penguji1',
            'penguji2' => 'Penguji2',
            'penguji3' => 'Penguji3',
            'penguji4' => 'Penguji4',
            'kategori' => 'Kategori',
            'total_nilaipembimbing' => 'Total Nilaipembimbing',
            'total_nilaipenguji' => 'Total Nilaipenguji',
            'total_nilaiakhir' => 'Total Nilaiakhir',
            'akumulasi' => 'Akumulasi',
            'pembimbing_persen' => 'Pembimbing Persen',
            'penguji_persen' => 'Penguji Persen',
            'nilai_pembimbing1' => 'Nilai Pembimbing1',
            'nilai_pembimbing2' => 'Nilai Pembimbing2',
            'nilai_penguji1' => 'Nilai Penguji1',
            'nilai_penguji2' => 'Nilai Penguji2',
            'nilai_penguji3' => 'Nilai Penguji3',
            'nilai_penguji4' => 'Nilai Penguji4',
            'tanggal_seminar'=>'Tanggal Seminar',
            'created_at'=>'Created at',
            'dosen_input'=>'Dosen Input',
            'prodi'=>'Prodi',
            'nim'=>'NIM',
            'nama_mahasiswa'=>'Nama Mahasiswa',
			  'dosenpemb1.nama' => 'Dosen Pembimbing 1',
             'dosenpemb2.nama' => 'Dosen Pembimbing 2',
              'dosenpeng1.nama' => 'Dosen Penguji 1',
               'dosenpeng2.nama' => 'Dosen Penguji 2',
                'dosenpeng3.nama' => 'Dosen Penguji 3',
                 'dosenpeng4.nama' => 'Dosen Penguji 4',
				   'kategoriid.kategori' => 'Kategori Seminar',
                  'mahasiswaid.nama' => 'Mahasiswa',
        ];
    }
   
    
           public function getIdDosen1()
    {
        return $this->hasOne(Dosen::className(), ['id_dosen' => 'dosen_pembimbing1']);
    }
       public function getIdProdi()
    {
        return $this->hasOne(Prodi::className(), ['id_prodi' => 'prodi']);
    }
     public function getIdDosen2()
    {
        return $this->hasOne(Dosen::className(), ['id_dosen' => 'dosen_pembimbing2']);
    }
    
      public function getIdPenguji4()
    {
        return $this->hasOne(Dosen::className(), ['id_dosen' => 'penguji4']);
    }
      public function getIdPenguji1()
    {
        return $this->hasOne(Dosen::className(), ['id_dosen' => 'penguji1']);
    }
      public function getIdPenguji2()
    {
        return $this->hasOne(Dosen::className(), ['id_dosen' => 'penguji2']);
    }
      public function getIdPenguji3()
    {
        return $this->hasOne(Dosen::className(), ['id_dosen' => 'penguji3']);
    }
      public function getIdKategori()
    {
        return $this->hasOne(KategoriSeminar::className(), ['id_kategori' => 'kategori']);
    }
     public function getIdMahasiswa()
    {
        return $this->hasOne(Mahasiswa::className(), ['id_mahasiswa' => 'mahasiswa']);
    }
	
	 public function getDosenpemb1(){
        return Dosen::find()
                ->where(['id_dosen' => $this->dosen_pembimbing1])
                ->one();
    }
      public function getDosenpemb2(){
        return Dosen::find()
                ->where(['id_dosen' => $this->dosen_pembimbing2])
                ->one();
    }
     public function getDosenpeng1(){
        return Dosen::find()
                ->where(['id_dosen' => $this->penguji1])
                ->one();
    }
    
     public function getDosenpeng2(){
        return Dosen::find()
                ->where(['id_dosen' => $this->penguji2])
                ->one();
    }
    
     public function getDosenpeng3(){
        return Dosen::find()
                ->where(['id_dosen' => $this->penguji3])
                ->one();
    }
    
      public function getDosenpeng4(){
        return Dosen::find()
                ->where(['id_dosen' => $this->penguji4])
                ->one();
    }
	 public function getMahasiswaid(){
        return Mahasiswa::find()
                ->where(['id_mahasiswa' => $this->mahasiswa])
                ->one();
    }
	 public function getKategoriid(){
        return KategoriSeminar::find()
                ->where(['id_kategori' => $this->kategori])
                ->one();
    }
	
     public static function viewskripsi($tglAwal,$tglAkhir,$prodi)
    {
        
        $model = self::find();
      // $model->andWhere(['idkonfirmasi'=>$id])->one();
     $model->andWhere(['kategori'=>3])->andWhere(['prodi'=>$prodi])->andWhere(['between','tanggal_seminar', $tglAwal,$tglAkhir]);
          

        return $model->each();
        
    }
    public static function viewproposal($tglAwal,$tglAkhir,$prodi)
    {
        
        $model = self::find();
      // $model->andWhere(['idkonfirmasi'=>$id])->one();
       $model->andWhere(['kategori'=>1])->andWhere(['between','tanggal_seminar', $tglAwal,$tglAkhir])
              ->andFilterWhere(['=', 'prodi', $prodi]) ;
        return $model->each();
        
    }
     public static function viewmagang($tglAwal,$tglAkhir,$prodi)
    {
        
        $model = self::find();
      // $model->andWhere(['idkonfirmasi'=>$id])->one();
       $model->andWhere(['kategori'=>2])->andWhere(['between','tanggal_seminar', $tglAwal,$tglAkhir])
              ->andFilterWhere(['=', 'prodi', $prodi]) ;
        return $model->each();
        
    }
       public static function viewpbl($tglAwal,$tglAkhir,$prodi)
    {
        
        $model = self::find();
      // $model->andWhere(['idkonfirmasi'=>$id])->one();
       $model->andWhere(['kategori'=>4])->andWhere(['between','tanggal_seminar', $tglAwal,$tglAkhir])
              ->andFilterWhere(['=', 'prodi', $prodi]) ;
        return $model->each();
        
    }
   
   
    
    
}
