<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "coordinat".
 *
 * @property integer $id_coordinat
 * @property string $latitdude
 * @property string $longitude
 * @property string $nama
 * @property string $kategori
 * @property string $id_data
 */
class Coordinat extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'coordinat';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['latitdude', 'longitude', 'nama', 'kategori', 'id_data'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_coordinat' => 'Id Coordinat',
            'latitdude' => 'Latitdude',
            'longitude' => 'Longitude',
            'nama' => 'Nama',
            'kategori' => 'Kategori',
            'id_data' => 'Id Data',
        ];
    }
}
