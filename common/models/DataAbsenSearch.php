<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\DataAbsen;

/**
 * DataAbsenSearch represents the model behind the search form about `\common\models\DataAbsen`.
 */
class DataAbsenSearch extends DataAbsen
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_dataabsen', 'id_absen'], 'integer'],
            [['dosen', 'mahasiswa', 'keterangan'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = DataAbsen::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_dataabsen' => $this->id_dataabsen,
            'id_absen' => $this->id_absen,
        ]);

        $query->andFilterWhere(['like', 'dosen', $this->dosen])
            ->andFilterWhere(['like', 'mahasiswa', $this->mahasiswa])
            ->andFilterWhere(['like', 'keterangan', $this->keterangan]);

        return $dataProvider;
    }
}
