<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Absen;

/**
 * AbsenSearch represents the model behind the search form about `\common\models\Absen`.
 */
class AbsenSearch extends Absen
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_absen', 'status_hadir'], 'integer'],
            [['status','id_jadwal','periode','kelas_nama','dosen', 'mahasiswa', 'jam_mulai', 'jam_akhir', 'tanggal', 'materi_kuliah', 'pertemuan', 'mata_kuliah', 'keterangan','prodi'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Absen::find()->where(['status'=>1]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_absen' => $this->id_absen,
            'status_hadir' => $this->status_hadir,
        ]);

        $query->andFilterWhere(['like', 'dosen', $this->dosen])
            ->andFilterWhere(['like', 'mahasiswa', $this->mahasiswa])
            ->andFilterWhere(['like', 'jam_mulai', $this->jam_mulai])
            ->andFilterWhere(['like', 'jam_akhir', $this->jam_akhir])
            ->andFilterWhere(['like', 'tanggal', $this->tanggal])
            ->andFilterWhere(['like', 'materi_kuliah', $this->materi_kuliah])
            ->andFilterWhere(['like', 'pertemuan', $this->pertemuan])
            ->andFilterWhere(['like', 'mata_kuliah', $this->mata_kuliah])
                  ->andFilterWhere(['like', 'id_jadwal', $this->id_jadwal])
            ->andFilterWhere(['like', 'keterangan', $this->keterangan]);

        return $dataProvider;
    }
        public function searchDosen($params)
    {
        $query = Absen::find()->where(['id_jadwal'=>$params['cari'] ])->where(['status'=>1]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_absen' => $this->id_absen,
            'status_hadir' => $this->status_hadir,
        ]);

        $query->andFilterWhere(['like', 'dosen', $this->dosen])
            ->andFilterWhere(['like', 'mahasiswa', $this->mahasiswa])
            ->andFilterWhere(['like', 'jam_mulai', $this->jam_mulai])
            ->andFilterWhere(['like', 'jam_akhir', $this->jam_akhir])
            ->andFilterWhere(['like', 'tanggal', $this->tanggal])
            ->andFilterWhere(['like', 'materi_kuliah', $this->materi_kuliah])
            ->andFilterWhere(['like', 'pertemuan', $this->pertemuan])
            ->andFilterWhere(['like', 'mata_kuliah', $this->mata_kuliah])
                 ->andFilterWhere(['like', 'id_jadwal', $this->id_jadwal])
            ->andFilterWhere(['like', 'keterangan', $this->keterangan]);

          if (($params['tglAwal'] && $params['tglAkhir'])){
            $query->andWhere(['between', 'tanggal', $params['tglAwal'],$params['tglAkhir']]);
        }
        return $dataProvider;
    }
        public function searchMahasiswa($params)
    {
        $query = Absen::find()->where(['status'=>1]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_absen' => $this->id_absen,
            'status_hadir' => $this->status_hadir,
        ]);

        $query->andFilterWhere(['like', 'dosen', $this->dosen])
            ->andFilterWhere(['like', 'mahasiswa', $this->mahasiswa])
            ->andFilterWhere(['like', 'jam_mulai', $this->jam_mulai])
            ->andFilterWhere(['like', 'jam_akhir', $this->jam_akhir])
            ->andFilterWhere(['like', 'tanggal', $this->tanggal])
            ->andFilterWhere(['like', 'materi_kuliah', $this->materi_kuliah])
            ->andFilterWhere(['like', 'pertemuan', $this->pertemuan])
            ->andFilterWhere(['like', 'mata_kuliah', $this->mata_kuliah])
            ->andFilterWhere(['like', 'keterangan', $this->keterangan])
                  ->andFilterWhere(['like', 'periode', $this->periode])
                  ->andFilterWhere(['like', 'semester', $this->semester])
                 ->andFilterWhere(['like', 'id_jadwal', $this->id_jadwal])
                  ->andFilterWhere(['like', 'kelas_nama', $this->kelas_nama]);
        if (($params['tglAwal'] && $params['tglAkhir'])){
            $query->andWhere(['between', 'tanggal', $params['tglAwal'],$params['tglAkhir']]);
        }
        return $dataProvider;
    }
    
       public function searchReport($params)
    {
        $query = Absen::find()->where(['status'=>1]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_absen' => $this->id_absen,
            'status_hadir' => $this->status_hadir,
        ]);

        $query->andFilterWhere(['like', 'dosen', $this->dosen])
            ->andFilterWhere(['like', 'mahasiswa', $this->mahasiswa])
            ->andFilterWhere(['like', 'jam_mulai', $this->jam_mulai])
            ->andFilterWhere(['like', 'jam_akhir', $this->jam_akhir])
            ->andFilterWhere(['like', 'tanggal', $this->tanggal])
            ->andFilterWhere(['like', 'materi_kuliah', $this->materi_kuliah])
            ->andFilterWhere(['like', 'pertemuan', $this->pertemuan])
            ->andFilterWhere(['like', 'mata_kuliah', $this->mata_kuliah])
            ->andFilterWhere(['like', 'keterangan', $this->keterangan])
                  ->andFilterWhere(['like', 'periode', $this->periode])
                  ->andFilterWhere(['like', 'semester', $this->semester])
                 ->andFilterWhere(['like', 'id_jadwal', $this->id_jadwal])
                  ->andFilterWhere(['like', 'prodi', $this->prodi])
                  ->andFilterWhere(['like', 'kelas_nama', $this->kelas_nama]);
//        if (($params['tglAwal'] && $params['tglAkhir'])){
//            $query->andWhere(['between', 'tanggal', $params['tglAwal'],$params['tglAkhir']]);
//        }
        return $dataProvider;
    }
}
