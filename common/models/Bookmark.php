<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "bookmark".
 *
 * @property integer $id_bookmark
 * @property string $kategori
 * @property integer $id_data
 * @property string $id_desa
 * @property string $nama
 * @property string $harga
 * @property string $foto
 */
class Bookmark extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bookmark';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['kategori', 'id_data', 'id_desa', 'nama', 'harga'], 'required'],
            [['id_data','id_user'], 'integer'],
            [['kategori', 'id_desa', 'nama', 'harga', 'foto'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_bookmark' => 'Id Bookmark',
            'kategori' => 'Kategori',
            'id_data' => 'Id Data',
            'id_desa' => 'Id Desa',
            'nama' => 'Nama',
            'harga' => 'Harga',
            'foto' => 'Foto',
            'id_user'=>'Id User'
        ];
    }
}
