<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "jadwal_kuliah".
 *
 * @property integer $id_jadwal
 * @property string $kelas_nama
 * @property string $mata_kuliah
 * @property string $dosen
 * @property string $jadwal_awal
 * @property string $jadwal_akhir
 * @property string $jumlah_mhs
 * @property string $pertemuan
 */
class JadwalKuliah extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'jadwal_kuliah';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['kelas_nama', 'mata_kuliah', 'dosen', 'jadwal_awal', 'jadwal_akhir'], 'required'],
            [['jumlah_pertemuan','kode_matakuliah','prodi','periode','sks','kelas_nama', 'mata_kuliah', 'dosen','dosen2', 'jadwal_awal', 'jadwal_akhir', 'jumlah_mhs', 'pertemuan','semester','hari','tahun','semester'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_jadwal' => 'Id Jadwal',
            'kelas_nama' => 'Kelas Nama',
            'mata_kuliah' => 'Mata Kuliah',
            'dosen' => 'Dosen 1',
            'dosen2'=>'Dosen 2',
            'jadwal_awal' => 'Jadwal Awal',
            'jadwal_akhir' => 'Jadwal Akhir',
            'jumlah_mhs' => 'Jumlah Mhs',
            'pertemuan' => 'Pertemuan',
            'tahun'=>'tahun',
            'sks'=>'SKS', 
            'semester'=>'Periode',
            'periode'=>'Semester',
             'prodi'=>'Prodi',
            'kode_matakuliah'=>'Kode Mata Kuliah',
            'jumlah_pertemuan'=>'Jumlah Pertemuan',
			'dosen1id.nama' => 'Dosen 1',
                 'dosen2id.nama' => 'Dosen 2',
                  'matkulid.nama' => 'MataKuliah',
				    'prodiid.nama' => 'Prodi',
        ];
    }
     public function getIdDosen1()
    {
        return $this->hasOne(Dosen::className(), ['id_dosen' => 'dosen']);
    }
     public function getIdDosen2()
    {
        return $this->hasOne(Dosen::className(), ['id_dosen' => 'dosen2']);
    }
      public function getIdMatakuliah()
    {
        return $this->hasOne(MataKuliah::className(), ['id_matkul' => 'mata_kuliah']);
    }
      public function getIdProdi()
    {
        return $this->hasOne(Prodi::className(), ['id_prodi' => 'prodi']);
    }
	
	
	
     public function getDosen1id(){
        return Dosen::find()
                ->where(['id_dosen' => $this->dosen])
                ->one();
    }
	 public function getDosen2id(){
        return Dosen::find()
                ->where(['id_dosen' => $this->dosen2])
                ->one();
    }
	public function getMatkulid(){
        return MataKuliah::find()
                ->where(['id_matkul' => $this->mata_kuliah])
                ->one();
    }
	public function getProdiid(){
        return Prodi::find()
                ->where(['id_prodi' => $this->prodi])
                ->one();
    }
    
    public static function viewpertemuan($semester,$prodi)
    {
        
        $model = self::find();
      // $model->andWhere(['idkonfirmasi'=>$id])->one();
      $model
              ->andFilterWhere(['=', 'semester', $semester]) 
              ->andFilterWhere(['=', 'prodi', $prodi]) 
             ;
              

        return $model->each();
        
    }
    
     public static function viewbeban($prodi,$semester)
    {
        
        $model = self::find();
      // $model->andWhere(['idkonfirmasi'=>$id])->one();
      $model
           
              ->andFilterWhere(['like', 'prodi', $prodi]) 
              ->andFilterWhere(['like', 'semester', $semester]) ;
              

        return $model->each();
        
    }
    
}
