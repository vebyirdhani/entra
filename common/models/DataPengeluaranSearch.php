<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\DataPengeluaran;

/**
 * DataPengeluaranSearch represents the model behind the search form about `common\models\DataPengeluaran`.
 */
class DataPengeluaranSearch extends DataPengeluaran
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_pengeluaran'], 'integer'],
            [['jenis_pengeluaran', 'detail_pengeluaran', 'bukti','tanggal','detail1', 'detail2', 'detail3', 'detail4', 'detail5', 'jumlah_detail1', 'jumlah_detail2', 'jumlah_detail3', 'jumlah_detail4', 'jumlah_detail5', 'project','harga'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = DataPengeluaran::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_pengeluaran' => $this->id_pengeluaran,
        ]);

        $query->andFilterWhere(['like', 'jenis_pengeluaran', $this->jenis_pengeluaran])
            ->andFilterWhere(['like', 'detail_pengeluaran', $this->detail_pengeluaran])
            ->andFilterWhere(['like', 'detail1', $this->detail1])
            ->andFilterWhere(['like', 'detail2', $this->detail2])
            ->andFilterWhere(['like', 'detail3', $this->detail3])
            ->andFilterWhere(['like', 'detail4', $this->detail4])
            ->andFilterWhere(['like', 'detail5', $this->detail5])
            ->andFilterWhere(['like', 'jumlah_detail1', $this->jumlah_detail1])
            ->andFilterWhere(['like', 'jumlah_detail2', $this->jumlah_detail2])
            ->andFilterWhere(['like', 'jumlah_detail3', $this->jumlah_detail3])
            ->andFilterWhere(['like', 'jumlah_detail4', $this->jumlah_detail4])
            ->andFilterWhere(['like', 'jumlah_detail5', $this->jumlah_detail5])
            ->andFilterWhere(['like', 'project', $this->project])
                ->andFilterWhere(['like', 'harga', $this->harga])
                ->andFilterWhere(['like', 'tanggal', $this->tanggal])
                ->andFilterWhere(['like', 'bukti', $this->bukti]);

        return $dataProvider;
    }
     public function searchReport($params)
    {
        $query = DataPengeluaran::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_pengeluaran' => $this->id_pengeluaran,
        ]);

        $query->andFilterWhere(['like', 'jenis_pengeluaran', $this->jenis_pengeluaran])
            ->andFilterWhere(['like', 'detail_pengeluaran', $this->detail_pengeluaran])
            ->andFilterWhere(['like', 'detail1', $this->detail1])
            ->andFilterWhere(['like', 'detail2', $this->detail2])
            ->andFilterWhere(['like', 'detail3', $this->detail3])
            ->andFilterWhere(['like', 'detail4', $this->detail4])
            ->andFilterWhere(['like', 'detail5', $this->detail5])
            ->andFilterWhere(['like', 'jumlah_detail1', $this->jumlah_detail1])
            ->andFilterWhere(['like', 'jumlah_detail2', $this->jumlah_detail2])
            ->andFilterWhere(['like', 'jumlah_detail3', $this->jumlah_detail3])
            ->andFilterWhere(['like', 'jumlah_detail4', $this->jumlah_detail4])
            ->andFilterWhere(['like', 'jumlah_detail5', $this->jumlah_detail5])
            ->andFilterWhere(['like', 'project', $this->project])
                ->andFilterWhere(['like', 'harga', $this->harga])
                ->andFilterWhere(['like', 'tanggal', $this->tanggal])
                ->andFilterWhere(['like', 'bukti', $this->bukti]);
 if (($params['tglAwal'] && $params['tglAkhir'])){
            $query->andWhere(['between', 'tanggal', $params['tglAwal'],$params['tglAkhir']]);
        }
        return $dataProvider;
    }
}
