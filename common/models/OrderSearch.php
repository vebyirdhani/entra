<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Order;

/**
 * OrderSearch represents the model behind the search form about `\common\models\Order`.
 */
class OrderSearch extends Order
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_order', 'id_user', 'id_plan', 'status'], 'integer'],
            [['catatan', 'id_desa', 'total_pemesanan', 'total_pembayaran', 'created_at', 'tipe_pembayaran', 'foto'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Order::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_order' => $this->id_order,
            'id_user' => $this->id_user,
            'id_plan' => $this->id_plan,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'catatan', $this->catatan])
            ->andFilterWhere(['like', 'id_desa', $this->id_desa])
            ->andFilterWhere(['like', 'total_pemesanan', $this->total_pemesanan])
            ->andFilterWhere(['like', 'total_pembayaran', $this->total_pembayaran])
            ->andFilterWhere(['like', 'created_at', $this->created_at])
            ->andFilterWhere(['like', 'tipe_pembayaran', $this->tipe_pembayaran])
            ->andFilterWhere(['like', 'foto', $this->foto]);

        return $dataProvider;
    }
}
