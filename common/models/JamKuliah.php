<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "jam_kuliah".
 *
 * @property integer $id_jam
 * @property string $jam_kuliah
 */
class JamKuliah extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'jam_kuliah';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['jam_kuliah'], 'required'],
            [['jam_kuliah'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_jam' => 'Id Jam',
            'jam_kuliah' => 'Jam Kuliah',
        ];
    }
}
