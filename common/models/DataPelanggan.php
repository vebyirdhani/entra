<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "data_pelanggan".
 *
 * @property integer $id_pelanggan
 * @property string $nama
 * @property string $nik
 * @property string $type
 * @property string $blok
 * @property string $harga
 * @property string $dp
 * @property string $biaya_notaris
 * @property string $biaya_kelebihan_tanah
 * @property string $project
 * @property string $status
 */
class DataPelanggan extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'data_pelanggan';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nama', 'nik', 'type', 'blok', 'harga', 'dp', 'biaya_notaris', 'biaya_kelebihan_tanah', 'project', 'status','created_at','total','no_hp','sisa','tipe_pembayaran','bank','keterangan','tanggal_akad'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_pelanggan' => 'Id Pelanggan',
            'nama' => 'Nama',
            'nik' => 'Nik',
            'type' => 'Type',
            'blok' => 'Blok',
            'harga' => 'Harga',
            'dp' => 'Dp',
            'biaya_notaris' => 'Biaya Notaris',
            'biaya_kelebihan_tanah' => 'Biaya Kelebihan Tanah',
            'project' => 'Project',
            'status' => 'Status',
            'created_at'=>'Tanggal',
            'total'=>'Total',
            'sisa'=>'Sisa',
            'tipe_pembayaran'=>'Tipe Pembayaran',
            'bank'=>'Bank',
           'keterangan'=>'Keterangan',
            'tanggal_akad'=>'Tanggal Akad',
          'no_hp'=>'No Handphone',
        ];
    }
          public static function viewpelanggan($tglAwal,$tglAkhir,$id)
    {
        
        $model = self::find();
      // $model->andWhere(['idkonfirmasi'=>$id])->one();
      $model->andWhere(['project'=>$id])->andWhere(['between','tanggal_akad', $tglAwal,$tglAkhir]);
          

        return $model->each();
        
    }
}
