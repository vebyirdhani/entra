<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Merchandise;

/**
 * MerchandiseSearch represents the model behind the search form about `common\models\Merchandise`.
 */
class MerchandiseSearch extends Merchandise
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_merchandise', 'id_user', 'harga'], 'integer'],
            [['nama', 'deskripsi', 'alamat', 'id_desa'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Merchandise::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_merchandise' => $this->id_merchandise,
            'id_user' => $this->id_user,
            'id_desa' => $this->id_desa,
        ]);

        $query->andFilterWhere(['like', 'nama', $this->nama])
            ->andFilterWhere(['like', 'deskripsi', $this->deskripsi])
            ->andFilterWhere(['like', 'alamat', $this->alamat])
            ->andFilterWhere(['like', 'harga', $this->harga]);

        return $dataProvider;
    }
}
