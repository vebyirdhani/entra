<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "villages".
 *
 * @property string $id
 * @property string $district_id
 * @property string $name
 * @property string $address
 * @property string $description
 */
class Villages extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'villages';
    }

    /**
     * @inheritdoc
     */
    public $picture;
    public function rules()
    {
        return [
            [['id', 'district_id', 'name'], 'required'],
            [['description'], 'string'],
            [['provinsi'], 'integer'],
            [['id'], 'string', 'max' => 10],
            [['district_id'], 'string', 'max' => 7],
            [['name', 'address','rating'], 'string', 'max' => 255],
             [['picture'],'file','maxFiles'=>5,'maxSize'=>2048*1024],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'district_id' => 'District ID',
            'name' => 'Name',
            'address' => 'Address',
            'description' => 'Description',
            'provinsi'=>'Provinsi',
            'provinsii.name'=>'Provinsi',
                'kecamatann.name'=>'Daerah',
        ];
    }
    public function getProvinsii(){
        return Provinces::find()
                ->where(['id' => $this->provinsi])
                ->one();
    }
    // public function getKotaa(){
    //     return Regencies::find()
    //             ->where(['id' => $this->kota])
    //             ->one();
    // }
      public function getKecamatann(){
        return Districts::find()
                ->where(['id' => $this->district_id])
                ->one();
    }
}
