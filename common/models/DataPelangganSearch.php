<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\DataPelanggan;

/**
 * DataPelangganSearch represents the model behind the search form about `common\models\DataPelanggan`.
 */
class DataPelangganSearch extends DataPelanggan
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_pelanggan'], 'integer'],
            [['nama', 'nik', 'type', 'blok', 'harga', 'dp', 'biaya_notaris', 'biaya_kelebihan_tanah', 'project', 'status','created_at','no_hp','total','sisa','tipe_pembayaran','bank','keterangan','tanggal_akad'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = DataPelanggan::find()->where('status IS NULL');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_pelanggan' => $this->id_pelanggan,
        ]);

        $query->andFilterWhere(['like', 'nama', $this->nama])
            ->andFilterWhere(['like', 'nik', $this->nik])
            ->andFilterWhere(['like', 'type', $this->type])
            ->andFilterWhere(['like', 'blok', $this->blok])
            ->andFilterWhere(['like', 'harga', $this->harga])
            ->andFilterWhere(['like', 'dp', $this->dp])
            ->andFilterWhere(['like', 'biaya_notaris', $this->biaya_notaris])
            ->andFilterWhere(['like', 'biaya_kelebihan_tanah', $this->biaya_kelebihan_tanah])
            ->andFilterWhere(['like', 'project', $this->project])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'status', $this->created_at])
                ->andFilterWhere(['like', 'total', $this->total])
                ->andFilterWhere(['like', 'sisa', $this->sisa])
                 ->andFilterWhere(['like', 'tipe_pembayaran', $this->tipe_pembayaran])
                ->andFilterWhere(['like', 'bank', $this->bank])
                 ->andFilterWhere(['like', 'keterangan', $this->keterangan])
                ->andFilterWhere(['like', 'tanggal_akad', $this->tanggal_akad])
        ->andFilterWhere(['like', 'no_hp', $this->no_hp]);

        return $dataProvider;
    }
    
     public function searchReport($params)
    {
        $query = DataPelanggan::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_pelanggan' => $this->id_pelanggan,
        ]);

        $query->andFilterWhere(['like', 'nama', $this->nama])
            ->andFilterWhere(['like', 'nik', $this->nik])
            ->andFilterWhere(['like', 'type', $this->type])
            ->andFilterWhere(['like', 'blok', $this->blok])
            ->andFilterWhere(['like', 'harga', $this->harga])
            ->andFilterWhere(['like', 'dp', $this->dp])
            ->andFilterWhere(['like', 'biaya_notaris', $this->biaya_notaris])
            ->andFilterWhere(['like', 'biaya_kelebihan_tanah', $this->biaya_kelebihan_tanah])
            ->andFilterWhere(['like', 'project', $this->project])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'status', $this->created_at])
                 ->andFilterWhere(['like', 'total', $this->total])
                ->andFilterWhere(['like', 'sisa', $this->sisa])
                 ->andFilterWhere(['like', 'tipe_pembayaran', $this->tipe_pembayaran])
                ->andFilterWhere(['like', 'bank', $this->bank])
                 ->andFilterWhere(['like', 'keterangan', $this->keterangan])
        ->andFilterWhere(['like', 'no_hp', $this->no_hp]);
 if (($params['tglAwal'] && $params['tglAkhir'])){
            $query->andWhere(['between', 'tanggal_akad', $params['tglAwal'],$params['tglAkhir']]);
        }
        return $dataProvider;
    }
}
