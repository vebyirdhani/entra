<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "operational_experience".
 *
 * @property integer $id_operational
 * @property string $hari
 * @property string $jam_buka
 * @property string $jam_tutup
 * @property string $harga
 * @property string $status
 * @property string $id_experience
 */
class OperationalExperience extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'operational_experience';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['hari', 'jam_buka', 'jam_tutup', 'harga', 'status', 'id_experience'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_operational' => 'Id Operational',
            'hari' => 'Hari',
            'jam_buka' => 'Jam Buka',
            'jam_tutup' => 'Jam Tutup',
            'harga' => 'Harga',
            'status' => 'Status',
            'id_experience' => 'Id Experience',
        ];
    }
}
