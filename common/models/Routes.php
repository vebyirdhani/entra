<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "routes".
 *
 * @property integer $id_route
 * @property string $type
 * @property string $alias
 * @property string $name
 * @property integer $status
 */
class Routes extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'routes';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type', 'alias', 'name'], 'required'],
            [['type', 'alias', 'name','status'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_route' => 'Id Route',
            'type' => 'Type',
            'alias' => 'Alias',
            'name' => 'Name',
            'status' => 'Status',
        ];
    }
}
