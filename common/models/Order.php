<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "order".
 *
 * @property integer $id_order
 * @property integer $id_user
 * @property integer $id_plan
 * @property string $catatan
 * @property string $id_desa
 * @property string $total_pemesanan
 * @property string $total_pembayaran
 * @property integer $status
 * @property string $created_at
 * @property string $tipe_pembayaran
 * @property string $foto
 */
class Order extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'order';
    }

    /**
     * @inheritdoc
     */
    public $imageFile;
    public function rules()
    {
        return [
            [['id_user', 'total_pemesanan', 'total_pembayaran', 'status', 'created_at'], 'required'],
            [['id_user', 'id_plan', 'status'], 'integer'],
            [['catatan', 'id_desa', 'total_pemesanan', 'total_pembayaran', 'created_at', 'tipe_pembayaran', 'foto'], 'string', 'max' => 255],
              [['imageFile'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg','jpeg'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_order' => 'Id Order',
            'id_user' => 'Id User',
            'id_plan' => 'Id Plan',
            'catatan' => 'Catatan',
            'id_desa' => 'Id Desa',
            'total_pemesanan' => 'Total Pemesanan',
            'total_pembayaran' => 'Total Pembayaran',
            'status' => 'Status',
            'created_at' => 'Created At',
            'tipe_pembayaran' => 'Tipe Pembayaran',
            'foto' => 'Foto',
        ];
    }
}
