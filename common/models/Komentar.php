<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "komentar".
 *
 * @property integer $id_komentar
 * @property string $first_name
 * @property string $last_name
 * @property string $email
 * @property string $subject
 * @property string $body
 * @property string $status
 * @property string $created_at
 */
class Komentar extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'komentar';
    }

    /**
     * @inheritdoc
     */
    public $verifyCode;
    public function rules()
    {
        return [
            [['first_name', 'email', 'subject', 'status', 'created_at'], 'required'],
            [['body'], 'string'],
            [['first_name', 'last_name', 'email', 'subject', 'status', 'created_at'], 'string', 'max' => 255],
          ['verifyCode', 'captcha','captchaAction'=>'/site/captcha']    
      
            ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_komentar' => 'Id Komentar',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'email' => 'Email',
            'subject' => 'Subject',
            'body' => 'Body',
            'status' => 'Status',
            'created_at' => 'Created At',
        ];
    }
}
