<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "team".
 *
 * @property integer $id_team
 * @property string $nama
 * @property string $jabatan
 * @property string $deskripsi
 * @property string $twitter
 * @property string $instagram
 * @property string $facebook
 * @property string $foto
 */
class Team extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'team';
    }

    /**
     * @inheritdoc
     */
    public $picture;
    public function rules()
    {
        return [
            [['nama', 'foto'], 'required'],
            [['deskripsi'], 'string'],
            [['nama', 'jabatan', 'twitter', 'instagram', 'facebook', 'foto'], 'string', 'max' => 255],
       [['picture'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpeg,jpg','maxSize'=>2048*1024],
            ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_team' => 'Id Team',
            'nama' => 'Nama',
            'jabatan' => 'Jabatan',
            'deskripsi' => 'Deskripsi',
            'twitter' => 'Twitter',
            'instagram' => 'Instagram',
            'facebook' => 'Facebook',
            'foto' => 'Foto',
        ];
    }
}
