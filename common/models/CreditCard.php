<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "credit_card".
 *
 * @property integer $id_credit
 * @property string $name_card
 * @property string $card_number
 * @property string $expired_date
 * @property integer $cvv
 * @property integer $id_user
 */
class CreditCard extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'credit_card';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name_card', 'card_number', 'expired_date', 'cvv', 'id_user'], 'required'],
            [['cvv', 'id_user'], 'integer'],
            [['name_card', 'expired_date'], 'string', 'max' => 255],
            [['card_number'], 'string', 'max' => 16],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_credit' => 'Id Credit',
            'name_card' => 'Name Card',
            'card_number' => 'Card Number',
            'expired_date' => 'Expired Date',
            'cvv' => 'Cvv',
            'id_user' => 'Id User',
        ];
    }
}
