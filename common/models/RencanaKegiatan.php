<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "rencana_kegiatan".
 *
 * @property integer $id_rencanakegiatan
 * @property integer $id_plan
 * @property integer $id_desa
 * @property integer $kategori_pemesanan
 * @property integer $id_pemesanan
 * @property string $jumlah_orang
 */
class RencanaKegiatan extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'rencana_kegiatan';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_plan', 'id_desa', 'kategori_pemesanan', 'id_pemesanan', 'jumlah_orang'], 'required'],
            [['id_plan', 'id_desa', 'kategori_pemesanan', 'id_pemesanan'], 'integer'],
            [['jumlah_orang'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_rencanakegiatan' => 'Id Rencanakegiatan',
            'id_plan' => 'Id Plan',
            'id_desa' => 'Id Desa',
            'kategori_pemesanan' => 'Kategori Pemesanan',
            'id_pemesanan' => 'Id Pemesanan',
            'jumlah_orang' => 'Jumlah Orang',
        ];
    }
}
