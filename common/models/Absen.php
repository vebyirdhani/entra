<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "absen".
 *
 * @property integer $id_absen
 * @property string $dosen
 * @property string $mahasiswa
 * @property string $jam_mulai
 * @property string $jam_akhir
 * @property string $tanggal
 * @property string $materi_kuliah
 * @property string $pertemuan
 * @property string $mata_kuliah
 * @property integer $status_hadir
 * @property string $keterangan
 */
class Absen extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'absen';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
          //  [['dosen', 'mahasiswa', 'jam_mulai', 'jam_akhir', 'tanggal'], 'required'],
            [['status_hadir'], 'integer'],
            [['status','kode_matkul','kelas_nama','prodi','dosen','dosen2','periode', 'mahasiswa','semester','id_jadwal' ,'jam_mulai', 'jam_akhir', 'tanggal', 'materi_kuliah', 'pertemuan', 'mata_kuliah', 'keterangan','total_hadir','total_alfa','total_mahasiswa'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_absen' => 'Id Absen',
            'dosen' => 'Dosen 1',
            'dosen2'=>'Dosen 2',
            'mahasiswa' => 'Mahasiswa',
            'jam_mulai' => 'Jam Mulai',
            'jam_akhir' => 'Jam Akhir',
            'tanggal' => 'Tanggal',
            'materi_kuliah' => 'Materi Kuliah',
            'pertemuan' => 'Pertemuan',
            'mata_kuliah' => 'Mata Kuliah',
            'status_hadir' => 'Status Hadir',
            'keterangan' => 'Keterangan',
            'semester'=>'Periode',
            'total_hadir'=>'Total Hadir',
            'total_alfa'=>'Total Alfa',
            'total_mahasiswa'=>'total_mahasiswa',
            'id_jadwal'=>'Id jadwal',
            'periode'=>'Semester',
            'prodi'=>'Prodi',
            'kelas_nama'=>'Kelas Nama',
            'kode_matkul'=>'Kode Matkul'
        ];
    }
     public function getIdDosen1()
    {
        return $this->hasOne(Dosen::className(), ['id_dosen' => 'dosen']);
    }
     public function getIdDosen2()
    {
        return $this->hasOne(Dosen::className(), ['id_dosen' => 'dosen2']);
    }
      public function getIdMatakuliah()
    {
        return $this->hasOne(MataKuliah::className(), ['id_matkul' => 'mata_kuliah']);
    }
     public static function viewabsen($tglAwal,$tglAkhir,$id)
    {
        
        $model = self::find();
      // $model->andWhere(['idkonfirmasi'=>$id])->one();
      $model->andWhere(['id_jadwal'=>$id])->andWhere(['between','tanggal', $tglAwal,$tglAkhir]);
          

        return $model->each();
        
    }
    
     public static function viewmahasiswa($periode,$prodi, $mata_kuliah, $kelas_nama,$tglAwal, $tglAkhir,$id)
    {
        
        $model = self::find();
      // $model->andWhere(['idkonfirmasi'=>$id])->one();
      $model->andWhere(['between','tanggal', $tglAwal,$tglAkhir])->andWhere(['status'=>1])
              ->andWhere(['id_jadwal'=>$id])
              ->andFilterWhere(['=', 'periode', $periode]) 
              ->andFilterWhere(['=', 'prodi', $prodi]) 
           
               ->andFilterWhere(['=', 'mata_kuliah', $mata_kuliah]) 
              ->andFilterWhere(['=', 'kelas_nama', $kelas_nama]) ;
              

        return $model->each();
        
    }
}
