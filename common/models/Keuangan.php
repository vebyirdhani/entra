<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "keuangan".
 *
 * @property integer $id_keuangan
 * @property string $keterangan
 * @property string $nama
 * @property string $harga
 * @property string $created_at
 * @property string $project
 */
class Keuangan extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'keuangan';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['keterangan', 'nama', 'harga', 'created_at', 'project'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_keuangan' => 'Id Keuangan',
            'keterangan' => 'Keterangan',
            'nama' => 'Nama',
            'harga' => 'Harga',
            'created_at' => 'Tanggal',
            'project' => 'Project',
        ];
    }
             public static function viewlabarugi($tglAwal,$tglAkhir,$id)
    {
        
        $model = self::find();
      // $model->andWhere(['idkonfirmasi'=>$id])->one();
      $model->andWhere(['project'=>$id])
           
            ->andWhere(['between','created_at', $tglAwal,$tglAkhir]);
          

        return $model->each();
        
    }
}
