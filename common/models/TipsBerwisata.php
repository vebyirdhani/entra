<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tips_berwisata".
 *
 * @property integer $id_tips
 * @property string $judul
 * @property string $deskripsi
 * @property string $foto
 * @property string $created_at
 * @property string $created_by
 */
class TipsBerwisata extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tips_berwisata';
    }

    /**
     * @inheritdoc
     */
    public $picture;
    public function rules()
    {
        return [
            [['id_tips', 'judul', 'deskripsi'], 'required'],
            [['id_tips'], 'integer'],
            [['deskripsi'], 'string'],
            [['judul', 'foto', 'created_at', 'created_by'], 'string', 'max' => 255],
                [['picture'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpeg,jpg','maxSize'=>2048*1024],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_tips' => 'Id Tips',
            'judul' => 'Judul',
            'deskripsi' => 'Deskripsi',
            'foto' => 'Foto',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
        ];
    }
}
