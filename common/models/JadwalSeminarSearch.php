<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\JadwalSeminar;

/**
 * JadwalSeminarSearch represents the model behind the search form about `\common\models\JadwalSeminar`.
 */
class JadwalSeminarSearch extends JadwalSeminar
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_jadwal'], 'integer'],
            [['jadwal_awal', 'jadwal_akhir', 'kategori'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = JadwalSeminar::find()->orderBy('id_jadwal DESC');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_jadwal' => $this->id_jadwal,
        ]);

        $query->andFilterWhere(['like', 'jadwal_awal', $this->jadwal_awal])
            ->andFilterWhere(['like', 'jadwal_akhir', $this->jadwal_akhir])
            ->andFilterWhere(['like', 'kategori', $this->kategori]);

        return $dataProvider;
    }
}
