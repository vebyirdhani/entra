<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "foto_accommodation".
 *
 * @property integer $id_foto
 * @property string $foto
 * @property integer $id_homestay
 */
class FotoAccommodation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'foto_accommodation';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_homestay'], 'required'],
            [['id_homestay'], 'integer'],
            [['foto'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_foto' => 'Id Foto',
            'foto' => 'Foto',
            'id_homestay' => 'Id Homestay',
        ];
    }
}
