<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "foto_desatraveling".
 *
 * @property integer $id_foto
 * @property string $id_desa
 * @property string $foto
 */
class FotoDesatraveling extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'foto_desatraveling';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_desa', 'foto'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_foto' => 'Id Foto',
            'id_desa' => 'Id Desa',
            'foto' => 'Foto',
        ];
    }
}
