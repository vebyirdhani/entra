<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "data_seminar".
 *
 * @property integer $id_data
 * @property string $kategori
 * @property string $judul
 * @property string $dosen_pembimbing1
 * @property string $dosen_pembimbing2
 * @property string $penguji1
 * @property string $penguji2
 * @property string $penguji3
 * @property string $penguji4
 * @property string $mahasiswa
 * @property string $tanggal_seminar
 * @property string $id_jadwal
 * @property string $jadwal_mulai
 * @property string $jadwal_selesai
 */
class DataSeminar extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'data_seminar';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['kategori', 'judul', 'dosen_pembimbing1', 'mahasiswa', 'tanggal_seminar'], 'required'],
           // [['tanggal_seminar'], 'safe'],
            [['nama','nim','status','tanggal_seminar','kategori', 'judul', 'dosen_pembimbing1', 'dosen_pembimbing2', 'penguji1', 'penguji2', 'penguji3', 'penguji4', 'mahasiswa', 'id_jadwal', 'jadwal_mulai', 'jadwal_selesai'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_data' => 'Id Data',
            'kategori' => 'Kategori',
            'judul' => 'Judul',
            'dosen_pembimbing1' => 'Dosen Pembimbing1',
            'dosen_pembimbing2' => 'Dosen Pembimbing2',
            'penguji1' => 'Penguji1',
            'penguji2' => 'Penguji2',
            'penguji3' => 'Penguji3',
            'penguji4' => 'Penguji4',
            'mahasiswa' => 'Mahasiswa',
            'tanggal_seminar' => 'Tanggal Seminar',
            'id_jadwal' => 'Id Jadwal',
            'jadwal_mulai' => 'Jam Mulai',
            'jadwal_selesai' => 'Jam Selesai',
            'status'=>'Status',
             'kategoriid.kategori' => 'Kategori',
            'dosenpemb1.nama' => 'Dosen Pembimbing 1',
             'dosenpemb2.nama' => 'Dosen Pembimbing 2',
              'dosenpeng1.nama' => 'Dosen Penguji 1',
               'dosenpeng2.nama' => 'Dosen Penguji 2',
                'dosenpeng3.nama' => 'Dosen Penguji 3',
                 'dosenpeng4.nama' => 'Dosen Penguji 4',
                  'mahasiswaid.nama' => 'Mahasiswa',
				  'nama'=>'Nama',
				  'nim'=>'NIM',

        ];
    }
      public function getIdDosen1()
    {
        return $this->hasOne(Dosen::className(), ['id_dosen' => 'dosen_pembimbing1']);
    }
    
     public function getIdDosen2()
    {
        return $this->hasOne(Dosen::className(), ['id_dosen' => 'dosen_pembimbing2']);
    }
    
      public function getIdPenguji4()
    {
        return $this->hasOne(Dosen::className(), ['id_dosen' => 'penguji4']);
    }
      public function getIdPenguji1()
    {
        return $this->hasOne(Dosen::className(), ['id_dosen' => 'penguji1']);
    }
      public function getIdPenguji2()
    {
        return $this->hasOne(Dosen::className(), ['id_dosen' => 'penguji2']);
    }
      public function getIdPenguji3()
    {
        return $this->hasOne(Dosen::className(), ['id_dosen' => 'penguji3']);
    }
      public function getIdKategori()
    {
        return $this->hasOne(KategoriSeminar::className(), ['id_kategori' => 'kategori']);
    }
     public function getIdMahasiswa()
    {
        return $this->hasOne(Mahasiswa::className(), ['id_mahasiswa' => 'mahasiswa']);
    }
     public function getKategoriid(){
        return KategoriSeminar::find()
                ->where(['id_kategori' => $this->kategori])
                ->one();
    }
      public function getDosenpemb1(){
        return Dosen::find()
                ->where(['id_dosen' => $this->dosen_pembimbing1])
                ->one();
    }
      public function getDosenpemb2(){
        return Dosen::find()
                ->where(['id_dosen' => $this->dosen_pembimbing2])
                ->one();
    }
     public function getDosenpeng1(){
        return Dosen::find()
                ->where(['id_dosen' => $this->penguji1])
                ->one();
    }
    
     public function getDosenpeng2(){
        return Dosen::find()
                ->where(['id_dosen' => $this->penguji2])
                ->one();
    }
    
     public function getDosenpeng3(){
        return Dosen::find()
                ->where(['id_dosen' => $this->penguji3])
                ->one();
    }
    
      public function getDosenpeng4(){
        return Dosen::find()
                ->where(['id_dosen' => $this->penguji4])
                ->one();
    }
     public function getMahasiswaid(){
        return Mahasiswa::find()
                ->where(['id_mahasiswa' => $this->mahasiswa])
                ->one();
    }
    
}
