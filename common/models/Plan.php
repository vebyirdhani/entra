<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "plan".
 *
 * @property integer $id_plan
 * @property integer $id_user
 * @property string $tanggal_awal
 * @property string $tanggal_akhir
 * @property string $judul
 * @property string $status
 * @property string $created_at
 */
class Plan extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'plan';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_user', 'tanggal_awal', 'tanggal_akhir', 'judul', 'status', 'created_at'], 'required'],
            [['id_user'], 'integer'],
            [['tanggal_awal', 'tanggal_akhir', 'judul', 'status', 'created_at','foto','activity','list_plan','total_pembayaran'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_plan' => 'Id Plan',
            'id_user' => 'Id User',
            'tanggal_awal' => 'Tanggal Awal',
            'tanggal_akhir' => 'Tanggal Akhir',
            'judul' => 'Judul',
            'status' => 'Status',
            'created_at' => 'Created At',
            'foto'=>'Foto',
        ];
    }
}
