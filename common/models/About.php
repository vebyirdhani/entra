<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "about".
 *
 * @property integer $id_about
 * @property string $judul
 * @property string $deskripsi
 * @property string $foto
 */
class About extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'about';
    }

    /**
     * @inheritdoc
     */
    public $picture;
    public function rules()
    {
        return [
            [['judul', 'deskripsi'], 'required'],
            [['deskripsi'], 'string'],
            [['judul', 'foto'], 'string', 'max' => 255],
             [['picture'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpeg,jpg','maxSize'=>2048*1024],
       
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_about' => 'Id About',
            'judul' => 'Judul',
            'deskripsi' => 'Deskripsi',
            'foto' => 'Foto',
        ];
    }
}
