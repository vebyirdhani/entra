<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "fasilitas_transport".
 *
 * @property integer $id_fasilitas
 * @property integer $id_transportasi
 * @property string $nama
 */
class FasilitasTransport extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'fasilitas_transport';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_transportasi', 'nama'], 'required'],
            [['id_transportasi'], 'integer'],
            [['nama'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_fasilitas' => 'Id Fasilitas',
            'id_transportasi' => 'Id Transportasi',
            'nama' => 'Nama',
        ];
    }
}
