<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\DataSeminar;

/**
 * DataSeminarSearch represents the model behind the search form about `\common\models\DataSeminar`.
 */
class DataSeminarSearch extends DataSeminar
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_data'], 'integer'],
            [['kategori', 'judul', 'dosen_pembimbing1', 'dosen_pembimbing2', 'mahasiswa', 'tanggal_seminar'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = DataSeminar::find()->orderBy('id_data DESC');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_data' => $this->id_data,
        ]);

        $query->andFilterWhere(['like', 'kategori', $this->kategori])
            ->andFilterWhere(['like', 'judul', $this->judul])
            ->andFilterWhere(['like', 'dosen_pembimbing1', $this->dosen_pembimbing1])
            ->andFilterWhere(['like', 'dosen_pembimbing2', $this->dosen_pembimbing2])
            ->andFilterWhere(['like', 'mahasiswa', $this->mahasiswa])
            ->andFilterWhere(['like', 'tanggal_seminar', $this->tanggal_seminar]);

        return $dataProvider;
    }
}
