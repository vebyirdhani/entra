<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "accommodation".
 *
 * @property integer $id_homestay
 * @property integer $id_kategori
 * @property string $nama_homestay
 * @property integer $id_user
 * @property integer $id_desa
 * @property integer $id_provinsi
 */
class Accommodation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'accommodation';
    }

    /**
     * @inheritdoc
     */
    public $picture;
    public function rules()
    {
        return [
            [['id_kategori'], 'required'],
            [['id_user', 'id_provinsi','id_pulau','harga'], 'integer'],
            [['nama_homestay','rating','id_kategori','id_desa','id_kecamatan','gallery'], 'string', 'max' => 255],
               [['picture'],'file','maxFiles'=>5,'maxSize'=>2048*1024],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_homestay' => 'Id',
            'id_kategori' => 'Kategori',
            'nama_homestay' => 'Nama',
            'id_user' => 'Id User',
            'id_desa' => 'Desa',
            'id_provinsi' => 'Provinsi',
            'rating'=>'Rating',
            'harga'=>'Harga',
            'id_kecamatan'=>'Id Kecamatan'
        ];
    }
     public function getDesa(){
        return Villages::find()
                ->where(['id' => $this->id_desa])
                ->one();
    }
      public function getProvinsii(){
        return Provinces::find()
                ->where(['id' => $this->id_provinsi])
                ->one();
    }
}
