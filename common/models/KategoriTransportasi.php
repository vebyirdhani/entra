<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "kategori_transportasi".
 *
 * @property integer $id_kategori
 * @property string $nama
 */
class KategoriTransportasi extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'kategori_transportasi';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nama'], 'required'],
            [['nama'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_kategori' => 'Id Kategori',
            'nama' => 'Nama',
        ];
    }
}
