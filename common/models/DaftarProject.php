<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "daftar_project".
 *
 * @property integer $id_project
 * @property string $nama_project
 */
class DaftarProject extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'daftar_project';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nama_project'], 'required'],
            [['nama_project'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_project' => 'Id Project',
            'nama_project' => 'Nama Project',
        ];
    }
}
