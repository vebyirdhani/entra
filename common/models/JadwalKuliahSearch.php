<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\JadwalKuliah;

/**
 * JadwalKuliahSearch represents the model behind the search form about `\common\models\JadwalKuliah`.
 */
class JadwalKuliahSearch extends JadwalKuliah
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_jadwal'], 'integer'],
            [['kelas_nama', 'mata_kuliah', 'dosen','dosen2', 'jadwal_awal', 'jadwal_akhir', 'jumlah_mhs', 'pertemuan','semester','periode','prodi'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = JadwalKuliah::find()->orderBy('id_jadwal DESC');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_jadwal' => $this->id_jadwal,
        ]);

        $query->andFilterWhere(['like', 'kelas_nama', $this->kelas_nama])
            ->andFilterWhere(['like', 'mata_kuliah', $this->mata_kuliah])
            ->andFilterWhere(['like', 'dosen', $this->dosen])
            ->andFilterWhere(['like', 'jadwal_awal', $this->jadwal_awal])
            ->andFilterWhere(['like', 'jadwal_akhir', $this->jadwal_akhir])
            ->andFilterWhere(['like', 'jumlah_mhs', $this->jumlah_mhs])
            ->andFilterWhere(['like', 'pertemuan', $this->pertemuan])
                 ->andFilterWhere(['like', 'semester', $this->semester])
                 ->andFilterWhere(['like', 'prodi', $this->prodi])
                 ->andFilterWhere(['like', 'periode', $this->periode])
             ->andFilterWhere(['like', 'dosen2', $this->dosen2]);

        return $dataProvider;
    }
      public function searchReport($params)
    {
        $query = JadwalKuliah::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_jadwal' => $this->id_jadwal,
        ]);

        $query->andFilterWhere(['like', 'kelas_nama', $this->kelas_nama])
            ->andFilterWhere(['like', 'mata_kuliah', $this->mata_kuliah])
            ->andFilterWhere(['like', 'dosen', $this->dosen])
            ->andFilterWhere(['like', 'jadwal_awal', $this->jadwal_awal])
            ->andFilterWhere(['like', 'jadwal_akhir', $this->jadwal_akhir])
            ->andFilterWhere(['like', 'jumlah_mhs', $this->jumlah_mhs])
            ->andFilterWhere(['like', 'pertemuan', $this->pertemuan])
                 ->andFilterWhere(['like', 'semester', $this->semester])
                 ->andFilterWhere(['like', 'prodi', $this->prodi])
                 ->andFilterWhere(['like', 'periode', $this->periode])
             ->andFilterWhere(['like', 'dosen2', $this->dosen2]);

        return $dataProvider;
    }
}
