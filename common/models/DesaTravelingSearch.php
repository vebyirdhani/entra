<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\DesaTraveling;

/**
 * DesaTravelingSearch represents the model behind the search form about `common\models\DesaTraveling`.
 */
class DesaTravelingSearch extends DesaTraveling
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'id_provinsi', 'id_pulau'], 'integer'],
            [['id_desa', 'id_kecamatan', 'rating', 'nama', 'deskripsi', 'foto', 'alamat'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = DesaTraveling::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'id_provinsi' => $this->id_provinsi,
            'id_pulau' => $this->id_pulau,
        ]);

        $query->andFilterWhere(['like', 'id_desa', $this->id_desa])
            ->andFilterWhere(['like', 'id_kecamatan', $this->id_kecamatan])
            ->andFilterWhere(['like', 'rating', $this->rating])
          
            ->andFilterWhere(['like', 'nama', $this->nama])
            ->andFilterWhere(['like', 'deskripsi', $this->deskripsi])
            ->andFilterWhere(['like', 'foto', $this->foto])
            ->andFilterWhere(['like', 'alamat', $this->alamat]);

        return $dataProvider;
    }
}
