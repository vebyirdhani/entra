<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\FotoTransportasi;

/**
 * FotoTransportasiSearch represents the model behind the search form about `common\models\FotoTransportasi`.
 */
class FotoTransportasiSearch extends FotoTransportasi
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_foto'], 'integer'],
            [['id_transportasi', 'foto'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = FotoTransportasi::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_foto' => $this->id_foto,
        ]);

        $query->andFilterWhere(['like', 'id_transportasi', $this->id_transportasi])
            ->andFilterWhere(['like', 'foto', $this->foto]);

        return $dataProvider;
    }
}
