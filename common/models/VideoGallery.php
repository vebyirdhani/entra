<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "video_gallery".
 *
 * @property integer $id_video
 * @property string $id_videoyoutube
 */
class VideoGallery extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'video_gallery';
    }

    /**
     * @inheritdoc
     */
    public $picture;
    public function rules()
    {
        return [
            [['id_videoyoutube'], 'required'],
            [['id_videoyoutube','judul_video','image_gallery'], 'string', 'max' => 255],
             [['picture'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpeg,jpg','maxSize'=>2048*1024],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_video' => 'Id Video',
            'id_videoyoutube' => 'Id Videoyoutube',
            'judul_video'=>'Judul Video',
            'image_gallery'=>'Image Gallery',
        ];
    }
}
