<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "foto_desa".
 *
 * @property integer $id_foto
 * @property string $foto
 * @property integer $id_desa
 */
class FotoDesa extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'foto_desa';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_desa'], 'required'],
            [['id_desa'], 'integer'],
            [['foto'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_foto' => 'Id Foto',
            'foto' => 'Foto',
            'id_desa' => 'Id Desa',
        ];
    }
}
