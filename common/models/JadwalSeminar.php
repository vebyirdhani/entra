<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "jadwal_seminar".
 *
 * @property integer $id_jadwal
 * @property string $jadwal_awal
 * @property string $jadwal_akhir
 * @property string $kategori
 * @property string $status
 */
class JadwalSeminar extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'jadwal_seminar';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['jadwal_awal', 'jadwal_akhir'], 'required'],
           // [['jadwal_awal', 'jadwal_akhir'], 'safe'],
            [['kategori', 'status','jadwal_awal', 'jadwal_akhir','status'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_jadwal' => 'Id Jadwal',
            'jadwal_awal' => 'Jadwal Awal',
            'jadwal_akhir' => 'Jadwal Akhir',
            'kategori' => 'Kategori',
            'status' => 'Status',
			'kategoriid.kategori'=>'Kategori',
        ];
    }
     public function getIdKategori()
    {
        return $this->hasOne(KategoriSeminar::className(), ['id_kategori' => 'kategori']);
    }
	public function getKategoriid(){
        return KategoriSeminar::find()
                ->where(['id_kategori' => $this->kategori])
                ->one();
    }
	
}
