<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "experience".
 *
 * @property integer $id_experience
 * @property integer $id_kategori
 * @property string $nama
 * @property string $jam_mulai
 * @property string $jam_berakhir
 * @property string $deskripsi
 * @property string $price
 * @property integer $id_user
 * @property integer $id_desa
 */
class Experience extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'experience';
    }

    /**
     * @inheritdoc
     */
    public $picture;
    public function rules()
    {
        return [
            [['id_kategori', 'nama', 'price', 'id_user', 'id_desa'], 'required'],
            [[ 'id_user', 'id_desa','id_pulau','price'], 'integer'],
            [['deskripsi','alamat'], 'string'],
            [['nama','id_kecamatan', 'jam_mulai','id_kategori','id_provinsi', 'jam_berakhir','longitude','latitude','contact','gallery','status'], 'string', 'max' => 255],
                [['picture'],'file','maxFiles'=>5,'maxSize'=>2048*1024],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_experience' => 'Id Experience',
            'id_kategori' => 'Id Kategori',
            'nama' => 'Nama',
            'jam_mulai' => 'Jam Mulai',
            'jam_berakhir' => 'Jam Berakhir',
            'deskripsi' => 'Deskripsi',
            'alamat'=>'Alamat',
            'longitude'=>'Longitude',
            'latitude'=>'Latitdue',
            'price' => 'Price',
            'id_user' => 'Id User',
            'id_desa' => 'Id Desa',
            'contact'=>'Contact',
            'id_kecamatan'=>'Id Kecamatan'

        ];
    }
}
