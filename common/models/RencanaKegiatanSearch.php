<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\RencanaKegiatan;

/**
 * RencanaKegiatanSearch represents the model behind the search form about `common\models\RencanaKegiatan`.
 */
class RencanaKegiatanSearch extends RencanaKegiatan
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_rencanakegiatan', 'id_plan', 'id_desa', 'kategori_pemesanan', 'id_pemesanan'], 'integer'],
            [['jumlah_orang'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = RencanaKegiatan::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_rencanakegiatan' => $this->id_rencanakegiatan,
            'id_plan' => $this->id_plan,
            'id_desa' => $this->id_desa,
            'kategori_pemesanan' => $this->kategori_pemesanan,
            'id_pemesanan' => $this->id_pemesanan,
        ]);

        $query->andFilterWhere(['like', 'jumlah_orang', $this->jumlah_orang]);

        return $dataProvider;
    }
}
