<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "detail_pengeluaran".
 *
 * @property integer $id_detail
 * @property string $jenis_pengeluaran
 * @property string $detail1
 * @property string $detail2
 * @property string $detail3
 * @property string $detail4
 * @property string $detail5
 */
class DetailPengeluaran extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'detail_pengeluaran';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['jenis_pengeluaran', 'detail1', 'detail2', 'detail3', 'detail4', 'detail5'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_detail' => 'Id Detail',
            'jenis_pengeluaran' => 'Jenis Pengeluaran',
            'detail1' => 'Detail1',
            'detail2' => 'Detail2',
            'detail3' => 'Detail3',
            'detail4' => 'Detail4',
            'detail5' => 'Detail5',
        ];
    }
}
