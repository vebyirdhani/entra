<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "mahasiswa".
 *
 * @property integer $id_mahasiswa
 * @property string $nim
 * @property string $nama
 * @property string $jurusan
 * @property string $prodi
 * @property string $angkatan
 */
class Mahasiswa extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mahasiswa';
    }

    /**
     * @inheritdoc
     */
    public $picture;
    public function rules()
    {
        return [
            [['nim', 'nama', 'angkatan'], 'required'],
            [['nim', 'jurusan', 'prodi', 'angkatan'], 'string', 'max' => 20],
            [['nama','semester'], 'string', 'max' => 255],
             [['nim'], 'unique'],
              [['picture'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpeg,jpg','maxSize'=>2048*1024],
     
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_mahasiswa' => 'Id Mahasiswa',
            'nim' => 'Nim',
            'nama' => 'Nama',
            'jurusan' => 'Jurusan',
            'prodi' => 'Prodi',
            'angkatan' => 'Angkatan',
			'prodiid.nama_prodi'=>'Prodi',
        ];
    }
      public function getIdProdi()
    {
        return $this->hasOne(Prodi::className(), ['id_prodi' => 'prodi']);
    }
		public function getProdiid(){
        return Prodi::find()
                ->where(['id_prodi' => $this->prodi])
                ->one();
    }
}
