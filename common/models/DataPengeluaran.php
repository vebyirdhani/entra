<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "data_pengeluaran".
 *
 * @property integer $id_pengeluaran
 * @property string $jenis_pengeluaran
 * @property string $detail_pengeluaran
 * @property string $detail1
 * @property string $detail2
 * @property string $detail3
 * @property string $detail4
 * @property string $detail5
 * @property string $jumlah_detail1
 * @property string $jumlah_detail2
 * @property string $jumlah_detail3
 * @property string $jumlah_detail4
 * @property string $jumlah_detail5
 * @property string $project
 */
class DataPengeluaran extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'data_pengeluaran';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['jenis_pengeluaran', 'detail_pengeluaran','harga', 'detail1', 'detail2', 'detail3', 'detail4', 'detail5', 'jumlah_detail1', 'jumlah_detail2', 'jumlah_detail3', 'jumlah_detail4', 'jumlah_detail5', 'project','tanggal','bukti'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_pengeluaran' => 'Id Pengeluaran',
            'jenis_pengeluaran' => 'Jenis Pengeluaran',
            'detail_pengeluaran' => 'Detail Pengeluaran',
            'detail1' => 'Detail1',
            'detail2' => 'Detail2',
            'detail3' => 'Detail3',
            'detail4' => 'Detail4',
            'detail5' => 'Detail5',
            'jumlah_detail1' => 'Jumlah Detail1',
            'jumlah_detail2' => 'Jumlah Detail2',
            'jumlah_detail3' => 'Jumlah Detail3',
            'jumlah_detail4' => 'Jumlah Detail4',
            'jumlah_detail5' => 'Jumlah Detail5',
            'project' => 'Project',
            'harga'=>'Harga',
            'bukti'=>'Bukti',
            'tanggal'=>'Tanggal',
        ];
    }
           public static function viewpengeluaran($tglAwal,$tglAkhir,$id,$nama)
    {
        
        $model = self::find();
      // $model->andWhere(['idkonfirmasi'=>$id])->one();
      $model->andWhere(['project'=>$id])
            ->andWhere(['jenis_pengeluaran'=>$nama])
            ->andWhere(['between','tanggal', $tglAwal,$tglAkhir]);
          

        return $model->each();
        
    }
}
