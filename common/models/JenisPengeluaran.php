<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "jenis_pengeluaran".
 *
 * @property integer $id_pengeluaran
 * @property string $nama
 */
class JenisPengeluaran extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'jenis_pengeluaran';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nama'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_pengeluaran' => 'Id Pengeluaran',
            'nama' => 'Nama',
        ];
    }
}
