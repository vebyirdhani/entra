<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "mata_kuliah".
 *
 * @property integer $id_matkul
 * @property string $nama
 */
class MataKuliah extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mata_kuliah';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nama'], 'required'],
            [['nama'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_matkul' => 'Id Matkul',
            'nama' => 'Nama',
        ];
    }
}
