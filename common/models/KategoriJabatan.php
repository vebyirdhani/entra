<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "kategori_jabatan".
 *
 * @property integer $id_kategori
 * @property string $jabatan
 */
class KategoriJabatan extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'kategori_jabatan';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['jabatan'], 'required'],
            [['jabatan'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_kategori' => 'Id Kategori',
            'jabatan' => 'Jabatan',
        ];
    }
}
