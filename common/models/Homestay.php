<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "homestay".
 *
 * @property integer $id_homestay
 * @property integer $id_kategori
 * @property string $nama_homestay
 * @property integer $id_user
 * @property integer $id_desa
 */
class Homestay extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'homestay';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_kategori'], 'required'],
            [['id_kategori', 'id_user', 'id_desa'], 'integer'],
            [['nama_homestay'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_homestay' => 'Id Homestay',
            'id_kategori' => 'Id Kategori',
            'nama_homestay' => 'Nama Homestay',
            'id_user' => 'Id User',
            'id_desa' => 'Id Desa',
        ];
    }
}
