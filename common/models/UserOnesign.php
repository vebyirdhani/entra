<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "user_onesign".
 *
 * @property integer $id
 * @property integer $id_user
 * @property string $player_id
 * @property string $device
 * @property string $judul
 * @property string $isi
 */
class UserOnesign extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_onesign';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            
            [['id_user'], 'integer'],
            [['isi'], 'string'],
            [['player_id', 'device', 'judul'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_user' => 'Id User',
            'player_id' => 'Player ID',
            'device' => 'Device',
            'judul' => 'Judul',
            'isi' => 'Isi',
        ];
    }
}
