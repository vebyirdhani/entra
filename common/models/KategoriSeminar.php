<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "kategori_seminar".
 *
 * @property integer $id_kategori
 * @property string $kategori
 */
class KategoriSeminar extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'kategori_seminar';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['kategori'], 'required'],
            [['kategori'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_kategori' => 'Id Kategori',
            'kategori' => 'Kategori',
        ];
    }
}
