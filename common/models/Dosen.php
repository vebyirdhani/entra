<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "dosen".
 *
 * @property integer $id_dosen
 * @property string $nama
 * @property string $nidn
 * @property string $nip
 * @property string $jabatan
 * @property string $status_pegawai
 * @property string $tempat_lahir
 * @property string $tanggal_lahir
 * @property string $jenis_kelamin
 * @property string $agama
 * @property string $status_nikah
 */
class Dosen extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'dosen';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nama', 'nidn', 'jabatan', 'status_pegawai', 'tempat_lahir', 'tanggal_lahir', 'jenis_kelamin', 'agama', 'status_nikah','email','prodi'], 'required'],
            [['nama', 'nidn', 'nip', 'jabatan', 'status_pegawai', 'tempat_lahir', 'tanggal_lahir', 'jenis_kelamin', 'agama', 'status_nikah','email','prodi','jurusan'], 'string', 'max' => 255],
          [['nidn'],'unique','message'=>'Email already exist. Please try another one.'],
            ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_dosen' => 'Id Dosen',
            'nama' => 'Nama',
            'nidn' => 'Nidn',
            'nip' => 'Nip',
            'jabatan' => 'Jabatan',
            'status_pegawai' => 'Status Pegawai',
            'tempat_lahir' => 'Tempat Lahir',
            'tanggal_lahir' => 'Tanggal Lahir',
            'jenis_kelamin' => 'Jenis Kelamin',
            'agama' => 'Agama',
            'status_nikah' => 'Status Nikah',
            'email'=>'Email',
            'prodi'=>'Prodi',
            'jurusan'=>'Jurusan',
        ];
    }
}
