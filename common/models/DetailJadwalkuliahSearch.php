<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\DetailJadwalkuliah;

/**
 * DetailJadwalkuliahSearch represents the model behind the search form about `\common\models\DetailJadwalkuliah`.
 */
class DetailJadwalkuliahSearch extends DetailJadwalkuliah
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_detail'], 'integer'],
            [['nama_matkul', 'jam_mulai', 'jam_akhir', 'mahasiswa', 'dosen','dosen2'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = DetailJadwalkuliah::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_detail' => $this->id_detail,
        ]);

        $query->andFilterWhere(['like', 'nama_matkul', $this->nama_matkul])
            ->andFilterWhere(['like', 'jam_mulai', $this->jam_mulai])
            ->andFilterWhere(['like', 'jam_akhir', $this->jam_akhir])
            ->andFilterWhere(['like', 'mahasiswa', $this->mahasiswa])
            ->andFilterWhere(['like', 'dosen', $this->dosen])
                 ->andFilterWhere(['like', 'dosen2', $this->dosen2]);

        return $dataProvider;
    }
}
