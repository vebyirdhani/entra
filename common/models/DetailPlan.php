<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "detail_plan".
 *
 * @property integer $id_detail
 * @property integer $id_plan
 * @property string $kategori
 * @property string $tanggal_awal
 * @property string $tanggal_akhir
 * @property string $jumlah_orang
 * @property string $catatan
 * @property string $harga
 */
class DetailPlan extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'detail_plan';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_plan'], 'integer'],
            [['kategori', 'tanggal_awal', 'tanggal_akhir', 'jumlah_orang', 'catatan', 'harga','id_user','id_data','biaya'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_detail' => 'Id Detail',
            'id_plan' => 'Id Plan',
            'kategori' => 'Kategori',
            'tanggal_awal' => 'Tanggal Awal',
            'tanggal_akhir' => 'Tanggal Akhir',
            'jumlah_orang' => 'Jumlah Orang',
            'catatan' => 'Catatan',
            'harga' => 'Harga',
        ];
    }
}
