<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "operational".
 *
 * @property integer $id_operational
 * @property string $total_room
 * @property string $checkin
 * @property string $checkout
 * @property string $status
 * @property string $harga
 * @property string $id_akomodasi
 */
class Operational extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'operational';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['total_room', 'checkin', 'checkout', 'status', 'harga', 'id_akomodasi'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_operational' => 'Id Operational',
            'total_room' => 'Total Room',
            'checkin' => 'Checkin',
            'checkout' => 'Checkout',
            'status' => 'Status',
            'harga' => 'Harga',
            'id_akomodasi' => 'Id Akomodasi',
        ];
    }
}
