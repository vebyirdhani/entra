<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "merchandise".
 *
 * @property integer $id_merchandise
 * @property string $nama
 * @property string $deskripsi
 * @property string $alamat
 * @property string $harga
 * @property integer $id_user
 * @property integer $id_desa
 */
class Merchandise extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'merchandise';
    }

    /**
     * @inheritdoc
     */
    public $picture;
    public function rules()
    {
        return [
            [['nama', 'harga', 'id_user', 'id_desa'], 'required'],
            [['deskripsi', 'alamat'], 'string'],
            [['id_user', 'id_desa','id_pulau'], 'integer'],
            [['nama', 'harga','kategori','id_kecamatan','id_provinsi','gallery','latitude','longitude','rating'], 'string', 'max' => 255],
              [['picture'],'file','maxFiles'=>5,'maxSize'=>2048*1024],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_merchandise' => 'Id Merchandise',
            'nama' => 'Nama',
            'deskripsi' => 'Deskripsi',
            'alamat' => 'Alamat',
            'harga' => 'Harga',
            'id_user' => 'Id User',
            'id_desa' => 'Desa',
             'kategori' => 'Kategori',
            'id_provinsi'=>'Provinsi',
            'id_kecamatan'=>'Kecamatan'
        ];
    }
     public function getDesa(){
        return Villages::find()
                ->where(['id' => $this->id_desa])
                ->one();
    }
}
