<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Transportation;

/**
 * TransportationSearch represents the model behind the search form about `common\models\Transportation`.
 */
class TransportationSearch extends Transportation
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_transportation', 'id_user', 'id_desa'], 'integer'],
            [['nama', 'rute', 'jenis_transportasi', 'price'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Transportation::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_transportation' => $this->id_transportation,
            'id_user' => $this->id_user,
            'id_desa' => $this->id_desa,
        ]);

        $query->andFilterWhere(['like', 'nama', $this->nama])
            ->andFilterWhere(['like', 'rute', $this->rute])
            ->andFilterWhere(['like', 'jenis_transportasi', $this->jenis_transportasi])
            ->andFilterWhere(['like', 'price', $this->price]);

        return $dataProvider;
    }
}
