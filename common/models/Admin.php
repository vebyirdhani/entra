<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "admin".
 *
 * @property integer $id_admin
 * @property string $nama_admin
 * @property integer $tanggal_lahir
 * @property string $tempat_lahir
 * @property string $alamat
 * @property integer $id_user
 * @property integer $level
 *
 * @property User $idUser
 */
class Admin extends \yii\db\ActiveRecord
{
    public $foto;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'admin';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nama_admin', 'tanggal_lahir', 'tempat_lahir', 'alamat', 'id_user', 'level'], 'required'],
            [['tanggal_lahir', 'id_user', 'level'], 'safe'],
            [['nama_admin', 'tempat_lahir', 'alamat'], 'string', 'max' => 255],
            [['id_user'], 'unique'],
            [['id_user'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['id_user' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_admin' => 'Id Admin',
            'nama_admin' => 'Nama Admin',
            'tanggal_lahir' => 'Tanggal Lahir',
            'tempat_lahir' => 'Tempat Lahir',
            'alamat' => 'Alamat',
            'id_user' => 'Id User',
            'level' => 'Level',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdUser()
    {
        return $this->hasOne(User::className(), ['id' => 'id_user']);
    }
}
