<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "slider".
 *
 * @property integer $id_slider
 * @property string $judul
 * @property string $deskripsi
 * @property string $foto
 * @property string $status
 */
class Slider extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'slider';
    }

    /**
     * @inheritdoc
     */
    public $picture;
    public function rules()
    {
        return [
            [['judul', 'foto', 'status'], 'required'],
            [['deskripsi'], 'string'],
            [['judul', 'foto', 'status'], 'string', 'max' => 255],
             [['picture'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpeg,jpg','maxSize'=>2048*1024],
       
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_slider' => 'Id Slider',
            'judul' => 'Judul',
            'deskripsi' => 'Deskripsi',
            'foto' => 'Foto',
            'status' => 'Status',
        ];
    }
}
