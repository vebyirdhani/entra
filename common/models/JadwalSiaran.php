<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "jadwal_siaran".
 *
 * @property integer $id_siaran
 * @property string $nama_siaran
 * @property string $jadwal_siaran
 * @property string $rating
 * @property string $set_alarm
 * @property string $foto_siaran
 */
class JadwalSiaran extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'jadwal_siaran';
    }

    /**
     * @inheritdoc
     */
    public $picture;
    public function rules()
    {
        return [
            [['nama_siaran'], 'required'],
            
           [['deskripsi'], 'string'],
            [['nama_siaran', 'waktu','hari', 'rating', 'set_alarm', 'foto_siaran','total_rating','jumlah_orang'], 'string', 'max' => 255],
          [['minggu'],'string','max'=>500],
              [['picture'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpeg,jpg','maxSize'=>2048*1024],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_siaran' => 'Id Siaran',
            'nama_siaran' => 'Nama Siaran',
            'waktu' => 'Waktu',
            'rating' => 'Rating',
            'set_alarm' => 'Set Alarm',
            'foto_siaran' => 'Foto Siaran',
          	'hari'=>'Hari',
            'jumlah_orang'=>'Jumlah Orang',
            'total_rating'=>'Total Rating',
       
        ];
    }
}
