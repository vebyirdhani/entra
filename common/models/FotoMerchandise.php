<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "foto_merchandise".
 *
 * @property integer $id_foto
 * @property string $foto
 * @property integer $id_merchandise
 */
class FotoMerchandise extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'foto_merchandise';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_merchandise'], 'required'],
            [['id_merchandise'], 'integer'],
            [['foto'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_foto' => 'Id Foto',
            'foto' => 'Foto',
            'id_merchandise' => 'Id Merchandise',
        ];
    }
}
