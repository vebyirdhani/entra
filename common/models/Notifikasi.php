<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "notifikasi".
 *
 * @property integer $id_notif
 * @property string $judul
 * @property string $isi
 */
class Notifikasi extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'notifikasi';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['isi'], 'string'],
            [['judul'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_notif' => 'Id Notif',
            'judul' => 'Judul',
            'isi' => 'Isi',
        ];
    }
}
