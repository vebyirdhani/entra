<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "kategori_merchandise".
 *
 * @property integer $id_kategori
 * @property string $nama
 */
class KategoriMerchandise extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'kategori_merchandise';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nama'], 'required'],
            [['nama'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_kategori' => 'Id Kategori',
            'nama' => 'Nama',
        ];
    }
}
