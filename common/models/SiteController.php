<?php
namespace frontend\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
            
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }
    
    public function actionError() {

        $exception = Yii::$app->errorHandler->exception;
        if ($exception !== null) {
            if (Yii::$app->user->id) {
                $this->layout = 'main';
            } else {
                $this->layout = 'error';
            }
            return $this->render('error', ['exception' => $exception]);
        }
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $slider = \common\models\Slider::find()->where(['status'=>1])->all();
        $kategori = \common\models\Kategori::find()->where(['status'=>1])->orderBy('kategori ASC')->all();
        $desa = \common\models\DesaTraveling::find()->orderBy('nama ASC')->limit(12)->all();
        $fitur = \common\models\Fitur::find()->all();
        $blog = \common\models\TipsBerwisata::find()->limit(2)->all();
        return $this->render('index',[
            'slider'=>$slider,
            'kategori'=>$kategori,
            'desa'=>$desa,
            'fitur'=>$fitur,
            'blog'=>$blog,
        ]);
    }
    
     public function actionAbout()
    {
       $team = \common\models\Team::find()->all();
       $about = \common\models\About::find()->orderBy('id_about DESC')->one();
        return $this->render('about',[
            'team'=>$team,
            'about'=>$about,
          
        ]);
    }

     public function actionBlog()
    {
        $blog = \common\models\TipsBerwisata::find()->all();
    
        return $this->render('blog',[
        
          'blog'=>$blog,
        ]);
    }

      public function actionDetailBlog($id)
    {
        $blog = \common\models\TipsBerwisata::find()->where(['id_tips'=>$id])->one();
    
        return $this->render('detail-blog',[
        
          'blog'=>$blog,
        ]);
    }


     public function actionAccommodation()
    {
      
             $data = \common\models\Accommodation::find()->all();
              return $this->render('accommodation',[
            'data'=>$data,
           
          
        ]); 
         
      
    }

      public function actionExperience()
    {
      
             $data = \common\models\Experience::find()->all();
              return $this->render('experience',[
            'data'=>$data,
           
          
        ]); 
         
      
    }
    
      public function actionTransportation()
    {
      
             $data = \common\models\Transportation::find()->all();
              return $this->render('transportation',[
            'data'=>$data,
           
          
        ]); 
         
      
    }
      public function actionMerchandise()
    {
      
             $data = \common\models\Merchandise::find()->all();
              return $this->render('merchandise',[
            'data'=>$data,
           
          
        ]); 
         
      
    }
    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {
        return $this->redirect(['/user/login']);
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return mixed
     */
    public function actionContact()
    {
        $contact = \common\models\Contact::find()->where(['status'=>'Aktif'])->one();
         $komentar = new \common\models\Komentar;
           $komentar->created_at = date('d-M-Y H:i:s');
                  
                    $komentar->status = 0;
                   
         if ($komentar->load(Yii::$app->request->post()) && $komentar->save(false)) {
           
                Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');
             
             return $this->refresh();
        } else {
            return $this->render('contact', [
                'komentar' => $komentar,
                'contact'=>$contact,
            ]);
        }
    }

    /**
     * Displays about page.
     *
     * @return mixed
     */
   
  

    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup()
    {
        
        $model = new \common\models\SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                if (Yii::$app->getUser()->login($user)) {
                    return $this->goHome();
                }
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for the provided email address.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }
}
