<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "contact".
 *
 * @property integer $id_contact
 * @property string $address
 * @property string $phone
 * @property string $email
 * @property string $twitter
 * @property string $instagram
 * @property string $facebook
 * @property string $status
 */
class Contact extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'contact';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['address'], 'string'],
            [['phone', 'email', 'status'], 'required'],
            [['phone', 'email', 'twitter', 'instagram', 'facebook', 'status'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_contact' => 'Id Contact',
            'address' => 'Address',
            'phone' => 'Phone',
            'email' => 'Email',
            'twitter' => 'Twitter',
            'instagram' => 'Instagram',
            'facebook' => 'Facebook',
            'status' => 'Status',
        ];
    }
}
