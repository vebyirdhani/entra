<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "rating".
 *
 * @property integer $id_rating
 * @property integer $user
 * @property string $rating
 */
class Rating extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'rating';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user', 'rating'], 'required'],
            [['user'], 'integer'],
            [['rating','siaran'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_rating' => 'Id Rating',
            'user' => 'User',
            'rating' => 'Rating',
            'siaran'=>'Siaran',
        ];
    }
}
