<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "agama".
 *
 * @property integer $id_agama
 * @property string $agama
 */
class Agama extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'agama';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['agama'], 'required'],
            [['agama'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_agama' => 'Id Agama',
            'agama' => 'Agama',
        ];
    }
}
