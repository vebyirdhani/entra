<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "semester".
 *
 * @property integer $id_semester
 * @property string $semester
 */
class Semester extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'semester';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['semester','tahun'], 'required'],
            [['semester','tahun','keterangan'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_semester' => 'Id Semester',
            'semester' => 'Semester',
            'tahun'=>'Tahun Ajaran',
            'keterangan'=>'Keterangan'
        ];
    }
}
