<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "detail_jadwalkuliah".
 *
 * @property integer $id_detail
 * @property string $nama_matkul
 * @property string $jam_mulai
 * @property string $jam_akhir
 * @property string $mahasiswa
 * @property string $dosen
 */
class DetailJadwalkuliah extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'detail_jadwalkuliah';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
         //   [['nama_matkul', 'jam_mulai', 'jam_akhir', 'mahasiswa', 'dosen'], 'required'],
            [['nama_matkul', 'jam_mulai', 'jam_akhir', 'mahasiswa', 'dosen','dosen2','angkatan','nim','nama','semester'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_detail' => 'Id Detail',
            'nama_matkul' => 'Mata Kuliah',
            'jam_mulai' => 'Jam Mulai',
            'jam_akhir' => 'Jam Akhir',
            'mahasiswa' => 'Mahasiswa',
            'dosen' => 'Dosen 1',
            'dosen2'=>'Dosen 2',
            'nama'=>'Nama',
            'nim'=>'NIM',
			'semester'=>'Semester',
        ];
    }
     public function getIdDosen1()
    {
        return $this->hasOne(Dosen::className(), ['id_dosen' => 'dosen']);
    }
     public function getIdDosen2()
    {
        return $this->hasOne(Dosen::className(), ['id_dosen' => 'dosen2']);
    }
      public function getIdMatakuliah()
    {
        return $this->hasOne(MataKuliah::className(), ['id_matkul' => 'nama_matkul']);
    }
      public function getIdMahasiswa()
    {
        return $this->hasOne(Mahasiswa::className(), ['id_mahasiswa' => 'mahasiswa']);
    }
      public function getIdProdi()
    {
        return $this->hasOne(Prodi::className(), ['id_prodi' => 'prodi']);
    }
}
