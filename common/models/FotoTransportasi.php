<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "foto_transportasi".
 *
 * @property integer $id_foto
 * @property string $id_transportasi
 * @property string $foto
 */
class FotoTransportasi extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'foto_transportasi';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_transportasi'], 'required'],
            [['id_transportasi', 'foto'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_foto' => 'Id Foto',
            'id_transportasi' => 'Id Transportasi',
            'foto' => 'Foto',
        ];
    }
}
