<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "search_fom".
 *
 * @property integer $id_search
 * @property string $keyword
 * @property integer $id_user
 */
class SearchFom extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'search_fom';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['keyword', 'id_user'], 'required'],
            [['id_user'], 'integer'],
            [['keyword'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_search' => 'Id Search',
            'keyword' => 'Keyword',
            'id_user' => 'Id User',
        ];
    }
}
