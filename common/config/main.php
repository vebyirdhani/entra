<?php

return [
    'name' => 'Xpd',
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
//    'as access' => [
//     'class' => '\hscstudio\mimin\components\AccessControl',
//	 'allowActions' => [
//		// add wildcard allowed action here!
//		'site/*',
//		'debug/*',
//		'mimin/*', // only in dev mode
//                'rbac/*',
//                'service/*',
//                'komentar/*',
//                'gii/*'
//	],
//],
    'modules' => [
        'mimin' => [
		'class' => '\hscstudio\mimin\Module',
	],
        'gridview' => [
            'class' => 'kartik\grid\Module'
        ],
        'redactor' => [
            'class' => 'yii\redactor\RedactorModule',
            'uploadDir' => '@webroot/uploads',
            'uploadUrl' => '@web/uploads',
            'imageAllowExtensions' => ['jpg', 'png', 'gif']
        ],
        'user' => [
            'class' => 'dektrium\user\Module',
            'enableUnconfirmedLogin' => true,
            'confirmWithin' => 21600,
            'cost' => 12,
            'modelMap' => [
//                    'User' => 'dektrium\user\Module',
              ],
            'admins' => ['akatrust'],
        ],
         'social' => [
        // the module class
        'class' => 'kartik\social\Module',
 
        // the global settings for the Disqus widget
        'disqus' => [
            'settings' => ['shortname' => 'DISQUS_SHORTNAME'] // default settings
        ],
 
        // the global settings for the Facebook plugins widget
        'facebook' => [
            'appId' => 'FACEBOOK_APP_ID',
            'secret' => 'FACEBOOK_APP_SECRET',
        ],
 
        // the global settings for the Google+ Plugins widget
        'google' => [
            'clientId' => 'GOOGLE_API_CLIENT_ID',
            'pageId' => 'GOOGLE_PLUS_PAGE_ID',
            'profileId' => 'GOOGLE_PLUS_PROFILE_ID',
        ],
 
        // the global settings for the Google Analytics plugin widget
        'googleAnalytics' => [
            'id' => 'TRACKING_ID',
            'domain' => 'TRACKING_DOMAIN',
        ],
 
        // the global settings for the Twitter plugin widget
        'twitter' => [
            'screenName' => 'TWITTER_SCREEN_NAME'
        ],
 
        // the global settings for the GitHub plugin widget
        'github' => [
            'settings' => ['user' => 'GITHUB_USER', 'repo' => 'GITHUB_REPO']
        ],
    ],
    // your other modules
        'rbac' => 'dektrium\rbac\RbacWebModule',
    ],
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'authManager' => [
            'class' => 'dektrium\rbac\components\DbManager',
        ],
       'reCaptcha' => [
 
       'name' => 'reCaptcha',
 
       'class' => 'himiklab\yii2\recaptcha\ReCaptcha',
 
       'siteKey' => '6LfJgoQUAAAAANUXE5zKtV1HfSM68ZprMAba9kzP',
 
       'secret' => '6LfJgoQUAAAAAESfrPq6M0QXPRH5lHZMjaG63Uqx',
 
       ],
           'formatter' => [
            'class' => 'yii\i18n\Formatter',
            //'dateFormat' => 'dd-MM-yyyy',
            'decimalSeparator' => ',',
            'thousandSeparator' => '.',
            'currencyCode' => 'Rp ',
            'nullDisplay' => '',
            'locale' => 'id_ID.utf8',
//            'locale' => 'id_ID',
            'timeZone' => 'Asia/Jakarta',
        ],
    ],
];
