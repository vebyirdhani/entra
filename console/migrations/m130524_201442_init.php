<?php

use yii\db\Migration;

class m130524_201442_init extends Migration
{
    protected $tableOptions;
    protected $restrict = 'RESTRICT';
    protected $cascade = 'CASCADE';
    protected $dbType;
    
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%user}}', [
            'id' => $this->primaryKey(),
            'username' => $this->string()->notNull()->unique(),
            'auth_key' => $this->string(32)->notNull(),
            'password_hash' => $this->string()->notNull(),
            'password_reset_token' => $this->string()->unique(),
            'email' => $this->string()->notNull()->unique(),

            'status' => $this->smallInteger()->notNull()->defaultValue(10),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            
            'photo' => $this->string(255)->null(),
            
            'last_login_at' => $this->integer()->null(),
            'confirmed_at' => $this->integer()->null(),
            'blocked_at' => $this->integer()->null(),
            'registration_ip' => $this->string(255)->null(),
        ], $tableOptions);
        
        $this->createTable('{{%admin}}', [
            'id_admin' => $this->primaryKey(),
            'nama_admin' => $this->string(255)->notNull(),
            'tanggal_lahir' => $this->integer()->notNull(),
            'tempat_lahir' => $this->string(255)->notNull(),
            'alamat' => $this->string(255)->notNull(),
            'id_user' => $this->integer()->notNull()->unique(),
            'level' => $this->integer()->notNull(),
        ], $tableOptions);
        
        $this->createTable('{{%profile}}', [
            'user_id'        => $this->integer()->notNull()->append('PRIMARY KEY'),
            'name'           => $this->string(255)->null(),
            'born_date' => $this->integer()->null(),
            'address' => $this->string(255)->null(),
            'public_email'   => $this->string(255)->null(),
            'photo' => $this->string(255)->null(),
            'gravatar_email' => $this->string(255)->null(),
            'gravatar_id'    => $this->string(32)->null(),
            'location'       => $this->string(255)->null(),
            'website'        => $this->string(255)->null(),
            'bio'            => $this->text()->null(),
            'timezone'            => $this->string(40)->null(),
        ], $this->tableOptions);

        $this->createTable('{{%contacts}}', [
            'id_contact'        => $this->primaryKey(),
            'name' => $this->string(255)->null(),
            'email' => $this->string(255)->null(),
            'subject' => $this->string(255)->null(),
            'body' => $this->text()->null(), 
            'status' => $this->integer()->notNull(),
            'created_at' =>$this->integer()->notNull(),
            ], $this->tableOptions);
           
         $this->createTable('{{%email}}', [
            'id_email'        => $this->primaryKey(),
            'from' => $this->string(255)->null(),
            'to' => $this->string(255)->null(),
            'subject' => $this->string(255)->null(),
            'body' => $this->text()->null(), 
            'file' => $this->string(255)->null(),
            'created_at' =>$this->integer()->null(),
            'status_baca' => $this->integer()->null(),
            'trash' => $this->integer()->null(),
            'bintang'=>$this->integer()->null(),
            ], $this->tableOptions);
         
        $this->addForeignKey('{{%fk_user_profile}}', '{{%profile}}', 'user_id', '{{%user}}', 'id', $this->cascade, $this->restrict);
        
        $this->addForeignKey($tableOptions, 'admin', 'id_user', 'user', 'id');
        
        $user = new common\models\SignupForm();
        $user->username = 'akatrust';
        $user->password = 'devaka123';
        $user->email = '7technologia@gmail.com';
        if($user->signup()){
            $user = common\models\User::findOne(['username' => $user->username]);
            $user->photo = 'akatrust.png';
            $user->registration_ip = '::1';
            $user->save();
//            $admin = new common\models\Admin();
//            $admin->nama_admin = 'Akatrust';
//            $admin->tanggal_lahir = strtotime(date('05-06-1998'));
//            $admin->tempat_lahir = 'Padang';
//            $admin->alamat = 'Padang';
//            $admin->level = 1;
//            $admin->id_user = $user->id;
//            $admin->save();
             $profile = new \common\models\Profile();
        $profile->user_id = 1;
        $profile->name = 'Akatrust';
        $profile->born_date = strtotime(date('05-06-1998'));
        $profile->address = 'Padang';
        $profile->public_email = '7technologia@gmail.com';
        $profile->photo = 'akatrust.png';
        $profile->save();
        }
        
       
    }

    public function down()
    {
        $this->dropTable('{{%profile}}');
        $this->dropTable('{{%admin}}');
        $this->dropTable('{{%user}}');
    }
}
